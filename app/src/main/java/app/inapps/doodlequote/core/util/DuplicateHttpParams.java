package app.inapps.doodlequote.core.util;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DuplicateHttpParams extends HashMap<String, List<String>> {

    private static final long serialVersionUID = 1L;

    public DuplicateHttpParams() {
        super();
    }

    public DuplicateHttpParams(int capacity) {
        super(capacity);
    }

    public DuplicateHttpParams(Map<String, List<String>> map) {
        super(map);
    }

    public DuplicateHttpParams(int capacity, float loadFactor) {
        super(capacity, loadFactor);
    }

    /*
     * This is the method to use for adding post parameters
     */
    public void add(String key, String value) {
        if (containsKey(key)) {
            get(key).add(value);
        }
        else {
            ArrayList<String> list = new ArrayList<String>();
            list.add(value);
            put(key, list);
        }
    }

    public HashMap<String, String> getPathMap() {
        HashMap<String, String> paramMap = new HashMap<>();
        String paramValue;
        for (Map.Entry<String, List<String>> entry : entrySet()) {
            String key = entry.getKey();
            for (String value : entry.getValue()) {
                if (paramMap.containsKey(key)) {
                    // Add the duplicate key and new value onto the previous value
                    // so (key, value) will now look like (key, value&key=value2)
                    paramValue = paramMap.get(key);
                    paramValue += "&" + key + "=" + value;
                } else {
                    // This is the first value, so directly map it
                    paramValue = value;
                }
                paramMap.put(key, paramValue);
            }
        }

        return paramMap;
    }

    public HashMap<String, String> oldGetPathMap() {
        HashMap<String, String> paramMap = new HashMap<>();
        String paramValue;
        try {
            for (Map.Entry<String, List<String>> entry : entrySet()) {
                String key = entry.getKey();
                for (String value : entry.getValue()) {
                    if (paramMap.containsKey(key)) {
                        // Add the duplicate key and new value onto the previous value
                        // so (key, value) will now look like (key, value&key=value2)
                        paramValue = paramMap.get(key);
                        paramValue += "&" + key + "=" + URLEncoder.encode(value, "UTF-8");
                    } else {
                        // This is the first value, so directly map it
                        paramValue = URLEncoder.encode(value, "UTF-8");
                    }
                    paramMap.put(key, paramValue);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return paramMap;
    }

    /**
     * Converts the Map into an application/x-www-form-urlencoded encoded string.
     */
    public byte[] encodeParameters(String paramsEncoding) {
        StringBuilder encodedParams = new StringBuilder();
        try {
            for (Map.Entry<String, List<String>> entry : entrySet()) {
                String key = URLEncoder.encode(entry.getKey(), paramsEncoding);
                for (String value : entry.getValue()) {
                    encodedParams.append(key);
                    encodedParams.append('=');
                    encodedParams.append(URLEncoder.encode(value, paramsEncoding));
                    encodedParams.append('&');
                }
            }
            return encodedParams.toString().getBytes(paramsEncoding);
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return null;
    }

}

package app.inapps.doodlequote.core.widget;

import android.os.SystemClock;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import java.util.Map;
import java.util.WeakHashMap;

public class ClickSpan extends ClickableSpan {
    private static final long MINIMUM_INTERVAL_TIME = 500; // 500ms
    private View.OnClickListener mListener;
    private Map<View, Long> mLastClickedMap;
    private boolean mIsPressed;
    private int mTextColor;
    private int mPressedBackgroundColor;
    private boolean mIsHighLight;


    public ClickSpan(View.OnClickListener listener) {
        mListener = listener;
    }

    public ClickSpan(View.OnClickListener listener, int pressedBackgroundColor, int textColor) {
        mPressedBackgroundColor = pressedBackgroundColor;
        mTextColor = textColor;
        mListener = listener;
        mIsHighLight = true;
        mLastClickedMap = new WeakHashMap<View, Long>();
    }

    @Override
    public void onClick(View widget) {
        Long previousClickTimestamp = mLastClickedMap.get(widget);
        long currentTimestamp = SystemClock.uptimeMillis();

        mLastClickedMap.put(widget, currentTimestamp);
        if (previousClickTimestamp == null || (currentTimestamp - previousClickTimestamp > MINIMUM_INTERVAL_TIME)) {
            if (mListener != null) {
                mListener.onClick(widget);
            }
        }

    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(mTextColor);
        ds.setUnderlineText(true);
        if (mIsHighLight) {
            ds.bgColor = mIsPressed ? mPressedBackgroundColor : 0;
        }
    }

    public void setPressed(boolean isSelected) {
        mIsPressed = isSelected;
    }
}

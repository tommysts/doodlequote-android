package app.inapps.doodlequote.core.util;

import android.content.Context;
import android.content.pm.PackageManager;

public class CoreUtil {
    public static String getAppVersion(Context context) {
        String currentVersion = "";
        try {
            currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            currentVersion = "1.0";
        }
        return currentVersion;
    }
}

package app.inapps.doodlequote.core.api.response;

import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.db.DbUtil;

public abstract class BaseResponse {
    @SerializedName("success")String success;
    @SerializedName("message")String message;
    @SerializedName("error")String error;
    @SerializedName("error_code")String errorCode;

    public boolean isSuccess(){
        return DbUtil.isTrue(success);
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
package app.inapps.doodlequote.core;

import java.io.File;

public final class SystemConst {
    public static final String APP_NAME = "doodlequote";
    public static final String DATABASE_NAME = "doodlequote.db";
    public static final int DATABASE_VERSION = 1;
    public static final String PATH_SEPARATOR = File.separator;
    public static final String LINE_SEPARATOR = "\n";
    public static final String BUG_REPORT_FILE = "BugReport";
    public static final String BUNDLE_KEY_CONTAINER_MAP = "bundleKey.containerMap";
    public static final String CONTAINER_MAP_FILENAME_PREFIX = "doodlequote_";
    public static final long TIME_REFRESH_INTERVAL = 60000; // 60s
    public static final long NOTIFICATION_INTERVAL = 5000; // 5s
    public static final long REQUEST_DATA_DELAY = 1000; // 1s
    public static final long ANIMATE_CLICK_DELAY = 100; // ms
    public static final String DEVICE_TYPE = "android";

    private SystemConst() {
    }
}

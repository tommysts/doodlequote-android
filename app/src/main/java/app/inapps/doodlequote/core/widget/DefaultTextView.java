package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class DefaultTextView extends TextView {
    private final WeakReference<Context> refContext;

    public DefaultTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        refContext = new WeakReference<Context>(context);
    }

    public DefaultTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        refContext = new WeakReference<Context>(context);
    }

    protected final Context getWeakReferenceContext() {
        return refContext.get();
    }
}

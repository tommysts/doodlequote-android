package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

public class DefaultHeaderListView extends ListView {

    public DefaultHeaderListView(Context context) {
        super(context);
    }

    public DefaultHeaderListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DefaultHeaderListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    // Overriding onInterceptTouchEvent to handle the horizontal scroll properly
    private float mDistanceX;
    private float mDistanceY;
    private float mLastX;
    private float mLastY;


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mDistanceX = mDistanceY = 0f;
                mLastX = ev.getX();
                mLastY = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                final float curX = ev.getX();
                final float curY = ev.getY();
                mDistanceX += Math.abs(curX - mLastX);
                mDistanceY += Math.abs(curY - mLastY);
                mLastX = curX;
                mLastY = curY;
                if (mDistanceX > mDistanceY)
                    return false;
        }

        return super.onInterceptTouchEvent(ev);
    }

}

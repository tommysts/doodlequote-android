package app.inapps.doodlequote.core.widget;

public interface DefaultViewListener {
    void onFinishInflate();
}

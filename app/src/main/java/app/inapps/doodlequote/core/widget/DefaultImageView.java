package app.inapps.doodlequote.core.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class DefaultImageView extends ImageView {
    private final WeakReference<Context> refContext;

    public DefaultImageView(Context context) {
        super(context);
        refContext = new WeakReference<Context>(context);
    }

    public DefaultImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        refContext = new WeakReference<Context>(context);
    }

    public DefaultImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        refContext = new WeakReference<Context>(context);
    }

    @TargetApi(21)
    public DefaultImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        refContext = new WeakReference<Context>(context);
    }

    protected final Context getWeakReferenceContext() {
        return refContext.get();
    }
}

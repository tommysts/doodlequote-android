package app.inapps.doodlequote.core.widget;

import android.os.SystemClock;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * A Debounced OnItemClickListener
 * Rejects clicks that are too close together in time.
 * This class is safe to use as an OnItemClickListener for multiple views, and will debounce each one separately.
 */
public abstract class DebouncedOnItemClickListener implements OnItemClickListener {

    /**
     * MINIMUM_INTERVAL_TIME The minimum allowed time between clicks
     * any click sooner than this after a previous click will be rejected
     */
    private static final long MINIMUM_INTERVAL_TIME = 500; // 500ms
    private Map<View, Long> mLastClickedMap;

    public DebouncedOnItemClickListener() {
        this.mLastClickedMap = new WeakHashMap<View, Long>();
    }

    public abstract void onDebouncedItemClick(AdapterView<?> parent, View clickedView, int position, long id);

    @Override
    public void onItemClick(AdapterView<?> parent, View clickedView, int position, long id) {
        Long previousClickTimestamp = mLastClickedMap.get(clickedView);
        long currentTimestamp = SystemClock.uptimeMillis();

        mLastClickedMap.put(clickedView, currentTimestamp);
        if (previousClickTimestamp == null || (currentTimestamp - previousClickTimestamp > MINIMUM_INTERVAL_TIME)) {
            onDebouncedItemClick(parent, clickedView, position, id);
        }
    }

}

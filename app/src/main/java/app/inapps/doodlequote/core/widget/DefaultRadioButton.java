package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

import java.lang.ref.WeakReference;

public class DefaultRadioButton extends RadioButton {
    private final WeakReference<Context> refContext;

    public DefaultRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        refContext = new WeakReference<Context>(context);
    }

    public DefaultRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        refContext = new WeakReference<Context>(context);
    }

    protected final Context getWeakReferenceContext() {
        return refContext.get();
    }
}

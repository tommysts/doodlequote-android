package app.inapps.doodlequote.core.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.NumberPicker;

import app.inapps.doodlequote.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpiryPickerPopup extends Dialog {
    public interface ExpiryPickerPopupListener{
        void onDismiss(int month, int year);
    }

    @BindView(R.id.monthPicker)
    NumberPicker mMonthPicker;

    @BindView(R.id.yearPicker)
    NumberPicker mYearPicker;

    @BindView(R.id.llButton)
    ViewGroup mLlButton;

    private ExpiryPickerPopupListener mListener;

    public ExpiryPickerPopup(Context context) {
        super(context);
        setup();
    }

    public ExpiryPickerPopup(Context context, int themeResId) {
        super(context, themeResId);
        setup();
    }

    protected ExpiryPickerPopup(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setup();
    }

    private void setup(){
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.popup_expiry_picker);
        setCancelable(false);

        ButterKnife.bind(this);

        mLlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.onDismiss(mMonthPicker.getValue(), mYearPicker.getValue());
                }
                dismiss();
            }
        });
    }

    public int getMonth(){
        return mMonthPicker.getValue();
    }

    public int getYear(){
        return mYearPicker.getValue();
    }

    public void setData(int currentMonth, int currentYear, int minYear, int maxYear, ExpiryPickerPopupListener listener){
        mListener = listener;

        mMonthPicker.setMinValue(1);
        mMonthPicker.setMaxValue(12);
        mMonthPicker.setValue(currentMonth);
        mMonthPicker.setWrapSelectorWheel(false);

        mYearPicker.setMinValue(minYear);
        mYearPicker.setMaxValue(maxYear);
        mYearPicker.setValue(currentYear);
        mYearPicker.setWrapSelectorWheel(false);
    }
}

package app.inapps.doodlequote.core;

public class VideoItem extends FileItem {
    private int width;
    private int height;
    private long duration;
    private String previewImage;
    private int orientation;

    public VideoItem(String path, int width, int height, long duration, String previewImage, int orientation) {
        super(path);
        this.width = width;
        this.height = height;
        this.duration = duration;
        this.previewImage = previewImage;
        this.orientation = orientation;
    }

    public VideoItem(String path) {
        super(path);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getPreviewImage() {
        return previewImage;
    }

    public void setPreviewImage(String previewImage) {
        this.previewImage = previewImage;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }
}

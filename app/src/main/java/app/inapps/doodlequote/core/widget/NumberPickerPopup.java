package app.inapps.doodlequote.core.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.NumberPicker;

import app.inapps.doodlequote.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NumberPickerPopup extends Dialog {
    public interface NumberPickerPopupListener{
        void onDismiss(int number);
    }

    @BindView(R.id.numberPicker)
    NumberPicker mNumberPicker;

    @BindView(R.id.llButton)
    ViewGroup mLlButton;

    private NumberPickerPopupListener mListener;

    public NumberPickerPopup(Context context) {
        super(context);
        setup();
    }

    public NumberPickerPopup(Context context, int themeResId) {
        super(context, themeResId);
        setup();
    }

    protected NumberPickerPopup(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setup();
    }

    private void setup(){
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.popup_number_picker);
        setCancelable(false);

        ButterKnife.bind(this);

        mLlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.onDismiss(mNumberPicker.getValue());
                }
                dismiss();
            }
        });
    }

    public int getNumber(){
        return mNumberPicker.getValue();
    }

    public void setData(int current, int max, String[] displayValues, NumberPickerPopupListener listener){
        mListener = listener;
        mNumberPicker.setMinValue(1);
        if(displayValues != null){
            mNumberPicker.setMaxValue(displayValues.length);
            mNumberPicker.setDisplayedValues(displayValues);
            mNumberPicker.setValue(current);
        } else {
            mNumberPicker.setMaxValue(max);
            mNumberPicker.setValue(current);
        }
        mNumberPicker.setWrapSelectorWheel(false);
    }
}

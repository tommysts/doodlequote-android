package app.inapps.doodlequote.core.db;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import app.inapps.doodlequote.common.logger.BaseLogManager;
import app.inapps.doodlequote.core.db.row.BaseRow;
import nl.qbusict.cupboard.DatabaseCompartment;
import nl.qbusict.cupboard.QueryResultIterable;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class BaseDbManager {
    private Application mApplication;
    private BaseLogManager mLogManager;
    private final DbUtility mDbUtility;

    public BaseDbManager(Application application, BaseLogManager logManager) {
        mApplication = application;
        mLogManager = logManager;
        mDbUtility = new DbUtility(application);
    }

    private <T extends BaseRow> List<T> sort(Class<T> baseClass, QueryResultIterable<T> itr) {
        List<T> list = itr.list();

        Collections.sort(list, new Comparator<T>() {
            @Override
            public int compare(T lhs, T rhs) {
                if (!TextUtils.isEmpty(lhs.getDescription()) && !TextUtils.isEmpty(rhs.getDescription())) {
                    return lhs.getDescription().compareToIgnoreCase(rhs.getDescription());
                }
                return 0;
            }
        });

        return list;
    }

    public <T extends BaseRow> Observable<List<T>> getList(final Class<T> base) {
        return Observable.create(new Observable.OnSubscribe<List<T>>() {
            @Override
            public void call(Subscriber<? super List<T>> subscriber) {
                SQLiteDatabase db = mDbUtility.getReadableDatabase();
                DatabaseCompartment dc = cupboard().withDatabase(db);
                DatabaseCompartment.QueryBuilder<T> queryBuilder = dc.query(base);

                QueryResultIterable<T> itr = queryBuilder.query();
                List<T> list = sort(base, itr);
                subscriber.onNext(list);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends BaseRow> Observable<List<T>> getList(final Class<T> base, final String id) {
        return Observable.create(new Observable.OnSubscribe<List<T>>() {
            @Override
            public void call(Subscriber<? super List<T>> subscriber) {
                SQLiteDatabase db = mDbUtility.getReadableDatabase();
                DatabaseCompartment dc = cupboard().withDatabase(db);
                DatabaseCompartment.QueryBuilder<T> queryBuilder = dc.query(base);

                QueryResultIterable<T> itr = queryBuilder.query();
                List<T> list = sort(base, itr);
                subscriber.onNext(list);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends BaseRow> Observable<T> getItem(final Class<T> base, final String id) {
        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                SQLiteDatabase db = mDbUtility.getReadableDatabase();
                DatabaseCompartment dc = cupboard().withDatabase(db);
                QueryResultIterable<T> itr = dc.query(base).withSelection("id = ?", new String[]{id}).query();
                subscriber.onNext(itr.get());
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends BaseRow> Observable<T> getItem(final Class<T> base, final long _id) {
        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                SQLiteDatabase db = mDbUtility.getReadableDatabase();
                DatabaseCompartment dc = cupboard().withDatabase(db);
                QueryResultIterable<T> itr = dc.query(base).byId(_id).query();
                subscriber.onNext(itr.get());
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends BaseRow> T getItemRaw(final Class<T> base, final String id) {
        SQLiteDatabase db = mDbUtility.getReadableDatabase();
        DatabaseCompartment dc = cupboard().withDatabase(db);
        QueryResultIterable<T> itr = dc.query(base).withSelection("id = ?", new String[]{id}).query();
        return itr.get();
    }

    public <T extends BaseRow> Observable<Boolean> putItem(final T row) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                boolean success = false;
                SQLiteDatabase db = mDbUtility.getWritableDatabase();
                db.beginTransaction();
                try {
                    DatabaseCompartment dc = cupboard().withDatabase(db);
                    long id = dc.put(row);
                    success = id > 0;
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                subscriber.onNext(success);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Boolean> removeData(final Class base) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                SQLiteDatabase db = mDbUtility.getWritableDatabase();
                db.beginTransaction();
                try {
                    DatabaseCompartment dc = cupboard().withDatabase(db);
                    dc.delete(base, null);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                subscriber.onNext(true);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Boolean> removeAllData() {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                Boolean success = internalRemoveAllData();
                subscriber.onNext(success);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private Boolean internalRemoveAllData() {
        SQLiteDatabase db = mDbUtility.getWritableDatabase();
        db.beginTransaction();
        try {
            DatabaseCompartment dc = cupboard().withDatabase(db);
            for (Class base : DbUtility.CLASSES) {
                dc.delete(base, null);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return true;
    }
}

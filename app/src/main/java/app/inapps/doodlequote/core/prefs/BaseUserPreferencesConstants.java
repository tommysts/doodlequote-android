package app.inapps.doodlequote.core.prefs;

public class BaseUserPreferencesConstants {
    public static final String TOKEN = "token";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String END_POINT = "end_point";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";
    public static final String LANGUAGE = "language";
    public static final String LOGIN_TYPE = "user_type";
}

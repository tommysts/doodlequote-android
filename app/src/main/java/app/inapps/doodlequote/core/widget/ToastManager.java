package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.common.util.AssertUtil;

public final class ToastManager {
    private static Toast sToast;

    private ToastManager() {
    }

    public static Toast makeText(Context context, CharSequence text, int duration) {
        AssertUtil.assertNotNull(context);
        AssertUtil.assertNotNull(text);

        return makeText(context, R.layout.view_toast, text, duration);
    }

    public static Toast makeText(Context context, int rId, CharSequence text, int duration) {
        AssertUtil.assertNotNull(context);
        AssertUtil.assertNotNull(text);

        Toast result = new Toast(context);
        LayoutInflater inflate =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(rId, null);
        TextView tv = (TextView) v.findViewById(R.id.toastMessage);
        tv.setText(text);
        result.setView(v);
        result.setDuration(duration);
        setToast(result);
        return result;
    }

    public static Toast makeTextLong(Context context, CharSequence text) {
        AssertUtil.assertNotNull(context);
        AssertUtil.assertNotNull(text);
        return makeText(context, text, Toast.LENGTH_LONG);
    }

    public static Toast makeTextLong(Context context, int rId, CharSequence text) {
        AssertUtil.assertNotNull(context);
        AssertUtil.assertNotNull(text);
        return makeText(context, rId, text, Toast.LENGTH_LONG);
    }

    public static Toast makeTextShort(Context context, CharSequence text) {
        AssertUtil.assertNotNull(context);
        AssertUtil.assertNotNull(text);
        return makeText(context, text, Toast.LENGTH_SHORT);
    }

    public static Toast makeTextShort(Context context, int rId, CharSequence text) {
        AssertUtil.assertNotNull(context);
        AssertUtil.assertNotNull(text);
        return makeText(context, rId, text, Toast.LENGTH_SHORT);
    }

    static void setToast(Toast toast) {
        if (sToast != null) {
            sToast.cancel();
        }
        sToast = toast;
    }

    public static void cancelToast() {
        if (sToast != null) {
            sToast.cancel();
        }
        sToast = null;
    }
}

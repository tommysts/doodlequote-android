package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

public class TabRelativeLayout extends RelativeLayout {
    Context mContext;
    List<ViewGroup> mChild;
    int mCurrentTab;

    public TabRelativeLayout(Context context) {
        super(context);
        init(context);
    }

    public TabRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TabRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mChild = new ArrayList<ViewGroup>();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (mChild == null) {
            mChild = new ArrayList<ViewGroup>();
        }

        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = getChildAt(i);
            if (view instanceof ViewGroup) {
                if (view.getVisibility() == View.VISIBLE) {
                    view.setVisibility(View.INVISIBLE);
                }
                mChild.add((ViewGroup) view);
            }
        }
    }

    public int getCurrentTab() {
        return mCurrentTab;
    }

    public void setCurrentTab(int index) {
        if (mChild != null && mChild.size() > 0) {
            if (index < 0 || index >= mChild.size()) {
                return;
            }
            for (int i = 0; i < mChild.size(); i++) {
                ViewGroup viewGroup = mChild.get(i);
                if (i != index && viewGroup != null && viewGroup.getVisibility() == View.VISIBLE) {
                    viewGroup.setVisibility(View.INVISIBLE);
                }
            }
            ViewGroup viewGroup = mChild.get(index);
            if (viewGroup != null && viewGroup.getVisibility() != View.VISIBLE) {
                mCurrentTab = index;
                viewGroup.setVisibility(View.VISIBLE);
            }
        }
    }
}

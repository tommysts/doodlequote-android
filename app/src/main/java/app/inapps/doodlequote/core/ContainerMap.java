package app.inapps.doodlequote.core;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import app.inapps.doodlequote.common.util.AssertUtil;

public class ContainerMap implements Parcelable{
    private Bundle mBundle = new Bundle();

    public void remove(String key) {
        mBundle.remove(key);
    }

    public void setString(String key, String value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putString(key, value);
    }

    public void setInt(String key, int value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putInt(key, value);
    }

    public void setBoolean(String key, boolean value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putBoolean(key, value);
    }

    public void setLong(String key, long value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putLong(key, value);
    }

    public void setByte(String key, byte value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putByte(key, value);
    }

    public void setStringArray(String key, String[] value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putStringArray(key, value);
    }

    public void setIntArray(String key, int[] value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putIntArray(key, value);
    }

    public void setLongArray(String key, long[] value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putLongArray(key, value);
    }

    public void setByteArray(String key, byte[] value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putByteArray(key, value);
    }

    public <T extends Parcelable> void setList(String key, ArrayList<T> value) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putParcelableArrayList(key, value);
    }

    public <T extends Parcelable> void setObject(String key, T obj) {
        AssertUtil.assertNotEmpty(key);
        mBundle.putParcelable(key, obj);
    }

    public String getString(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getString(key);
    }

    public int getInt(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getInt(key);
    }

    public boolean getBoolean(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getBoolean(key);
    }

    public long getLong(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getLong(key);
    }

    public byte getByte(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getByte(key);
    }

    public String[] getStringArray(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getStringArray(key);
    }

    public int[] getIntArray(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getIntArray(key);
    }

    public long[] getLongArray(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getLongArray(key);
    }

    public byte[] getByteArray(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getByteArray(key);
    }

    public <T extends Parcelable> ArrayList<T> getList(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getParcelableArrayList(key);
    }

    public <T extends Parcelable> T getObject(String key) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getParcelable(key);
    }

    public int getInt(String key, int defaultValue) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getInt(key, defaultValue);
    }

    public long getLong(String key, long defaultValue) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getLong(key, defaultValue);
    }

    public byte getByte(String key, byte defaultValue) {
        AssertUtil.assertNotEmpty(key);
        return mBundle.getByte(key, defaultValue);
    }

    @Override
    public String toString() {
        return mBundle.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeBundle(this.mBundle);
    }

    public ContainerMap() {
    }

    protected ContainerMap(Parcel in) {
        this.mBundle = in.readBundle(Bundle.class.getClassLoader());
    }

    public static final Creator<ContainerMap> CREATOR = new Creator<ContainerMap>() {
        @Override
        public ContainerMap createFromParcel(Parcel source) {
            return new ContainerMap(source);
        }

        @Override
        public ContainerMap[] newArray(int size) {
            return new ContainerMap[size];
        }
    };
}

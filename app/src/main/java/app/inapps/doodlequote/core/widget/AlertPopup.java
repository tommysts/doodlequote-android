package app.inapps.doodlequote.core.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.LayoutRes;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.R2;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AlertPopup extends Dialog {
    @BindView(R2.id.rlTitle)
    protected ViewGroup mRlTitle;

    @BindView(R2.id.tvTitle)
    protected TextView mTvTitle;

    @BindView(R2.id.ivClose)
    protected ImageView mIvClose;

    @BindView(R2.id.tvMessage)
    protected TextView mTvMessage;

    @BindView(R2.id.tvButton1)
    protected TextView mTvButton1;

    @BindView(R2.id.llButton1)
    protected ViewGroup mLlButton1;

    @BindView(R2.id.ivButton1)
    protected ImageView mIvButton1;

    @BindView(R2.id.tvButton2)
    protected TextView mTvButton2;

    @BindView(R2.id.llButton2)
    protected ViewGroup mLlButton2;

    @BindView(R2.id.ivButton2)
    protected ImageView mIvButton2;

    @BindView(R2.id.scvData)
    protected ViewGroup mScvData;

    protected Unbinder mUnbinder;
    protected AlertPopupListener mListener;

    public interface AlertPopupListener {
        void onStart();
    }

    public AlertPopup(Context context) {
        super(context);
        setup();
    }

    public AlertPopup(Context context, int themeResId) {
        super(context, themeResId);
        setup();
    }

    protected AlertPopup(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setup();
    }

    protected void setup() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(getLayoutResID());
        setCancelable(false);

        Window window = getWindow();
        if(window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        mUnbinder = ButterKnife.bind(this);

        mRlTitle.setVisibility(View.INVISIBLE);
        mLlButton1.setVisibility(View.GONE);
        mLlButton2.setVisibility(View.GONE);
        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @LayoutRes
    protected int getLayoutResID() {
        return R.layout.popup_layout;
    }

    public void setListener(AlertPopupListener listener) {
        mListener = listener;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mUnbinder == null) {
            mUnbinder = ButterKnife.bind(this);
        }
        if(mListener != null) {
            mListener.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public void setTitle(String title) {
        //mRlTitle.setVisibility(View.VISIBLE);
        //mTvTitle.setText(title);
    }

    public void setMessage(String message) {
        mTvMessage.setText(message);
    }

    public void setMessage(Spanned message) {
        mTvMessage.setText(message);
    }

    public void setCloseAction(View.OnClickListener onClickListener) {
        mIvClose.setOnClickListener(onClickListener);
    }

    public void setButton1Action(String text, View.OnClickListener onClickListener) {
        mLlButton1.setVisibility(View.VISIBLE);
        mTvButton1.setText(text);
        mIvButton1.clearAnimation();
        mIvButton1.setOnClickListener(onClickListener);
    }

    public void setButton2Action(String text, View.OnClickListener onClickListener) {
        mLlButton2.setVisibility(View.VISIBLE);
        mTvButton2.setText(text);
        mIvButton2.clearAnimation();
        mIvButton2.setOnClickListener(onClickListener);
    }

    public void setPopupHeight(int dimenId) {
        int height = getContext().getResources().getDimensionPixelSize(dimenId);
        ViewGroup.LayoutParams layoutParams = mScvData.getLayoutParams();
        layoutParams.height = height;
        mScvData.setLayoutParams(layoutParams);
    }
}

package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

import java.lang.ref.WeakReference;

public class DefaultToggleButton extends ToggleButton {
    private final WeakReference<Context> refContext;

    public DefaultToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        refContext = new WeakReference<Context>(context);
    }

    public DefaultToggleButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        refContext = new WeakReference<Context>(context);
    }

    protected final Context getWeakReferenceContext() {
        return refContext.get();
    }
}

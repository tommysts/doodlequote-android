package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;

import java.lang.ref.WeakReference;

public class DefaultRadioGroup extends RadioGroup {
    private final WeakReference<Context> refContext;

    public DefaultRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);

        refContext = new WeakReference<Context>(context);
    }

    protected final Context getWeakReferenceContext() {
        return refContext.get();
    }
}

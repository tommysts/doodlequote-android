package app.inapps.doodlequote.core.db;

import android.text.TextUtils;

public class DbUtil {
    public static final String TRUE = "1";
    public static final String FALSE = "0";

    public static boolean isTrue(String value){
        return !TextUtils.isEmpty(value) && ("1".equals(value) || "true".equalsIgnoreCase(value));
    }
}

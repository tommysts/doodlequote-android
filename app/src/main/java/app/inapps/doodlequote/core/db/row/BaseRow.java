package app.inapps.doodlequote.core.db.row;

import android.os.Parcel;

import app.inapps.doodlequote.core.Cacheable;

public abstract class BaseRow implements Cacheable {
    Long _id;
    String id;
    String description;

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }

    @Override
    public String getCacheKey() {
        return getClass().getSimpleName();
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this._id);
        dest.writeString(this.id);
        dest.writeString(this.description);
    }

    protected BaseRow() {

    }

    protected BaseRow(Parcel in) {
        this._id = (Long) in.readValue(Long.class.getClassLoader());
        this.id = in.readString();
        this.description = in.readString();
    }
}

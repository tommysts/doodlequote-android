package app.inapps.doodlequote.core.service;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by ChungPV1 on 8/2/2016.
 */
public abstract class BaseIntentService extends IntentService {
    public BaseIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }
}

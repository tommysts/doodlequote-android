package app.inapps.doodlequote.core;

public class FileItem {
    private String path;

    public FileItem(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

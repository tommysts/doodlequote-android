package app.inapps.doodlequote.core.util;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.core.ImageItem;
import app.inapps.doodlequote.core.VideoItem;
import app.inapps.doodlequote.core.widget.ViewUtil;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DisplayImageUtil {
    public static final String TAG = DisplayImageUtil.class.getSimpleName();
    public static final String IMAGES_PATH = "/Images";

    private static BitmapFactory.Options sBitmapOptions;
    private static Rect sLogoRect;

    public static Observable<List<ImageItem>> rxOptimizeUploadImages(final Context context, final List<ImageItem> imageItems) {
        return Observable.create(new Observable.OnSubscribe<List<ImageItem>>() {
            @Override
            public void call(Subscriber<? super List<ImageItem>> subscriber) {
                List<ImageItem> newPaths = new ArrayList<ImageItem>();
                for(ImageItem imageItem : imageItems) {
                    ImageItem newImageItem = optimizeUploadImage(context, imageItem);
                    newPaths.add(newImageItem);
                }
                subscriber.onNext(newPaths);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<List<VideoItem>> rxOptimizeUploadVideos(final Context context, final List<VideoItem> videoItems) {
        return Observable.create(new Observable.OnSubscribe<List<VideoItem>>() {
            @Override
            public void call(Subscriber<? super List<VideoItem>> subscriber) {
                List<VideoItem> newPaths = new ArrayList<VideoItem>();
                for(VideoItem videoItem : videoItems) {
                    VideoItem newVideoItem = optimizeUploadVideo(context, videoItem);
                    newPaths.add(newVideoItem);
                }
                subscriber.onNext(newPaths);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ImageItem> rxOptimizeUploadImage(final Context context, final ImageItem imageItem) {
        return Observable.create(new Observable.OnSubscribe<ImageItem>() {
            @Override
            public void call(Subscriber<? super ImageItem> subscriber) {
                ImageItem newImageItem = optimizeUploadImage(context, imageItem);
                subscriber.onNext(newImageItem);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public static ImageItem optimizeUploadImage(Context context, ImageItem imageItem) {
        imageItem = rotateImageIfNeed(context, imageItem);
        int[] widthHeight = getImageWidthHeight(imageItem);
        float width = widthHeight[0];
        float height = widthHeight[1];
        boolean valid = isValidImage(imageItem, width, height);
        if (!valid) {
            float ratio = width / AppConst.IMAGE_WIDTH_BIG;
            if(ratio > 1) {
                int newWidth = AppConst.IMAGE_WIDTH_BIG;
                int newHeight = (int)(height / ratio);
                imageItem = resizeImage(context, imageItem, newWidth, newHeight);
                valid = isValidImage(imageItem, newWidth, newHeight);
            }
        }
        if (!valid) {
            float ratio = width / AppConst.IMAGE_WIDTH_MEDIUM;
            if(ratio > 1) {
                int newWidth = AppConst.IMAGE_WIDTH_MEDIUM;
                int newHeight = (int)(height / ratio);
                imageItem = resizeImage(context, imageItem, newWidth, newHeight);
                valid = isValidImage(imageItem, newWidth, newHeight);
            }
        }
        if (!valid) {
            float ratio = width / AppConst.IMAGE_WIDTH_SMALL;
            if(ratio > 1) {
                int newWidth = AppConst.IMAGE_WIDTH_SMALL;
                int newHeight = (int)(height / ratio);
                imageItem = resizeImage(context, imageItem, newWidth, newHeight);
                valid = isValidImage(imageItem, newWidth, newHeight);
            }
        }
        if (!valid) {
            float ratio = width / AppConst.IMAGE_WIDTH_TINY;
            if(ratio > 1) {
                int newWidth = AppConst.IMAGE_WIDTH_TINY;
                int newHeight = (int)(height / ratio);
                imageItem = resizeImage(context, imageItem, newWidth, newHeight);
            }
        }
        if(BuildConfig.DEBUG) {
            widthHeight = getImageWidthHeight(imageItem);
            Log.d(TAG, String.format(Locale.getDefault(), "Optimize to %d - %d", widthHeight[0], widthHeight[1]));
        }
        return imageItem;
    }

    public static VideoItem optimizeUploadVideo(Context context, VideoItem videoItem) {
        return videoItem;
    }

    public static boolean isValidImage(ImageItem imageItem, float width, float height){
        File imgFile = new File(imageItem.getPath());
        if (imgFile.exists()) {
            long length = imgFile.length();
            if(length < AppConst.MAX_FILE_SIZE_IN_BYTES && width < AppConst.IMAGE_WIDTH_BIG) {
                return true;
            }
        }
        return false;
    }

    public static int[] getImageWidthHeight(ImageItem imageItem) {
        if (sBitmapOptions == null) {
            sBitmapOptions = new BitmapFactory.Options();
            sBitmapOptions.inMutable = true;
        }
        if (sLogoRect == null) {
            sLogoRect = new Rect(0, 0, 0, 0);
        }

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imageItem.getPath());
            sBitmapOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(fis, sLogoRect, sBitmapOptions);
            return new int[] {sBitmapOptions.outWidth, sBitmapOptions.outHeight};
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
        return new int[] {0, 0};
    }

    public static ImageItem resizeImage(Context context, ImageItem imageItem, int width, int height){
        File imgFile = new File(imageItem.getPath());
        if (imgFile.exists()) {
            Bitmap bitmap1 = ViewUtil.loadImage(new BitmapFactory.Options(), imgFile, width, height);
            Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap1, width, height, false);
            String newPath = FileUtils.createUploadDirIfNotExists(context) + File.separator + "resize" + System.currentTimeMillis() + ".png";
            try {
                OutputStream fOut = new BufferedOutputStream(new FileOutputStream(newPath));
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                return new ImageItem(newPath);
            } catch (Exception e) {
                if( BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            } finally {
                bitmap1.recycle();
                bitmap2.recycle();
            }
        }
        return imageItem;
    }

    public static ImageItem rotateImageIfNeed(Context context, ImageItem imageItem){
        File imgFile = new File(imageItem.getPath());
        if (imgFile.exists()) {
            Bitmap bitmap1 = null;
            Bitmap bitmap2 = null;
            try {
                int orientation = imageItem.getRotation();
                if(orientation != 0) {
                    bitmap1 = ViewUtil.loadImage(imgFile);
                    if (orientation == 6) {
                        bitmap2 = rotate(bitmap1, 90);
                    } else if (orientation == 8) {
                        bitmap2 = rotate(bitmap1, 270);
                    } else if (orientation == 3) {
                        bitmap2 = rotate(bitmap1, 180);
                    }
                }
                if(bitmap2 != null) {
                    String newPath = FileUtils.createUploadDirIfNotExists(context) + File.separator + "resize" + System.currentTimeMillis() + ".png";
                    OutputStream fOut = new BufferedOutputStream(new FileOutputStream(newPath));
                    bitmap2.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    return new ImageItem(newPath);
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            } finally {
                if(bitmap1 != null) {
                    bitmap1.recycle();
                }
                if(bitmap2 != null) {
                    bitmap2.recycle();
                }
            }
        }
        return imageItem;
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.setRotate(degree);

        if(BuildConfig.DEBUG) {
            Log.d(TAG, "Rotate image " + degree);
        }

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public static String getPath(final Context context, final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap loadImage(Context context, Bitmap reuseBitmap, String imageUri, int width, int height) {
        if (sBitmapOptions == null) {
            sBitmapOptions = new BitmapFactory.Options();
            sBitmapOptions.inMutable = true;
        }
        if (sLogoRect == null) {
            sLogoRect = new Rect(0, 0, 0, 0);
        }

        String imagePath = context.getFilesDir().getPath() + imageUri;
        FileInputStream fis = null;
        Bitmap bitmap = null;
        try {
            fis = new FileInputStream(imagePath);
            sBitmapOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(fis, sLogoRect, sBitmapOptions);
            sBitmapOptions.inSampleSize = calculateInSampleSize(sBitmapOptions, width, height);
            sBitmapOptions.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeStream(fis, sLogoRect, sBitmapOptions);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
        return bitmap;
    }

    public static void loadImage(Context context, ImageView imageView, String imageUri, int scaleWidth, int scaleHeight) {
        String imagePath = context.getFilesDir().getPath() + imageUri;
        Bitmap originalBitmap = BitmapFactory.decodeFile(imagePath);
        if (originalBitmap != null) {
            imageView.setImageBitmap(Bitmap.createScaledBitmap(originalBitmap, scaleWidth, scaleHeight, true));
        }
    }

    public static void loadImage(Context context, ImageView imageView, String imageUri, int scaleWidth, int scaleHeight, boolean rotate, int angle) {
        String imagePath = context.getFilesDir().getPath() + imageUri;
        Bitmap originalBitmap = BitmapFactory.decodeFile(imagePath);
        if (originalBitmap != null) {
            if(rotate){
                Matrix matrix = new Matrix();
                if(angle == 180){
                    matrix.preScale(-1.0f, 1.0f);
                } else {
                    matrix.postRotate(angle);
                }
                imageView.setImageBitmap(Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight(), matrix, true));
            } else {
                //imageView.setImageBitmap(Bitmap.createScaledBitmap(originalBitmap, scaleWidth, scaleHeight, true));
                imageView.setImageBitmap(originalBitmap);
            }
        }
    }

    public static boolean isImagesDownloadSuccess(Context context, String... imageUris){
        String path = context.getFilesDir().getPath();
        for (String imageUri : imageUris) {
            File file = new File(path + imageUri);
            if(!file.exists()){
                return false;
            }
        }
        return true;
    }

    public static void loadImageBaseOnMaxWidthHeight(Context context, ImageView imageView, String imageUri, int maxWidth, int maxHeight) {
        String imagePath = context.getFilesDir().getPath() + imageUri;
        int scaleWidth;
        int scaleHeight;
        Bitmap originalBitmap = BitmapFactory.decodeFile(imagePath);
        if (originalBitmap != null) {
            scaleWidth = maxWidth;
            scaleHeight = scaleWidth * originalBitmap.getHeight() / originalBitmap.getWidth();
            if (scaleHeight > maxHeight) {
                scaleHeight = maxHeight;
                scaleWidth = scaleHeight * originalBitmap.getWidth() / originalBitmap.getHeight();
            }
            imageView.setImageBitmap(Bitmap.createScaledBitmap(originalBitmap, scaleWidth, scaleHeight, true));
        }
    }

    public static void loadImageBaseOnMaxHeight(Context context, ImageView imageView, String imageUri, int maxHeight) {
        String imagePath = context.getFilesDir().getPath() + imageUri;
        int scaleWidth;
        int scaleHeight;
        Bitmap originalBitmap = BitmapFactory.decodeFile(imagePath);
        if (originalBitmap != null) {
            scaleHeight = maxHeight;
            scaleWidth = scaleHeight * originalBitmap.getWidth() / originalBitmap.getHeight();
            imageView.setImageBitmap(Bitmap.createScaledBitmap(originalBitmap, scaleWidth, scaleHeight, true));
        }
    }
}

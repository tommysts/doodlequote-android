package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Spinner;

import java.lang.ref.WeakReference;

public class DefaultSpinner extends Spinner {
    private final WeakReference<Context> refContext;

    public DefaultSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        refContext = new WeakReference<Context>(context);
    }

    public DefaultSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        refContext = new WeakReference<Context>(context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    ViewUtil.closeKeyboard(v);
                }
                return false;
            }
        });
    }
}

package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.util.AttributeSet;

import app.inapps.doodlequote.R;

public class SegmentedButton extends AnimateButton {
    private int mMarginDp;
    private Resources resources;
    private int mTintColor;
    private int mBorderColor;
    private int mDefaultTextColor = Color.WHITE;
    private int mCheckedTextColor = Color.WHITE;
    private LayoutSelector mLayoutSelector;
    private Float mCornerRadius;

    public SegmentedButton(Context context) {
        super(context);
        resources = getResources();
        mTintColor = resources.getColor(R.color.button_color);
        mBorderColor = resources.getColor(R.color.button_color);
        mMarginDp = (int) getResources().getDimension(R.dimen.radio_button_stroke_border);
        mCornerRadius = getResources().getDimension(R.dimen.radio_button_conner_radius);
        mLayoutSelector = new LayoutSelector(mCornerRadius);
    }

    public SegmentedButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        resources = getResources();
        mTintColor = resources.getColor(R.color.button_color);
        mBorderColor = resources.getColor(R.color.button_color);
        mMarginDp = (int) getResources().getDimension(R.dimen.radio_button_stroke_border);
        mCornerRadius = getResources().getDimension(R.dimen.radio_button_conner_radius);
        initAttrs(attrs);
        mLayoutSelector = new LayoutSelector(mCornerRadius);
    }

    /* Reads the attributes from the layout */
    private void initAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SegmentedButton,
                0, 0);

        try {
            mMarginDp = (int) typedArray.getDimension(
                    R.styleable.SegmentedButton_sb_border_width,
                    getResources().getDimension(R.dimen.radio_button_stroke_border));

            mCornerRadius = typedArray.getDimension(
                    R.styleable.SegmentedButton_sb_corner_radius,
                    getResources().getDimension(R.dimen.radio_button_conner_radius));

            mTintColor = typedArray.getColor(
                    R.styleable.SegmentedButton_sb_tint_color,
                    getResources().getColor(R.color.button_color));

            mBorderColor = typedArray.getColor(
                    R.styleable.SegmentedButton_sb_border_color,
                    getResources().getColor(R.color.button_color));

            mDefaultTextColor = typedArray.getColor(
                    R.styleable.SegmentedButton_sb_default_text_color,
                    getResources().getColor(android.R.color.white));

            mCheckedTextColor = typedArray.getColor(
                    R.styleable.SegmentedButton_sb_checked_text_color,
                    getResources().getColor(android.R.color.white));

        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //Use holo light for default
        updateBackground();
    }

    public void setTintColor(@ColorRes int tintColor) {
        mTintColor = getResources().getColor(tintColor);
        updateBackground();
    }

    public void setBorderColor(@ColorRes int tintColor) {
        mBorderColor = getResources().getColor(tintColor);
        updateBackground();
    }

    public void setTintColor(@ColorRes int tintColor, @ColorRes int checkedTextColor) {
        mTintColor = getResources().getColor(tintColor);
        mCheckedTextColor = getResources().getColor(checkedTextColor);
        updateBackground();
    }

    private void updateBackground() {
        int defaultLayout = mLayoutSelector.getDefault();
        //Set text color
        ColorStateList colorStateList = new ColorStateList(new int[][]{
                {android.R.attr.state_pressed},
                {-android.R.attr.state_pressed, -android.R.attr.state_selected},
                {-android.R.attr.state_pressed, android.R.attr.state_selected}},
                new int[]{Color.GRAY, mDefaultTextColor, mCheckedTextColor});
        setTextColor(colorStateList);

        //Redraw with tint color
        Drawable defaultDrawable = resources.getDrawable(defaultLayout).mutate();
        ((GradientDrawable) defaultDrawable).setColor(mTintColor);
        ((GradientDrawable) defaultDrawable).setStroke(mMarginDp, mBorderColor);
        //Set proper radius
        ((GradientDrawable) defaultDrawable).setCornerRadii(mLayoutSelector.getRadii());

        //Create drawable
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{-android.R.attr.state_selected}, defaultDrawable);
        stateListDrawable.addState(new int[]{android.R.attr.state_selected}, defaultDrawable);

        //Set button background
        if (Build.VERSION.SDK_INT >= 16) {
            setBackground(stateListDrawable);
        } else {
            setBackgroundDrawable(stateListDrawable);
        }
    }

    /*
         * This class is used to provide the proper layout based on the view.
         * Also provides the proper radius for corners.
         * The layout is the same for each selected left/top middle or right/bottom button.
         * float tables for setting the radius via Gradient.setCornerRadii are used instead
         * of multiple xml drawables.
         */
    private class LayoutSelector {
        private final int DEFAULT_LAYOUT = R.drawable.radio_checked;
        //private final float r1 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP
        //        , 0.1f, getResources().getDisplayMetrics());    //0.1 dp to px
        //private final float[] rLeft;    // left radio button
        //private final float[] rRight;   // right radio button
        //private final float[] rMiddle;  // middle radio button
        private final float[] rDefault; // default radio button
        private float r;    //this is the radios read by attributes or xml dimens
        //private final float[] rTop;     // top radio button
        //private final float[] rBot;     // bot radio button
        private float[] radii;          // result radii float table

        public LayoutSelector(float cornerRadius) {
            r = cornerRadius;
            //rLeft = new float[]{r, r, r1, r1, r1, r1, r, r};
            //rRight = new float[]{r1, r1, r, r, r, r, r1, r1};
            //rMiddle = new float[]{r1, r1, r1, r1, r1, r1, r1, r1};
            rDefault = new float[]{r, r, r, r, r, r, r, r};
            //rTop = new float[]{r, r, r, r, r1, r1, r1, r1};
            //rBot = new float[]{r1, r1, r1, r1, r, r, r, r};

            radii = rDefault;
        }

        /* Returns the selected layout id based on view */
        public int getDefault() {
            return DEFAULT_LAYOUT;
        }

        /* Returns the radii float table based on view for Gradient.setRadii()*/
        public float[] getRadii() {
            return radii;
        }
    }
}
package app.inapps.doodlequote.core;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BaseMap extends ConcurrentHashMap<String, Object> {
    private static final long serialVersionUID = -8767119785198810766L;

    /**
     * NULL-Object
     */
    private static final Object NULL = new NullObject();

    public BaseMap() {
        super();
    }

    public BaseMap(int initialCapacity) {
        super(initialCapacity);
    }

    public BaseMap(Map<String, Object> m) {
        super(m);
    }

    public BaseMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public BaseMap(int initialCapacity, float loadFactor,
                   int concurrencyLevel) {
        super(initialCapacity, loadFactor, concurrencyLevel);
    }

    @Override
    public Object get(Object key) {
        Object o = super.get(key);
        if (o instanceof NullObject) {
            return null;
        }
        return o;
    }

    @Override
    public Object put(String key, Object value) {
        Object chkVal = value;
        if (null == chkVal) {
            chkVal = NULL;
        }
        return super.put(key, chkVal);
    }

    @Override
    public boolean containsValue(Object value) {
        Object chkVal = value;
        if (null == chkVal) {
            chkVal = NULL;
        }
        return super.containsValue(chkVal);
    }

    @Override
    public boolean contains(Object value) {
        Object chkVal = value;
        if (null == chkVal) {
            chkVal = NULL;
        }
        return super.contains(chkVal);
    }

    @Override
    public Object putIfAbsent(String key, Object value) {
        Object chkVal = value;
        if (null == chkVal) {
            chkVal = NULL;
        }
        return super.putIfAbsent(key, chkVal);
    }

    @Override
    public boolean remove(Object key, Object value) {
        Object chkVal = value;
        if (null == chkVal) {
            chkVal = NULL;
        }
        return super.remove(key, chkVal);
    }

    @Override
    public boolean replace(String key, Object oldValue, Object newValue) {
        Object oldChkVal = oldValue;
        if (null == oldChkVal) {
            oldChkVal = NULL;
        }
        Object newChkVal = newValue;
        if (null == newChkVal) {
            newChkVal = NULL;
        }
        return super.replace(key, oldChkVal, newChkVal);
    }

    /**
     * Null-Object
     */
    private static class NullObject implements Serializable {
        private static final long serialVersionUID = -6073254237028335432L;
    }
}

package app.inapps.doodlequote.core.util;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

public class NetworkStateMonitorReceiver extends BroadcastReceiver {
    public NetworkStateMonitorReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!isInitialStickyBroadcast() && !intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false)) {
            NetworkStateMonitor networkState = NetworkStateMonitor.getInstance(context);
            networkState.handleConnectivityChange();
        }
    }
}
package app.inapps.doodlequote.core.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.ColorRes;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.MovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

import app.inapps.doodlequote.R;

public class ViewUtil {
    public static final String NEW_LINE = "<br/>";

    public static int convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static int convertPixelsToDp(Context context, float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    public static int getActionBarHeight(Activity activity) {
        TypedValue tv = new TypedValue();
        if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
        }
        return 0;
    }

    public static int getStatusBarHeight(Activity activity) {
        Rect r = new Rect();
        Window w = activity.getWindow();
        w.getDecorView().getWindowVisibleDisplayFrame(r);
        return r.top;
    }

    public static int getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getScreenHeight(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static void closeKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view
                .getContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static SpannableStringBuilder makeSectionOfTextBold(String text, String... textToBold) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        for (String textItem :
                textToBold) {
            if (textItem.length() > 0 && !textItem.trim().equals("")) {
                //for counting start/end indexes
                String testText = text.toLowerCase(Locale.US);
                String testTextToBold = textItem.toLowerCase(Locale.US);
                int startingIndex = testText.indexOf(testTextToBold);
                int endingIndex = startingIndex + testTextToBold.length();

                if (startingIndex >= 0 && endingIndex >= 0) {
                    builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                }
            }
        }
        return builder;
    }

    public static void setBoldText(Context context, TextView view, String boldText) {
        CharSequence text = view.getText();
        String string = text.toString();
        StyleSpan span = new StyleSpan(Typeface.BOLD);

        int start = string.indexOf(boldText);
        int end = start + boldText.length();
        if (start == -1)
            return;

        if (text instanceof Spannable) {
            ((Spannable) text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(text);
        } else {
            SpannableString s = SpannableString.valueOf(text);
            s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(s);
        }
    }

    public static void setColorText(Context context, TextView view, String colorText, @ColorRes int color) {
        CharSequence text = view.getText();
        String string = text.toString();
        ForegroundColorSpan span = new ForegroundColorSpan(context.getResources().getColor(color));

        int start = string.indexOf(colorText);
        int end = start + colorText.length();
        if (start == -1)
            return;

        if (text instanceof Spannable) {
            ((Spannable) text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(text);
        } else {
            SpannableString s = SpannableString.valueOf(text);
            s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(s);
        }
    }

    public static void setColorsText(Context context, EditText view, String[] colorText, @ColorRes int color) {
        Editable et = view.getText();
        ForegroundColorSpan[] toRemoveSpans = et.getSpans(0, view.getText().length(), ForegroundColorSpan.class);
        for (int i = 0; i < toRemoveSpans.length; i++) {
            et.removeSpan(toRemoveSpans[i]);
        }
        for(String key : colorText) {
            ViewUtil.setColorText(context, view, key, color);
        }
    }

    public static void setColorsText(Context context, TextView view, String[] colorText, @ColorRes int color) {
        CharSequence text = view.getText();
        if (text instanceof Spannable) {
            Spannable sp = (Spannable) text;
            ForegroundColorSpan[] toRemoveSpans = sp.getSpans(0, text.length(), ForegroundColorSpan.class);
            for (int i = 0; i < toRemoveSpans.length; i++) {
                sp.removeSpan(toRemoveSpans[i]);
            }
        }
        for(String key : colorText) {
            ViewUtil.setColorText(context, view, key, color);
        }
    }

    public static void setColorText(Context context, EditText view, String colorText, @ColorRes int color) {
        CharSequence text = view.getText();
        String string = text.toString();
        ForegroundColorSpan span = new ForegroundColorSpan(context.getResources().getColor(color));

        int start = string.indexOf(colorText);
        int end = start + colorText.length();
        if (start == -1)
            return;

        if (text instanceof Spannable) {
            ((Spannable) text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(text);
        } else {
            SpannableString s = SpannableString.valueOf(text);
            s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(s);
        }
    }

    public static void setClickableText(Context context, TextView view, String clickableText, View.OnClickListener listener) {
        CharSequence text = view.getText();
        String string = text.toString();
        Resources resources = context.getResources();
        ClickSpan span = new ClickSpan(listener, resources.getColor(R.color.black),
                resources.getColor(R.color.orange));

        int start = string.indexOf(clickableText);
        int end = start + clickableText.length();
        if (start == -1)
            return;

        if (text instanceof Spannable) {
            ((Spannable) text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(text);
        } else {
            SpannableString s = SpannableString.valueOf(text);
            s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(s);
        }

        MovementMethod m = view.getMovementMethod();
        if (m == null || !(m instanceof LinkTouchMovementMethod)) {
            view.setMovementMethod(LinkTouchMovementMethod.getInstance());
        }
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeResource(BitmapFactory.Options options, Resources res, int id, int width, int height) {
        options.inMutable = true;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, id, options);
        options.inSampleSize = ViewUtil.calculateInSampleSize(options, width, height);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, id, options);
    }

    public static Bitmap decodeResource(Context context, Resources res, int resId) {
        float density;

        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        density = context.getResources().getDisplayMetrics().density;
        float deviceHeight = outMetrics.heightPixels / density;
        float deviceWidth = outMetrics.widthPixels / density;

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, (int) deviceWidth, (int) deviceHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap loadImage(BitmapFactory.Options options, File file, int width, int height) {
        options.inMutable = true;
        FileInputStream fis = null;
        Bitmap bitmap = null;
        Rect logoRect = new Rect(0, 0, 0, 0);
        try {
            fis = new FileInputStream(file);
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(fis, logoRect, options);
            if (fis.markSupported()) {
                fis.reset();
            } else {
                try {
                    fis.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
                fis = new FileInputStream(file);
            }
            options.inSampleSize = calculateInSampleSize(options, width, height);
            options.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeStream(fis, logoRect, options);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
        return bitmap;
    }

    public static Bitmap loadImage(File file) {
        FileInputStream fis = null;
        Bitmap bitmap = null;
        try {
            fis = new FileInputStream(file);
            if (fis.markSupported()) {
                fis.reset();
            } else {
                try {
                    fis.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
                fis = new FileInputStream(file);
            }
            bitmap = BitmapFactory.decodeStream(fis);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
        return bitmap;
    }
}


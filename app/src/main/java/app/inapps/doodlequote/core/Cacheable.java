package app.inapps.doodlequote.core;

import android.os.Parcelable;

public interface Cacheable extends Parcelable{
    String getCacheKey();
    void clearCache();
}

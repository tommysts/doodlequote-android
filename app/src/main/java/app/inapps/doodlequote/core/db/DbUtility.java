package app.inapps.doodlequote.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import app.inapps.doodlequote.core.SystemConst;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class DbUtility extends SQLiteOpenHelper {
    public DbUtility(Context context) {
        this(context, SystemConst.DATABASE_NAME);
    }

    public DbUtility(Context context, String name) {
        super(context, name, null, SystemConst.DATABASE_VERSION);
    }

    public static final Class[] CLASSES = new Class[]{

    };
    
    static {
        for (Class base : CLASSES){
            cupboard().register(base);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
    }
}

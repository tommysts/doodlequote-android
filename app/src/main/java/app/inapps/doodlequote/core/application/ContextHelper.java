package app.inapps.doodlequote.core.application;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.PowerManager;

import java.util.Locale;

import app.inapps.doodlequote.common.exception.GenericRuntimeException;
import app.inapps.doodlequote.common.util.AssertUtil;

public class ContextHelper {
    private Context mContext;
    private ContextWrapper mContextWrapper;

    public ContextHelper(Context context) {
        AssertUtil.assertNotNull(context);

        mContext = context;
        mContextWrapper = new ContextWrapper(mContext);
    }

    public Context getContext() {
        return mContext;
    }

    public ContextWrapper getContextWrapper() {
        return mContextWrapper;
    }

    public PowerManager getPowerManager() {
        return (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
    }

    public PowerManager.WakeLock getPartialWakeLock(String name) {
        AssertUtil.assertNotNull(name);
        return getPowerManager().newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, name);
    }

    public PackageInfo getMyPackageInfo() {
        PackageInfo packageInfo = null;
        try {
            PackageManager manager = mContext.getPackageManager();
            String name = mContext.getPackageName();
            packageInfo = manager.getPackageInfo(name, PackageManager.GET_ACTIVITIES);
        } catch (NameNotFoundException e) {
            throw new GenericRuntimeException(e);
        }
        return packageInfo;
    }

    public int getMyPackageInfoVersionCode() {
        return getMyPackageInfo().versionCode;
    }

    public String getMyPackageInfoVersionName() {
        return getMyPackageInfo().versionName;
    }

    public String getApplicationName() {
        ApplicationInfo a = getMyPackageInfo().applicationInfo;
        return getContext().getPackageManager().getApplicationLabel(a).toString();
    }

    public String getApplicationPackageName() {
        return mContext.getPackageName();
    }

    public String[] getPermissionList() {
        PackageManager packageManager = mContext.getPackageManager();
        String[] result = null;
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    mContext.getPackageName(), PackageManager.GET_PERMISSIONS);
            result = packageInfo.requestedPermissions;
        } catch (NameNotFoundException e) {
            return null;
        }
        return result;
    }

    public Locale getLocale() {
        return mContext.getResources().getConfiguration().locale;
    }
}

package app.inapps.doodlequote.core.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextClock;

import java.lang.ref.WeakReference;

public class DefaultTextClock extends TextClock {
    private final WeakReference<Context> refContext;

    public DefaultTextClock(Context context, AttributeSet attrs) {
        super(context, attrs);

        refContext = new WeakReference<Context>(context);
    }

    public DefaultTextClock(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        refContext = new WeakReference<Context>(context);
    }

    protected final Context getWeakReferenceContext() {
        return refContext.get();
    }
}

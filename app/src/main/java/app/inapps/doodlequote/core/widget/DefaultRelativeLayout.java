package app.inapps.doodlequote.core.widget;


import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class DefaultRelativeLayout extends RelativeLayout {
    protected DefaultViewListener mListener;
    protected boolean mIsFinishInflate;

    public DefaultRelativeLayout(Context context) {
        super(context);
        //setup();
    }

    public DefaultRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setup();
    }

    public DefaultRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //setup();
    }

    @TargetApi(21)
    public DefaultRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        //setup();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mIsFinishInflate = true;
        setup();
        if(mListener != null) {
            mListener.onFinishInflate();
        }
    }

    public void setListener(DefaultViewListener listener) {
        mListener = listener;
        if(mIsFinishInflate && mListener != null) {
            mListener.onFinishInflate();
        }
    }

    protected void setup() {

    }

    public ImageView[] getImageViews() {
        return null;
    }

    public EditText[] getEditTexts() {
        return null;
    }
}

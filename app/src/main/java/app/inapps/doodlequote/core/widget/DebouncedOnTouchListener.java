package app.inapps.doodlequote.core.widget;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * A Debounced OnTouchListener
 * Rejects clicks that are too close together in time.
 * This class is safe to use as an OnTouchListener for multiple views, and will debounce each one separately.
 */
public abstract class DebouncedOnTouchListener implements View.OnTouchListener {
    /**
     * MINIMUM_INTERVAL_TIME The minimum allowed time between clicks
     * any click sooner than this after a previous click will be rejected
     */
    private static final long MINIMUM_INTERVAL_TIME = 500; // 500ms
    private long startClickTime;
    private Map<View, Long> mLastClickedMap;

    public DebouncedOnTouchListener() {
        this.mLastClickedMap = new WeakHashMap<View, Long>();
    }

    /**
     * Implement this in your subclass instead of onClick
     *
     * @param v The view that was clicked
     */
    public abstract boolean onDebouncedTouch(View v, MotionEvent event);

    @Override
    public boolean onTouch(View clickedView, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            startClickTime = System.currentTimeMillis();
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            if (System.currentTimeMillis() - startClickTime < ViewConfiguration.getTapTimeout()) {
                // Touch was a simple tap.
                Long previousClickTimestamp = mLastClickedMap.get(clickedView);
                long currentTimestamp = SystemClock.uptimeMillis();
                mLastClickedMap.put(clickedView, currentTimestamp);
                if (previousClickTimestamp == null || (currentTimestamp - previousClickTimestamp > MINIMUM_INTERVAL_TIME)) {
                    return onDebouncedTouch(clickedView, motionEvent);
                }
            }
        }
        return false;
    }
}
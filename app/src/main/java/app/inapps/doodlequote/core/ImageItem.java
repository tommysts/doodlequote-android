package app.inapps.doodlequote.core;

public class ImageItem extends FileItem {
    private int rotation;

    public ImageItem(String path, int rotation) {
        super(path);
        this.rotation = rotation;
    }

    public ImageItem(String path) {
        super(path);
        this.rotation = 0;
    }

    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }
}

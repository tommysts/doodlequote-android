package app.inapps.doodlequote.core.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import app.inapps.doodlequote.app.AppConst;

public class BaseUserPreferences {
    private static final char PERSONALIZED_SEPARATOR = '_';
    private final SharedPreferences mSharedPrefs;
    private StringBuilder mPersonalizedStringBuilder = new StringBuilder();
    private StringBuilder sb = new StringBuilder();

    public BaseUserPreferences(Context context) {
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPrefs;
    }

    public synchronized String personalize(String preference) {
        mPersonalizedStringBuilder.setLength(0);
        mPersonalizedStringBuilder.append(preference).append(PERSONALIZED_SEPARATOR);

//        String uid = "uid";
//        if (uid != null) {
//            mPersonalizedStringBuilder.append(uid);
//        }

        return mPersonalizedStringBuilder.toString();
    }

    public void setToken(String token) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.TOKEN, token).apply();
    }

    public String getToken() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.TOKEN, "");
    }

    public void setDeviceToken(String token) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.DEVICE_TOKEN, token).apply();
    }

    public String getDeviceToken() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.DEVICE_TOKEN, "");
    }

    public void setEndPoint(String value) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.END_POINT, value).apply();
    }

    public String getEndPoint() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.END_POINT, "");
    }

    public void setId(String value) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.ID, value).apply();
    }

    public String getId() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.ID, "");
    }
    
    public void setName(String value) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.NAME, value).apply();
    }

    public String getName() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.NAME, "");
    }
    
    public void setEmail(String value) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.EMAIL, value).apply();
    }

    public String getEmail() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.EMAIL, "");
    }
    
    public void setCreatedAt(String value) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.CREATED_AT, value).apply();
    }

    public String getCreatedAt() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.CREATED_AT, "");
    }
    
    public void setUpdatedAt(String value) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.UPDATED_AT, value).apply();
    }

    public String getUpdatedAt() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.UPDATED_AT, "");
    }

    public void setLanguage(String value) {
        mSharedPrefs.edit().putString(BaseUserPreferencesConstants.LANGUAGE, value).apply();
    }

    public String getLanguage() {
        return mSharedPrefs.getString(BaseUserPreferencesConstants.LANGUAGE, "");
    }

    public void setLoginType(int value) {
        mSharedPrefs.edit().putInt(BaseUserPreferencesConstants.LOGIN_TYPE, value).apply();
    }

    public int getLoginType() {
        return mSharedPrefs.getInt(BaseUserPreferencesConstants.LOGIN_TYPE, AppConst.LoginType.User.ordinal());
    }

    public void logout(){
        setToken("");
    }
}

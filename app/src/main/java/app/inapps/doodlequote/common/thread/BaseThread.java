package app.inapps.doodlequote.common.thread;

import app.inapps.doodlequote.common.util.AssertUtil;

public class BaseThread extends Thread {
    private static final int MAX_ALIVE_THREAD_COUNT = 30;
    private Runnable mRunnable;

    public BaseThread() {
    }

    public BaseThread(Runnable runnable) {
        AssertUtil.assertNotNull(runnable);
        mRunnable = runnable;
    }

    @Override
    public void interrupt() {
        super.interrupt();
    }

    @Override
    public final void run() {
        try {
            try {
                try {
                    runEx();
                } finally {
                    ThreadPool.getInstance().deleteThread(this);
                }
            } finally {
            }
        } finally {

        }
    }

    private void runEx() {
        try {
            initializeInstance();
            execute();
        } catch (Throwable e) {

        } finally {
            try {
                terminateInstance();
            } catch (Throwable e) {
            }
        }
    }

    protected void initializeInstance() throws Throwable {
    }

    protected void terminateInstance() throws Throwable {
    }

    protected void execute() throws Throwable {
        AssertUtil.assertNotNull(mRunnable);
        mRunnable.run();
    }

    public final void startThread() {
        ThreadPool.getInstance().addThread(this);
    }
}

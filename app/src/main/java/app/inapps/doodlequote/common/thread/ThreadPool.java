package app.inapps.doodlequote.common.thread;

import app.inapps.doodlequote.common.exception.BaseInterruptedException;
import app.inapps.doodlequote.common.util.AssertUtil;

import java.util.ArrayList;
import java.util.List;

public final class ThreadPool {

    /**
     * スレッドプール
     */
    private static ThreadPool mThreadPool = new ThreadPool();

    /**
     * スレッドリスト
     */
    private List<BaseThread> list = new ArrayList<BaseThread>();

    private Runnable onThreadCountIsZeroListener;

    private ThreadPool() {
        initialize();
    }

    public static ThreadPool getInstance() {
        return mThreadPool;
    }

    public synchronized void initialize() {
        list.clear();
    }

    public synchronized void setOnThreadCountIsZeroListener(Runnable r) {
        onThreadCountIsZeroListener = r;
    }

    public synchronized void addThread(BaseThread t) {
        AssertUtil.assertNotNull(t);
        addThreadLoose(t);
    }

    public synchronized void addThreadLoose(BaseThread t) {
        AssertUtil.assertNotNull(t);

        t.start();
        list.add(t);
    }

    private synchronized BaseThread[] getAllThreads() {
        BaseThread[] bb = list.toArray(new BaseThread[list.size()]);
        AssertUtil.assertNotNull(bb);
        return bb;
    }

    public synchronized int getThreadCount() {
        return list.size();
    }

    protected synchronized void deleteThread(BaseThread b) {
        AssertUtil.assertNotNull(b);
        if (list.remove(b)) {

        }

        if (list.size() == 0 && onThreadCountIsZeroListener != null) {
            onThreadCountIsZeroListener.run();
        }
    }

    public void join() {
        BaseThread[] bb = getAllThreads();
        for (BaseThread b : bb) {
            AssertUtil.assertNotNull(b);

            try {
                b.join();
            } catch (InterruptedException e) {
                throw new BaseInterruptedException(e);
            }
        }
    }

    public void waitForAllThreadExit() {
        BaseThread[] bb = getAllThreads();
        for (BaseThread b : bb) {
            AssertUtil.assertNotNull(b);
            //b.waitForExit();
        }
    }

    public void interruptAllThread() {
        BaseThread[] tt = getAllThreads();
        for (BaseThread t : tt) {
            AssertUtil.assertNotNull(t);
            t.interrupt();
        }
    }
}

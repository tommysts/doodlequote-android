package app.inapps.doodlequote.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import app.inapps.doodlequote.common.exception.GenericRuntimeException;

public final class ReflectionUtil {

    private ReflectionUtil() {
    }

    public static Object getInstance(String clsNm) {
        AssertUtil.assertNotNull(clsNm);

        Object obj = null;
        try {
            obj = Class.forName(clsNm).newInstance();

        } catch (ClassNotFoundException e) {
            throw new GenericRuntimeException(e);
        } catch (InstantiationException e) {
            throw new GenericRuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new GenericRuntimeException(e);
        }
        return obj;
    }

    public static Object getInstance(Class<?> cls) {
        AssertUtil.assertNotNull(cls);

        Object obj = null;
        try {
            obj = cls.newInstance();

        } catch (InstantiationException e) {
            throw new GenericRuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new GenericRuntimeException(e);
        }
        return obj;
    }

    public static boolean hasMethod(Object obj, String methodName, Class<?>[] clsArray) {
        AssertUtil.assertNotNull(obj);
        AssertUtil.assertNotEmpty(methodName);

        boolean ret = false;
        try {
            obj.getClass().getMethod(methodName, clsArray);
            ret = true;
        } catch (SecurityException e) {
            ret = false;
        } catch (NoSuchMethodException e) {
            ret = false;
        }
        return ret;
    }

    public static Object invokeMethod(Object obj,
                                      String methodName,
                                      Class<?>[] clsArray,
                                      Object[] paramArray) {
        AssertUtil.assertNotNull(obj);
        AssertUtil.assertNotEmpty(methodName);

        Method method = null;
        try {
            method = obj.getClass().getMethod(methodName, clsArray);
        } catch (SecurityException e) {
            throw new GenericRuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new GenericRuntimeException(e);
        }

        Object ret = null;
        try {
            ret = method.invoke(obj, paramArray);
        } catch (IllegalArgumentException e) {
            throw new GenericRuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new GenericRuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new GenericRuntimeException(e);
        }
        return ret;
    }

    public static Object getStaticFieldValue(Class<?> cls, String fieldName) {
        AssertUtil.assertNotNull(cls);
        AssertUtil.assertNotNull(fieldName);

        Object result = null;
        try {
            Field field = cls.getField(fieldName);
            result = field.get(null);
        } catch (NoSuchFieldException e) {
            throw new GenericRuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new GenericRuntimeException(e);
        } catch (IllegalArgumentException e) {
            throw new GenericRuntimeException(e);
        }
        return result;
    }

    public static Class<?> getClass(String clazz) {
        AssertUtil.assertNotNull(clazz);
        Class<?> cls = null;
        try {
            cls = Class.forName(clazz);
        } catch (ClassNotFoundException e) {
            throw new GenericRuntimeException(e);
        }
        return cls;
    }
}

package app.inapps.doodlequote.common.exception;

public class DBException extends SystemException {

    private static final long serialVersionUID = 5753519603221866898L;

    public DBException() {
        super();
    }


    public DBException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DBException(String detailMessage) {
        super(detailMessage);
    }

    public DBException(Throwable throwable) {
        super(throwable);
    }

}

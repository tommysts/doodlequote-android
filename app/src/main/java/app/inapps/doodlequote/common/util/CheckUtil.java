package app.inapps.doodlequote.common.util;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CheckUtil {

    private static final String EMAIL_REGEX = "[\\w\\.\\-]+@(?:[\\w\\-]+\\.)+[\\w\\-]+";
    private static final String URL_REGEX =
            "^(https?|ftp)(:\\/\\/[-_.!~*\\'()a-zA-Z0-9;\\/?:\\@&=+\\$,%#]+)$";

    private CheckUtil() {
    }

    public static boolean isEmailAddress(String email) {
        if (ValidatorUtil.isEmpty(email)) {
            return true;
        }

        boolean result = match(email, EMAIL_REGEX);
        return result;
    }

    public static boolean isUrl(String url) {
        if (ValidatorUtil.isEmpty(url)) {
            return true;
        }

        boolean result = match(url, URL_REGEX);
        return result;
    }

    public static boolean compareDate(String valueFrom, String valueTo, String format) {
        AssertUtil.assertNotEmpty(format);
        if (ValidatorUtil.isEmpty(valueFrom)) {
            return true;
        }
        if (ValidatorUtil.isEmpty(valueTo)) {
            return true;
        }

        Date dateFrom = DateUtil.parse(valueFrom, format);
        Date dateTo = DateUtil.parse(valueTo, format);
        if (dateFrom.compareTo(dateTo) > 0) {
            return false;
        }
        return true;
    }

    public static boolean match(String src, String regex) {
        AssertUtil.assertNotNull(regex);
        if (ValidatorUtil.isEmpty(src)) {
            return true;
        }

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(src);
        boolean result = m.find();
        return result;
    }
}

package app.inapps.doodlequote.common.exception;

public class GenericException extends Exception {

    private static final long serialVersionUID = -6066192609196595124L;

    public GenericException() {
        super();
    }

    public GenericException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public GenericException(String detailMessage) {
        super(detailMessage);
    }

    public GenericException(Throwable throwable) {
        super(throwable);
    }

}

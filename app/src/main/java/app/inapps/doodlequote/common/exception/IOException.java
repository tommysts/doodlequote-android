package app.inapps.doodlequote.common.exception;

public class IOException extends SystemException {

    private static final long serialVersionUID = 5891526398313216508L;

    public IOException() {
        super();
    }

    public IOException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public IOException(String detailMessage) {
        super(detailMessage);
    }

    public IOException(Throwable throwable) {
        super(throwable);
    }

}

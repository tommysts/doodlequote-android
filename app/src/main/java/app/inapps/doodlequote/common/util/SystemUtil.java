package app.inapps.doodlequote.common.util;

public final class SystemUtil {
    private SystemUtil() {
    }

    public static void executeJavaVmGC() {
        String s1 = getJvmMemoryInfo();
        System.runFinalization();

        System.gc();
        String s2 = getJvmMemoryInfo();
    }

    public static String getJvmMemoryInfo() {

        Runtime r = Runtime.getRuntime();

        String s = "" + FormatUtil.convertMegaByte(r.freeMemory()) + "MB/"
                + FormatUtil.convertMegaByte(r.totalMemory()) + "MB("
                + FormatUtil.convertMegaByte(r.totalMemory() - r.freeMemory()) + "MB:"
                + FormatUtil.convertMegaByte(r.maxMemory()) + "MB)";
        String s1 = s;

        return s1;
    }
}

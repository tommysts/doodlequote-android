package app.inapps.doodlequote.common.exception;

public class SystemException extends GenericException {

    private static final long serialVersionUID = 1686719363459875354L;

    public SystemException() {
        super();
    }

    public SystemException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public SystemException(String detailMessage) {
        super(detailMessage);
    }

    public SystemException(Throwable throwable) {
        super(throwable);
    }

}

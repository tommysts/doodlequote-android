package app.inapps.doodlequote.common.exception;

public class BaseInterruptedException extends GenericRuntimeException {

    private static final long serialVersionUID = -5198664230633517360L;

    public BaseInterruptedException() {
        super();
    }

    public BaseInterruptedException(Throwable t) {
        super(t.getMessage());
    }

    public BaseInterruptedException(String detailMessage) {
        super(detailMessage);
    }
}

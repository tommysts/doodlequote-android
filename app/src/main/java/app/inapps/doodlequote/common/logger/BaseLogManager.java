package app.inapps.doodlequote.common.logger;

import app.inapps.doodlequote.BuildConfig;
import timber.log.Timber;

/**
 * Created by ChungPV1 on 8/1/2016.
 */
public class BaseLogManager {
    private static final String TAG = BaseLogManager.class.getSimpleName();

    public BaseLogManager() {
    }

    public void log(String message) {
        if(BuildConfig.DEBUG) {
            Timber.tag(TAG);
            Timber.d(message);
        }
    }

    public void e(String message) {
        if(BuildConfig.DEBUG) {
            Timber.tag(TAG);
            Timber.e(message);
        }
    }

    public void log(Throwable e) {
        if(BuildConfig.DEBUG) {
            e.printStackTrace();
        }
    }
}

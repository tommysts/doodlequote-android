package app.inapps.doodlequote.common.util;

import android.text.TextUtils;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import app.inapps.doodlequote.common.exception.GenericRuntimeException;

/**
 * Created by ChungPV1 on 8/1/2016.
 */
public class ValidatorUtil {
    private static final String NUMBER_CHAR = "0123456789-";
    private static final char REPLACEMENT_CHARACTER = '\uFFFD';

    private ValidatorUtil() {
    }

    public static boolean isEmpty(String text) {
        if (text == null || text.length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean isAscii(String text) {

        if (text == null) {
            return false;
        }
        if (text.length() == 0) {
            return true;
        }

        int length = text.length();
        for (int i = 0; i < length; i++) {
            char ch = text.charAt(i);

            if (!(ch >= 0 && ch <= 0x7F)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isComposedOf(String text, String consistValue) {

        AssertUtil.assertNotEmpty(consistValue, "consistValue");

        if (text == null) {
            return false;
        }
        if (text.length() == 0) {
            return true;
        }

        int length = text.length();
        for (int i = 0; i < length; i++) {
            char ch = text.charAt(i);

            if (consistValue.indexOf(ch) == -1) {
                return false;
            }
        }

        return true;
    }

    public static boolean doesNotContain(String text, String unacceptableValue) {

        AssertUtil.assertNotEmpty(unacceptableValue, "unacceptableValue");

        if (text == null) {
            return false;
        }
        if (text.length() == 0) {
            return true;
        }

        for (int i = 0; i < unacceptableValue.length(); i++) {
            char ch = unacceptableValue.charAt(i);

            if (text.indexOf(ch) != -1) {
                return false;
            }
        }
        return true;
    }

    public static boolean isDuplicate(String[] texts) {

        AssertUtil.assertNotNull(texts, "texts");
        AssertUtil.assertTrue(texts.length != 0, "texts.length");

        for (int i = 0; i < texts.length; i++) {
            AssertUtil.assertNotNull(texts[i], "texts[" + i + "]");
        }

        String[] tmp = new String[texts.length];
        System.arraycopy(texts, 0, tmp, 0, texts.length);

        Arrays.sort(tmp);

        int length = tmp.length;
        for (int i = 0; i < length - 1; i++) {
            if (tmp[i].length() != 0 && tmp[i].equals(tmp[i + 1])) {
                return true;
            }
        }
        return false;
    }

    public static boolean contains(String text, String[] valueList) {

        AssertUtil.assertNotNull(text, "text");
        AssertUtil.assertNotNull(valueList, "valueList");
        AssertUtil.assertTrue(valueList.length != 0, "valueList.length");

        for (int i = 0; i < valueList.length; i++) {
            AssertUtil.assertNotNull(valueList[i], "valueList[" + i + "]");
        }

        int length = valueList.length;
        for (int i = 0; i < length; i++) {
            if (text.equals(valueList[i])) {
                return true;
            }
        }

        return false;
    }

    public static boolean isDouble(String text) {

        if (isEmpty(text)) {
            return false;
        }

        try {
            Double.parseDouble(text);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static boolean isFloat(String text) {

        if (isEmpty(text)) {
            return false;
        }

        try {
            Float.parseFloat(text);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static boolean isInt(String text) {

        if (isEmpty(text)) {
            return false;
        }

        try {
            Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return false;
        }

        return isComposedOf(text, NUMBER_CHAR);
    }

    public static boolean isLong(String text) {

        if (isEmpty(text)) {
            return false;
        }

        try {
            Long.parseLong(text);
        } catch (NumberFormatException e) {
            return false;
        }

        return isComposedOf(text, NUMBER_CHAR);
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPassword(String password){
        return !TextUtils.isEmpty(password) && password.length() == 4;
    }

    public static boolean isValidPhone(String phone) {
        AssertUtil.assertNotEmpty(phone);
        Pattern pattern = Pattern.compile("^\\+[1-9]{1}[0-9]{3,14}$");
        return pattern.matcher(phone).matches();
    }

    public static boolean isValidUrl(String url) {
        if(TextUtils.isEmpty(url)) {
            return false;
        }
        return true;
    }

    public static boolean isSafeText(String text, String encoding) {

        AssertUtil.assertNotEmpty(encoding, "encoding");

        if (text == null) {
            return false;
        }
        if (text.length() == 0) {
            return true;
        }

        if (text.indexOf(REPLACEMENT_CHARACTER) != -1) {
            return false;
        }

        CharsetEncoder encoder = null;
        boolean ret = false;
        try {
            encoder = Charset.forName(encoding).newEncoder();
            ret = encoder.canEncode(text);

        } catch (Exception e) {
            throw new GenericRuntimeException("It is invalid encoding.", e);
        }
        return ret;
    }

    public static boolean isValidDate(String text, String datePattern) {
        return isValidDate(text, datePattern, Locale.getDefault());
    }

    public static boolean isValidDate(String text, String datePattern, Locale locale) {

        AssertUtil.assertNotEmpty(datePattern, "datePattern");
        AssertUtil.assertNotNull(locale, "locale");

        if (isEmpty(text)) {
            return false;
        }
        Date dt = null;
        try {
            dt = DateUtil.parse(text, datePattern, locale);

        } catch (GenericRuntimeException e) {
            return false;
        }

        String formattedDate = DateUtil.format(dt, datePattern, locale);

        if (text.charAt(text.length() - 1) != formattedDate
                .charAt(formattedDate.length() - 1)) {
            return false;
        }

        return true;
    }

    public static boolean isRange(int value, int min, int max) {
        return min <= value && value <= max;
    }

    public static boolean isRange(long value, long min, long max) {
        boolean ret = false;
        if (value >= min && value <= max) {
            ret = true;
        }
        return ret;
    }

    public static boolean isRange(float value, float min, float max) {
        boolean ret = false;
        if (value >= min && value <= max) {
            ret = true;
        }
        return ret;
    }

    public static boolean isRange(double value, double min, double max) {
        return min <= value && value <= max;
    }
}

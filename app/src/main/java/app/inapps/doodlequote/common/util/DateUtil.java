package app.inapps.doodlequote.common.util;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import app.inapps.doodlequote.common.exception.GenericRuntimeException;

/**
 * Created by ChungPV1 on 8/1/2016.
 */
public class DateUtil {
    public static final String FMT_YYYY_MM_DD_WITH_HYPHEN = "yyyy-MM-dd";
    public static final String FMT_YYYY_MM_DD_HH_MM_SS_WITH_HYPHEN = "yyyy-MM-dd HH:mm:ss";
    public static final String FMT_YYYY_MM_DD_HH_MM_SS_SSS_WITH_HYPHEN = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String FMT_YYYY_MM = "yyyy/MM";
    public static final String FMT_MM_DD = "MM/dd";
    public static final String FMT_YYYY_MM_DD = "yyyy/MM/dd";
    public static final String FMT_YYYY_MM_DD_HH_MM_SS = "yyyy/MM/dd HH:mm:ss";
    public static final String FMT_YYYY_MM_DD_HH_MM_SS_SSS = "yyyy/MM/dd HH:mm:ss.SSS";
    public static final String FMT_HH_MM_SS = "HH:mm:ss";
    public static final String FMT_HH_MM = "HH:mm";
    public static final String FMT_YYYYMM = "yyyyMM";
    public static final String FMT_YYYYMMDD = "yyyyMMdd";
    public static final String FMT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String FMT_YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
    public static final String FMT_HHMM = "HHmm";
    public static final String FMT_HHMMSS = "HHmmss";
    public static final String FMT_ELLIPSIS_DAY_OF_WEEK = "E";
    public static final String FMT_DAY_OF_WEEK = "EEEE";

    private DateUtil() {
    }

    public static Date toDate(int year, int month, int day) {
        AssertUtil.assertTrue(year > 0);
        AssertUtil.assertTrue(month > 0);
        AssertUtil.assertTrue(day > 0);

        return toDate(
                new GregorianCalendar(
                        year,
                        month - 1,
                        day,
                        0,
                        0,
                        0));
    }

    public static Date toDate(int year, int month, int day, int hour, int minute, int second) {
        AssertUtil.assertTrue(year > 0);
        AssertUtil.assertTrue(month > 0);
        AssertUtil.assertTrue(day > 0);
        AssertUtil.assertTrue(hour >= 0);
        AssertUtil.assertTrue(minute >= 0);
        AssertUtil.assertTrue(second >= 0);

        return toDate(
                new GregorianCalendar(
                        year,
                        month - 1,
                        day,
                        hour,
                        minute,
                        second));
    }

    public static Date toDate(int year, int month, int day,
                              int hour, int minute, int second, int millisec) {
        AssertUtil.assertTrue(year > 0);
        AssertUtil.assertTrue(month > 0);
        AssertUtil.assertTrue(day > 0);
        AssertUtil.assertTrue(hour >= 0);
        AssertUtil.assertTrue(minute >= 0);
        AssertUtil.assertTrue(second >= 0);
        AssertUtil.assertTrue(millisec >= 0);


        Date date = toDate(
                new GregorianCalendar(
                        year,
                        month - 1,
                        day,
                        hour,
                        minute,
                        second));

        date = setMillisecond(date, millisec);

        return date;
    }

    public static Date toDate(long time) {
        AssertUtil.assertTrue(time >= 0);

        Date date = new Date();

        date.setTime(time);

        return date;
    }

    public static Date toDate(GregorianCalendar calendar) {
        AssertUtil.assertNotNull(calendar, "calendar");

        return calendar.getTime();
    }

    public static GregorianCalendar toGregorianCalendar(Date date) {
        AssertUtil.assertNotNull(date, "date");

        GregorianCalendar calendar = new GregorianCalendar();

        calendar.setTime(date);

        return calendar;
    }

    public static GregorianCalendar toGregorianCalendar(long time) {
        AssertUtil.assertTrue(time >= 0);

        return toGregorianCalendar(toDate(time));
    }

    public static Date truncDate(Date date, String format) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotEmpty(format);

        String result = format(date, format);

        return parse(result, format);
    }

    public static Date truncDate(Date date, String format, Locale locale) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotEmpty(format);
        AssertUtil.assertNotNull(locale);

        String result = format(date, format, locale);

        return parse(result, format, locale);
    }

    public static int get(int type, Date date) {
        AssertUtil.assertNotNull(date);

        GregorianCalendar calendar = toGregorianCalendar(date);

        return calendar.get(type);
    }

    public static int getYear(Date date) {
        AssertUtil.assertNotNull(date);

        return get(Calendar.YEAR, date);
    }

    public static int getMonth(Date date) {
        AssertUtil.assertNotNull(date);

        return get(Calendar.MONTH, date) + 1;
    }

    public static int getDay(Date date) {
        AssertUtil.assertNotNull(date);

        return get(Calendar.DATE, date);
    }

    public static int getHour(Date date) {
        AssertUtil.assertNotNull(date);

        return get(Calendar.HOUR_OF_DAY, date);
    }

    public static int getMinute(Date date) {
        AssertUtil.assertNotNull(date);

        return get(Calendar.MINUTE, date);
    }

    public static int getSecond(Date date) {
        AssertUtil.assertNotNull(date);

        return get(Calendar.SECOND, date);
    }

    public static int getMillisecond(Date date) {
        AssertUtil.assertNotNull(date);

        return get(Calendar.MILLISECOND, date);
    }

    public static String getDayOfWeek(Date date) {
        return getDayOfWeek(date, Locale.getDefault());
    }

    public static String getDayOfWeek(Date date, Locale locale) {
        SimpleDateFormat format = new SimpleDateFormat(FMT_ELLIPSIS_DAY_OF_WEEK, locale);

        return format.format(date);
    }

    public static int getLastDayOfMonth(Date date) {
        AssertUtil.assertNotNull(date);

        Date result = truncDate(date, FMT_YYYY_MM);
        result = addMonth(result, 1);
        result = addDay(result, -1);

        return getDay(result);
    }

    public static Date set(int type, int value, Date date) {
        AssertUtil.assertNotNull(date);

        GregorianCalendar calendar = toGregorianCalendar(date);

        calendar.set(type, value);

        return toDate(calendar);
    }

    public static Date setYear(Date date, int year) {
        AssertUtil.assertNotNull(date);

        return set(Calendar.YEAR, year, date);
    }

    public static Date setMonth(Date date, int month) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotNull(month);

        return set(Calendar.MONTH, month - 1, date);
    }

    public static Date setDay(Date date, int day) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotNull(day);

        return set(Calendar.DATE, day, date);
    }

    public static Date setHour(Date date, int hour) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotNull(hour);

        return set(Calendar.HOUR_OF_DAY, hour, date);
    }

    public static Date setMinute(Date date, int minute) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotNull(minute);

        return set(Calendar.MINUTE, minute, date);
    }

    public static Date setSecond(Date date, int second) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotNull(second);

        return set(Calendar.SECOND, second, date);
    }

    public static Date setMillisecond(Date date, int millisecond) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotNull(millisecond);

        return set(Calendar.MILLISECOND, millisecond, date);
    }

    public static Date add(int field, int amount, Date date) {
        AssertUtil.assertNotNull(field);
        AssertUtil.assertNotNull(date);

        GregorianCalendar calendar = toGregorianCalendar(date);

        calendar.add(field, amount);

        return toDate(calendar);
    }

    public static Date addYear(Date date, int amount) {
        AssertUtil.assertNotNull(date);

        return add(Calendar.YEAR, amount, date);
    }

    public static Date addMonth(Date date, int amount) {
        AssertUtil.assertNotNull(date);

        return add(Calendar.MONTH, amount, date);
    }

    public static Date addDay(Date date, int amount) {
        AssertUtil.assertNotNull(date);

        return add(Calendar.DATE, amount, date);
    }

    public static Date addHour(Date date, int amount) {
        AssertUtil.assertNotNull(date);

        return add(Calendar.HOUR_OF_DAY, amount, date);
    }

    public static Date addMinute(Date date, int amount) {
        AssertUtil.assertNotNull(date);

        return add(Calendar.MINUTE, amount, date);
    }

    public static Date addSecond(Date date, int amount) {
        AssertUtil.assertNotNull(date);

        return add(Calendar.SECOND, amount, date);
    }

    public static Date addMillisecond(Date date, int amount) {
        AssertUtil.assertNotNull(date);

        return add(Calendar.MILLISECOND, amount, date);
    }

    public static String toString(Date date) {
        AssertUtil.assertNotNull(date);

        return format(date, FMT_YYYY_MM_DD_HH_MM_SS_SSS);
    }

    public static String format(Date date, String format) {
        AssertUtil.assertNotNull(date);
        AssertUtil.assertNotEmpty(format);

        return format(date, format, Locale.getDefault());
    }

    public static String format(Date date, String format, Locale locale) {
        AssertUtil.assertNotNull(date, "date");
        AssertUtil.assertNotEmpty(format, "format");
        AssertUtil.assertNotNull(locale, "locale");

        SimpleDateFormat formatter;
        try {
            formatter = new SimpleDateFormat(format, locale);

        } catch (IllegalArgumentException e) {
            throw new GenericRuntimeException("It is not the date.", e);
        }

        return formatter.format(date);
    }

    public static Date parse(String date, String format) {
        AssertUtil.assertNotEmpty(date);
        AssertUtil.assertNotEmpty(format);

        return parse(date, format, Locale.getDefault());
    }

    public static Date parse(String date, String format, Locale locale) {
        AssertUtil.assertNotEmpty(date, "date");
        AssertUtil.assertNotEmpty(format, "format");
        AssertUtil.assertNotNull(locale, "locale");

        SimpleDateFormat formatter;

        try {
            formatter = new SimpleDateFormat(format, locale);

        } catch (IllegalArgumentException e) {
            throw new GenericRuntimeException("It is not the date.", e);
        }

        formatter.setLenient(false);

        Date parsedDate = null;
        try {
            parsedDate = formatter.parse(date);
        } catch (ParseException e) {
            throw new GenericRuntimeException("It is not the date.", e);
        }

        return parsedDate;
    }

    public static boolean equalsDate(Date date1, Date date2) {
        AssertUtil.assertNotNull(date1);
        AssertUtil.assertNotNull(date2);

        return truncDate(date1, FMT_YYYY_MM_DD).equals(truncDate(date2, FMT_YYYY_MM_DD));
    }

    public static String getServerFormattedDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FMT_YYYY_MM_DD_HH_MM_SS_WITH_HYPHEN, Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }

    public static Date getServerFormattedDate(String date) {
        if(!TextUtils.isEmpty(date)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(FMT_YYYY_MM_DD_HH_MM_SS_WITH_HYPHEN, Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            try {
                return dateFormat.parse(date);
            } catch (ParseException e) {
                //e.printStackTrace();
            }
        }
        return null;
    }

    public static Date getDateFromShortFormatted(String strDateTime) {
        if(TextUtils.isEmpty(strDateTime)){
            return null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(strDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static String getServerFormattedDate(String dateOccurred, String hrOccurred, String minOccurred) {
        if(!TextUtils.isEmpty(dateOccurred) && !TextUtils.isEmpty(hrOccurred) && !TextUtils.isEmpty(minOccurred)){
            Date date = getDateFromShortFormatted(dateOccurred);
            if(date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.set(Calendar.HOUR_OF_DAY, DataUtil.getInteger(hrOccurred));
                calendar.set(Calendar.MINUTE, DataUtil.getInteger(minOccurred));
                return getServerFormattedDate(calendar.getTime());
            }
        }
        return "";
    }

    public static String getTimeFromDuration(String durationInMinute) {
        String value = "";
        if(!TextUtils.isEmpty(durationInMinute)){
            int minutes = DataUtil.getInteger(durationInMinute, 0);
            if(minutes > 1440) {
                int days = minutes / 1440;
                int remain = minutes - (days * 1440);
                if(remain > 60) {
                    int hours = remain / 60;
                    remain = remain - (hours * 60);
                    if(remain > 0) {
                        value = days + "d " + hours + "h " + remain + "m";
                    } else {
                        value = days + "d " + hours + "h";
                    }
                } else {
                    value = days + "d";
                }

            } else if(minutes > 60){
                int hours = minutes / 60;
                int remain = minutes - (hours * 60);
                if(remain > 0) {
                    value = hours + "h " + remain + "m";
                } else {
                    value = hours + "h";
                }
            } else {
                value = minutes + "m";
            }
        }
        return value;
    }

    public static int compareDate(Date date1, Date date2){
        if(date1 != null && date2 != null){
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date1);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date2);
            int year = calendar1.get(Calendar.YEAR) - calendar2.get(Calendar.YEAR);
            int month = calendar1.get(Calendar.MONTH) - calendar2.get(Calendar.MONTH);
            int day = calendar1.get(Calendar.DAY_OF_MONTH) - calendar2.get(Calendar.DAY_OF_MONTH);
            if(year > 0){
                return 1;
            } else if(year < 0){
                return -1;
            } else {
                if(month > 0) {
                    return 1;
                } else if(month < 0) {
                    return -1;
                } else {
                    if(day > 0) {
                        return 1;
                    } else if(day < 0) {
                        return -1;
                    }
                }
            }
        }
        return 0;
    }

    public static int compareTime(Date date1, Date date2){
        if(date1 != null && date2 != null){
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date1);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date2);
            int hour = calendar1.get(Calendar.HOUR_OF_DAY) - calendar2.get(Calendar.HOUR_OF_DAY);
            int minute = calendar1.get(Calendar.MINUTE) - calendar2.get(Calendar.MINUTE);
            int second = calendar1.get(Calendar.SECOND) - calendar2.get(Calendar.SECOND);
            if(hour > 0){
                return 1;
            } else if(hour < 0){
                return -1;
            } else {
                if(minute > 0) {
                    return 1;
                } else if(minute < 0) {
                    return -1;
                } else {
                    if(second > 0) {
                        return 1;
                    } else if(second < 0) {
                        return -1;
                    }
                }
            }
        }
        return 0;
    }
}

package app.inapps.doodlequote.common.util;

import rx.Observer;

public class SimpleObserver<T> implements Observer<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable t) {

    }

    @Override
    public void onNext(T t) {

    }
}

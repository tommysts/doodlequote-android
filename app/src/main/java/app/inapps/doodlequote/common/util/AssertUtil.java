package app.inapps.doodlequote.common.util;

import app.inapps.doodlequote.common.exception.GenericRuntimeException;

/**
 * Created by ChungPV1 on 8/1/2016.
 */
public class AssertUtil {

    private AssertUtil() {
    }

    public static void assertNotNull(Object target) {
        assertNotNull(target, null);
    }

    public static void assertNotNull(Object target, String fieldName) {
        if (target == null) {
            if (ValidatorUtil.isEmpty(fieldName)) {
                throw new GenericRuntimeException("target is null.");
            } else {
                throw new GenericRuntimeException(fieldName + " is null.");
            }
        }
    }

    public static void assertNull(Object target) {
        assertNull(target, null);
    }

    public static void assertNull(Object target, String fieldName) {
        if (target != null) {
            if (ValidatorUtil.isEmpty(fieldName)) {
                throw new GenericRuntimeException("target is not null.");
            } else {
                throw new GenericRuntimeException(fieldName + " is not null.");
            }
        }
    }

    public static void assertNotEmpty(String target) {
        assertNotEmpty(target, null);
    }

    public static void assertNotEmpty(String target, String fieldName) {
        if (ValidatorUtil.isEmpty(target)) {
            if (ValidatorUtil.isEmpty(fieldName)) {
                throw new GenericRuntimeException("target is empty.");
            } else {
                throw new GenericRuntimeException(fieldName + " is empty.");
            }
        }
    }

    public static void assertEmpty(String target) {
        assertEmpty(target, null);
    }

    public static void assertEmpty(String target, String fieldName) {
        if (!ValidatorUtil.isEmpty(target)) {
            if (ValidatorUtil.isEmpty(fieldName)) {
                throw new GenericRuntimeException("target is not empty.");
            } else {
                throw new GenericRuntimeException(fieldName + " is not empty.");
            }
        }
    }

    public static void assertStringArrayNotNull(String[] array) {
        assertStringArrayNotNull(array, null);
    }

    public static void assertStringArrayNotNull(String[] array, String fieldName) {
        assertNotNull(array, fieldName);
        for (int i = 0; i < array.length; i++) {
            assertNotNull(array[i], fieldName);
        }
    }

    public static void assertStringArrayNotEmpty(String[] array) {
        assertStringArrayNotEmpty(array, null);
    }

    public static void assertStringArrayNotEmpty(String[] array, String fieldName) {
        assertNotNull(array, fieldName);
        for (int i = 0; i < array.length; i++) {
            assertNotEmpty(array[i], null);
        }
    }

    public static void assertTrue(boolean condition) {
        assertTrue(condition, null);
    }

    public static void assertTrue(boolean condition, String fieldName) {
        if (!condition) {
            if (ValidatorUtil.isEmpty(fieldName)) {
                throw new GenericRuntimeException("target is false.");
            } else {
                throw new GenericRuntimeException(fieldName + " is false.");
            }
        }
    }
}

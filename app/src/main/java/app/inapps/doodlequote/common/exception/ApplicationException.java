package app.inapps.doodlequote.common.exception;

import app.inapps.doodlequote.common.entity.ErrorEntity;

public class ApplicationException extends GenericException {

    private static final long serialVersionUID = -5407312247152756813L;
    private ErrorEntity mErrorEntity;

    public ApplicationException() {
        super();
    }

    public ApplicationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ApplicationException(String detailMessage) {
        super(detailMessage);
    }

    public ApplicationException(Throwable throwable) {
        super(throwable);
    }

    public ApplicationException(ErrorEntity errorEntity) {
        super();
        mErrorEntity = errorEntity;
    }

    public ApplicationException(String detailMessage, Throwable throwable,
                                ErrorEntity errorEntity) {
        super(detailMessage, throwable);
        mErrorEntity = errorEntity;
    }

    public ApplicationException(String detailMessage, ErrorEntity errorEntity) {
        super(detailMessage);
        mErrorEntity = errorEntity;
    }

    public ApplicationException(Throwable throwable, ErrorEntity errorEntity) {
        super(throwable);
        mErrorEntity = errorEntity;
    }

    public String getErrorCode() {
        if (mErrorEntity == null) {
            return null;
        }
        return mErrorEntity.getErrorCode();
    }

    public String getErrorMessage() {
        if (mErrorEntity == null) {
            return null;
        }
        return mErrorEntity.getErrorMessage();
    }

}

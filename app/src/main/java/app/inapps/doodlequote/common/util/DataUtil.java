package app.inapps.doodlequote.common.util;

import android.text.TextUtils;

import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DataUtil {
    public static final int INVALID_INT = -9999;
    public static final long INVALID_LONG = -9999;
    public static final float INVALID_FLOAT = -9999;
    public static final double INVALID_DOUBLE = -9999;
    public static final String TRUE = "1";
    public static final String FALSE = "0";

    public static boolean isTrue(String value){
        return !TextUtils.isEmpty(value) && ("1".equals(value) || "true".equalsIgnoreCase(value));
    }

    public static String getTrue(boolean value){
        return value ? TRUE : FALSE;
    }

    public static Integer getInteger(String value){
        Integer d = INVALID_INT;
        try{
            d = Integer.parseInt(value);
        } catch (Exception e){
        }
        return d;
    }

    public static Integer getInteger(String value, int defaultValue){
        Integer d = defaultValue;
        try{
            d = Integer.parseInt(value);
        } catch (Exception e){
            d = defaultValue;
        }
        return d;
    }

    public static Long getLong(String value){
        Long d = INVALID_LONG;
        try{
            d = Long.parseLong(value);
        } catch (Exception e){
        }
        return d;
    }

    public static Float getFloat(String value){
        Float d = INVALID_FLOAT;
        try{
            d = Float.parseFloat(value);
        } catch (Exception e){
        }
        return d;
    }

    public static Float getFloat(String value, float defaultValue){
        Float d = defaultValue;
        try{
            d = Float.parseFloat(value);
        } catch (Exception e){
            d = defaultValue;
        }
        return d;
    }
    
    public static Double getDouble(String value){
        Double d = INVALID_DOUBLE;
        try{
            d = Double.parseDouble(value);
        } catch (Exception e){
        }
        return d;
    }

    public static Double getDouble(String value, double defaultValue){
        Double d = defaultValue;
        try{
            d = Double.parseDouble(value);
        } catch (Exception e){
            d = defaultValue;
        }
        return d;
    }

    /** From m **/
    public static String formatDistanceFromM(double distance) {
        String unit = "m";
        if (distance >= 100) {
            distance /= 1000;
            unit = "km";
            return String.format("%.2f%s", distance, unit);
        }
        return String.format("%.0f%s", distance, unit);
    }

    /** From km **/
    public static String formatDistanceFromKm(double distance) {
        String unit = "km";
        return String.format("%.2f%s", distance, unit);
    }

    public static double formatPriceF(double price){
        // Check if this value has decimals
        //if(price % 1 != 0){
        //    price = price + 1; // Round to next value
        //}
        return price;
    }

    public static double formatPriceF(String value){
        double price = getDouble(value);
        price = formatPriceF(price);
        return price;
    }

    public static String formatPrice(double price){
        price = formatPriceF(price);
        return String.format(Locale.getDefault(), "$%.0f", price);
    }

    public static String formatPrice(String price){
        double value = getDouble(price, 0f);
        return formatPrice(value);
    }

    public static int getTimeZoneOffsetInSeconds(){
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        return tz.getOffset(now.getTime()) / 1000;
    }

    public static <T> void copy(Collection<T> destination, Collection<T> source){
        if(source != null && destination != null){
            destination.clear();
            for (T t : source){
                destination.add(t);
            }
        }
    }
}

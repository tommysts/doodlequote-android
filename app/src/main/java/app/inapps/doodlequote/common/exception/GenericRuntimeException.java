package app.inapps.doodlequote.common.exception;

public class GenericRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 2007500559089805701L;

    public GenericRuntimeException() {
        super();
    }

    public GenericRuntimeException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public GenericRuntimeException(String detailMessage) {
        super(detailMessage);
    }

    public GenericRuntimeException(Throwable throwable) {
        super(throwable);
    }

}

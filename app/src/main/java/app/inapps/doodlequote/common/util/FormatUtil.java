package app.inapps.doodlequote.common.util;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Locale;

public final class FormatUtil {
    /**
     * 1024
     */
    public static final int KILLO = 1024;
    /**
     * KILLO * KILLO
     */
    public static final int MEGA = KILLO * KILLO;
    /**
     * MEGGA * KILLO
     */
    public static final long GIGA = MEGA * KILLO;
    /**
     * GIGA * KILLO
     */
    public static final long TERA = GIGA * KILLO;
    /**
     * TERA * KILLO
     */
    public static final long PETA = TERA * KILLO;
    /**
     * PETA * KILLO
     */
    public static final long EXA = PETA * KILLO;

    /**
     * 1000
     */
    public static final long D_KILLO = 1000;
    /**
     * D_KILLO * 1000
     */
    public static final long D_MEGA = D_KILLO * D_KILLO;
    /**
     * D_MEGA * 1000
     */
    public static final long D_GIGA = D_MEGA * D_KILLO;
    /**
     * D_GIGA * 1000
     */
    public static final long D_TERA = D_GIGA * D_KILLO;
    /**
     * D_TERA * 1000
     */
    public static final long D_PETA = D_TERA * D_KILLO;
    /**
     * D_PETA * 1000
     */
    public static final long D_EXA = D_PETA * D_KILLO;

    /**
     * KILLOO, MEGA, GIGA, TERA, PETA, EXA の配列
     */
    private static final double[] KILLOS = {KILLO, MEGA, GIGA, TERA, PETA, EXA};
    /**
     * D_KIOO, D_MEGA, D_GIGA, D_TERA, D_PETA, D_EXA の配列
     */
    private static final double[] D_KILLOS = {D_KILLO, D_MEGA, D_GIGA, D_TERA, D_PETA, D_EXA};
    /**
     * [K, M, G, T, P, E] の配列
     */
    private static final String[] KILLOS_UNIT = {"K", "M", "G", "T", "P", "E"};

    private FormatUtil() {
    }

    public static String formatBigValueByte(double value) {
        return formatBigValue(value, "B");
    }

    public static String formatBigValue(double value, String unit) {
        AssertUtil.assertNotNull(unit);
        AssertUtil.assertNotEmpty(unit);

        if (unit.equalsIgnoreCase("B")
                || unit.equalsIgnoreCase("Byte")
                || unit.equalsIgnoreCase("Bytes")) {
            return formatBigValue(KILLOS, value, unit);
        } else {
            return formatBigValue(D_KILLOS, value, unit);
        }
    }

    private static String formatBigValue(double[] killos, double v, String unit) {
        AssertUtil.assertNotNull(killos);
        AssertUtil.assertNotNull(unit);
        AssertUtil.assertTrue(killos.length == KILLOS_UNIT.length);

        for (int i = killos.length - 1; i >= 0; i--) {
            double k = killos[i];
            if (v >= k) {
                return formatDouble(v / k) + KILLOS_UNIT[i] + unit;
            }
        }
        return formatDouble(v) + unit;
    }

    public static String formatDouble(double value) {
        return Double.toString(convertDouble(value));
    }

    public static String formatComma(String value) {
        AssertUtil.assertNotEmpty(value);

        return formatComma(Long.parseLong(value));
    }

    public static String formatComma(long value) {
        FieldPosition fpos = new FieldPosition(DecimalFormat.INTEGER_FIELD);
        StringBuffer sbuff = new StringBuffer();
        DecimalFormat df = new DecimalFormat();

        df.setGroupingSize(3);
        df.format(value, sbuff, fpos);

        return sbuff.toString();
    }

    public static String formatNumber(String value) {
        AssertUtil.assertNotEmpty(value);

        return formatNumber(Long.parseLong(value));
    }

    public static String formatNumber(long value) {
        AssertUtil.assertNotNull(value);

        return formatNumber(value, Locale.getDefault());
    }

    public static String formatNumber(long value, Locale locale) {
        AssertUtil.assertNotNull(value);
        AssertUtil.assertNotNull(locale);

        FieldPosition fpos = new FieldPosition(DecimalFormat.INTEGER_FIELD);
        StringBuffer sbuff = new StringBuffer();
        NumberFormat df = NumberFormat.getNumberInstance(locale);
        df.format(value, sbuff, fpos);

        return sbuff.toString();
    }

    public static String formatCurrency(String value) {
        AssertUtil.assertNotEmpty(value);

        return formatCurrency(Long.parseLong(value));
    }

    public static String formatCurrency(long value) {
        AssertUtil.assertNotNull(value);

        return formatCurrency(value, Locale.getDefault());
    }

    public static String formatCurrency(long value, Locale locale) {
        AssertUtil.assertNotNull(value);
        AssertUtil.assertNotNull(locale);

        FieldPosition fpos = new FieldPosition(DecimalFormat.INTEGER_FIELD);
        StringBuffer sbuff = new StringBuffer();
        NumberFormat df = NumberFormat.getCurrencyInstance(locale);
        df.format(value, sbuff, fpos);

        return sbuff.toString();
    }

    public static double convertKilloByte(double bt) {
        AssertUtil.assertNotNull(bt);
        return convertDouble(bt / KILLO, 1);
    }

    public static double convertMegaByte(double bt) {
        AssertUtil.assertNotNull(bt);
        return convertDouble(bt / MEGA, 1);
    }

    public static double convertDouble(double bt) {
        AssertUtil.assertNotNull(bt);
        return convertDouble(bt, 1);
    }

    public static double convertDouble(double bt, int d) {
        AssertUtil.assertNotNull(bt);
        AssertUtil.assertNotNull(d);

        double bb = bt;
        double dd = Math.pow(10, d);

        bb = Math.ceil(Math.round(bb * dd));

        return bb / dd;
    }

    public static String formatToHex(byte[] data) {
        AssertUtil.assertNotNull(data);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            builder.append(formatToHex(data[i]));
        }
        return builder.toString();
    }

    public static String formatToHex(byte data) {
        String s = "";
        byte dt = data;

        for (int j = 0; j < 2; j++) {
            char c = (char) (dt & 0x0f);

            if (c >= 0 && c <= 9) {
                c = (char) ('0' + c);
            } else {
                c = (char) ('A' + (c - 10));
            }
            s = c + s;
            dt = (byte) (dt >>> 4);
        }
        return s;
    }

    public static byte[] formatBinary(String src) {
        AssertUtil.assertNotNull(src);
        AssertUtil.assertTrue(src.length() > 1);

        byte[] ba = new byte[src.length() / 2];
        for (int i = 0, idx = 0; i < src.length(); i = i + 2) {
            String buf = src.substring(i, i + 2);
            int cnvBuf = Integer.parseInt(buf, 16);
            ba[idx++] = (byte) cnvBuf;
        }
        return ba;
    }

    public static String formatToMemoryAddress(int address) {
        AssertUtil.assertNotNull(address);

        String s = "";
        int add = address;
        for (int j = 0; j < 8; j++) {
            char c = (char) (add & 0x0f);
            if (c >= 0 && c <= 9) {
                c = (char) ('0' + c);
            } else {
                c = (char) ('A' + (c - 10));
            }

            if (j == 4) {
                s = c + ":" + s;
            } else {
                s = c + s;
            }
            add = add >>> 4;
        }
        return s;
    }

    public static String formatMessage(String msg, Object replaceString) {
        AssertUtil.assertNotNull(replaceString);
        return formatMessage(msg, new Object[]{replaceString});
    }

    public static String formatMessage(String msg, Object[] replaceString) {
        AssertUtil.assertNotEmpty(msg);
        String result = msg;
        if (replaceString != null) {
            result = MessageFormat.format(result, replaceString);
        }
        return result;
    }
}

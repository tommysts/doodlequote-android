package app.inapps.doodlequote.common.util;

import java.security.SecureRandom;

/**
 * Created by ChungPV1 on 8/1/2016.
 */
public class StringUtil {
    public static final char CR = '\r';
    public static final char LF = '\n';

    private static final String RANDOM_STRINGS_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "abcdefghijklmnopqrstuvwxyz"
            + "0123456789";

    private StringUtil() {
    }

    public static String formatNumber(String value) {
        return value.replaceAll("[^\\d.,]", "");
    }

    public static String nullToEmpty(Object value) {
        String s1;

        if (value == null) {
            s1 = "";
            return s1;
        }
        s1 = value.toString();
        return s1;
    }

    public static String nullToString(Object value, String defaultValue) {
        AssertUtil.assertNotEmpty(defaultValue);
        String s1;

        if (value == null) {
            s1 = defaultValue;
            return s1;
        }
        s1 = value.toString();
        return s1;
    }

    public static String createRandomString(int length) {
        AssertUtil.assertTrue(length > 0, "length is not larger than zero.");
        SecureRandom rnd = new SecureRandom();
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int val = rnd.nextInt(RANDOM_STRINGS_CHARS.length());
            buf.append(RANDOM_STRINGS_CHARS.charAt(val));
        }
        return buf.toString();
    }

    public static boolean compareSequence(CharSequence sequence1, CharSequence sequence2){
        if((sequence1 != null && sequence2 == null) || (sequence1 == null && sequence2 != null)){
            return false;
        } else if(sequence1 != null) {
            if(sequence1.length() != sequence2.length()){
                return false;
            } else {
                for(int i = 0; i < sequence1.length(); i++){
                    if(sequence1.charAt(i) != sequence2.charAt(i)){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static boolean containsIgnoreCase(final CharSequence str, final CharSequence searchStr) {
        if (str == null || searchStr == null) {
            return false;
        }
        final int len = searchStr.length();
        final int max = str.length() - len;
        for (int i = 0; i <= max; i++) {
            if (regionMatches(str, true, i, searchStr, 0, len)) {
                return true;
            }
        }
        return false;
    }

    private static boolean regionMatches(final CharSequence cs, final boolean ignoreCase, final int thisStart,
                                         final CharSequence substring, final int start, final int length) {
        if (cs instanceof String && substring instanceof String) {
            return ((String) cs).regionMatches(ignoreCase, thisStart, (String) substring, start, length);
        }
        int index1 = thisStart;
        int index2 = start;
        int tmpLen = length;

        while (tmpLen-- > 0) {
            final char c1 = cs.charAt(index1++);
            final char c2 = substring.charAt(index2++);

            if (c1 == c2) {
                continue;
            }
            if (!ignoreCase) {
                return false;
            }
            // The same check as in String.regionMatches():
            if (Character.toUpperCase(c1) != Character.toUpperCase(c2)
                    && Character.toLowerCase(c1) != Character.toLowerCase(c2)) {
                return false;
            }
        }
        return true;
    }
}

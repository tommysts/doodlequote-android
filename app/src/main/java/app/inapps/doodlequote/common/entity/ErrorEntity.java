package app.inapps.doodlequote.common.entity;

public class ErrorEntity {

    private String mErrorCode;
    private String mErrorMessage;

    public ErrorEntity() {
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(String errorCode) {
        mErrorCode = errorCode;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.mErrorMessage = errorMessage;
    }

}

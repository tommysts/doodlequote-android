package app.inapps.doodlequote.app.util;

import android.content.Context;
import android.util.Log;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.common.util.EncryptionUtil;

public class DebugUtil {
    public static boolean isDebugMode(Context context) {
        if (BuildConfig.DEBUG) {
            String deviceId = EncryptionUtil.getDeviceId(context);
            Log.d("BaseLogManager", "DeviceId = " + deviceId);
            return "b8cf40dd-02a0-3873-af51-c4a5465a0807".equalsIgnoreCase(deviceId)
                    || "9caa7ea7-1c56-3d33-97d3-acae8c40c235".equalsIgnoreCase(deviceId)
                    || "6c959e00-66fb-3285-bf79-ecd396ba729b".equalsIgnoreCase(deviceId)
                    || "28a5f062-a23c-3673-b060-fc80cf560336".equalsIgnoreCase(deviceId)
                    || "fd2db653-664f-3b38-bcd1-ddc87d78ec99".equalsIgnoreCase(deviceId)
                    || "d9a08b46-ea0c-3b8e-8df3-7ad034f6cb41".equalsIgnoreCase(deviceId)
                    || "7502131f-ec5a-365c-b94b-9fb151fbcab8".equalsIgnoreCase(deviceId)
                    || "2c77a6a0-bd90-3a2c-bbb3-633e931d7a2e".equalsIgnoreCase(deviceId)
                    || "248c2923-9acc-3bab-b28d-3645ea584f7e".equalsIgnoreCase(deviceId)
                    || "6deff8df-cf72-3955-a4fc-4e1bfe96524a".equalsIgnoreCase(deviceId)
                    || "27ce30d5-da79-3ea5-baf1-3197a53d2fa7".equalsIgnoreCase(deviceId)
                    || "e2bc25dd-e048-3a8f-8b78-d026020bdf29".equalsIgnoreCase(deviceId);

        }
        return false;
    }
}

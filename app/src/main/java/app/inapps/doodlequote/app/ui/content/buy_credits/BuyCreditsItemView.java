package app.inapps.doodlequote.app.ui.content.buy_credits;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.DefaultLinearLayout;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.DefaultViewListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BuyCreditsItemView extends DefaultLinearLayout {

    @BindView(R.id.tvDescription1) DefaultTextView mTvDescription1;
    @BindView(R.id.layout1) RelativeLayout mLayout1;
    @BindView(R.id.tvDescription2) DefaultTextView mTvDescription2;
    @BindView(R.id.layout2) RelativeLayout mLayout2;

    private int mX;

    interface BuyCreditsItemViewListener extends DefaultViewListener {
        void onItemClick(int x, int y);
    }

    private Unbinder mUnbinder;

    public BuyCreditsItemView(Context context) {
        super(context);
    }

    public BuyCreditsItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BuyCreditsItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public BuyCreditsItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void setup() {
        super.setup();
        mUnbinder = ButterKnife.bind(this, this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public ImageView[] getImageViews() {
        return null;
    }

    public EditText[] getEditTexts() {
        return new EditText[]{};
    }

    public void setX(int x) {
        mX = x;
    }

    public void registerActions() {
        mLayout1.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if(mListener instanceof BuyCreditsItemViewListener) {
                    ((BuyCreditsItemViewListener) mListener).onItemClick(mX, 0);
                }
            }
        });
        mLayout2.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if(mListener instanceof BuyCreditsItemViewListener) {
                    ((BuyCreditsItemViewListener) mListener).onItemClick(mX, 1);
                }
            }
        });
    }
}

package app.inapps.doodlequote.app.ui.content.company_info;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CompanyInfoAdapter extends BaseAdapter {
    private List<PaymentItem> mItems;
    private Context mContext;
    private Picasso mPicasso;
    private AdapterListener mListener;

    public interface AdapterListener {
        void onDeleteClick(int position);
    }

    public CompanyInfoAdapter(Context context, Picasso picasso, AdapterListener listener) {
        super();
        mContext = context;
        mPicasso = picasso;
        mListener = listener;
        mItems = new ArrayList<>();
    }

    public void clear() {
        mItems.clear();
    }

    public void addItem(PaymentItem item) {
        mItems.add(item);
    }

    public List<PaymentItem> getItems() {
        return mItems;
    }

    public void addItems(List<PaymentItem> items) {
        mItems.addAll(items);
    }

    public void deleteItem(int position) {
        mItems.remove(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.fragment_company_info_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        PaymentItem item = (PaymentItem) getItem(position);

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.ivDelete.setTag(position);
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = DataUtil.getInteger(view.getTag().toString(), 0);
                mListener.onDeleteClick(pos);
            }
        });
        viewHolder.tvCardType.setText(item.getCardType());
        viewHolder.tvCardNumber.setText(item.getCardNumber());
        viewHolder.tvExpiry.setText(item.getExpiry());

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.ivDelete) DefaultImageView ivDelete;
        @BindView(R.id.tvCardType) DefaultTextView tvCardType;
        @BindView(R.id.tvCardNumber) DefaultTextView tvCardNumber;
        @BindView(R.id.tvExpiry) DefaultTextView tvExpiry;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}


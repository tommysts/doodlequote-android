package app.inapps.doodlequote.app.ui.content.user;

import android.os.Parcel;
import android.os.Parcelable;

public class UsersItem implements Parcelable {
    private int id;
    private String name;
    private String firstName;
    private String lastName;
    private String email;
    private String mobile;
    private String title;
    private String imageName;
    private int type;
    private int usedCredit;
    private boolean isDisabled;
    private long createdTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isDisabled() {
        return isDisabled;
    }

    public void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUsedCredit() {
        return usedCredit;
    }

    public void setUsedCredit(int usedCredit) {
        this.usedCredit = usedCredit;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }


    public UsersItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.email);
        dest.writeString(this.mobile);
        dest.writeString(this.title);
        dest.writeString(this.imageName);
        dest.writeInt(this.type);
        dest.writeInt(this.usedCredit);
        dest.writeByte(this.isDisabled ? (byte) 1 : (byte) 0);
        dest.writeLong(this.createdTime);
    }

    protected UsersItem(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.email = in.readString();
        this.mobile = in.readString();
        this.title = in.readString();
        this.imageName = in.readString();
        this.type = in.readInt();
        this.usedCredit = in.readInt();
        this.isDisabled = in.readByte() != 0;
        this.createdTime = in.readLong();
    }

    public static final Creator<UsersItem> CREATOR = new Creator<UsersItem>() {
        @Override
        public UsersItem createFromParcel(Parcel source) {
            return new UsersItem(source);
        }

        @Override
        public UsersItem[] newArray(int size) {
            return new UsersItem[size];
        }
    };
}

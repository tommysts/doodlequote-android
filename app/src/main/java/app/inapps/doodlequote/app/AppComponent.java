package app.inapps.doodlequote.app;

import javax.inject.Singleton;

import app.inapps.doodlequote.app.api.ApiModule;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.PaymentActivity;
import app.inapps.doodlequote.app.ui.SplashActivity;
import app.inapps.doodlequote.app.ui.content.buy_credits.BuyCreditsFragment;
import app.inapps.doodlequote.app.ui.content.buy_credits.SmsCreditsFragment;
import app.inapps.doodlequote.app.ui.content.change_password.ChangePasswordFragment;
import app.inapps.doodlequote.app.ui.content.company_info.AddPaymentFragment;
import app.inapps.doodlequote.app.ui.content.company_info.CompanyInfoFragment;
import app.inapps.doodlequote.app.ui.content.dashboard.DashboardFragment;
import app.inapps.doodlequote.app.ui.content.forgot_password.ForgotPasswordFragment;
import app.inapps.doodlequote.app.ui.content.login.LoginFragment;
import app.inapps.doodlequote.app.ui.content.login.LoginPinFragment;
import app.inapps.doodlequote.app.ui.content.product.ProductCreateFragment;
import app.inapps.doodlequote.app.ui.content.product.ProductDetailFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesCreateFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesDetailFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesTemplatesFragment;
import app.inapps.doodlequote.app.ui.content.register.RegisterFragment;
import app.inapps.doodlequote.app.ui.content.templates.TemplatesCreateFragment;
import app.inapps.doodlequote.app.ui.content.templates.TemplatesFragment;
import app.inapps.doodlequote.app.ui.content.user.AddUserFragment;
import app.inapps.doodlequote.app.ui.content.user.UsersFragment;
import app.inapps.doodlequote.app.ui.content.user_profile.UserProfileFragment;
import dagger.Component;

@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class
        }
)
public interface AppComponent {
    void inject(SplashActivity activity);
    void inject(MainActivity activity);
    void inject(PaymentActivity activity);

    void inject(LoginFragment fragment);
    void inject(RegisterFragment fragment);
    void inject(DashboardFragment fragment);
    void inject(ForgotPasswordFragment fragment);
    void inject(QuotesFragment fragment);
    void inject(QuotesDetailFragment fragment);
    void inject(QuotesCreateFragment fragment);
    void inject(TemplatesFragment fragment);
    void inject(UserProfileFragment fragment);
    void inject(ChangePasswordFragment fragment);
    void inject(CompanyInfoFragment fragment);
    void inject(BuyCreditsFragment fragment);
    void inject(ProductDetailFragment fragment);
    void inject(ProductCreateFragment fragment);
    void inject(TemplatesCreateFragment fragment);
    void inject(QuotesTemplatesFragment fragment);
    void inject(AddPaymentFragment fragment);
    void inject(SmsCreditsFragment fragment);
    void inject(UsersFragment fragment);
    void inject(AddUserFragment fragment);
    void inject(LoginPinFragment fragment);

}

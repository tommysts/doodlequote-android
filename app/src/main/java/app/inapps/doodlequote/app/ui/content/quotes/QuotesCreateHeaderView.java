package app.inapps.doodlequote.app.ui.content.quotes;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import java.util.concurrent.TimeUnit;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultLinearLayout;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.DefaultViewListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;

public class QuotesCreateHeaderView extends DefaultLinearLayout {
    @BindView(R.id.edtCustomer) DefaultEditText mEdtCustomer;
    @BindView(R.id.edtMobile) DefaultEditText mEdtMobile;
    @BindView(R.id.tvStatus) DefaultTextView mTvStatus;
    @BindView(R.id.rlStatus) RelativeLayout mRlStatus;
    @BindView(R.id.rlTemplate) RelativeLayout mRlTemplate;
    @BindView(R.id.tvTemplate) DefaultTextView mTvTemplate;
    @BindView(R.id.tvTemplatePreview) DefaultTextView mTvTemplatePreview;
    @BindView(R.id.rlAddProduct) RelativeLayout mRlAddProduct;

    interface QuoteCreateHeaderViewListener extends DefaultViewListener{
        void onCustomerChange(String value);
        void onTemplateClick();
        void onAddProductClick();
    }

    private Unbinder mUnbinder;

    public QuotesCreateHeaderView(Context context) {
        super(context);
    }

    public QuotesCreateHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuotesCreateHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public QuotesCreateHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void setup() {
        super.setup();
        mUnbinder = ButterKnife.bind(this, this);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public ImageView[] getImageViews() {
        return null;
    }

    public EditText[] getEditTexts() {
        return new EditText[]{mEdtCustomer, mEdtMobile};
    }

    public void registerTextChangeEvents() {
        RxTextView.textChangeEvents(mEdtCustomer)
                .debounce(AppConst.DELAY_TEXT_CHANGED_INTERVAL_MILLISECONDS, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<TextViewTextChangeEvent>() {
                    @Override
                    public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                        if(mUnbinder != null && mListener instanceof QuoteCreateHeaderViewListener) {
                            ((QuoteCreateHeaderViewListener) mListener).onCustomerChange(mEdtCustomer.getText().toString());
                        }
                    }
                });
    }

    public void registerActions() {
        mRlTemplate.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                 if(mListener instanceof QuoteCreateHeaderViewListener) {
                     ((QuoteCreateHeaderViewListener) mListener).onTemplateClick();
                 }
            }
        });
        mRlAddProduct.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                 if(mListener instanceof QuoteCreateHeaderViewListener) {
                     ((QuoteCreateHeaderViewListener) mListener).onAddProductClick();
                 }
            }
        });
    }

    public String getCustomerName() {
        return mEdtCustomer.getText().toString();
    }

    public String getCustomerMobile() {
        return mEdtMobile.getText().toString();
    }
}

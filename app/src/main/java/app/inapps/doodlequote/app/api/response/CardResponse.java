package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class CardResponse extends BaseResponse {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("brand")
    @Expose
    public String brand;

    @SerializedName("customer")
    @Expose
    public String customer;

    @SerializedName("exp_month")
    @Expose
    public int expMonth;

    @SerializedName("exp_year")
    @Expose
    public int expYear;

    @SerializedName("last4")
    @Expose
    public String last4;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }
}

package app.inapps.doodlequote.app.ui.content.quotes;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.core.widget.CircleImageView;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultLinearLayout;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class QuotesDetailHeaderView extends DefaultLinearLayout {
    @BindView(R.id.ivAvatar) CircleImageView mIvAvatar;
    @BindView(R.id.tvName) DefaultTextView mTvName;
    @BindView(R.id.tvPhone) DefaultTextView mTvPhone;
    @BindView(R.id.ivStatus) DefaultImageView mIvStatus;
    @BindView(R.id.tvStatus) DefaultTextView mTvStatus;
    @BindView(R.id.llStatus) LinearLayout mLlStatus;
    @BindView(R.id.tvDescription) DefaultTextView mTvDescription;

    private Unbinder mUnbinder;

    public QuotesDetailHeaderView(Context context) {
        super(context);
    }

    public QuotesDetailHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuotesDetailHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public QuotesDetailHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void setup() {
        super.setup();
        mUnbinder = ButterKnife.bind(this, this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public ImageView[] getImageViews() {
        return new ImageView[]{mIvAvatar};
    }
}

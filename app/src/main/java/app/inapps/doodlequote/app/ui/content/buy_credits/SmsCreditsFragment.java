package app.inapps.doodlequote.app.ui.content.buy_credits;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CreditResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class SmsCreditsFragment extends BaseFragment {
    public static final String TAG = SmsCreditsFragment.class.getSimpleName();

    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.tvAvailable) DefaultTextView mTvAvailable;
    @BindView(R.id.ivAddPayment) AnimateImageView mIvAddPayment;
    @BindView(R.id.tvUsedWeek) DefaultTextView mTvUsedWeek;
    @BindView(R.id.tvUsedMonth) DefaultTextView mTvUsedMonth;
    @BindView(R.id.rlGetMore) RelativeLayout mRlGetMore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sms_credits, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavBack.setOnClickListener(this);
        mIvAddPayment.setOnClickListener(this);

        AppConst.LoginType loginType = AppUtil.getLoginType(mContainerMap);
        if (loginType == AppConst.LoginType.Staff) {
            mRlGetMore.setVisibility(View.GONE);
        }

        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        displayProgress(true);
        Subscription subscription = mApiManager.creditInfo(token).subscribe(new SimpleObserver<CreditResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                displayProgress(false);
            }

            @Override
            public void onNext(CreditResponse creditResponse) {
                super.onNext(creditResponse);
                displayProgress(false);
                if (creditResponse.isSuccess()) {
                    mTvAvailable.setText(creditResponse.getCreditAvailable());
                    mTvUsedWeek.setText(creditResponse.getCreditUsedInWeek());
                    mTvUsedMonth.setText(creditResponse.getCreditUsedInMonth());
                } else {
                    showError(creditResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        displayProgress(true);
        Subscription subscription = mApiManager.creditInfo(token).subscribe(new SimpleObserver<CreditResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                displayProgress(false);
            }

            @Override
            public void onNext(CreditResponse creditResponse) {
                super.onNext(creditResponse);
                displayProgress(false);
                if (creditResponse.isSuccess()) {
                    mTvAvailable.setText(creditResponse.getCreditAvailable());
                    mTvUsedWeek.setText(creditResponse.getCreditUsedInWeek());
                    mTvUsedMonth.setText(creditResponse.getCreditUsedInMonth());
                } else {
                    showError(creditResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.SmsCredits.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavBack) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavBack) {
            backClick(getFragmentId());
        } else if (v == mIvAddPayment) {
            buyCredits();
        }
    }

    private void buyCredits() {
        Bundle bundle = new Bundle();
        bundle.putString(OnFragmentInteractionListener.DATA_PAGE_STRING_EXTRA, getFragmentName());
        bundle.putBoolean(OnFragmentInteractionListener.DATA_ADD_BACK_STACK_BOOLEAN_EXTRA, true);
        openPage(MainActivity.Pages.BuyCredits.ordinal(), bundle);
    }

}

package app.inapps.doodlequote.app.ui.content;

import android.os.Bundle;
import android.support.annotation.StringRes;

import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.core.api.response.BaseResponse;

public interface OnFragmentInteractionListener {
    // This is special case when login/logout, we need to inject data again
    String ACTION_USER_UPDATE = "ACTION_USER_UPDATE";
    String ACTION_HIDE_KEYBOARD = "ACTION_HIDE_KEYBOARD";
    String ACTION_CHANGE_KEYBOARD = "ACTION_CHANGE_KEYBOARD";
    String ACTION_REGISTER_GLOBAL_LAYOUT_LISTENER = "ACTION_REGISTER_GLOBAL_LAYOUT_LISTENER";
    String ACTION_UNREGISTER_GLOBAL_LAYOUT_LISTENER = "ACTION_UNREGISTER_GLOBAL_LAYOUT_LISTENER";

    String ACTION_LOGOUT = "ACTION_LOGOUT";
    String ACTION_MENU_CLICK = "ACTION_MENU_CLICK";
    String ACTION_BACK_CLICK = "ACTION_BACK_CLICK";
    String ACTION_OPEN_PAGE = "ACTION_OPEN_PAGE";
    String DATA_MODE_INT_EXTRA = "DATA_MODE_INT_EXTRA";
    String DATA_PAGE_INT_EXTRA = "DATA_PAGE_INT_EXTRA";
    String DATA_PAGE_STRING_EXTRA = "DATA_PAGE_STRING_EXTRA";
    String DATA_IS_DRAWER_BOOLEAN_EXTRA = "DATA_IS_DRAWER_BOOLEAN_EXTRA";
    String DATA_ADD_BACK_STACK_BOOLEAN_EXTRA = "DATA_ADD_BACK_STACK_BOOLEAN_EXTRA";
    String DATA_BACK_TO_PAGE_BOOLEAN_EXTRA = "DATA_BACK_TO_PAGE_BOOLEAN_EXTRA";
    String DATA_REFRESH_BOOLEAN_EXTRA = "DATA_REFRESH_BOOLEAN_EXTRA";

    AppComponent getMainActivityComponent();

    boolean isLoadingData();

    void onFragmentInteraction(String action, Bundle data);

    void showError(String message);

    void showError(@StringRes int id);

    void showError(Throwable t, @StringRes int id);

    void showError(BaseResponse response, @StringRes int id);

    void showError(BaseResponse response, String message);

    void showMessage(String message);

    void showMessage(@StringRes int id);

    void showToast(String message);

    void showToast(@StringRes int id);

    void displayProgress(boolean show);

    void displayMessage(boolean show, String message);

    void displayMessage(boolean show, @StringRes int id);
}

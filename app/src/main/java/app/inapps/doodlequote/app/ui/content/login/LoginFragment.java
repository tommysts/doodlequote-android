package app.inapps.doodlequote.app.ui.content.login;

import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.ForgotPasswordResponse;
import app.inapps.doodlequote.app.api.response.LoginResponse;
import app.inapps.doodlequote.app.api.response.UpdateDeviceTokenResponse;
import app.inapps.doodlequote.app.api.response.UserProfileResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.app.util.gcm.GcmPreferences;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.util.CoreUtil;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.AnimateButton;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class LoginFragment extends BaseFragment {
    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.rlRootContent) RelativeLayout mRlRootContent;
    @BindView(R.id.llContent) LinearLayout mLlContent;
    @BindView(R.id.ivCoverImage) ImageView mIvCoverImage;
    @BindView(R.id.ivLogoImage) ImageView mIvLogoImage;
    @BindView(R.id.edt_email) DefaultEditText mEdtEmail;
    @BindView(R.id.edt_pin) DefaultEditText mEdtPin;
    @BindView(R.id.btn_sign_in) AnimateButton mBtnSignIn;
    @BindView(R.id.llForgotPasword) LinearLayout mLlForgotPasword;
    @BindView(R.id.llSignUp) LinearLayout mLlSignUp;
    @BindView(R.id.txtVersion) DefaultTextView mTxtVersion;
    @BindView(R.id.txtCopyrightInfo) DefaultTextView mTxtCopyrightInfo;

    private boolean mIsFocus;
    private boolean mIsShown;
    private boolean mIsAdjustUp;
    private boolean mIsAdjustDown;
    private boolean mNeedAdjustUp;
    private boolean mNeedAdjustDown;
    private boolean mIsGetLocation;
    private int mWindowHeight;
    private int mDistanceFromBottom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        unregisterGlobalLayoutListener(getFragmentId());
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        //mPicasso.load(R.drawable.back_ground).fit().into(mIvCoverImage);
        //mPicasso.load(R.drawable.logo).fit().into(mIvLogoImage);
        mTxtVersion.setText(getString(R.string.app_version, CoreUtil.getAppVersion(getActivity())));

        mBtnSignIn.setOnClickListener(this);
        mTxtCopyrightInfo.setOnClickListener(this);
        mLlForgotPasword.setOnClickListener(this);
        mLlSignUp.setOnClickListener(this);

        addEditTexts("1", mEdtEmail, mEdtPin);
        addImageViews(mIvLogoImage, mIvCoverImage);

        String email = mUserPreferences.getEmail();
        if(!TextUtils.isEmpty(email)) {
            mEdtEmail.setText(email);
        }

        mEdtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                mIsFocus = focus;
                if(mIsFocus) {
                    adjustLayoutUp();
                } else {
                    adjustLayoutDown();
                }
            }
        });
        mEdtEmail.setOnTouchListener(this);
        mEdtPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                mIsFocus = focus;
                if(mIsFocus) {
                    adjustLayoutUp();
                } else {
                    adjustLayoutDown();
                }
            }
        });
        mEdtPin.setOnTouchListener(this);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mWindowHeight = size.y;
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.Login.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mLlForgotPasword || v == mLlSignUp) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mBtnSignIn) {
            login();
        } else if (v == mLlForgotPasword) {
            forgotPassword();
        } else if (v == mLlSignUp) {
            openPage(MainActivity.Pages.Register);
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {
        if(v == mEdtPin || v == mEdtEmail) {
            mIsFocus = true;
            adjustLayoutUp();
        }
        return super.onInternalTouch(v, event);
    }

    @Override
    public void onResume() {
        super.onResume();
        changeKeyboard(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        registerGlobalLayoutListener(getFragmentId());
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
        changeKeyboard(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        registerGlobalLayoutListener(getFragmentId());
    }

    @Override
    public void onPause() {
        super.onPause();
        changeKeyboard(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        unregisterGlobalLayoutListener(getFragmentId());
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
        changeKeyboard(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        unregisterGlobalLayoutListener(getFragmentId());
    }

    @Override
    public void onGlobalLayout() {
        super.onGlobalLayout();
        if(!isAdded()) {
            return;
        }
        mLogManager.log("onGlobalLayout");

        if(!mIsGetLocation) {
            mLogManager.log("getLocation");
            mIsGetLocation = true;
            int[] locations = new int[2];
            mLlForgotPasword.getLocationOnScreen(locations);
            if (locations[1] == 0) {
                mIsGetLocation = false;
            } else {
                mDistanceFromBottom = mWindowHeight - locations[1];
                if (mDistanceFromBottom < 0) {
                    mDistanceFromBottom = 0;
                }
            }
        }

        int heightDiff = mRlRootContent.getRootView().getHeight() - mRlRootContent.getHeight();
        if (heightDiff > ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_KEYBOARD_HEIGHT)) {
            // if more than 200 dp, it's probably a keyboard do something here
            mIsShown = true;
            //mDistanceFromBottom = heightDiff - mDistanceFromBottom + 10;
            mDistanceFromBottom = heightDiff / 2;
            if(mDistanceFromBottom < 0) {
                mDistanceFromBottom = 0;
            }
            adjustLayoutUp();
        } else {
            mIsShown = false;
            adjustLayoutDown();
        }
    }

    private void adjustLayoutUp() {
        if(!isAdded()) {
            return;
        }
        if(mIsFocus && mIsShown && !mIsAdjustUp) {
            mLogManager.log("Adjust UP");
            mIsAdjustUp = true;
            mIsAdjustDown = false;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mLlContent.getLayoutParams();
            if(layoutParams != null) {
                layoutParams.setMargins(0, -mDistanceFromBottom, 0, 0);
                mLlContent.setLayoutParams(layoutParams);
            }
        }
    }

    private void adjustLayoutDown() {
        if(!isAdded()) {
            return;
        }
        if(!mIsAdjustDown) {
            mLogManager.log("Adjust DOWN");
            mIsAdjustUp = false;
            mIsAdjustDown = true;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mLlContent.getLayoutParams();
            if(layoutParams != null) {
                layoutParams.setMargins(0, 0, 0, 0);
                mLlContent.setLayoutParams(layoutParams);
            }
        }
    }

    private void login() {
        String email = mEdtEmail.getText().toString();
        String pin = mEdtPin.getText().toString();
        if (TextUtils.isEmpty(email)) {
            showError(R.string.error_email_require);
            return;
        }
        if (!ValidatorUtil.isValidEmail(email)) {
            showError(R.string.error_email_invalid);
            return;
        }
        if (TextUtils.isEmpty(pin)) {
            showError(R.string.error_pin_require);
            return;
        }
        displayProgress(true);
        Subscription subscription = mApiManager.login(email, pin).subscribe(new SimpleObserver<LoginResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(LoginResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    mUserPreferences.setToken(response.getToken());
                    mContainerMap.setBoolean(AppConst.KEY_LOG_IN, true);
                    mContainerMap.setString(AppConst.KEY_TOKEN, response.getToken());
                    UserProfileResponse userProfileResponse = response.getUser();
                    if(userProfileResponse != null) {
                        AppConst.LoginType loginType = AppUtil.getLoginType(userProfileResponse);
                        mContainerMap.setInt(AppConst.KEY_USER_TYPE, loginType.getType());
                        mContainerMap.setString(AppConst.KEY_COMPANY_NAME, userProfileResponse.getCompanyName());
                        mUserPreferences.setEmail(userProfileResponse.getEmail());
                        mUserPreferences.setLoginType(loginType.getType());
                    }
                    // GCM
                    GcmPreferences gcmPreferences = GcmPreferences.getInstance(getActivity());
                    gcmPreferences.storeUser(response);
                    Subscription subscription1 = mApiManager.updateDeviceToken(response.getToken(), gcmPreferences.getRegistrationId(), gcmPreferences.getDeviceType())
                            .subscribe(new SimpleObserver<UpdateDeviceTokenResponse>(){
                                @Override
                                public void onError(Throwable t) {
                                    super.onError(t);

                                    updateUser();
                                    openPage(MainActivity.Pages.Dashboard);
                                }

                                @Override
                                public void onNext(UpdateDeviceTokenResponse updateDeviceTokenResponse) {
                                    super.onNext(updateDeviceTokenResponse);

                                    updateUser();
                                    openPage(MainActivity.Pages.Dashboard);
                                }
                            });
                    addSubscription(subscription1);
                } else {
                    showError(response, R.string.error_login);
                }
            }
        });
        addSubscription(subscription);
    }

    public void forgotPassword() {
        String email = mEdtEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            showError(R.string.error_email_require);
            return;
        }
        if (!ValidatorUtil.isValidEmail(email)) {
            showError(R.string.error_email_invalid);
            return;
        }
        displayProgress(true);
        Subscription subscription = mApiManager.forgotPassword(email).subscribe(new SimpleObserver<ForgotPasswordResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ForgotPasswordResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    final AlertPopup alertPopup = new AlertPopup(getActivity());
                    alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                        @Override
                        public void onStart() {
                            alertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertPopup.dismiss();
                                    String email = mEdtEmail.getText().toString();
                                    mContainerMap.setString(AppConst.KEY_EMAIL, email);
                                    openPage(MainActivity.Pages.ForgotPassword);
                                }
                            });
                            alertPopup.setMessage(getString(R.string.forgot_password_success));
                        }
                    });
                    alertPopup.show();
                } else {
                    showError(response, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }
}

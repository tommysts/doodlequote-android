package app.inapps.doodlequote.app.db;

import android.app.Application;

import app.inapps.doodlequote.app.util.LogManager;
import app.inapps.doodlequote.core.db.BaseDbManager;

public class DatabaseManager extends BaseDbManager {
    public DatabaseManager(Application application, LogManager logManager) {
        super(application, logManager);
    }
}

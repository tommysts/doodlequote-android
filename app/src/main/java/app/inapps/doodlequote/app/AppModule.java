package app.inapps.doodlequote.app;

import javax.inject.Singleton;

import app.inapps.doodlequote.app.db.DatabaseManager;
import app.inapps.doodlequote.app.prefs.UserPreferences;
import app.inapps.doodlequote.app.util.LogManager;
import app.inapps.doodlequote.core.ContainerMap;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private AppApplication mApplication;
    private ContainerMap mContainerMap;

    public AppModule(AppApplication application, ContainerMap containerMap) {
        mApplication = application;
        mContainerMap = containerMap;
    }

    @Provides
    @Singleton
    public AppApplication provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public ContainerMap provideContainerMap() {
        return mContainerMap;
    }

    @Provides
    @Singleton
    public LogManager provideLogManager() {
        return new LogManager();
    }

    @Provides
    @Singleton
    public UserPreferences provideUserPreferences(AppApplication application) {
        return new UserPreferences(application);
    }

    @Provides
    @Singleton
    public DatabaseManager provideDatabaseManager(LogManager logManager) {
        return new DatabaseManager(mApplication, logManager);
    }
}

package app.inapps.doodlequote.app.ui.content.product;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.VideoPicker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kbeanie.multipicker.api.entity.ChosenVideo;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.widget.BrowsePopup;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.common.util.EncryptionUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.ImageItem;
import app.inapps.doodlequote.core.VideoItem;
import app.inapps.doodlequote.core.util.DisplayImageUtil;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.DefaultHeaderListView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class ProductCreateFragment extends BaseFragment implements ImagePickerCallback, VideoPickerCallback {
    public static final String TAG = ProductCreateFragment.class.getSimpleName();
    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.tvTitle) DefaultTextView mTvTitle;
    @BindView(R.id.rlDone) RelativeLayout mRlDone;
    @BindView(R.id.lvItems) DefaultHeaderListView mLvItems;

    private ProductItem mProductItem;
    private ProductCreateHeaderView mHeaderView;
    private ProductCreateAdapter mAdapter;
    private boolean mHandleBackPress = true;
    private String mPickerPath;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraPicker;
    private VideoPicker mVideoPicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_create, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavBack.setOnClickListener(this);
        mRlDone.setOnClickListener(this);

        Object item = mContainerMap.getObject(AppConst.KEY_PRODUCT_ITEM);
        if(item instanceof ProductItem) {
            mProductItem = (ProductItem) item;
            mContainerMap.remove(AppConst.KEY_PRODUCT_ITEM);
        }

        mHeaderView = (ProductCreateHeaderView) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_product_create_header, mLvItems, false);
        mHeaderView.setListener(new ProductCreateHeaderView.ProductCreateHeaderViewListener() {
            @Override
            public void onBrowseClick() {
                browse();
            }

            @Override
            public void onFinishInflate() {
                if(mProductItem != null) {
                    mHeaderView.mEdtName.setText(mProductItem.getName());
                    mHeaderView.mEdtDescription.setText(mProductItem.getDescription());

                    String clearStringPattern = "(^\\$)|(\\.0+$)|(,)";
                    String replacePattern = String.format("(^%s)|(\\.0+$)", AppUtil.getRegexCurrencySymbol());
                    String cleanString = AppUtil.formatPrice(mProductItem.getPrice()).replaceAll(clearStringPattern, "");
                    if(cleanString.contains(".")) {
                        String value = cleanString.substring(0, cleanString.indexOf("."));
                        String subValue = cleanString.substring(cleanString.indexOf("."), cleanString.length());
                        double parsed = DataUtil.getDouble(value, -1);
                        if(parsed != -1) {
                            try {
                                NumberFormat numberFormat = AppUtil.getCurrencyInstance();
                                String formatted = numberFormat.format(parsed).replaceAll(replacePattern, "") + subValue;
                                mHeaderView.mEdtPrice.setText(formatted);
                            } catch (NumberFormatException e) {
                                if(BuildConfig.DEBUG) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        double parsed = DataUtil.getDouble(cleanString, -1);
                        if(parsed != -1) {
                            try {
                                NumberFormat numberFormat = AppUtil.getCurrencyInstance();
                                String formatted = numberFormat.format(parsed).replaceAll(replacePattern, "");
                                mHeaderView.mEdtPrice.setText(formatted);
                            } catch (NumberFormatException e) {
                                if(BuildConfig.DEBUG) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }

                mHeaderView.setupPriceFormatter();
                mHeaderView.registerActions();
            }
        });
        mLvItems.addHeaderView(mHeaderView);
        mAdapter = new ProductCreateAdapter(getActivity(), mPicasso, new ProductCreateAdapter.AdapterListener() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onDeleteClick(int position) {
                mAdapter.deleteItem(position);
                mAdapter.notifyDataSetChanged();
            }
        });
        mLvItems.setAdapter(mAdapter);

        addEditTexts("1", mHeaderView.getEditTexts());
        addImageViews(mHeaderView.getImageViews());

        if(mProductItem != null) {
            mTvTitle.setText(R.string.product_edit);
            mAdapter.clear();
            if (savedInstanceState == null) {
                List<String> images = mProductItem.getImages();
                for(String image : images) {
                    UploadImageItem uploadImageItem = new UploadImageItem();
                    uploadImageItem.setName("Image " + (mAdapter.getCount() + 1));
                    uploadImageItem.setPath(image);
                    mAdapter.addItem(uploadImageItem);
                }
            } else {
                if(savedInstanceState.containsKey(AppConst.KEY_UPLOAD_IMAGES)) {
                    ArrayList<UploadImageItem> uploadImageItems = savedInstanceState.getParcelableArrayList(AppConst.KEY_UPLOAD_IMAGES);
                    if(uploadImageItems != null && uploadImageItems.size() > 0) {
                        for (UploadImageItem uploadImageItem : uploadImageItems) {
                            mAdapter.addItem(uploadImageItem);
                        }
                    }
                }
                if(savedInstanceState.containsKey(AppConst.KEY_UPLOAD_VIDEOS)) {
                    ArrayList<UploadVideoItem> uploadVideoItems = savedInstanceState.getParcelableArrayList(AppConst.KEY_UPLOAD_VIDEOS);
                    if(uploadVideoItems != null && uploadVideoItems.size() > 0) {
                        for (UploadVideoItem uploadVideoItem : uploadVideoItems) {
                            mAdapter.addItem(uploadVideoItem);
                        }
                    }
                }
            }
            mAdapter.notifyDataSetChanged();
        } else if (savedInstanceState != null) {
            if(savedInstanceState.containsKey(AppConst.KEY_UPLOAD_IMAGES)) {
                ArrayList<UploadImageItem> uploadImageItems = savedInstanceState.getParcelableArrayList(AppConst.KEY_UPLOAD_IMAGES);
                if(uploadImageItems != null && uploadImageItems.size() > 0) {
                    mAdapter.clear();
                    for (UploadImageItem uploadImageItem : uploadImageItems) {
                        mAdapter.addItem(uploadImageItem);
                    }
                    mAdapter.notifyDataSetChanged();
                }
            }
            if(savedInstanceState.containsKey(AppConst.KEY_UPLOAD_VIDEOS)) {
                ArrayList<UploadVideoItem> uploadVideoItems = savedInstanceState.getParcelableArrayList(AppConst.KEY_UPLOAD_VIDEOS);
                if(uploadVideoItems != null && uploadVideoItems.size() > 0) {
                    mAdapter.clear();
                    for (UploadVideoItem uploadVideoItem : uploadVideoItems) {
                        mAdapter.addItem(uploadVideoItem);
                    }
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(AppConst.KEY_PICKER_PATH)) {
                mPickerPath = savedInstanceState.getString(AppConst.KEY_PICKER_PATH);
            }
        }
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        if (mHandleBackPress) {
            final AlertPopup alertPopup = new AlertPopup(getActivity());
            alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    alertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                            mHandleBackPress = false;
                            backClick(getFragmentId());
                        }
                    });
                    alertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                        }
                    });
                    alertPopup.setMessage(getString(R.string.confirm_discard_changes));
                    alertPopup.setTitle(getString(R.string.app_name));
                }
            });
            alertPopup.show();
        }
        return mHandleBackPress;
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.ProductCreate.ordinal();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (mImagePicker == null) {
                    mImagePicker = new ImagePicker(this);
                    mImagePicker.shouldGenerateMetadata(true);
                    mImagePicker.shouldGenerateThumbnails(false);
                    mImagePicker.setImagePickerCallback(this);
                }
                mImagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (mCameraPicker == null) {
                    mCameraPicker = new CameraImagePicker(this);
                    mCameraPicker.setImagePickerCallback(this);
                    mCameraPicker.shouldGenerateMetadata(true);
                    mCameraPicker.shouldGenerateThumbnails(false);
                    mCameraPicker.reinitialize(mPickerPath);
                }
                mCameraPicker.submit(data);
            } else if (requestCode == Picker.PICK_VIDEO_DEVICE) {
                if (mVideoPicker == null) {
                    mVideoPicker = new VideoPicker(this);
                    mVideoPicker.setVideoPickerCallback(this);
                }
                mVideoPicker.submit(data);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(AppConst.KEY_PICKER_PATH, mPickerPath);
        if(mProductItem != null) {
            mContainerMap.setObject(AppConst.KEY_PRODUCT_ITEM, mProductItem);
        }
        if(mAdapter != null) {
            ArrayList<UploadItem> uploadItems = mAdapter.getItems();
            if(uploadItems != null && uploadItems.size() > 0) {
                ArrayList<UploadImageItem> uploadImageItems = new ArrayList<>();
                ArrayList<UploadVideoItem> uploadVideoItems = new ArrayList<>();
                for(UploadItem uploadItem : uploadItems) {
                    if(uploadItem instanceof UploadImageItem) {
                        uploadImageItems.add((UploadImageItem) uploadItem);
                    } else if(uploadItem instanceof UploadVideoItem) {
                        uploadVideoItems.add((UploadVideoItem) uploadItem);
                    }
                }
                outState.putParcelableArrayList(AppConst.KEY_UPLOAD_IMAGES, uploadImageItems);
                outState.putParcelableArrayList(AppConst.KEY_UPLOAD_VIDEOS, uploadVideoItems);
            }
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavBack) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavBack) {
            backClick(getFragmentId());
        } else if (v == mRlDone) {
            done();
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {
        return super.onInternalTouch(v, event);
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        List<ImageItem> imagePaths = new ArrayList<>();
        for(ChosenImage image : images) {
            imagePaths.add(AppUtil.createImageItem(image));
        }
        if(imagePaths.size() > 0) {
            displayProgress(true);
            Subscription subscription = DisplayImageUtil.rxOptimizeUploadImages(getActivity(), imagePaths).subscribe(new SimpleObserver<List<ImageItem>>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    showToast(R.string.error_loading_data);
                    displayProgress(false);
                }

                @Override
                public void onNext(List<ImageItem> imageItems) {
                    super.onNext(imageItems);
                    for(ImageItem imageItem : imageItems) {
                        loadImageFromPath(imageItem);
                        mLogManager.log("File Path: " + imageItem);
                    }
                    displayProgress(false);
                }
            });
            addSubscription(subscription);
        }
    }

    @Override
    public void onVideosChosen(List<ChosenVideo> videos) {
        List<VideoItem> videoPaths = new ArrayList<>();
        for(ChosenVideo video : videos) {
            videoPaths.add(AppUtil.createVideoItem(video));
        }
        if(videoPaths.size() > 0) {
            displayProgress(true);
            Subscription subscription = DisplayImageUtil.rxOptimizeUploadVideos(getActivity(), videoPaths).subscribe(new SimpleObserver<List<VideoItem>>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    showToast(R.string.error_loading_data);
                    displayProgress(false);
                }

                @Override
                public void onNext(List<VideoItem> videoItems) {
                    super.onNext(videoItems);
                    for(VideoItem videoItem : videoItems) {
                        loadVideoFromPath(videoItem);
                        mLogManager.log("File Path: " + videoItem);
                    }
                    displayProgress(false);
                }
            });
            addSubscription(subscription);
        }
    }

    @Override
    public void onError(String message) {
        showError(message);
    }

    private void browse() {
        BrowsePopup browsePopup = new BrowsePopup(getActivity());
        browsePopup.show();
        browsePopup.setListener(new BrowsePopup.BrowsePopupListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onTakePhotoClick(Dialog dialog) {
                dialog.dismiss();
                takePhoto();
            }

            @Override
            public void onOpenPhotosClick(Dialog dialog) {
                dialog.dismiss();
                openPhotos();
            }

            @Override
            public void onTakeVideoClick(Dialog dialog) {

            }

            @Override
            public void onOpenVideosClick(Dialog dialog) {
                dialog.dismiss();
                openVideos();
            }

            @Override
            public void onTakeRecordClick(Dialog dialog) {

            }

            @Override
            public void onOpenRecordsClick(Dialog dialog) {

            }
        });
    }

    private void done() {
        String name = mHeaderView.getProductName();
        String price = mHeaderView.getProductPrice();
        String description = mHeaderView.getProductDescription();

        if(TextUtils.isEmpty(name)) {
            showError(R.string.error_name_require);
            return;
        }
        if(TextUtils.isEmpty(price)) {
            showError(R.string.error_price_require);
            return;
        }
        price = AppUtil.trimCommaOfString(price); // remove comma from price
        if(!ValidatorUtil.isDouble(price)) {
            showError(R.string.error_price_invalid);
            return;
        }
        if(DataUtil.getDouble(price, 0) > 1000000) {
            showError(R.string.error_price_invalid_max);
            return;
        }
        if(TextUtils.isEmpty(description)) {
            showError(R.string.error_description_require);
            return;
        }
        if(mAdapter.getCount() == 0) {
            showError(R.string.error_image_require);
            return;
        }
        List<String> images = new ArrayList<>();
        for(UploadItem uploadItem : mAdapter.getItems()) {
            images.add(uploadItem.getPath());
        }

        if(mProductItem == null) {
            mProductItem = new ProductItem();
            mProductItem.setId(DataUtil.getInteger(EncryptionUtil.randomNumeric(5), 0));
        }
        mProductItem.setName(name);
        mProductItem.setDescription(description);
        mProductItem.setPrice(DataUtil.getDouble(price));
        mProductItem.setStatus(AppConst.ProductStatus.NewItem);
        mProductItem.setImages(images);

        mContainerMap.setObject(AppConst.KEY_PRODUCT_ITEM, mProductItem);
        mHandleBackPress = false;
        backClick(getFragmentId());
    }

    private void takePhoto() {
        if (!AppUtil.verifyCameraPermissions(getActivity())) {
            return;
        }
        mCameraPicker = new CameraImagePicker(this);
        mCameraPicker.shouldGenerateMetadata(true);
        mCameraPicker.shouldGenerateThumbnails(false);
        mCameraPicker.setImagePickerCallback(this);
        mPickerPath = mCameraPicker.pickImage();
    }

    private void openPhotos() {
        if (!AppUtil.verifyStoragePermissions(getActivity())) {
            return;
        }
        mImagePicker = new ImagePicker(this);
        mImagePicker.allowMultiple();
        mImagePicker.shouldGenerateMetadata(true);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.pickImage();
    }

    private void openVideos() {
        if (!AppUtil.verifyStoragePermissions(getActivity())) {
            return;
        }
        mVideoPicker = new VideoPicker(this);
        mVideoPicker.shouldGenerateMetadata(true);
        mVideoPicker.shouldGeneratePreviewImages(true);
        mVideoPicker.setVideoPickerCallback(this);
        mVideoPicker.allowMultiple();
        mVideoPicker.pickVideo();
    }

    private void loadImageFromPath(ImageItem imageItem){
        UploadImageItem uploadImageItem = new UploadImageItem();
        uploadImageItem.setName("Image " + (mAdapter.getCount() + 1));
        uploadImageItem.setPath(imageItem.getPath());
        mAdapter.addItem(uploadImageItem);
        mAdapter.notifyDataSetChanged();
    }

    private void loadVideoFromPath(VideoItem videoItem){
        UploadVideoItem uploadVideoItem = new UploadVideoItem();
        uploadVideoItem.setName("Video " + (mAdapter.getCount() + 1));
        uploadVideoItem.setPreviewImagePath(videoItem.getPreviewImage());
        uploadVideoItem.setPath(videoItem.getPath());
        mAdapter.addItem(uploadVideoItem);
        mAdapter.notifyDataSetChanged();
    }
}

package app.inapps.doodlequote.app.ui.content.change_password;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.UpdateUserProfileResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class ChangePasswordFragment extends BaseFragment {
    public static final String TAG = ChangePasswordFragment.class.getSimpleName();

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.edtCurrentPin) DefaultEditText mEdtCurrentPin;
    @BindView(R.id.edtNewPin) DefaultEditText mEdtNewPin;
    @BindView(R.id.edtConfirmPin) DefaultEditText mEdtConfirmPin;
    @BindView(R.id.ivUpdate) AnimateImageView mIvUpdate;
    @BindView(R.id.rlUpdate) RelativeLayout mRlUpdate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);
        mRlUpdate.setOnClickListener(this);

        addImageViews(mIvUpdate);
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.ChangePassword.ordinal();
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            menuClick();
        } else if (v == mRlUpdate) {
            update();
        }
    }

    private void update() {
        final String currentPin = mEdtCurrentPin.getText().toString();
        final String newPin = mEdtNewPin.getText().toString();
        final String confirmPin = mEdtConfirmPin.getText().toString();
        if (TextUtils.isEmpty(currentPin)) {
            showError(R.string.error_current_pin_require);
            return;
        }
        if (TextUtils.isEmpty(newPin)) {
            showError(R.string.error_new_pin_require);
            return;
        }
        if (!AppUtil.isValidPassword(newPin)) {
            showError(R.string.error_pin_invalid);
            return;
        }
        if (!newPin.equals(confirmPin)) {
            showError(R.string.error_pin_mismatch);
            return;
        }
        displayProgress(true);
        final String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.updateUserPassword(token, currentPin, newPin, confirmPin).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(UpdateUserProfileResponse userProfileResponse) {
                super.onNext(userProfileResponse);
                displayProgress(false);
                if (userProfileResponse.isSuccess()) {
                    mEdtCurrentPin.setText("");
                    mEdtNewPin.setText("");
                    mEdtConfirmPin.setText("");
                    showToast(R.string.update_success);
                } else {
                    showError(userProfileResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }
}

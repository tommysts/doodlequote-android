package app.inapps.doodlequote.app.util.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.LoginResponse;
import app.inapps.doodlequote.core.ContainerMap;

public class GcmPreferences {
    public static final String REGISTRATION_COMPLETE_ACTION = "GcmRegistrationComplete";
    public static final String RECEIVE_MESSAGE_ACTION = "GcmReceiveMessage";

    public static final String REGISTRATION_ID = "gcm_registration_id";
    public static final String DEVICE_TYPE = "gcm_device_type";
    public static final String IS_ENABLE_NOTIFICATION = "gcm_is_enable_notification";
    public static final String NOTIFICATION_COUNT = "gcm_notification_count";
    public static final String USER_TOKEN = "gcm_user_token";
    public static final String LOGIN_TYPE = "gcm_login_type";
    public static final String LANGUAGE = "gcm_language";

    public static final String DATA_QUOTE_ID_STRING_MESSAGE = "gcm_data_quote_id_string_message";
    public static final String DATA_CUSTOMER_NAME_STRING_MESSAGE = "gcm_data_customer_name_string_message";
    public static final String DATA_CUSTOMER_MOBILE_STRING_MESSAGE = "gcm_data_customer_mobile_string_message";

    private final SharedPreferences mSharedPrefs;
    private StringBuilder sb = new StringBuilder();

    private static GcmPreferences sGcmPreferences;

    public static GcmPreferences getInstance(Context context){
        if(sGcmPreferences == null) {
            sGcmPreferences = new GcmPreferences(context.getApplicationContext());
        }
        return sGcmPreferences;
    }

    private GcmPreferences(Context context) {
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setUserToken(String token) {
        mSharedPrefs.edit().putString(USER_TOKEN, token).apply();
    }

    public String getUserToken() {
        return mSharedPrefs.getString(USER_TOKEN, "");
    }

    public void setLoginType(int value) {
        mSharedPrefs.edit().putInt(LOGIN_TYPE, value).apply();
    }

    public int getLoginType() {
        return mSharedPrefs.getInt(LOGIN_TYPE, -1);
    }

    public void setRegistrationID(String value){
        mSharedPrefs.edit().putString(REGISTRATION_ID, value).apply();
    }

    public String getRegistrationId(){
        return mSharedPrefs.getString(REGISTRATION_ID, "");
    }

    public void setDeviceType(String value){
        mSharedPrefs.edit().putString(DEVICE_TYPE, value).apply();
    }

    public String getDeviceType(){
        return mSharedPrefs.getString(DEVICE_TYPE, "");
    }

    public void setLanguage(String value){
        mSharedPrefs.edit().putString(LANGUAGE, value).apply();
    }

    public String getLanguage(){
        return mSharedPrefs.getString(LANGUAGE, "");
    }

    public boolean isEnableNotification(){
        return mSharedPrefs.getBoolean(IS_ENABLE_NOTIFICATION, true);
    }

    public void setEnableNotification(boolean value){
        mSharedPrefs.edit().putBoolean(IS_ENABLE_NOTIFICATION, value).apply();
    }

    public int getNotificationCount(){
        return mSharedPrefs.getInt(NOTIFICATION_COUNT, 0);
    }

    public void setNotificationCount(int value){
        mSharedPrefs.edit().putInt(NOTIFICATION_COUNT, value).apply();
    }

    public void logout(){
        setUserToken("");
        setLoginType(-1);
    }

    public void storeUser(LoginResponse loginResponse){
        setUserToken(loginResponse.getToken());
    }

    public void storeUser(ContainerMap containerMap){
        setUserToken(containerMap.getString(AppConst.KEY_TOKEN));
    }
}

package app.inapps.doodlequote.app.ui.content.quotes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.ui.content.product.ProductItem;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

public class QuotesCreateAdapter extends BaseAdapter {
    private List<ProductItem> mItems;
    private Context mContext;
    private AdapterListener mListener;
    private Picasso mPicasso;

    public interface AdapterListener {
        void onItemClick(int position);
    }

    public QuotesCreateAdapter(Context context, Picasso picasso, AdapterListener listener) {
        super();
        mContext = context;
        mPicasso = picasso;
        mListener = listener;
        mItems = new ArrayList<>();
    }

    public void clear() {
        mItems.clear();
    }

    public void addItem(ProductItem item) {
        mItems.add(item);
    }

    public void addItems(List<ProductItem> items) {
        mItems.addAll(items);
    }

    public void addOrReplaceItem(ProductItem item) {
        boolean exist = false;
        for(int i = 0; i < mItems.size(); i++) {
            if(mItems.get(i).getId().intValue() == item.getId().intValue()) {
                mItems.remove(i);
                mItems.add(i, item);
                exist = true;
                break;
            }
        }
        if(!exist) {
            mItems.add(item);
        }
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.fragment_quotes_create_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.layout.setTag(position);
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = DataUtil.getInteger(view.getTag().toString(), 0);
                mListener.onItemClick(pos);
            }
        });

        ProductItem productItem = (ProductItem)getItem(position);
        viewHolder.tvName.setText(productItem.getName());
        String price = AppUtil.formatPrice(productItem.getPrice());
        viewHolder.tvPrice.setText(price);
        List<String> images = productItem.getImages();
        if(images != null && images.size() > 0) {
            File file = new File(images.get(0));
            if(file.exists()) {
                mPicasso.load(file).resize(ViewUtil.convertDpToPixel(mContext, 100), 0).into(viewHolder.ivImage);
            } else {
                viewHolder.ivImage.setImageDrawable(null);
            }
        } else {
            viewHolder.ivImage.setImageDrawable(null);
        }

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.ivImage) DefaultImageView ivImage;
        @BindView(R.id.tvName) DefaultTextView tvName;
        @BindView(R.id.tvPrice) DefaultTextView tvPrice;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}


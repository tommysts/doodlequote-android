package app.inapps.doodlequote.app.ui.content.product;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductCreateAdapter extends BaseAdapter {
    private ArrayList<UploadItem> mItems;
    private Context mContext;
    private Picasso mPicasso;
    private AdapterListener mListener;
    private int mScreenWidth = 0;

    public interface AdapterListener {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public ProductCreateAdapter(Context context, Picasso picasso, AdapterListener listener) {
        super();
        mContext = context;
        mPicasso = picasso;
        mListener = listener;
        mItems = new ArrayList<>();
        if(context instanceof Activity) {
            mScreenWidth = ViewUtil.getScreenWidth((Activity) context);
        }
    }

    public void clear() {
        mItems.clear();
    }

    public void addItem(UploadItem item) {
        mItems.add(item);
    }

    public ArrayList<UploadItem> getItems() {
        return mItems;
    }

    public void addItems(List<UploadItem> items) {
        mItems.addAll(items);
    }

    public void deleteItem(int position) {
        mItems.remove(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.fragment_product_create_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.layout.setTag(position);
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = DataUtil.getInteger(view.getTag().toString(), 0);
                mListener.onItemClick(pos);
            }
        });
        viewHolder.ivDelete.setTag(position);
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = DataUtil.getInteger(view.getTag().toString(), 0);
                mListener.onDeleteClick(pos);
            }
        });

        UploadItem uploadItem = (UploadItem) getItem(position);
        viewHolder.tvDescription.setText(uploadItem.getName());
        if(uploadItem instanceof UploadImageItem) {
            File file = new File(uploadItem.getPath());
            if (file.exists()) {
                if (mScreenWidth > 0) {
                    mPicasso.load(file).resize(mScreenWidth, 0).into(viewHolder.ivPicture);
                } else {
                    mPicasso.load(file).into(viewHolder.ivPicture);
                }
            } else {
                viewHolder.ivPicture.setImageDrawable(null);
            }
            viewHolder.ivPlay.setVisibility(View.GONE);
        } else if(uploadItem instanceof UploadVideoItem) {
            UploadVideoItem uploadVideoItem = (UploadVideoItem) uploadItem;
            if(TextUtils.isEmpty(uploadVideoItem.getPreviewImagePath())) {
                viewHolder.ivPicture.setImageDrawable(null);
            } else {
                File file = new File(uploadVideoItem.getPreviewImagePath());
                if (file.exists()) {
                    if (mScreenWidth > 0) {
                        mPicasso.load(file).resize(mScreenWidth, 0).into(viewHolder.ivPicture);
                    } else {
                        mPicasso.load(file).into(viewHolder.ivPicture);
                    }
                }
            }
            viewHolder.ivPlay.setVisibility(View.VISIBLE);
        } else {
            viewHolder.ivPicture.setImageDrawable(null);
        }
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.ivDelete) DefaultImageView ivDelete;
        @BindView(R.id.ivPicture) DefaultImageView ivPicture;
        @BindView(R.id.tvDescription) DefaultTextView tvDescription;
        @BindView(R.id.ivPlay) DefaultImageView ivPlay;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}


package app.inapps.doodlequote.app.ui;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.ApiManager;
import app.inapps.doodlequote.app.db.DatabaseManager;
import app.inapps.doodlequote.app.prefs.UserPreferences;
import app.inapps.doodlequote.app.util.LogManager;
import app.inapps.doodlequote.core.widget.AlertPopup;
import butterknife.Unbinder;

public abstract class BaseActivity extends FragmentActivity {
    @Inject
    ApiManager mApiManager;

    @Inject
    DatabaseManager mDatabaseManager;

    @Inject
    UserPreferences mUserPreferences;

    @Inject
    LogManager mLogManager;

    protected AlertPopup mAlertPopup;
    private List<WeakReference<ImageView>> mImageViews;
    protected Unbinder mUnbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupActivityComponent(savedInstanceState);

        String language = mUserPreferences.getLanguage();
        if(TextUtils.isEmpty(language)){
            forceLocale(AppConst.LANGUAGE_EN);
        } else if(AppConst.LANGUAGE_EN.equals(language)){
            forceLocale(AppConst.LANGUAGE_EN);
        } else if(AppConst.LANGUAGE_VI.equals(language)){
            forceLocale(AppConst.LANGUAGE_VI);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mImageViews != null){
            for (int i = 0; i < mImageViews.size(); i++) {
                WeakReference<ImageView> viewWeakReference = mImageViews.get(i);
                ImageView imageView = viewWeakReference.get();
                if(imageView != null){
                    imageView.setImageDrawable(null);
                }
            }
            mImageViews.clear();
            mImageViews = null;
        }
        if(mUnbinder != null){
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    protected void addImageViews(ImageView... imageViews){
        if(mImageViews == null){
            mImageViews = new ArrayList<>();
        }
        for (int i = 0; i < imageViews.length; i++) {
            mImageViews.add(new WeakReference<ImageView>(imageViews[i]));
        }
    }

    private void forceLocale(String locale) {
        Locale myLocale = new Locale(locale);
        Resources res = getResources();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, res.getDisplayMetrics());
    }

    abstract void setupActivityComponent(Bundle savedInstanceState);

    public void dismissDialog(){
        if(mAlertPopup != null){
            mAlertPopup.dismiss();
            mAlertPopup = null;
        }
    }

    public void showError(String message){
        showError(null, message);
    }

    public void showError(@StringRes int id){
        showError(null, getString(id));
    }

    public void showError(Throwable e, @StringRes int id) {
        showError(e, getString(id));
    }

    public void showError(Throwable e, final String message) {
        if (TextUtils.isEmpty(message)) {
            return;
        }
        if(mAlertPopup != null) {
            mAlertPopup.dismiss();
            mAlertPopup = null;
        }
        mAlertPopup = new AlertPopup(this);
        mAlertPopup.setListener(new AlertPopup.AlertPopupListener() {
            @Override
            public void onStart() {
                mAlertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertPopup.dismiss();
                    }
                });
                String newMessage = message;
                if (newMessage.length() > 500) {
                    newMessage = newMessage.substring(0, 500) + "...";
                }
                mAlertPopup.setMessage(newMessage);
                mAlertPopup.setTitle(getString(R.string.app_name));
            }
        });
        mAlertPopup.show();
    }

    public void showMessage(int id){
        showMessage(getString(id));
    }

    public void showMessage(final String message) {
        if (TextUtils.isEmpty(message)) {
            return;
        }
        if(mAlertPopup != null) {
            mAlertPopup.dismiss();
            mAlertPopup = null;
        }
        mAlertPopup = new AlertPopup(this);
        mAlertPopup.setListener(new AlertPopup.AlertPopupListener() {
            @Override
            public void onStart() {
                mAlertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertPopup.dismiss();
                    }
                });
                String newMessage = message;
                if (newMessage.length() > 500) {
                    newMessage = newMessage.substring(0, 500) + "...";
                }
                mAlertPopup.setMessage(newMessage);
                mAlertPopup.setTitle(getString(R.string.app_name));
            }
        });
        mAlertPopup.show();
    }

    public void showToast(String message){
        if (TextUtils.isEmpty(message)) {
            return;
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showToast(int id){
        showToast(getString(id));
    }
}

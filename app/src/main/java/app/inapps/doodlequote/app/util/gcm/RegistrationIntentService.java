package app.inapps.doodlequote.app.util.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.api.response.UpdateDeviceTokenResponse;
import app.inapps.doodlequote.common.util.SimpleObserver;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationIntentService extends IntentService {
    public static final String DEVICE_TYPE = "android";

    private static final String TAG = "GCM";
    private static final String[] TOPICS = {"global"};

    private GcmApiManager mGcmApiManager;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            // In the (unlikely) event that multiple refresh operations occur simultaneously,
            // ensure that they are processed sequentially.
            synchronized (TAG) {
                // [START register_for_gcm]
                // Initially this call goes out to the network to retrieve the token, subsequent calls
                // are local.
                // [START get_token]
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcm_sender_id),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                // [END get_token]

                sendRegistrationToServer(token);

                // Subscribe to topic channels
                subscribeTopics(token);

                // You should store a boolean that indicates whether the generated token has been
                // sent to your server. If the boolean is false, send the token to your server,
                // otherwise your server should have already received the token.
                //sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
                // [END register_for_gcm]
            }
        } catch (Exception e) {
            if(BuildConfig.DEBUG) {
                Log.d(TAG, "Failed to complete token refresh - " + e.getMessage());
            }
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            //sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();

            notifyUI();
        }
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
        GcmPreferences preferences = GcmPreferences.getInstance(getApplicationContext());
        preferences.setRegistrationID(token);
        if(BuildConfig.DEBUG) {
            Log.d("GCM", "Token " + token);
        }
        String userToken = preferences.getUserToken();
        if(TextUtils.isEmpty(userToken)) {
            notifyUI();
        } else {
            GcmApiManager gcmApiManager = getGcmApiManager();
            gcmApiManager.updateDeviceToken(preferences.getUserToken(), token, preferences.getDeviceType()).subscribe(new SimpleObserver<UpdateDeviceTokenResponse>() {
                @Override
                public void onError(Throwable e) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace();
                    }
                    notifyUI();
                }

                @Override
                public void onNext(UpdateDeviceTokenResponse updateDeviceTokenResponse) {
                    if (updateDeviceTokenResponse != null && updateDeviceTokenResponse.isSuccess() && BuildConfig.DEBUG) {
                        Log.d("GCM", "Update token to server COMPLETED");
                    }
                    notifyUI();
                }
            });
        }
    }

    private void notifyUI(){
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(GcmPreferences.REGISTRATION_COMPLETE_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private GcmApiManager getGcmApiManager(){
        if(mGcmApiManager == null){
            OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES);
            if(BuildConfig.DEBUG) {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpBuilder.addInterceptor(interceptor);
            }
            Gson gson = new GsonBuilder().create();
            Retrofit.Builder builder = new Retrofit.Builder();
            builder.client(httpBuilder.build());
            builder.addConverterFactory(GsonConverterFactory.create(gson));
            builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
            builder.baseUrl(getString(R.string.app_end_point));

            GcmApiClient client = builder.build().create(GcmApiClient.class);
            mGcmApiManager = new GcmApiManager(client);
        }
        return mGcmApiManager;
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]

}

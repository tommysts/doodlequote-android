package app.inapps.doodlequote.app;

import com.google.android.gms.wallet.WalletConstants;

import app.inapps.doodlequote.core.SystemConst;

public class AppConst {
    public static final int MAX_THREAD_POOL = 10;
    public static final int DISK_CACHE_SIZE = 50 * 1024; // 50mb
    public static final String KEY_END_POINT = SystemConst.APP_NAME + ".KEY_END_POINT";
    public static final String KEY_LOGOUT = SystemConst.APP_NAME + ".KEY_LOGOUT";
    public static final String KEY_TOKEN = SystemConst.APP_NAME + ".KEY_TOKEN";
    public static final String KEY_LOG_IN = SystemConst.APP_NAME + ".KEY_LOG_IN";
    public static final String KEY_EMAIL = SystemConst.APP_NAME + ".KEY_EMAIL";
    public static final String KEY_USER_TYPE = SystemConst.APP_NAME + ".KEY_USER_TYPE";
    public static final String KEY_QUOTE_ID = SystemConst.APP_NAME + ".KEY_QUOTE_ID";
    public static final String KEY_PRODUCT_ID = SystemConst.APP_NAME + ".KEY_PRODUCT_ID";
    public static final String KEY_PRODUCT_ITEM = SystemConst.APP_NAME + ".KEY_PRODUCT_ITEM";
    public static final String KEY_TEMPLATE_ITEM = SystemConst.APP_NAME + ".KEY_TEMPLATE_ITEM";
    public static final String KEY_USER_ITEM = SystemConst.APP_NAME + ".KEY_USER_ITEM";
    public static final String KEY_CUSTOMER_NAME = SystemConst.APP_NAME + ".KEY_CUSTOMER_NAME";
    public static final String KEY_CUSTOMER_MOBILE = SystemConst.APP_NAME + ".KEY_CUSTOMER_MOBILE";
    public static final String KEY_COMPANY_NAME = SystemConst.APP_NAME + ".KEY_COMPANY_NAME";
    public static final String KEY_UPLOAD_IMAGES = SystemConst.APP_NAME + ".KEY_UPLOAD_IMAGES";
    public static final String KEY_UPLOAD_VIDEOS = SystemConst.APP_NAME + ".KEY_UPLOAD_VIDEOS";
    public static final String KEY_PICKER_PATH = SystemConst.APP_NAME + ".KEY_PICKER_PATH";
    public static final String KEY_PACKAGE_ITEM = SystemConst.APP_NAME + ".KEY_PACKAGE_ITEM";

    public static final String ERROR_TOKEN_NOT_PROVIDED = "token_not_provided";
    public static final String ERROR_AUTH_TOKEN = "auth_token";
    public static final String ERROR_TOKEN_EXPIRED = "token_expired";
    public static final String ERROR_TOKEN_INVALID = "token_invalid";
    public static final String ERROR_USER_NOT_FOUND = "user_not_found";
    public static final String ERROR_OUT_OF_CREDIT = "submit_quote_out_of_credit";

    public static final int PASSWORD_LENGTH = 4;
    public static final String LANGUAGE_EN = "EN";
    public static final String LANGUAGE_VI = "VI";

    public static final int DELAY_SPLASH_LOADER_MILLISECONDS = 2000; // milliseconds
    public static final int DELAY_SEARCH_INTERVAL_MILLISECONDS = 1000; // milliseconds
    public static final int DELAY_TEXT_CHANGED_INTERVAL_MILLISECONDS = 1000; // milliseconds

    public static final long MAX_FILE_SIZE_IN_BYTES = 2 * 1048576; // 2 Mb
    public static final long FILE_SIZE_IN_BYTES = 1048576; // 1 Mb
    public static final int IMAGE_WIDTH_BIG = 1280;
    public static final int IMAGE_WIDTH_MEDIUM = 720;
    public static final int IMAGE_WIDTH_SMALL = 600;
    public static final int IMAGE_WIDTH_TINY = 400;

    public static final int DEFAULT_AVATAR_WIDTH = 200;
    public static final int DEFAULT_KEYBOARD_HEIGHT = 200;

    public static final String DEFAULT_COUNTRY_CODE = "+61";

    // Keys and Values must have same length
    public static final String[] TEMPLATE_KEYS = new String[] {"FIRST", "LAST", "COMPANY", "LINK"};
    public static final String TEMPLATE_FIRST = "FIRST";
    public static final String TEMPLATE_LAST = "LAST";
    public static final String TEMPLATE_COMPANY = "COMPANY";
    public static final String TEMPLATE_LINK = "LINK";
    public static final String[] PREVIEW_VALUES = new String[] {"John", "Smith", "Abc.Inc", "https://bit.ly/abcd"};
    public static final String PREVIEW_FIRST = "John";
    public static final String PREVIEW_LAST = "Smith";
    public static final String PREVIEW_COMPANY = "Abc.Inc";
    public static final String PREVIEW_LINK = "https://bit.ly/abcd";
    public static final String DEVICE_TYPE = "android";

    // Payments
    public static final String STRIPE_PUBLISHABLE_KEY = "pk_test_XGAYOwBKFuZ6DMrhyr8Pjkuv";
    //public static final String STRIPE_PUBLISHABLE_KEY = "pk_live_rciFZ7sAR9VMr3BLcG7KLyut";
    public static final int WALLET_ENVIRONMENT = WalletConstants.ENVIRONMENT_TEST;


    /**
     1:  => normal/standalone/individual
     2:  => company
     **/
    public enum LoginType{
        User(1), Company(2), Staff(3);
        LoginType(int value) {
            type = value;
        }
        private int type;

        public int getType() {
            return type;
        }
    }

    public enum DeviceType{
        Phone, Tablet
    }

    public enum QuoteStatus {
        Approved(0),
        Rejected(1),
        Callback(2),
        MessageSent(3),
        None(4);

        private int order; // used to sort quotes

        QuoteStatus(int order) {
            this.order = order;
        }

        public int getOrder() {
            return order;
        }
    }

    public enum ProductStatus {
        Completed(3),
        Rejected(2),
        Approved(1),
        NewItem(0);

        private int order; // used to sort quotes

        ProductStatus(int order) {
            this.order = order;
        }

        public int getOrder() {
            return order;
        }
    }

    public enum UploadType {
        Normal, Fixed
    }

    public static final String CARD_NUMBER = "XXXXXXXXXXXX";
    public static final String CARD_PATTERN_VISA = "^4\\d*";
    public static final String CARD_PATTERN_MASTERCARD = "^5\\d*";
    public static final String CARD_VISA = "visa";
    public static final String CARD_MASTERCARD = "mastercard";

    public enum CardType {
        VISA, MASTERCARD, UNKNOWN
    }

    private AppConst() {
    }
}

package app.inapps.doodlequote.app.ui.content.quotes;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CreateQuoteResponse;
import app.inapps.doodlequote.app.api.response.QuoteProductResponse;
import app.inapps.doodlequote.app.api.response.QuoteResponse;
import app.inapps.doodlequote.app.api.response.SubmitQuoteResponse;
import app.inapps.doodlequote.app.api.response.UploadResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.ui.content.product.ProductItem;
import app.inapps.doodlequote.app.ui.content.templates.TemplateItem;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.EncryptionUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.util.DuplicateHttpParams;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultHeaderListView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

import static app.inapps.doodlequote.app.AppConst.PREVIEW_VALUES;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_COMPANY;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_FIRST;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_KEYS;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_LAST;

public class QuotesCreateFragment extends BaseFragment {
    public static final String TAG = QuotesCreateFragment.class.getSimpleName();

    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.rlDone) RelativeLayout mRlDone;
    @BindView(R.id.ivSubmit) AnimateImageView mIvSubmit;
    @BindView(R.id.lvItems) DefaultHeaderListView mLvItems;

    private QuotesCreateHeaderView mHeaderView;
    private QuotesCreateAdapter mAdapter;
    private CreateQuoteResponse mCreateQuoteResponse;
    private UploadResponse mUploadResponse;
    private TemplateItem mTemplateItem;
    private boolean mHandleBackPress = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quotes_create, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavBack.setOnClickListener(this);
        mRlDone.setOnClickListener(this);
        mIvSubmit.setOnClickListener(this);

        mHeaderView = (QuotesCreateHeaderView) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_quotes_create_header, mLvItems, false);
        mHeaderView.setListener(new QuotesCreateHeaderView.QuoteCreateHeaderViewListener() {
            @Override
            public void onFinishInflate() {
                mHeaderView.registerTextChangeEvents();
                mHeaderView.registerActions();
            }

            @Override
            public void onAddProductClick() {
                addProduct();
            }

            @Override
            public void onTemplateClick() {
                chooseTemplate();
            }

            @Override
            public void onCustomerChange(String value) {
                templateToPreview(value);
                highlightPreview();
            }
        });
        mLvItems.addHeaderView(mHeaderView);
        mAdapter = new QuotesCreateAdapter(getActivity(), mPicasso, new QuotesCreateAdapter.AdapterListener() {
            @Override
            public void onItemClick(int position) {
                ProductItem productItem = (ProductItem) mAdapter.getItem(position);
                mContainerMap.setObject(AppConst.KEY_PRODUCT_ITEM, productItem);
                openPage(MainActivity.Pages.ProductCreate);
            }
        });
        mLvItems.setAdapter(mAdapter);

        addEditTexts("1", mHeaderView.getEditTexts());
        addImageViews(mIvSubmit);
        addImageViews(mHeaderView.getImageViews());
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        if(mHandleBackPress) {
            final AlertPopup alertPopup = new AlertPopup(getActivity());
            alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    alertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                            mHandleBackPress = false;
                            backClick(getFragmentId());
                        }
                    });
                    alertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                        }
                    });
                    alertPopup.setMessage(getString(R.string.confirm_discard_changes));
                    alertPopup.setTitle(getString(R.string.app_name));
                }
            });
            alertPopup.show();
        }
        return mHandleBackPress;
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);

        Object productItem = mContainerMap.getObject(AppConst.KEY_PRODUCT_ITEM);
        if(productItem instanceof ProductItem) {
            mAdapter.addOrReplaceItem((ProductItem)productItem);
            mAdapter.notifyDataSetChanged();
            mContainerMap.remove(AppConst.KEY_PRODUCT_ITEM);
        }
        Object templateItem = mContainerMap.getObject(AppConst.KEY_TEMPLATE_ITEM);
        if(templateItem instanceof TemplateItem) {
            mTemplateItem = ((TemplateItem) templateItem);
            mHeaderView.mTvTemplate.setText(((TemplateItem) templateItem).getDescription());
            highlightTemplate();
            templateToPreview(mHeaderView.mEdtCustomer.getText().toString());
            highlightPreview();
            mContainerMap.remove(AppConst.KEY_TEMPLATE_ITEM);
        }
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.QuotesCreate.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavBack) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavBack) {
            backClick(getFragmentId());
        } else if (v == mRlDone || v == mIvSubmit) {
            submit();
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {
        return super.onInternalTouch(v, event);
    }

    private void addProduct() {
        openPage(MainActivity.Pages.ProductCreate);
    }

    private void chooseTemplate() {
        openPage(MainActivity.Pages.QuotesTemplates);
    }

    private void submit() {
        if(mCreateQuoteResponse == null) {
            createQuote();
        } else if(mUploadResponse == null) {
            uploadProductImages();
        } else {
            submitQuote();
        }
    }

    private void createQuote() {
        String customerName = mHeaderView.getCustomerName();
        String tempCustomerMobile = mHeaderView.getCustomerMobile();

        if(TextUtils.isEmpty(customerName)) {
            showError(R.string.error_customer_require);
            return;
        }
        if(TextUtils.isEmpty(tempCustomerMobile)) {
            showError(R.string.error_mobile_require);
            return;
        }
        if(mTemplateItem == null) {
            showError(R.string.error_template_require);
            return;
        }
        final String customerMobile = AppUtil.optimizeMobile(tempCustomerMobile);
        if (!ValidatorUtil.isValidPhone(customerMobile)) {
            showError(R.string.error_mobile_invalid);
            return;
        }
        if(mAdapter.getCount() == 0) {
            showError(R.string.error_product_require);
            return;
        }

        String templateId = String.valueOf(mTemplateItem.getId());
        DuplicateHttpParams products = new DuplicateHttpParams();
        for(int i = 0; i < mAdapter.getCount(); i++) {
            ProductItem productItem = (ProductItem)mAdapter.getItem(i);
            productItem.setKey(EncryptionUtil.randomAlphabetic(8));

            int index = i + 1;
            products.add("products[" + index + "][name]", productItem.getName());
            products.add("products[" + index + "][description]", productItem.getDescription());
            products.add("products[" + index + "][price]", String.valueOf(productItem.getPrice()));
            products.add("products[" + index + "][key]", productItem.getKey());
        }

        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.createQuote(token, templateId, customerName, customerMobile, products.getPathMap())
                .subscribe(new SimpleObserver<CreateQuoteResponse>(){
                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mLogManager.log(t);
                        displayProgress(false);
                        showError(t, R.string.error_loading_data);
                    }

                    @Override
                    public void onNext(CreateQuoteResponse createQuoteResponse) {
                        super.onNext(createQuoteResponse);
                        if(createQuoteResponse.isSuccess()) {
                            mCreateQuoteResponse = createQuoteResponse;
                            // Upload images for each product
                            final QuoteResponse quoteResponse = createQuoteResponse.getQuote();
                            List<QuoteProductResponse> quoteProductResponses = quoteResponse.getProducts();
                            if(quoteProductResponses != null && quoteProductResponses.size() > 0) {
                                final String token = mContainerMap.getString(AppConst.KEY_TOKEN);
                                Map<Integer, List<String>> products = new HashMap<>();
                                for(QuoteProductResponse quoteProductResponse : quoteProductResponses) {
                                    for(int i = 0; i < mAdapter.getCount(); i++) {
                                        ProductItem productItem = (ProductItem)mAdapter.getItem(i);
                                        if(productItem.getKey().equals(quoteProductResponse.getKey())) {
                                            List<String> images = products.get(quoteProductResponse.getId());
                                            if(images == null) {
                                                images = new ArrayList<String>();
                                                products.put(quoteProductResponse.getId(), images);
                                            }
                                            images.addAll(productItem.getImages());
                                            break;
                                        }
                                    }
                                }
                                Subscription subscription1 = mApiManager.uploadImages(token, products).subscribe(new SimpleObserver<UploadResponse>(){
                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                        mLogManager.log(t);
                                        displayProgress(false);
                                        showError(t, R.string.error_loading_data);
                                    }

                                    @Override
                                    public void onNext(UploadResponse uploadResponse) {
                                        super.onNext(uploadResponse);
                                        if(uploadResponse.isSuccess()) {
                                            mUploadResponse = uploadResponse;
                                            Subscription subscription2 = mApiManager.submitQuote(token, String.valueOf(quoteResponse.getId()), customerMobile).subscribe(new SimpleObserver<SubmitQuoteResponse>(){
                                                @Override
                                                public void onError(Throwable t) {
                                                    super.onError(t);
                                                    mLogManager.log(t);
                                                    displayProgress(false);
                                                    showError(t, R.string.error_loading_data);
                                                }

                                                @Override
                                                public void onNext(SubmitQuoteResponse submitQuoteResponse) {
                                                    super.onNext(submitQuoteResponse);
                                                    displayProgress(false);
                                                    if(submitQuoteResponse.isSuccess()) {
                                                        showToast("Create Successful");
                                                        Bundle data = new Bundle();
                                                        data.putBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA, true);
                                                        mHandleBackPress = false;
                                                        backClick(getFragmentId(), data);
                                                    } else {
                                                        showError(submitQuoteResponse, R.string.error_loading_data);
                                                    }
                                                }
                                            });
                                            addSubscription(subscription2);
                                        } else {
                                            displayProgress(false);
                                            showError(uploadResponse, R.string.error_loading_data);
                                        }
                                    }
                                });
                                addSubscription(subscription1);
                            }
                        } else {
                            displayProgress(false);
                            showError(createQuoteResponse, R.string.error_loading_data);
                        }
                    }
                });
        addSubscription(subscription);
    }

    private void uploadProductImages() {
        String customerName = mHeaderView.getCustomerName();
        String tempCustomerMobile = mHeaderView.getCustomerMobile();

        if(TextUtils.isEmpty(customerName)) {
            showError(R.string.error_customer_require);
            return;
        }
        if(TextUtils.isEmpty(tempCustomerMobile)) {
            showError(R.string.error_mobile_require);
            return;
        }
        final String customerMobile = AppUtil.optimizeMobile(tempCustomerMobile);
        if (!ValidatorUtil.isValidPhone(customerMobile)) {
            showError(R.string.error_mobile_invalid);
            return;
        }
        if(mAdapter.getCount() == 0) {
            showError(R.string.error_product_require);
            return;
        }

        displayProgress(true);
        final QuoteResponse quoteResponse = mCreateQuoteResponse.getQuote();
        List<QuoteProductResponse> quoteProductResponses = quoteResponse.getProducts();
        if (quoteProductResponses != null && quoteProductResponses.size() > 0) {
            final String token = mContainerMap.getString(AppConst.KEY_TOKEN);
            Map<Integer, List<String>> products = new HashMap<>();
            for (QuoteProductResponse quoteProductResponse : quoteProductResponses) {
                for (int i = 0; i < mAdapter.getCount(); i++) {
                    ProductItem productItem = (ProductItem) mAdapter.getItem(i);
                    if (productItem.getKey().equals(quoteProductResponse.getKey())) {
                        List<String> images = products.get(quoteProductResponse.getId());
                        if (images == null) {
                            images = new ArrayList<String>();
                            products.put(quoteProductResponse.getId(), images);
                        }
                        images.addAll(productItem.getImages());
                        break;
                    }
                }
            }
            Subscription subscription1 = mApiManager.uploadImages(token, products).subscribe(new SimpleObserver<UploadResponse>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    displayProgress(false);
                    showError(t, R.string.error_loading_data);
                }

                @Override
                public void onNext(UploadResponse uploadResponse) {
                    super.onNext(uploadResponse);
                    if (uploadResponse.isSuccess()) {
                        mUploadResponse = uploadResponse;
                        Subscription subscription2 = mApiManager.submitQuote(token, String.valueOf(quoteResponse.getId()), customerMobile).subscribe(new SimpleObserver<SubmitQuoteResponse>() {
                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                                mLogManager.log(t);
                                displayProgress(false);
                                showError(t, R.string.error_loading_data);
                            }

                            @Override
                            public void onNext(SubmitQuoteResponse submitQuoteResponse) {
                                super.onNext(submitQuoteResponse);
                                displayProgress(false);
                                if (submitQuoteResponse.isSuccess()) {
                                    showToast("Create Successful");
                                    Bundle data = new Bundle();
                                    data.putBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA, true);
                                    mHandleBackPress = false;
                                    backClick(getFragmentId(), data);
                                } else {
                                    showError(submitQuoteResponse, R.string.error_loading_data);
                                }
                            }
                        });
                        addSubscription(subscription2);
                    } else {
                        displayProgress(false);
                        showError(uploadResponse, R.string.error_loading_data);
                    }
                }
            });
            addSubscription(subscription1);
        }
    }

    private void submitQuote() {
        String customerName = mHeaderView.getCustomerName();
        String tempCustomerMobile = mHeaderView.getCustomerMobile();

        if(TextUtils.isEmpty(customerName)) {
            showError(R.string.error_customer_require);
            return;
        }
        if(TextUtils.isEmpty(tempCustomerMobile)) {
            showError(R.string.error_mobile_require);
            return;
        }
        final String customerMobile = AppUtil.optimizeMobile(tempCustomerMobile);
        if (!ValidatorUtil.isValidPhone(customerMobile)) {
            showError(R.string.error_mobile_invalid);
            return;
        }
        if(mAdapter.getCount() == 0) {
            showError(R.string.error_product_require);
            return;
        }

        displayProgress(true);
        final String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription2 = mApiManager.submitQuote(token, mCreateQuoteResponse.getQuoteId(), customerMobile).subscribe(new SimpleObserver<SubmitQuoteResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(SubmitQuoteResponse submitQuoteResponse) {
                super.onNext(submitQuoteResponse);
                displayProgress(false);
                if (submitQuoteResponse.isSuccess()) {
                    showToast("Create Successful");
                    Bundle data = new Bundle();
                    data.putBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA, true);
                    mHandleBackPress = false;
                    backClick(getFragmentId(), data);
                } else {
                    showError(submitQuoteResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription2);

    }

    private void templateToPreview(String firstLast) {
        String preview = mHeaderView.mTvTemplate.getText().toString();
        for(int i = 0; i < TEMPLATE_KEYS.length; i++) {
            if(TEMPLATE_KEYS[i].equals(TEMPLATE_COMPANY)) {
                String companyName = mContainerMap.getString(AppConst.KEY_COMPANY_NAME);
                if(TextUtils.isEmpty(companyName)) {
                    preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), "");
                } else {
                    preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), companyName);
                }
            } else if(TEMPLATE_KEYS[i].equals(TEMPLATE_FIRST) && !TextUtils.isEmpty(firstLast)) {
                int index = firstLast.lastIndexOf(" ");
                if(index != -1) {
                    preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), firstLast.substring(0, index));
                } else {
                    preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), "");
                }
            } else if(TEMPLATE_KEYS[i].equals(TEMPLATE_LAST) && !TextUtils.isEmpty(firstLast)) {
                int index = firstLast.lastIndexOf(" ");
                if(index != -1) {
                    preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), firstLast.substring(index + 1, firstLast.length()));
                } else {
                    preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), firstLast);
                }
            } else {
                preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), PREVIEW_VALUES[i]);
            }
        }
        mHeaderView.mTvTemplatePreview.setText(preview);
    }

    private void highlightTemplate() {
        ViewUtil.setColorsText(getActivity(), mHeaderView.mTvTemplate, TEMPLATE_KEYS, R.color.template);
    }

    private void highlightPreview() {
        ViewUtil.setColorsText(getActivity(), mHeaderView.mTvTemplatePreview, PREVIEW_VALUES, R.color.template);
    }

}

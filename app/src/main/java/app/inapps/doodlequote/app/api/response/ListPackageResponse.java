package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class ListPackageResponse extends BaseResponse {
    @SerializedName("packages")
    @Expose
    private List<PackageResponse> packages;

    public List<PackageResponse> getPackages() {
        return packages;
    }

    public void setPackages(List<PackageResponse> packages) {
        this.packages = packages;
    }
}

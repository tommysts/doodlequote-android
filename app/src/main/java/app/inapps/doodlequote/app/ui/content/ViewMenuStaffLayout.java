package app.inapps.doodlequote.app.ui.content;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ViewMenuStaffLayout extends RelativeLayout {
    @BindView(R.id.ivMenuBackground) DefaultImageView mIvMenuBackground;
    @BindView(R.id.llMenuDashBoard) LinearLayout mLlMenuDashBoard;
    @BindView(R.id.llMenuQuotes) LinearLayout mLlMenuQuotes;
    @BindView(R.id.llMenuTemplates) LinearLayout mLlMenuTemplates;
    @BindView(R.id.llMenuUserProfile) LinearLayout mLlMenuUserProfile;
    @BindView(R.id.llMenuChangePassword) LinearLayout mLlMenuChangePassword;
    @BindView(R.id.llMenuCompanyInfo) LinearLayout mLlMenuCompanyInfo;
    @BindView(R.id.llMenuLogout) LinearLayout mLlMenuLogout;

    private Context mContext;
    private Unbinder mUnbinder;
    private ViewMenuLayoutListener mListener;
    private DebouncedOnClickListener mClickListener;

    public ViewMenuStaffLayout(Context context) {
        super(context);
        mContext = context;
        //setup();
    }

    public ViewMenuStaffLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        //setup();
    }

    public ViewMenuStaffLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        //setup();
    }

    @TargetApi(21)
    public ViewMenuStaffLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        //setup();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mUnbinder = ButterKnife.bind(this);
        setup();
        if(mListener != null) {
            mListener.onFinishInflate();
        }
    }

    private void setup() {
        mClickListener = new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if(v.getTag() != null) {
                    int tag = DataUtil.getInteger(v.getTag().toString());
                    for(MainActivity.DrawerType item : MainActivity.DrawerType.values()) {
                        if(item.ordinal() == tag) {
                            if(mListener != null) {
                                mListener.onItemClick(item);
                            }
                            break;
                        }
                    }
                }
            }
        };
        mLlMenuDashBoard.setTag(MainActivity.DrawerType.Dashboard.ordinal());
        mLlMenuDashBoard.setOnClickListener(mClickListener);

        mLlMenuQuotes.setTag(MainActivity.DrawerType.Quotes.ordinal());
        mLlMenuQuotes.setOnClickListener(mClickListener);

        mLlMenuTemplates.setTag(MainActivity.DrawerType.Templates.ordinal());
        mLlMenuTemplates.setOnClickListener(mClickListener);

        mLlMenuUserProfile.setTag(MainActivity.DrawerType.UserProfile.ordinal());
        mLlMenuUserProfile.setOnClickListener(mClickListener);

        mLlMenuChangePassword.setTag(MainActivity.DrawerType.ChangePassword.ordinal());
        mLlMenuChangePassword.setOnClickListener(mClickListener);

        mLlMenuCompanyInfo.setTag(MainActivity.DrawerType.CompanyInfo.ordinal());
        mLlMenuCompanyInfo.setOnClickListener(mClickListener);

        mLlMenuLogout.setTag(MainActivity.DrawerType.Logout.ordinal());
        mLlMenuLogout.setOnClickListener(mClickListener);
    }

    public void setListener(ViewMenuLayoutListener listener) {
        mListener = listener;
    }

    public void destroy() {
        mListener = null;
        mClickListener = null;
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }
}

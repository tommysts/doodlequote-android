package app.inapps.doodlequote.app.ui.content.product;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import app.inapps.doodlequote.app.AppConst;

public class ProductItem implements Parcelable {
    private Integer quoteId;
    private Integer id;
    private String name;
    private String key;
    private String description;
    private AppConst.ProductStatus status;
    private double price;
    private List<String> images;
    private List<String> videos;

    public Integer getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(Integer quoteId) {
        this.quoteId = quoteId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getVideos() {
        return videos;
    }

    public void setVideos(List<String> videos) {
        this.videos = videos;
    }

    public AppConst.ProductStatus getStatus() {
        return status;
    }

    public void setStatus(AppConst.ProductStatus status) {
        this.status = status;
    }

    public ProductItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.quoteId);
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.key);
        dest.writeString(this.description);
        dest.writeInt(this.status == null ? -1 : this.status.ordinal());
        dest.writeDouble(this.price);
        dest.writeStringList(this.images);
        dest.writeStringList(this.videos);
    }

    protected ProductItem(Parcel in) {
        this.quoteId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.key = in.readString();
        this.description = in.readString();
        int tmpStatus = in.readInt();
        this.status = tmpStatus == -1 ? null : AppConst.ProductStatus.values()[tmpStatus];
        this.price = in.readDouble();
        this.images = in.createStringArrayList();
        this.videos = in.createStringArrayList();
    }

    public static final Creator<ProductItem> CREATOR = new Creator<ProductItem>() {
        @Override
        public ProductItem createFromParcel(Parcel source) {
            return new ProductItem(source);
        }

        @Override
        public ProductItem[] newArray(int size) {
            return new ProductItem[size];
        }
    };
}

package app.inapps.doodlequote.app.ui.content.quotes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.ui.content.product.ProductItem;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class QuotesDetailAdapter extends BaseAdapter {
    private List<ProductItem> mItems;
    private Context mContext;
    private Picasso mPicasso;
    private AdapterListener mListener;
    private String mEndPoint;
    private int mImageWidth;

    public interface AdapterListener {
        void onItemClick(int position);
    }

    public QuotesDetailAdapter(Context context, Picasso picasso, AdapterListener listener) {
        super();
        mContext = context;
        mPicasso = picasso;
        mListener = listener;
        mItems = new ArrayList<>();
        mEndPoint = mContext.getString(R.string.app_end_point);
        mImageWidth = context.getResources().getDimensionPixelSize(R.dimen.width_percent_18);
    }

    public void clear() {
        mItems.clear();
    }

    public void addItem(ProductItem item) {
        mItems.add(item);
    }

    public void addItems(List<ProductItem> items) {
        mItems.addAll(items);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.fragment_quotes_detail_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.layout.setTag(position);
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = DataUtil.getInteger(view.getTag().toString(), 0);
                mListener.onItemClick(pos);
            }
        });

        ProductItem productItem = (ProductItem) getItem(position);
        viewHolder.tvName.setText(productItem.getName());
        String price = AppUtil.formatPrice(productItem.getPrice());
        viewHolder.tvPrice.setText(price);

        AppConst.ProductStatus status = productItem.getStatus();
        switch (status) {
            case NewItem:
                viewHolder.tvStatus.setText(R.string.product_status_new_item);
                viewHolder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.product_new_item));
                break;
            case Approved:
                viewHolder.tvStatus.setText(R.string.product_status_approved);
                viewHolder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.product_approved));
                break;
            case Completed:
                viewHolder.tvStatus.setText(R.string.product_status_completed);
                viewHolder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.product_completed));
                break;
            case Rejected:
                viewHolder.tvStatus.setText(R.string.product_status_rejected);
                viewHolder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.product_rejected));
                break;
            default:
                viewHolder.tvStatus.setText("");
                break;
        }

        List<String> images = productItem.getImages();
        if(images != null && images.size() > 0) {
            String imageUrl = AppUtil.getImageUrl(mEndPoint, productItem.getQuoteId().toString(), productItem.getId().toString(), images.get(0));
            mPicasso.load(imageUrl).resize(mImageWidth, 0).into(viewHolder.ivImage);
        } else {
            viewHolder.ivImage.setImageDrawable(null);
        }

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.ivImage) DefaultImageView ivImage;
        @BindView(R.id.ivLike) DefaultImageView ivLike;
        @BindView(R.id.tvName) DefaultTextView tvName;
        @BindView(R.id.tvPrice) DefaultTextView tvPrice;
        @BindView(R.id.tvStatus) DefaultTextView tvStatus;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}


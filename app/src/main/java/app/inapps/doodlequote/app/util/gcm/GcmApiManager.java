package app.inapps.doodlequote.app.util.gcm;

import app.inapps.doodlequote.app.api.response.UpdateDeviceTokenResponse;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class GcmApiManager {
    private GcmApiClient mApiClient;

    public GcmApiManager(GcmApiClient apiClient) {
        mApiClient = apiClient;
    }

    public Observable<UpdateDeviceTokenResponse> updateDeviceToken(final String token, final String deviceToken, final String deviceType) {
        return mApiClient.updateDeviceToken(token, deviceToken, deviceType).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}

package app.inapps.doodlequote.app.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.LayoutRes;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.ui.content.company_info.PaymentItem;
import app.inapps.doodlequote.app.util.DebugUtil;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.ExpiryPickerPopup;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import app.inapps.doodlequote.core.widget.TabRelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CardInfoPopup extends Dialog {
    @BindView(R.id.ivClose) DefaultImageView mIvClose;
    @BindView(R.id.rlTapContent) TabRelativeLayout mRlTabContent;
    @BindView(R.id.edtName) DefaultEditText mEdtName;
    @BindView(R.id.edtNumber) DefaultEditText mEdtNumber;
    @BindView(R.id.edtExpiry) DefaultEditText mEdtExpiry;
    @BindView(R.id.ivCalendar) RecyclingImageView mIvCalendar;
    @BindView(R.id.edtCVN) DefaultEditText mEdtCVN;
    @BindView(R.id.tvButton1) DefaultTextView mTvButton1;
    @BindView(R.id.ivButton1) AnimateImageView mIvButton1;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;
    @BindView(R.id.rbSavedCards) RadioButton mRbSavedCards;
    @BindView(R.id.rbNewCard) RadioButton mRbNewCard;
    @BindView(R.id.rbAndroidPay) RadioButton mRbAndroidPay;
    @BindView(R.id.lvItems) ListView mLvItems;
    @BindView(R.id.tvMessage) TextView mTvMessage;

    protected Unbinder mUnbinder;
    protected CardInfoPopupListener mListener;
    protected ExpiryPickerPopup mExpiryPickerPopup;
    protected int mMonth;
    protected int mYear;
    protected CardInfoAdapter mAdapter;
    protected List<PaymentItem> mItems= new ArrayList<>();
    protected PaymentItem mSelectedPayment;

    public interface CardInfoPopupListener {
        void onStart();

        void showAndroidPay(CardInfoPopup dialog);

        void onOkClick(CardInfoPopup dialog);

        void onCancelClick(CardInfoPopup dialog);
    }

    public CardInfoPopup(Context context) {
        super(context);
        setup();
    }

    public CardInfoPopup(Context context, int themeResId) {
        super(context, themeResId);
        setup();
    }

    protected CardInfoPopup(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setup();
    }

    protected void setup() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(getLayoutResID());
        setCancelable(true);

        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        mUnbinder = ButterKnife.bind(this);
    }

    @LayoutRes
    protected int getLayoutResID() {
        return R.layout.popup_card_info;
    }

    public void setListener(CardInfoPopupListener listener) {
        mListener = listener;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mUnbinder == null) {
            mUnbinder = ButterKnife.bind(this);
        }
        if (mListener != null) {
            mListener.onStart();
        }
        Calendar calendar = Calendar.getInstance();
        mMonth = calendar.get(Calendar.MONTH) + 1;
        mYear = calendar.get(Calendar.YEAR);

        if (DebugUtil.isDebugMode(getContext())) {
            mEdtName.setText("Pham Van Chung");
            mEdtNumber.setText("4242424242424242");
            mEdtCVN.setText("123");
            mMonth = 12;
            mYear = 2017;
        }

        mEdtExpiry.setText(String.format(Locale.getDefault(), "%d/%d", mMonth, mYear));
        mEdtExpiry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showExpiryPopup();
                }
            }
        });
        mEdtExpiry.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    showExpiryPopup();
                    return true;
                }
                return false;
            }
        });
        mIvCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExpiryPopup();
            }
        });
        mIvButton1.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if (mListener != null) {
                    mListener.onOkClick(CardInfoPopup.this);
                }
            }
        });
        mIvClose.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if (mListener != null) {
                    mListener.onCancelClick(CardInfoPopup.this);
                }
            }
        });

        mRbSavedCards.setChecked(true);
        mRbNewCard.setChecked(false);
        mRbAndroidPay.setChecked(false);
        mRlTabContent.setCurrentTab(0);

        mRbSavedCards.setOnCheckedChangeListener(mShowSavedCards);
        mRbNewCard.setOnCheckedChangeListener(mShowNewCard);
        mRbAndroidPay.setOnCheckedChangeListener(mShowAndroidPay);

        mAdapter = new CardInfoAdapter(getContext(), new CardInfoAdapter.AdapterListener() {
            @Override
            public void onItemClick(int position) {
                mSelectedPayment = (PaymentItem) mAdapter.getItem(position);
                mAdapter.notifyDataSetChanged();
            }
        });
        mLvItems.setAdapter(mAdapter);
        mAdapter.clear();
        mAdapter.addItems(mItems);
        mAdapter.notifyDataSetChanged();
        if(mItems.size() == 0) {
            mTvMessage.setVisibility(View.VISIBLE);
        } else {
            mTvMessage.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public void addItems(List<PaymentItem> items) {
        mItems.clear();
        mItems.addAll(items);
        if(isShowing()) {
            mAdapter.clear();
            mAdapter.addItems(mItems);
            mAdapter.notifyDataSetChanged();
            if(mItems.size() == 0) {
                mTvMessage.setVisibility(View.VISIBLE);
            } else {
                mTvMessage.setVisibility(View.INVISIBLE);
            }
        }
    }

    private CompoundButton.OnCheckedChangeListener mShowSavedCards = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                mRlTabContent.setCurrentTab(0);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener mShowNewCard = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                mRlTabContent.setCurrentTab(1);
                mEdtCVN.setText("");
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener mShowAndroidPay = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                mRlTabContent.setCurrentTab(2);
                if(mListener != null) {
                    mListener.showAndroidPay(CardInfoPopup.this);
                }
            }
        }
    };

    public PaymentItem getSelectedPayment() {
        return mSelectedPayment;
    }

    public void setPrice(String price) {
        mTvButton1.setText(getContext().getString(R.string.card_buy, price));
    }

    public String getName() {
        return mEdtName.getText().toString();
    }

    public String getNumber() {
        return mEdtNumber.getText().toString();
    }

    public String getCvn() {
        return mEdtCVN.getText().toString();
    }

    public int getMonth() {
        return mMonth;
    }

    public int getYear() {
        return mYear;
    }

    public void displayProgress(boolean show) {
        if(mProgressBar != null) {
            mProgressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        }
    }

    public boolean isLoading() {
        return mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE;
    }

    public boolean isAddNew() {
        return mRbNewCard.isChecked();
    }

    public boolean isAndroidPay() {
        return mRbAndroidPay.isChecked();
    }

    protected void showExpiryPopup() {
        if (mExpiryPickerPopup == null) {
            mExpiryPickerPopup = new ExpiryPickerPopup(getContext());
        }
        mExpiryPickerPopup.show();
        Calendar calendar = Calendar.getInstance();
        mExpiryPickerPopup.setData(mMonth, mYear, calendar.get(Calendar.YEAR), calendar.get(Calendar.YEAR) + 15, new ExpiryPickerPopup.ExpiryPickerPopupListener() {
            @Override
            public void onDismiss(int month, int year) {
                mMonth = month;
                mYear = year;
                mEdtExpiry.setText(String.format(Locale.getDefault(), "%d/%d", mMonth, mYear));
            }
        });
    }
}

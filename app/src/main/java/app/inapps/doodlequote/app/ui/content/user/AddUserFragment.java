package app.inapps.doodlequote.app.ui.content.user;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CreateUserResponse;
import app.inapps.doodlequote.app.api.response.UpdateUserProfileResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.ui.content.product.UploadImageItem;
import app.inapps.doodlequote.app.ui.widget.BrowsePopup;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.ImageItem;
import app.inapps.doodlequote.core.util.DisplayImageUtil;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.CircleImageView;
import app.inapps.doodlequote.core.widget.DefaultCheckbox;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class AddUserFragment extends BaseFragment implements ImagePickerCallback {
    public static final String TAG = AddUserFragment.class.getSimpleName();

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.ivNavHome) DefaultImageView mIvNavHome;
    @BindView(R.id.rlDone) RelativeLayout mRlDone;
    @BindView(R.id.ivAvatar) CircleImageView mIvAvatar;
    @BindView(R.id.ivBrowse) AnimateImageView mIvBrowse;
    @BindView(R.id.edtFirstName) DefaultEditText mEdtFirstName;
    @BindView(R.id.edtLastName) DefaultEditText mEdtLastName;
    @BindView(R.id.edtMobile) DefaultEditText mEdtMobile;
    @BindView(R.id.edtEmail) DefaultEditText mEdtEmail;
    @BindView(R.id.edtTitle) DefaultEditText mEdtTitle;
    @BindView(R.id.tvNote) DefaultTextView mTvNote;
    @BindView(R.id.tvTitle) DefaultTextView mTvTitle;
    @BindView(R.id.llDisabled) ViewGroup mLlDisabled;
    @BindView(R.id.cbxDisabled) DefaultCheckbox mCbxDisabled;

    private UsersItem mUsersItem;
    private UploadImageItem mUploadImageItem;
    private boolean mRefresh;
    private boolean mEditMode;
    private boolean mHandleBackPress = true;
    private String mPickerPath;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraPicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_users_add, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (mImagePicker == null) {
                    mImagePicker = new ImagePicker(this);
                    mImagePicker.shouldGenerateMetadata(true);
                    mImagePicker.shouldGenerateThumbnails(false);
                    mImagePicker.setImagePickerCallback(this);
                }
                mImagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (mCameraPicker == null) {
                    mCameraPicker = new CameraImagePicker(this);
                    mCameraPicker.setImagePickerCallback(this);
                    mCameraPicker.shouldGenerateMetadata(true);
                    mCameraPicker.shouldGenerateThumbnails(false);
                    mCameraPicker.reinitialize(mPickerPath);
                }
                mCameraPicker.submit(data);
            }
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        if (images != null && images.size() > 0) {
            displayProgress(true);
            ImageItem imageItem = AppUtil.createImageItem(images.get(0));
            Subscription subscription = DisplayImageUtil.rxOptimizeUploadImage(getActivity(), imageItem).subscribe(new SimpleObserver<ImageItem>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    showToast(R.string.error_loading_data);
                    displayProgress(false);
                }

                @Override
                public void onNext(ImageItem item) {
                    super.onNext(item);
                    loadImageFromPath(item);
                    mLogManager.log("File Path: " + item);
                    displayProgress(false);
                }
            });
            addSubscription(subscription);
        }
    }

    @Override
    public void onError(String message) {
        showError(message);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(AppConst.KEY_PICKER_PATH, mPickerPath);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);
        mIvBrowse.setOnClickListener(this);
        mRlDone.setOnClickListener(this);

        Bundle data = getArguments();
        if (data != null && data.getString(OnFragmentInteractionListener.DATA_PAGE_STRING_EXTRA, "").equals(UsersFragment.TAG)) {
            mIvNavHome.setImageResource(R.drawable.ic_menu_back);
            mEditMode = true;
        }

        ViewUtil.setBoldText(getActivity(), mTvNote, getString(R.string.create_user_note));

        addImageViews(mIvAvatar);
        addEditTexts("1", mEdtFirstName, mEdtLastName, mEdtMobile, mEdtEmail, mEdtTitle);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(AppConst.KEY_PICKER_PATH)) {
                mPickerPath = savedInstanceState.getString(AppConst.KEY_PICKER_PATH);
            }
        }

        Object o = mContainerMap.getObject(AppConst.KEY_USER_ITEM);
        if(o instanceof UsersItem) {
            mTvTitle.setText(R.string.edit_user);
            mUsersItem = (UsersItem) o;
            mEdtFirstName.setText(mUsersItem.getFirstName());
            mEdtLastName.setText(mUsersItem.getLastName());
            mEdtMobile.setText(mUsersItem.getMobile());
            mEdtEmail.setText(mUsersItem.getEmail());
            mEdtTitle.setText(mUsersItem.getTitle());
            mCbxDisabled.setChecked(mUsersItem.isDisabled());
            String url = AppUtil.getAvatarImageUrl(getString(R.string.app_end_point), String.valueOf(mUsersItem.getId()), mUsersItem.getImageName());
            mPicasso.load(url).resize(ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_AVATAR_WIDTH), 0).into(mIvAvatar);
            mLlDisabled.setVisibility(View.VISIBLE);
            mContainerMap.remove(AppConst.KEY_USER_ITEM);
        } else {
            mLlDisabled.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        if (mHandleBackPress) {
            final AlertPopup alertPopup = new AlertPopup(getActivity());
            alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    alertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                            mHandleBackPress = false;
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(OnFragmentInteractionListener.DATA_IS_DRAWER_BOOLEAN_EXTRA, false);
                            backClick(getFragmentId(), bundle);
                        }
                    });
                    alertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                        }
                    });
                    alertPopup.setMessage(getString(R.string.confirm_discard_changes));
                    alertPopup.setTitle(getString(R.string.app_name));
                }
            });
            alertPopup.show();
        }
        return mHandleBackPress;
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.AddUser.ordinal();
    }

    @Override
    public Bundle beforeBackPress() {
        Bundle bundle = super.beforeBackPress();
        if (mEditMode) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA, mRefresh);
            bundle.putBoolean(OnFragmentInteractionListener.DATA_IS_DRAWER_BOOLEAN_EXTRA, false);
        }
        return bundle;
    }

    @Override
    protected boolean canHandleClick(View v) {
        if (v == mRlNavHome) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            if (mEditMode) {
                backClick(getFragmentId());
            } else {
                menuClick();
            }
        } else if (v == mRlDone) {
            done();
        } else if (v == mIvBrowse) {
            browse();
        }
    }

    public void done() {
        final String firstName = mEdtFirstName.getText().toString();
        final String lastName = mEdtLastName.getText().toString();
        final String mobile = mEdtMobile.getText().toString();
        final String email = mEdtEmail.getText().toString();
        final String title = mEdtTitle.getText().toString();
        final boolean isDisabled = mCbxDisabled.isChecked();
        if (TextUtils.isEmpty(firstName)) {
            showError(R.string.error_first_name_require);
            return;
        }
        if (TextUtils.isEmpty(lastName)) {
            showError(R.string.error_last_name_require);
            return;
        }
        if (TextUtils.isEmpty(mobile)) {
            showError(R.string.error_mobile_require);
            return;
        }
        if (!ValidatorUtil.isValidPhone(mobile)) {
            showError(R.string.error_mobile_invalid);
            return;
        }
        if (TextUtils.isEmpty(email)) {
            showError(R.string.error_email_require);
            return;
        }
        if (!ValidatorUtil.isValidEmail(email)) {
            showError(R.string.error_email_invalid);
            return;
        }
        if (TextUtils.isEmpty(title)) {
            showError(R.string.error_title_require);
            return;
        }
        displayProgress(true);
        final String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        String imagePath = "";
        if (mUploadImageItem != null) {
            imagePath = mUploadImageItem.getPath();
        }

        if(mUsersItem == null) {
            Subscription subscription = mApiManager.createCompanyUser(token, firstName, lastName, mobile, email, title, isDisabled, imagePath).subscribe(new SimpleObserver<CreateUserResponse>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    displayProgress(false);
                    showError(t, R.string.error_loading_data);
                }

                @Override
                public void onNext(CreateUserResponse createUserResponse) {
                    super.onNext(createUserResponse);
                    displayProgress(false);
                    if (createUserResponse.isSuccess()) {
                        showToast(R.string.create_success);
                        if (mEditMode) {
                            mHandleBackPress = false;
                            mRefresh = true;
                            backClick(getFragmentId());
                        } else {
                            mUploadImageItem = null;
                            mIvAvatar.setImageDrawable(null);
                            mEdtFirstName.setText("");
                            mEdtLastName.setText("");
                            mEdtMobile.setText("");
                            mEdtEmail.setText("");
                            mEdtTitle.setText("");
                            mCbxDisabled.setChecked(false);
                        }
                    } else {
                        showError(createUserResponse, R.string.error_loading_data);
                    }
                }
            });
            addSubscription(subscription);
        } else {
            if(TextUtils.isEmpty(imagePath)) {
                String newEmail = email;
                if(newEmail.equals(mUsersItem.getEmail())) {
                    newEmail = null;
                }
                Subscription subscription = mApiManager.updateUserProfile(token, mUsersItem.getId(), firstName, lastName, mobile, title, newEmail, isDisabled).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mLogManager.log(t);
                        displayProgress(false);
                        showError(t, R.string.error_loading_data);
                    }

                    @Override
                    public void onNext(UpdateUserProfileResponse updateUserProfileResponse) {
                        super.onNext(updateUserProfileResponse);
                        displayProgress(false);
                        if (updateUserProfileResponse.isSuccess()) {
                            showToast(R.string.update_success);
                            mHandleBackPress = false;
                            mRefresh = true;
                            backClick(getFragmentId());
                        } else {
                            showError(updateUserProfileResponse, R.string.error_loading_data);
                        }
                    }
                });
                addSubscription(subscription);
            } else {
                Subscription subscription1 = mApiManager.uploadUserAvatar(token, mUsersItem.getId(), imagePath).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mLogManager.log(t);
                        displayProgress(false);
                        showError(t, R.string.error_loading_data);
                    }

                    @Override
                    public void onNext(UpdateUserProfileResponse userProfileResponse) {
                        super.onNext(userProfileResponse);
                        if (userProfileResponse.isSuccess()) {
                            String newEmail = email;
                            if(newEmail.equals(mUsersItem.getEmail())) {
                                newEmail = null;
                            }
                            Subscription subscription2 = mApiManager.updateUserProfile(token, mUsersItem.getId(), firstName, lastName, mobile, title, newEmail, isDisabled).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                                @Override
                                public void onError(Throwable t) {
                                    super.onError(t);
                                    mLogManager.log(t);
                                    displayProgress(false);
                                    showError(t, R.string.error_loading_data);
                                }

                                @Override
                                public void onNext(UpdateUserProfileResponse updateUserProfileResponse) {
                                    super.onNext(updateUserProfileResponse);
                                    displayProgress(false);
                                    if (updateUserProfileResponse.isSuccess()) {
                                        showToast(R.string.update_success);
                                        mHandleBackPress = false;
                                        mRefresh = true;
                                        backClick(getFragmentId());
                                    } else {
                                        showError(updateUserProfileResponse, R.string.error_loading_data);
                                    }
                                }
                            });
                            addSubscription(subscription2);
                        } else {
                            displayProgress(false);
                            showError(userProfileResponse, R.string.error_loading_data);
                        }
                    }
                });
                addSubscription(subscription1);
            }
        }
    }

    private void browse() {
        BrowsePopup browsePopup = new BrowsePopup(getActivity());
        browsePopup.show();
        browsePopup.setListener(new BrowsePopup.BrowsePopupListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onTakePhotoClick(Dialog dialog) {
                dialog.dismiss();
                takePicture();
            }

            @Override
            public void onOpenPhotosClick(Dialog dialog) {
                dialog.dismiss();
                openPhotos();
            }

            @Override
            public void onTakeVideoClick(Dialog dialog) {

            }

            @Override
            public void onOpenVideosClick(Dialog dialog) {

            }

            @Override
            public void onTakeRecordClick(Dialog dialog) {

            }

            @Override
            public void onOpenRecordsClick(Dialog dialog) {

            }
        });
    }

    private void takePicture() {
        if (!AppUtil.verifyCameraPermissions(getActivity())) {
            return;
        }
        mCameraPicker = new CameraImagePicker(this);
        mCameraPicker.shouldGenerateMetadata(true);
        mCameraPicker.shouldGenerateThumbnails(false);
        mCameraPicker.setImagePickerCallback(this);
        mPickerPath = mCameraPicker.pickImage();
    }

    private void openPhotos() {
        if (!AppUtil.verifyStoragePermissions(getActivity())) {
            return;
        }
        mImagePicker = new ImagePicker(this);
        mImagePicker.shouldGenerateMetadata(true);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.pickImage();
    }

    private void loadImageFromPath(ImageItem imageItem) {
        mUploadImageItem = new UploadImageItem();
        mUploadImageItem.setPath(imageItem.getPath());
        File file = new File(imageItem.getPath());
        if (file.exists()) {
            mPicasso.load(file).resize(ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_AVATAR_WIDTH), 0).into(mIvAvatar);
        } else {
            mIvAvatar.setImageResource(R.drawable.ic_user_bg);
        }
    }
}

package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class CreateCardResponse extends BaseResponse {
    @SerializedName("card")
    @Expose
    private CardResponse card;

    public CardResponse getCard() {
        return card;
    }

    public void setCard(CardResponse card) {
        this.card = card;
    }
}

package app.inapps.doodlequote.app.ui.content.user;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.CustomViewHolder> {
    private List<UsersItem> mItems;
    private List<UsersItem> mDisplayItems;
    private Picasso mPicasso;
    private Context mContext;
    private AdapterListener mListener;
    private int mColor1;
    private int mColor2;
    private String mDomain;

    public interface AdapterListener {
        void onItemClick(int position);
    }

    public UsersAdapter(Context context, Picasso picasso, AdapterListener listener) {
        super();
        mPicasso = picasso;
        mItems = new ArrayList<UsersItem>();
        mDisplayItems = new ArrayList<UsersItem>();
        mContext = context;
        mListener = listener;
        mColor1 = Color.parseColor("#ffffff");
        mColor2 = Color.parseColor("#f6f6f6");
        mDomain = context.getString(R.string.app_end_point);
    }

    public void clear() {
        mItems.clear();
        mDisplayItems.clear();
    }

    public void addItems(List<UsersItem> items) {
        mItems.addAll(items);
        mDisplayItems.addAll(items);
    }

    public void updateDisplayItems() {
        mDisplayItems.clear();
        mDisplayItems.addAll(mItems);
    }

    public List<UsersItem> getItems() {
        return mItems;
    }

    public UsersItem getItem(int position) {
        if (position >= 0 && position < mDisplayItems.size()) {
            return mDisplayItems.get(position);
        }
        return null;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_users_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final UsersItem item = getItem(position);

        holder.layout.setTag(position);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = DataUtil.getInteger(v.getTag().toString(), 0);
                mListener.onItemClick(pos);
            }
        });

        if(position % 2 == 0) {
            holder.layout.setBackgroundColor(mColor1);
        } else {
            holder.layout.setBackgroundColor(mColor2);
        }

        holder.tvName.setText(item.getName());
        holder.tvTitle.setText(item.getTitle());
        holder.tvUsedCredit.setText(mContext.getString(R.string.user_used_credit, item.getUsedCredit()));

        String imageName = item.getImageName();
        if(TextUtils.isEmpty(imageName)) {
            holder.ivAvatar.setImageResource(R.drawable.ic_avatar);
        } else {
            String url = AppUtil.getAvatarImageUrl(mDomain, String.valueOf(item.getId()), imageName);
            mPicasso.load(url).resize(ViewUtil.convertDpToPixel(mContext, AppConst.DEFAULT_AVATAR_WIDTH), 0).into(holder.ivAvatar);
        }

        if(item.isDisabled()) {
            holder.tvStatus.setText(R.string.disabled);
        } else {
            holder.tvStatus.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return mDisplayItems != null ? mDisplayItems.size() : 0;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.ivAvatar) DefaultImageView ivAvatar;
        @BindView(R.id.tvName) DefaultTextView tvName;
        @BindView(R.id.tvTitle) DefaultTextView tvTitle;
        @BindView(R.id.tvUsedCredit) DefaultTextView tvUsedCredit;
        @BindView(R.id.tvStatus) DefaultTextView tvStatus;

        private CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


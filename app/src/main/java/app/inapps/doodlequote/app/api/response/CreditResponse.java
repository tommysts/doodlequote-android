package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class CreditResponse extends BaseResponse {
    @SerializedName("credit_available")
    @Expose
    private String creditAvailable;

    @SerializedName("credit_used_in_week")
    @Expose
    private String creditUsedInWeek;

    @SerializedName("credit_used_in_month")
    @Expose
    private String creditUsedInMonth;

    public String getCreditAvailable() {
        return creditAvailable;
    }

    public void setCreditAvailable(String creditAvailable) {
        this.creditAvailable = creditAvailable;
    }

    public String getCreditUsedInWeek() {
        return creditUsedInWeek;
    }

    public void setCreditUsedInWeek(String creditUsedInWeek) {
        this.creditUsedInWeek = creditUsedInWeek;
    }

    public String getCreditUsedInMonth() {
        return creditUsedInMonth;
    }

    public void setCreditUsedInMonth(String creditUsedInMonth) {
        this.creditUsedInMonth = creditUsedInMonth;
    }
}

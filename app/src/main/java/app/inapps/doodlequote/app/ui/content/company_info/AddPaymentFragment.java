package app.inapps.doodlequote.app.ui.content.company_info;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.stripe.android.Stripe;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.util.Calendar;
import java.util.Locale;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CreateCardResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.util.DebugUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.ExpiryPickerPopup;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AddPaymentFragment extends BaseFragment {
    public static final String TAG = AddPaymentFragment.class.getSimpleName();

    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.rlDone) RelativeLayout mRlDone;
    @BindView(R.id.edtName) DefaultEditText mEdtName;
    @BindView(R.id.edtNumber) DefaultEditText mEdtNumber;
    @BindView(R.id.edtExpiry) DefaultEditText mEdtExpiry;
    @BindView(R.id.ivCalendar) RecyclingImageView mIvCalendar;
    @BindView(R.id.edtCVN) DefaultEditText mEdtCVN;
    @BindView(R.id.edtAddress) DefaultEditText mEdtAddress;
    @BindView(R.id.ivAddPayment) AnimateImageView mIvAddPayment;

    private boolean mHandleBackPress = true;
    protected ExpiryPickerPopup mExpiryPickerPopup;
    protected int mMonth;
    protected int mYear;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_payment, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavBack.setOnClickListener(this);
        mRlDone.setOnClickListener(this);
        mIvAddPayment.setOnClickListener(this);
        mIvCalendar.setOnClickListener(this);

        addEditTexts("1", mEdtName, mEdtNumber, mEdtExpiry, mEdtCVN, mEdtAddress);

        Calendar calendar = Calendar.getInstance();
        mMonth = calendar.get(Calendar.MONTH) + 1;
        mYear = calendar.get(Calendar.YEAR);

        if(DebugUtil.isDebugMode(getContext())) {
            mEdtName.setText("Pham Van Chung");
            mEdtNumber.setText("4242424242424242");
            mEdtCVN.setText("123");
            mMonth = 12;
            mYear = 2017;
        }

        mEdtExpiry.setText(String.format(Locale.getDefault(), "%d/%d", mMonth, mYear));
        mEdtExpiry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    showExpiryPopup();
                }
            }
        });
        mEdtExpiry.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    showExpiryPopup();
                    return true;
                }
                return false;
            }
        });
        mIvCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExpiryPopup();
            }
        });
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        if (mHandleBackPress) {
            final AlertPopup alertPopup = new AlertPopup(getActivity());
            alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    alertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                            mHandleBackPress = false;
                            backClick(getFragmentId());
                        }
                    });
                    alertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                        }
                    });
                    alertPopup.setMessage(getString(R.string.confirm_discard_changes));
                    alertPopup.setTitle(getString(R.string.app_name));
                }
            });
            alertPopup.show();
        }
        return mHandleBackPress;
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.AddPayment.ordinal();
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavBack) {
            backClick(getFragmentId());
        } else if (v == mRlDone || v == mIvAddPayment) {
            addPayment();
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {
        return super.onInternalTouch(v, event);
    }

    protected void showExpiryPopup() {
        if(mExpiryPickerPopup == null) {
            mExpiryPickerPopup = new ExpiryPickerPopup(getContext());
        }
        mExpiryPickerPopup.show();
        Calendar calendar = Calendar.getInstance();
        mExpiryPickerPopup.setData(mMonth, mYear, calendar.get(Calendar.YEAR), calendar.get(Calendar.YEAR) + 15, new ExpiryPickerPopup.ExpiryPickerPopupListener() {
            @Override
            public void onDismiss(int month, int year) {
                mMonth = month;
                mYear = year;
                mEdtExpiry.setText(String.format(Locale.getDefault(), "%d/%d", mMonth, mYear));
            }
        });
    }

    private void addPayment() {
        final String name = mEdtName.getText().toString();
        String number = mEdtNumber.getText().toString();
        String cvn = mEdtCVN.getText().toString();
        String address = mEdtAddress.getText().toString();

        if(TextUtils.isEmpty(name)) {
            showError(R.string.error_name_require);
            return;
        }
        if(TextUtils.isEmpty(number)) {
            showError(R.string.error_card_number_require);
            return;
        }
        if(TextUtils.isEmpty(cvn)) {
            showError(R.string.error_card_cvn_require);
            return;
        }

        final Card card = new Card(number, mMonth, mYear, cvn, name,
                address, null, null, null, null, null, null);
        if (!card.validateCard()) {
            showError(R.string.error_card_invalid);
            return;
        }
        displayProgress(true);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<Token>() {
            @Override
            public void call(Subscriber<? super Token> subscriber) {
                try {
                    Stripe stripe = new Stripe(AppConst.STRIPE_PUBLISHABLE_KEY);
                    Token token = stripe.createTokenSynchronous(card);
                    subscriber.onNext(token);
                } catch (AuthenticationException | APIException | InvalidRequestException | CardException | APIConnectionException e) {
                    subscriber.onError(e);
                }
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new Observer<Token>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        mLogManager.log(e);
                        showError(e, R.string.error_loading_data);
                        displayProgress(false);
                    }
                    @Override
                    public void onNext(Token token) {
                        displayProgress(false);
                        if (token != null) {
                            createCard(token.getId());
                        } else {
                            showError(R.string.error_loading_data);
                        }
                    }
                });
        addSubscription(subscription);
    }

    void createCard(String cardToken) {
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.createCard(token, cardToken).subscribe(new SimpleObserver<CreateCardResponse>(){
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                displayProgress(false);
            }

            @Override
            public void onNext(CreateCardResponse createCardResponse) {
                super.onNext(createCardResponse);
                displayProgress(false);
                if(createCardResponse.isSuccess()) {
                    showToast("Add Successful");
                    mHandleBackPress = false;
                    backClick(getFragmentId());
                } else {
                    showError(createCardResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }
}

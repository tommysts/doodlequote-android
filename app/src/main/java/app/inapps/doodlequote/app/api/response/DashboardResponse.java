package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class DashboardResponse extends BaseResponse {
    @SerializedName("message_count")String messageCount;
    @SerializedName("callback_count")String callbackCount;
    @SerializedName("accepted_count")String acceptedCount;
    @SerializedName("rejected_count")String rejectedCount;
    @SerializedName("credit")String credit;
    @SerializedName("used")String used;
    @SerializedName("company_used")String companyUsed;
    @SerializedName("template_count")String templateCount;

    public String getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(String messageCount) {
        this.messageCount = messageCount;
    }

    public String getCallbackCount() {
        return callbackCount;
    }

    public void setCallbackCount(String callbackCount) {
        this.callbackCount = callbackCount;
    }

    public String getAcceptedCount() {
        return acceptedCount;
    }

    public void setAcceptedCount(String acceptedCount) {
        this.acceptedCount = acceptedCount;
    }

    public String getRejectedCount() {
        return rejectedCount;
    }

    public void setRejectedCount(String rejectedCount) {
        this.rejectedCount = rejectedCount;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getCompanyUsed() {
        return companyUsed;
    }

    public void setCompanyUsed(String companyUsed) {
        this.companyUsed = companyUsed;
    }

    public String getTemplateCount() {
        return templateCount;
    }

    public void setTemplateCount(String templateCount) {
        this.templateCount = templateCount;
    }
}

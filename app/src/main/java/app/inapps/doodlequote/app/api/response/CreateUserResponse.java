package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class CreateUserResponse extends BaseResponse {
    @SerializedName("user")
    @Expose
    private UserProfileResponse user;

    public UserProfileResponse getUser() {
        return user;
    }

    public void setUser(UserProfileResponse user) {
        this.user = user;
    }
}

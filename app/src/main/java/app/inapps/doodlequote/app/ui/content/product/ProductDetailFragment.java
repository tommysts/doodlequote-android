package app.inapps.doodlequote.app.ui.content.product;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.GetProductResponse;
import app.inapps.doodlequote.app.api.response.QuoteProductResponse;
import app.inapps.doodlequote.app.api.response.UploadResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.ui.widget.BrowsePopup;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.ImageItem;
import app.inapps.doodlequote.core.util.DisplayImageUtil;
import app.inapps.doodlequote.core.widget.DefaultHeaderListView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class ProductDetailFragment extends BaseFragment implements ImagePickerCallback {
    public static final String TAG = ProductDetailFragment.class.getSimpleName();

    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.ivUpload) RecyclingImageView mIvUpload;
    @BindView(R.id.rlUpload) RelativeLayout mRlUpload;
    @BindView(R.id.lvItems) DefaultHeaderListView mLvItems;
    @BindView(R.id.tvUpload) DefaultTextView mTvUpload;
    @BindView(R.id.ivArrowNext) RecyclingImageView mIvArrowNext;

    private ProductDetailHeaderView mHeaderView;
    private ProductDetailAdapter mAdapter;
    private String mEndPoint;
    private String mPickerPath;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraPicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (mImagePicker == null) {
                    mImagePicker = new ImagePicker(this);
                    mImagePicker.shouldGenerateMetadata(true);
                    mImagePicker.shouldGenerateThumbnails(false);
                    mImagePicker.setImagePickerCallback(this);
                }
                mImagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (mCameraPicker == null) {
                    mCameraPicker = new CameraImagePicker(this);
                    mCameraPicker.setImagePickerCallback(this);
                    mCameraPicker.shouldGenerateMetadata(true);
                    mCameraPicker.shouldGenerateThumbnails(false);
                    mCameraPicker.reinitialize(mPickerPath);
                }
                mCameraPicker.submit(data);
            }
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        List<ImageItem> imagePaths = new ArrayList<ImageItem>();
        for(ChosenImage image : images) {
            imagePaths.add(AppUtil.createImageItem(image));
        }
        if(imagePaths.size() > 0) {
            displayProgress(true);
            Subscription subscription = DisplayImageUtil.rxOptimizeUploadImages(getActivity(), imagePaths).subscribe(new SimpleObserver<List<ImageItem>>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    showToast(R.string.error_loading_data);
                    displayProgress(false);
                }

                @Override
                public void onNext(List<ImageItem> imageItems) {
                    super.onNext(imageItems);
                    for(ImageItem imageItem : imageItems) {
                        loadImageFromPath(imageItem);
                        mLogManager.log("File Path: " + imageItem);
                    }
                    displayProgress(false);
                }
            });
            addSubscription(subscription);
        }
    }

    @Override
    public void onError(String message) {
        showError(message);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(AppConst.KEY_PICKER_PATH, mPickerPath);
        if(mAdapter != null) {
            ArrayList<UploadItem> uploadItems = mAdapter.getItems();
            if(uploadItems != null && uploadItems.size() > 0) {
                ArrayList<UploadImageItem> uploadImageItems = new ArrayList<>();
                ArrayList<UploadVideoItem> uploadVideoItems = new ArrayList<>();
                for(UploadItem uploadItem : uploadItems) {
                    if(uploadItem instanceof UploadImageItem) {
                        uploadImageItems.add((UploadImageItem) uploadItem);
                    } else if(uploadItem instanceof UploadVideoItem) {
                        uploadVideoItems.add((UploadVideoItem) uploadItem);
                    }
                }
                outState.putParcelableArrayList(AppConst.KEY_UPLOAD_IMAGES, uploadImageItems);
                outState.putParcelableArrayList(AppConst.KEY_UPLOAD_VIDEOS, uploadVideoItems);
            }
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(final Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavBack.setOnClickListener(this);
        mRlUpload.setOnClickListener(this);
        mRlUpload.setVisibility(View.GONE);

        mEndPoint = getString(R.string.app_end_point);

        mHeaderView = (ProductDetailHeaderView) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_product_detail_header, mLvItems, false);
        mLvItems.addHeaderView(mHeaderView);
        mAdapter = new ProductDetailAdapter(getActivity(), mPicasso, new ProductDetailAdapter.AdapterListener() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onDeleteClick(int position) {
                mAdapter.deleteItem(position);
                mAdapter.notifyDataSetChanged();

                if(mAdapter.countLocal() == 0) {
                    mTvUpload.setText(R.string.upload_to_confirm);
                    if (mIvArrowNext.getVisibility() != View.VISIBLE) {
                        mIvArrowNext.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        mLvItems.setAdapter(mAdapter);

        addImageViews(mIvUpload);

        int productId = mContainerMap.getInt(AppConst.KEY_PRODUCT_ID);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        displayProgress(true);
        Subscription subscription = mApiManager.getProduct(token, String.valueOf(productId)).subscribe(new SimpleObserver<GetProductResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(GetProductResponse getProductResponse) {
                super.onNext(getProductResponse);
                displayProgress(false);
                if (getProductResponse.isSuccess()) {
                    QuoteProductResponse productResponse = getProductResponse.getProduct();
                    mHeaderView.mTvName.setText(productResponse.getName());
                    mHeaderView.mTvDescription.setText(productResponse.getDescription());
                    String price = AppUtil.formatPrice(productResponse.getPrice());
                    mHeaderView.mTvPrice.setText(price);
                    AppConst.ProductStatus status = AppUtil.getProductStatus(productResponse);
                    switch (status) {
                        case NewItem:
                            mHeaderView.mTvStatus.setText(R.string.product_status_new_item);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.product_new_item));
                            mRlUpload.setVisibility(View.GONE);
                            break;
                        case Approved:
                            mHeaderView.mTvStatus.setText(R.string.product_status_approved);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.product_approved));
                            mRlUpload.setVisibility(View.VISIBLE);
                            break;
                        case Rejected:
                            mHeaderView.mTvStatus.setText(R.string.product_status_rejected);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.product_rejected));
                            mRlUpload.setVisibility(View.GONE);
                            break;
                        case Completed:
                            mHeaderView.mTvStatus.setText(R.string.product_status_completed);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.product_completed));
                            mRlUpload.setVisibility(View.GONE);
                            break;
                    }
                    if(savedInstanceState == null) {
                        mAdapter.clear();
                        List<String> images = AppUtil.getImages(productResponse);
                        if (images != null && images.size() > 0) {
                            String imageUrl1 = AppUtil.getImageUrl(mEndPoint, productResponse.getQuoteId().toString(), productResponse.getId().toString(), images.get(0));
                            mPicasso.load(imageUrl1).resize(ViewUtil.convertDpToPixel(getActivity(), 100), 0).into(mHeaderView.mIvImage);
                            String customerName = mContainerMap.getString(AppConst.KEY_CUSTOMER_NAME);
                            for (String image : images) {
                                UploadImageItem uploadImageItem = new UploadImageItem();
                                uploadImageItem.setName("Image " + (mAdapter.getCount() + 1));
                                String imageUrl = AppUtil.getImageUrl(mEndPoint, productResponse.getQuoteId().toString(), productResponse.getId().toString(), image);
                                uploadImageItem.setPath(imageUrl);
                                uploadImageItem.setUploadBy(customerName);
                                uploadImageItem.setUploadTime(AppUtil.formatUploadTime(productResponse.getUpdatedAt()));
                                uploadImageItem.setType(AppConst.UploadType.Normal);
                                mAdapter.addItem(uploadImageItem);
                            }
                        }
                        List<String> fixedImages = AppUtil.getFixedImages(productResponse);
                        if (fixedImages != null && fixedImages.size() > 0) {
                            //String customerName = mContainerMap.getString(AppConst.KEY_CUSTOMER_NAME);
                            for (String image : fixedImages) {
                                UploadImageItem uploadImageItem = new UploadImageItem();
                                uploadImageItem.setName("Confirmation Image " + (mAdapter.countType(AppConst.UploadType.Fixed) + 1));
                                String imageUrl = AppUtil.getImageUrl(mEndPoint, productResponse.getQuoteId().toString(), productResponse.getId().toString(), image);
                                uploadImageItem.setPath(imageUrl);
                                //uploadImageItem.setUploadBy(customerName);
                                //uploadImageItem.setUploadTime(AppUtil.formatUploadTime(productResponse.getCreatedAt()));
                                uploadImageItem.setType(AppConst.UploadType.Fixed);
                                mAdapter.addItem(uploadImageItem);
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mAdapter.clear();
                        if(savedInstanceState.containsKey(AppConst.KEY_UPLOAD_IMAGES)) {
                            ArrayList<UploadImageItem> uploadImageItems = savedInstanceState.getParcelableArrayList(AppConst.KEY_UPLOAD_IMAGES);
                            if(uploadImageItems != null && uploadImageItems.size() > 0) {
                                for (UploadImageItem photoItem : uploadImageItems) {
                                    mAdapter.addItem(photoItem);
                                }
                            }
                        }
                        if(savedInstanceState.containsKey(AppConst.KEY_UPLOAD_VIDEOS)) {
                            ArrayList<UploadVideoItem> uploadVideoItems = savedInstanceState.getParcelableArrayList(AppConst.KEY_UPLOAD_VIDEOS);
                            if(uploadVideoItems != null && uploadVideoItems.size() > 0) {
                                for (UploadVideoItem uploadVideoItem : uploadVideoItems) {
                                    mAdapter.addItem(uploadVideoItem);
                                }
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    showError(getProductResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(AppConst.KEY_PICKER_PATH)) {
                mPickerPath = savedInstanceState.getString(AppConst.KEY_PICKER_PATH);
            }
        }
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.ProductDetail.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavBack) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavBack) {
            backClick(getFragmentId());
        } else if (v == mRlUpload) {
            upload();
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {

        return super.onInternalTouch(v, event);
    }

    public void upload() {
        String text = mTvUpload.getText().toString();
        if(getString(R.string.done).equalsIgnoreCase(text)) {
            done();
        } else {
            BrowsePopup browsePopup = new BrowsePopup(getActivity());
            browsePopup.show();
            browsePopup.setListener(new BrowsePopup.BrowsePopupListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onTakePhotoClick(Dialog dialog) {
                    dialog.dismiss();
                    takePicture();
                }

                @Override
                public void onOpenPhotosClick(Dialog dialog) {
                    dialog.dismiss();
                    openPhotos();
                }

                @Override
                public void onTakeVideoClick(Dialog dialog) {

                }

                @Override
                public void onOpenVideosClick(Dialog dialog) {

                }

                @Override
                public void onTakeRecordClick(Dialog dialog) {

                }

                @Override
                public void onOpenRecordsClick(Dialog dialog) {

                }
            });
        }
    }

    private void done() {
        int productId = mContainerMap.getInt(AppConst.KEY_PRODUCT_ID);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Map<Integer, List<String>> products = new HashMap<>();
        for (int i = 0; i < mAdapter.getCount(); i++) {
            UploadImageItem uploadImageItem = (UploadImageItem) mAdapter.getItem(i);
            if (uploadImageItem.getType() == AppConst.UploadType.Fixed
                    && !URLUtil.isNetworkUrl(uploadImageItem.getPath())) {
                List<String> images = products.get(productId);
                if (images == null) {
                    images = new ArrayList<String>();
                    products.put(productId, images);
                }
                images.add(uploadImageItem.getPath());
            }
        }
        displayProgress(true);
        Subscription subscription = mApiManager.uploadFixedImages(token, products).subscribe(new SimpleObserver<UploadResponse>(){
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(UploadResponse uploadResponse) {
                super.onNext(uploadResponse);
                displayProgress(false);
                if(uploadResponse.isSuccess()) {
                    showToast(R.string.update_success);
                    Bundle data = new Bundle();
                    data.putBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA, true);
                    backClick(getFragmentId(), data);
                } else {
                    showError(uploadResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void takePicture() {
        if(!AppUtil.verifyCameraPermissions(getActivity())) {
            return;
        }
        mCameraPicker = new CameraImagePicker(this);
        mCameraPicker.shouldGenerateMetadata(true);
        mCameraPicker.shouldGenerateThumbnails(false);
        mCameraPicker.setImagePickerCallback(this);
        mPickerPath = mCameraPicker.pickImage();
    }

    private void openPhotos() {
        if(!AppUtil.verifyStoragePermissions(getActivity())) {
            return;
        }
        mImagePicker = new ImagePicker(this);
        mImagePicker.allowMultiple();
        mImagePicker.shouldGenerateMetadata(true);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.pickImage();
    }

    private void loadImageFromPath(ImageItem imageItem) {
        UploadImageItem uploadImageItem = new UploadImageItem();
        uploadImageItem.setName("Confirmation Image " + (mAdapter.countType(AppConst.UploadType.Fixed) + 1));
        uploadImageItem.setPath(imageItem.getPath());
        //uploadImageItem.setUploadBy(customerName);
        //uploadImageItem.setUploadTime(AppUtil.formatUploadTime(productResponse.getCreatedAt()));
        uploadImageItem.setType(AppConst.UploadType.Fixed);
        mAdapter.addItem(uploadImageItem);
        mAdapter.notifyDataSetChanged();
        mTvUpload.setText(R.string.done);
        if(mIvArrowNext.getVisibility() != View.GONE) {
            mIvArrowNext.setVisibility(View.GONE);
        }
    }
}

package app.inapps.doodlequote.app.ui.content.forgot_password;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.ResetPasswordResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.util.CoreUtil;
import app.inapps.doodlequote.core.widget.AnimateButton;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class ForgotPasswordFragment extends BaseFragment {
    public static final String TAG = ForgotPasswordFragment.class.getSimpleName();

    @BindView(R.id.ivCoverImage) RecyclingImageView mIvCoverImage;
    @BindView(R.id.ivLogoImage) RecyclingImageView mIvLogoImage;
    @BindView(R.id.edt_otp) DefaultEditText mEdtOtp;
    @BindView(R.id.edt_password) DefaultEditText mEdtPassword;
    @BindView(R.id.edt_confirm_password) DefaultEditText mEdtConfirmPassword;
    @BindView(R.id.btn_change_password) AnimateButton mBtnChangePassword;
    @BindView(R.id.llSignIn) LinearLayout mLlSignIn;
    @BindView(R.id.txtVersion) DefaultTextView mTxtVersion;
    @BindView(R.id.txtCopyrightInfo) DefaultTextView mTxtCopyrightInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        //mPicasso.load(R.drawable.back_ground).fit().into(mIvCoverImage);
        //mPicasso.load(R.drawable.logo).fit().into(mIvLogoImage);
        mTxtVersion.setText(getString(R.string.app_version, CoreUtil.getAppVersion(getActivity())));

        mBtnChangePassword.setOnClickListener(this);
        mTxtCopyrightInfo.setOnClickListener(this);
        mLlSignIn.setOnClickListener(this);

        addEditTexts("1", mEdtOtp, mEdtPassword, mEdtConfirmPassword);
        addImageViews(mIvLogoImage, mIvCoverImage);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.ForgotPassword.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mLlSignIn) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mBtnChangePassword) {
            changePassword();
        } else if (v == mLlSignIn) {
            openPage(MainActivity.Pages.Login);
        }
    }

    private void changePassword() {
        String otp = mEdtOtp.getText().toString();
        String password = mEdtPassword.getText().toString();
        String confirmPassword = mEdtConfirmPassword.getText().toString();
        if (TextUtils.isEmpty(otp)) {
            showError(R.string.error_otp_require);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            showError(R.string.error_pin_require);
            return;
        }
        if (!password.equals(confirmPassword)) {
            showError(R.string.error_pin_mismatch);
            return;
        }
        displayProgress(true);
        String email = mContainerMap.getString(AppConst.KEY_EMAIL);
        Subscription subscription = mApiManager.resetPassword(email, otp, password, confirmPassword).subscribe(new SimpleObserver<ResetPasswordResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ResetPasswordResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    showToast(R.string.reset_pin_success);
                    openPage(MainActivity.Pages.Login);
                } else {
                    showError(response, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }
}

package app.inapps.doodlequote.app.ui.content.product;

import android.os.Parcel;

public class UploadVideoItem extends UploadItem {
    String previewImagePath;

    public String getPreviewImagePath() {
        return previewImagePath;
    }

    public void setPreviewImagePath(String previewImagePath) {
        this.previewImagePath = previewImagePath;
    }

    public UploadVideoItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.previewImagePath);
    }

    protected UploadVideoItem(Parcel in) {
        super(in);
        this.previewImagePath = in.readString();
    }

    public static final Creator<UploadVideoItem> CREATOR = new Creator<UploadVideoItem>() {
        @Override
        public UploadVideoItem createFromParcel(Parcel source) {
            return new UploadVideoItem(source);
        }

        @Override
        public UploadVideoItem[] newArray(int size) {
            return new UploadVideoItem[size];
        }
    };
}

package app.inapps.doodlequote.app.ui.content.templates;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.Locale;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CreateTemplateResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

import static app.inapps.doodlequote.app.AppConst.PREVIEW_VALUES;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_COMPANY;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_FIRST;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_KEYS;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_LAST;
import static app.inapps.doodlequote.app.AppConst.TEMPLATE_LINK;

public class TemplatesCreateFragment extends BaseFragment {
    public static final String TAG = TemplatesCreateFragment.class.getSimpleName();

    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.rlDone) RelativeLayout mRlDone;
    @BindView(R.id.ivFirst) AnimateImageView mIvFirst;
    @BindView(R.id.ivLast) AnimateImageView mIvLast;
    @BindView(R.id.ivCompany) AnimateImageView mIvCompany;
    @BindView(R.id.ivLink) AnimateImageView mIvLink;
    @BindView(R.id.edtTemplate) EditText mEdtTemplate;
    @BindView(R.id.edtPreview) DefaultEditText mEdtPreview;

    private boolean mHandleBackPress = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_templates_create, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavBack.setOnClickListener(this);
        mRlDone.setOnClickListener(this);
        mIvFirst.setOnClickListener(this);
        mIvLast.setOnClickListener(this);
        mIvCompany.setOnClickListener(this);
        mIvLink.setOnClickListener(this);

        addEditTexts("1", mEdtTemplate);
        addImageViews(mIvFirst, mIvLast, mIvCompany, mIvLink);

        String template = String.format(Locale.getDefault(), "Hey [%s], check [%s]", TEMPLATE_FIRST, TEMPLATE_LINK);
        mEdtTemplate.setText(template);
        highlightTemplate();
        templateToPreview();
        highlightPreview();

        mEdtTemplate.addTextChangedListener(mTextWatcher);
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        String current = "";

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            getHandler().removeCallbacks(mRunnable);
            getHandler().postDelayed(mRunnable, AppConst.DELAY_TEXT_CHANGED_INTERVAL_MILLISECONDS);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private Runnable mRunnable = new Runnable() {
        int selectionStart;

        @Override
        public void run() {
            mEdtTemplate.removeTextChangedListener(mTextWatcher);
            selectionStart = mEdtTemplate.getSelectionStart();
            highlightTemplate();
            templateToPreview();
            highlightPreview();
            mEdtTemplate.setSelection(selectionStart);
            mEdtTemplate.addTextChangedListener(mTextWatcher);
        }
    };

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        if (mHandleBackPress) {
            final AlertPopup alertPopup = new AlertPopup(getActivity());
            alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    alertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                            mHandleBackPress = false;
                            backClick(getFragmentId());
                        }
                    });
                    alertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertPopup.dismiss();
                        }
                    });
                    alertPopup.setMessage(getString(R.string.confirm_discard_changes));
                    alertPopup.setTitle(getString(R.string.app_name));
                }
            });
            alertPopup.show();
        }
        return mHandleBackPress;
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.TemplatesCreate.ordinal();
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavBack) {
            backClick(getFragmentId());
        } else if (v == mRlDone) {
            done();
        } else if (v == mIvFirst || v == mIvLast || v == mIvCompany || v == mIvLink) {
            editTemplate(v);
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {
        return super.onInternalTouch(v, event);
    }

    private void done() {
        String template = mEdtTemplate.getText().toString();
        if(TextUtils.isEmpty(template)) {
            showError(R.string.error_template_require);
            return;
        }
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.createTemplate(token, template).subscribe(new SimpleObserver<CreateTemplateResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(CreateTemplateResponse createTemplateResponse) {
                super.onNext(createTemplateResponse);
                displayProgress(false);
                if(createTemplateResponse.isSuccess()) {
                    showToast("Create Successful");
                    mHandleBackPress = false;
                    Bundle data = new Bundle();
                    data.putBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA, true);
                    backClick(getFragmentId(), data);
                } else {
                    showError(createTemplateResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void editTemplate(View v) {
        mEdtTemplate.removeTextChangedListener(mTextWatcher);
        int selectionStart = mEdtTemplate.getSelectionStart();
        String currentTemplate = mEdtTemplate.getText().toString();
        String replace = " [%s] ";
        int count = 4;
        boolean hasSpace = false;
        if(selectionStart > 0) {
            hasSpace = currentTemplate.charAt(selectionStart - 1) == ' ';
        }
        if(hasSpace) {
            replace = "[%s] ";
            count = 3;
        }
        if (v == mIvFirst) {
            currentTemplate = currentTemplate.substring(0, selectionStart)
                    + String.format(Locale.getDefault(), replace, TEMPLATE_FIRST)
                    + currentTemplate.substring(selectionStart, currentTemplate.length());
            selectionStart = selectionStart + TEMPLATE_FIRST.length() + count;
        } else if (v == mIvLast) {
            currentTemplate = currentTemplate.substring(0, selectionStart)
                    + String.format(Locale.getDefault(), replace, TEMPLATE_LAST)
                    + currentTemplate.substring(selectionStart, currentTemplate.length());
            selectionStart = selectionStart + TEMPLATE_LAST.length() + count;
        } else if (v == mIvCompany) {
            currentTemplate = currentTemplate.substring(0, selectionStart)
                    + String.format(Locale.getDefault(), replace, TEMPLATE_COMPANY)
                    + currentTemplate.substring(selectionStart, currentTemplate.length());
            selectionStart = selectionStart + TEMPLATE_COMPANY.length() + count;
        } else if (v == mIvLink) {
            currentTemplate = currentTemplate.substring(0, selectionStart)
                    + String.format(Locale.getDefault(), replace, TEMPLATE_LINK)
                    + currentTemplate.substring(selectionStart, currentTemplate.length());
            selectionStart = selectionStart + TEMPLATE_LINK.length() + count;
        }
        mEdtTemplate.setText(currentTemplate);
        highlightTemplate();
        templateToPreview();
        highlightPreview();
        mEdtTemplate.setSelection(selectionStart);
        mEdtTemplate.addTextChangedListener(mTextWatcher);
    }

    private void highlightTemplate() {
        ViewUtil.setColorsText(getActivity(), mEdtTemplate, TEMPLATE_KEYS, R.color.template);
    }

    private void templateToPreview() {
        String preview = mEdtTemplate.getText().toString();
        for(int i = 0; i < TEMPLATE_KEYS.length; i++) {
            preview = preview.replace(String.format(Locale.getDefault(), "[%s]", TEMPLATE_KEYS[i]), PREVIEW_VALUES[i]);
        }
        mEdtPreview.setText(preview);
    }

    private void highlightPreview() {
        ViewUtil.setColorsText(getActivity(), mEdtPreview, PREVIEW_VALUES, R.color.template);
    }
}

package app.inapps.doodlequote.app.ui.content.login;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.ForgotPasswordResponse;
import app.inapps.doodlequote.app.api.response.LoginResponse;
import app.inapps.doodlequote.app.api.response.UpdateDeviceTokenResponse;
import app.inapps.doodlequote.app.api.response.UserProfileResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.app.util.gcm.GcmPreferences;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class LoginPinFragment extends BaseFragment {
    public static final String TAG = LoginPinFragment.class.getSimpleName();

    @BindView(R.id.ivCoverImage) RecyclingImageView mIvCoverImage;
    @BindView(R.id.ivLogoImage) RecyclingImageView mIvLogoImage;
    @BindView(R.id.tvCancel) DefaultTextView mTvCancel;
    @BindView(R.id.ivPin1) DefaultImageView mIvPin1;
    @BindView(R.id.ivPin2) DefaultImageView mIvPin2;
    @BindView(R.id.ivPin3) DefaultImageView mIvPin3;
    @BindView(R.id.ivPin4) DefaultImageView mIvPin4;
    @BindView(R.id.llNum1) LinearLayout mLlNum1;
    @BindView(R.id.llNum2) LinearLayout mLlNum2;
    @BindView(R.id.llNum3) LinearLayout mLlNum3;
    @BindView(R.id.llNum4) LinearLayout mLlNum4;
    @BindView(R.id.llNum5) LinearLayout mLlNum5;
    @BindView(R.id.llNum6) LinearLayout mLlNum6;
    @BindView(R.id.llNum7) LinearLayout mLlNum7;
    @BindView(R.id.llNum8) LinearLayout mLlNum8;
    @BindView(R.id.llNum9) LinearLayout mLlNum9;
    @BindView(R.id.llForgot) LinearLayout mLlForgot;
    @BindView(R.id.llNum0) LinearLayout mLlNum0;
    @BindView(R.id.llSignUp) LinearLayout mLlSignUp;
    @BindView(R.id.llDelete) LinearLayout mLlDelete;

    private int mPinIndex = 0;
    private String[] mPins = new String[4];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_pin, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mTvCancel.setOnClickListener(this);
        mLlNum0.setOnClickListener(this);
        mLlNum1.setOnClickListener(this);
        mLlNum2.setOnClickListener(this);
        mLlNum3.setOnClickListener(this);
        mLlNum4.setOnClickListener(this);
        mLlNum5.setOnClickListener(this);
        mLlNum6.setOnClickListener(this);
        mLlNum7.setOnClickListener(this);
        mLlNum8.setOnClickListener(this);
        mLlNum9.setOnClickListener(this);
        mLlForgot.setOnClickListener(this);
        mLlSignUp.setOnClickListener(this);
        mLlDelete.setOnClickListener(this);

        mLlDelete.setVisibility(View.GONE);
        mLlSignUp.setVisibility(View.VISIBLE);

        addImageViews(mIvCoverImage, mIvLogoImage);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.LoginPin.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if (v == mTvCancel) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if(v == mTvCancel) {
            openPage(MainActivity.Pages.Login);
        } else if(v == mLlDelete) {
            deletePin();
        } else if(v == mLlSignUp) {
            openPage(MainActivity.Pages.Register);
        } else if(v == mLlForgot) {
            forgotPassword();
        } else if(v == mLlNum0) {
            setPin("0");
        } else if(v == mLlNum1) {
            setPin("1");
        } else if(v == mLlNum2) {
            setPin("2");
        } else if(v == mLlNum3) {
            setPin("3");
        } else if(v == mLlNum4) {
            setPin("4");
        } else if(v == mLlNum5) {
            setPin("5");
        } else if(v == mLlNum6) {
            setPin("6");
        } else if(v == mLlNum7) {
            setPin("7");
        } else if(v == mLlNum8) {
            setPin("8");
        } else if(v == mLlNum9) {
            setPin("9");
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {
        return super.onInternalTouch(v, event);
    }

    private void clearPins() {
        for(int i = 0; i < mPins.length; i++) {
            mPins[i] = "";
        }
        mIvPin1.setImageDrawable(null);
        mIvPin2.setImageDrawable(null);
        mIvPin3.setImageDrawable(null);
        mIvPin4.setImageDrawable(null);
        mPinIndex = 0;
        mLlDelete.setVisibility(View.GONE);
        mLlSignUp.setVisibility(View.VISIBLE);
    }

    private void deletePin() {
        mPinIndex--;
        if(mPinIndex == 0) {
            mIvPin1.setImageDrawable(null);
            mPins[mPinIndex] = "";
            mLlDelete.setVisibility(View.GONE);
            mLlSignUp.setVisibility(View.VISIBLE);
        } else if(mPinIndex == 1) {
            mIvPin2.setImageDrawable(null);
            mPins[mPinIndex] = "";
        } else if(mPinIndex == 2) {
            mIvPin3.setImageDrawable(null);
            mPins[mPinIndex] = "";
        } else if(mPinIndex == 3) {
            mIvPin4.setImageDrawable(null);
            mPins[mPinIndex] = "";
        }
    }

    private void setPin(String value) {
        if(mPinIndex == 0) {
            mIvPin1.setImageResource(R.drawable.pin_ic_dot);
            mPins[mPinIndex] = value;
            mPinIndex++;
        } else if(mPinIndex == 1) {
            mIvPin2.setImageResource(R.drawable.pin_ic_dot);
            mPins[mPinIndex] = value;
            mPinIndex++;
        } else if(mPinIndex == 2) {
            mIvPin3.setImageResource(R.drawable.pin_ic_dot);
            mPins[mPinIndex] = value;
            mPinIndex++;
        } else if(mPinIndex == 3) {
            mIvPin4.setImageResource(R.drawable.pin_ic_dot);
            mPins[mPinIndex] = value;
            login();
        }
        if(mPinIndex > 0) {
            mLlSignUp.setVisibility(View.GONE);
            mLlDelete.setVisibility(View.VISIBLE);
        }
    }

    private void login() {
        String email = mUserPreferences.getEmail();
        String pin = "";
        for(String p : mPins) {
            pin += p;
        }
        if (TextUtils.isEmpty(email)) {
            showError(R.string.error_email_require);
            return;
        }
        if (!ValidatorUtil.isValidEmail(email)) {
            showError(R.string.error_email_invalid);
            return;
        }
        if (TextUtils.isEmpty(pin)) {
            showError(R.string.error_pin_require);
            return;
        }
        displayProgress(true);
        Subscription subscription = mApiManager.login(email, pin).subscribe(new SimpleObserver<LoginResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(LoginResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    mUserPreferences.setToken(response.getToken());
                    mContainerMap.setBoolean(AppConst.KEY_LOG_IN, true);
                    mContainerMap.setString(AppConst.KEY_TOKEN, response.getToken());
                    UserProfileResponse userProfileResponse = response.getUser();
                    if (userProfileResponse != null) {
                        AppConst.LoginType loginType = AppUtil.getLoginType(userProfileResponse);
                        mContainerMap.setInt(AppConst.KEY_USER_TYPE, loginType.getType());
                        mContainerMap.setString(AppConst.KEY_COMPANY_NAME, userProfileResponse.getCompanyName());
                        mUserPreferences.setEmail(userProfileResponse.getEmail());
                        mUserPreferences.setLoginType(loginType.getType());
                    }
                    // GCM
                    GcmPreferences gcmPreferences = GcmPreferences.getInstance(getActivity());
                    gcmPreferences.storeUser(response);
                    Subscription subscription1 = mApiManager.updateDeviceToken(response.getToken(), gcmPreferences.getRegistrationId(), gcmPreferences.getDeviceType())
                            .subscribe(new SimpleObserver<UpdateDeviceTokenResponse>() {
                                @Override
                                public void onError(Throwable t) {
                                    super.onError(t);

                                    updateUser();
                                    openPage(MainActivity.Pages.Dashboard);
                                }

                                @Override
                                public void onNext(UpdateDeviceTokenResponse updateDeviceTokenResponse) {
                                    super.onNext(updateDeviceTokenResponse);

                                    updateUser();
                                    openPage(MainActivity.Pages.Dashboard);
                                }
                            });
                    addSubscription(subscription1);
                } else {
                    clearPins();
                    showError(response, R.string.error_login);
                }
            }
        });
        addSubscription(subscription);
    }

    public void forgotPassword() {
        String email = mUserPreferences.getEmail();
        if (TextUtils.isEmpty(email)) {
            showError(R.string.error_email_require);
            return;
        }
        if (!ValidatorUtil.isValidEmail(email)) {
            showError(R.string.error_email_invalid);
            return;
        }
        displayProgress(true);
        Subscription subscription = mApiManager.forgotPassword(email).subscribe(new SimpleObserver<ForgotPasswordResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ForgotPasswordResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    final AlertPopup alertPopup = new AlertPopup(getActivity());
                    alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                        @Override
                        public void onStart() {
                            alertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertPopup.dismiss();
                                    String email = mUserPreferences.getEmail();
                                    mContainerMap.setString(AppConst.KEY_EMAIL, email);
                                    openPage(MainActivity.Pages.ForgotPassword);
                                }
                            });
                            alertPopup.setMessage(getString(R.string.forgot_password_success));
                        }
                    });
                    alertPopup.show();
                } else {
                    showError(response, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }
}

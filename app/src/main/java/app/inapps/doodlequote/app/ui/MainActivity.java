package app.inapps.doodlequote.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppApplication;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.ui.content.SimpleSideDrawer;
import app.inapps.doodlequote.app.ui.content.ViewMenuCompanyLayout;
import app.inapps.doodlequote.app.ui.content.ViewMenuLayout;
import app.inapps.doodlequote.app.ui.content.ViewMenuLayoutListener;
import app.inapps.doodlequote.app.ui.content.ViewMenuStaffLayout;
import app.inapps.doodlequote.app.ui.content.buy_credits.BuyCreditsFragment;
import app.inapps.doodlequote.app.ui.content.buy_credits.SmsCreditsFragment;
import app.inapps.doodlequote.app.ui.content.change_password.ChangePasswordFragment;
import app.inapps.doodlequote.app.ui.content.company_info.AddPaymentFragment;
import app.inapps.doodlequote.app.ui.content.company_info.CompanyInfoFragment;
import app.inapps.doodlequote.app.ui.content.dashboard.DashboardFragment;
import app.inapps.doodlequote.app.ui.content.forgot_password.ForgotPasswordFragment;
import app.inapps.doodlequote.app.ui.content.login.LoginFragment;
import app.inapps.doodlequote.app.ui.content.login.LoginPinFragment;
import app.inapps.doodlequote.app.ui.content.product.ProductCreateFragment;
import app.inapps.doodlequote.app.ui.content.product.ProductDetailFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesCreateFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesDetailFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesFragment;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesTemplatesFragment;
import app.inapps.doodlequote.app.ui.content.register.RegisterFragment;
import app.inapps.doodlequote.app.ui.content.templates.TemplatesCreateFragment;
import app.inapps.doodlequote.app.ui.content.templates.TemplatesFragment;
import app.inapps.doodlequote.app.ui.content.user.AddUserFragment;
import app.inapps.doodlequote.app.ui.content.user.UsersFragment;
import app.inapps.doodlequote.app.ui.content.user_profile.UserProfileFragment;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.app.util.gcm.GcmPreferences;
import app.inapps.doodlequote.app.util.gcm.RegistrationIntentService;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.common.util.StringUtil;
import app.inapps.doodlequote.common.util.SystemUtil;
import app.inapps.doodlequote.core.ContainerMap;
import app.inapps.doodlequote.core.SystemConst;
import app.inapps.doodlequote.core.api.response.BaseResponse;
import app.inapps.doodlequote.core.util.NetworkStateMonitor;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.adapter.rxjava.HttpException;

import static app.inapps.doodlequote.app.ui.MainActivity.Pages.Login;
import static app.inapps.doodlequote.app.ui.MainActivity.Pages.LoginPin;
import static app.inapps.doodlequote.app.ui.MainActivity.Pages.Register;

public class MainActivity extends BaseActivity
        implements OnFragmentInteractionListener,
        NetworkStateMonitor.OnNetworkStateChangedListener,
        View.OnClickListener {

    public enum Pages {
        Empty,
        Login,
        LoginPin,
        Register,
        ForgotPassword,
        Dashboard,
        Quotes,
        QuotesDetail,
        QuotesCreate,
        QuotesTemplates,
        ProductDetail,
        ProductCreate,
        Templates,
        TemplatesCreate,
        UserProfile,
        ChangePassword,
        CompanyInfo,
        UserList,
        AddUser,
        BuyCredits,
        SmsCredits,
        AddPayment
    }

    public enum DrawerType {
        Empty(Pages.Empty.ordinal(), ""),
        Dashboard(Pages.Dashboard.ordinal(), DashboardFragment.TAG),
        Quotes(Pages.Quotes.ordinal(), QuotesFragment.TAG),
        Templates(Pages.Templates.ordinal(), TemplatesFragment.TAG),
        UserProfile(Pages.UserProfile.ordinal(), UserProfileFragment.TAG),
        ChangePassword(Pages.ChangePassword.ordinal(), ChangePasswordFragment.TAG),
        CompanyInfo(Pages.CompanyInfo.ordinal(), CompanyInfoFragment.TAG),
        UserList(Pages.UserList.ordinal(), UsersFragment.TAG),
        AddUser(Pages.AddUser.ordinal(), AddUserFragment.TAG),
        BuyCredits(Pages.BuyCredits.ordinal(), BuyCreditsFragment.TAG),
        Logout(-1, "");

        private int pageIndex;
        private String fragmentTag;

        DrawerType(int pageIndex, String fragmentTag) {
            this.pageIndex = pageIndex;
            this.fragmentTag = fragmentTag;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public String getFragmentTag() {
            return fragmentTag;
        }
    }

    @BindView(R.id.vRootContent) RelativeLayout mVRootContent;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;
    @BindView(R.id.tvMessageStatus) TextView mTvMessage;

    @Inject Picasso mPicasso;
    @Inject ContainerMap mContainerMap;

    private SimpleSideDrawer mSideDrawer;
    private Set<Integer> mGlobalLayoutRegisters;
    private boolean mIsNetworkEnable;
    private boolean mIsBackPress;
    private Handler mHandler; // Do not use this variable directly, use getHandler() instead
    private Runnable mBackPressTask = new Runnable() {
        @Override
        public void run() {
            mIsBackPress = false;
        }
    };
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUnbinder = ButterKnife.bind(this);

        if (savedInstanceState == null) {
            checkNotification(getIntent().getExtras());
        } else {
            mContainerMap = savedInstanceState.getParcelable(SystemConst.BUNDLE_KEY_CONTAINER_MAP);
            if (mContainerMap != null) {
                AppApplication.getInstance(this).createBaseComponent(mContainerMap);
                setupActivityComponent(savedInstanceState);
                mLogManager.log("onCreate ContainerMap " + mContainerMap.toString());
            } else {
                mContainerMap = new ContainerMap();
                mLogManager.e("onCreate ContainerMap is NULL!!! Why???");
            }

            if(BuildConfig.DEBUG) {
                StringBuilder sb = new StringBuilder();
                FragmentManager fragmentManager = getSupportFragmentManager();
                int count = fragmentManager.getBackStackEntryCount();
                for(int i = 0; i < count; i++) {
                    FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(i);
                    sb.append(backStackEntry.getName());
                    sb.append(" ");
                }
                mLogManager.log("BackStackEntries = " + sb.toString());
            }
        }
        resetMenu();
        checkMenu();

        mIsNetworkEnable = NetworkStateMonitor.getInstance(this).isConnected();
        checkNetwork(NetworkStateMonitor.getInstance(this));
        startGcmService();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                GcmPreferences gcmPreferences = GcmPreferences.getInstance(MainActivity.this);
                mUserPreferences.setDeviceToken(gcmPreferences.getRegistrationId());
            }
        };
        // Global layout listener
        setUpGlobalLayoutListener();

        // Storage Permissions
        AppUtil.verifyStoragePermissions(this);

        mLogManager.log("MainActivity onCreate");
        Tracker tracker = AppApplication.getInstance(this).getDefaultTracker();
        tracker.setScreenName(MainActivity.class.getSimpleName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mContainerMap != null) {
            outState.putParcelable(SystemConst.BUNDLE_KEY_CONTAINER_MAP, mContainerMap);
            mLogManager.log("onSaveInstanceState ContainerMap " + mContainerMap.toString());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        //super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.getBoolean(AppConst.KEY_LOGOUT)) {
                openPageFragment(new LoginFragment(), extras);
                return;
            }
        }
        resetMenu();
        checkMenu();
        checkNotification(getIntent().getExtras());
    }

    @Override
    protected void onResume() {
        super.onResume();
        NetworkStateMonitor.getInstance(this).registerListener(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
        if (baseFragment != null) {
            baseFragment.onFragmentResume(null);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GcmPreferences.REGISTRATION_COMPLETE_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        NetworkStateMonitor.getInstance(this).unregisterListener(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
        if (baseFragment != null) {
            baseFragment.onFragmentPause(null);
        }
        if (mRegistrationBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
        if (baseFragment != null) {
            baseFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    void setupActivityComponent(Bundle savedInstanceState) {
        AppApplication.getInstance(this).getAppComponent().inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NetworkStateMonitor.getInstance(this).unregisterListener(this);
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        if (mSideDrawer != null) {
            View view = mSideDrawer.getLeftBehindView();
            if (view instanceof ViewMenuLayout) {
                ((ViewMenuLayout) view).destroy();
            } else if (view instanceof ViewMenuCompanyLayout) {
                ((ViewMenuCompanyLayout) view).destroy();
            } else if (view instanceof ViewMenuStaffLayout) {
                ((ViewMenuStaffLayout) view).destroy();
            }
        }
        if (mRegistrationBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            mRegistrationBroadcastReceiver = null;
        }
        SystemUtil.executeJavaVmGC();
    }

    @Override
    public void onNetworkStateChanged(NetworkStateMonitor networkStateMonitor) {
        if (networkStateMonitor.isConnected()) {
            dismissDialog();
        } else {
//            if (mTvMessage.getVisibility() != View.VISIBLE
//                    && (mTvMessage.getText() == null || TextUtils.isEmpty(mTvMessage.getText().toString()))) {
//                //mTvMessage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.warning_icon, 0, 0, 0);
//                mTvMessage.setText(R.string.message_no_internet);
//                mTvMessage.setVisibility(View.VISIBLE);
//                //mTvMessage.setOnClickListener(this);
//            }
            showToast(R.string.message_no_internet);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mTvMessage) {
            String message = mTvMessage.getText().toString();
            if (!TextUtils.isEmpty(message) && message.equals(getString(R.string.message_no_internet_tap_to_refresh))) {
                NetworkStateMonitor networkStateMonitor = NetworkStateMonitor.getInstance(this);
                if (networkStateMonitor.isConnected()) {
                    mTvMessage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mTvMessage.setText("");
                    mTvMessage.setVisibility(View.GONE);
                    mTvMessage.setOnClickListener(null);

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
                    if (baseFragment != null) {
                        baseFragment.refreshData(null);
                    }
                }
            }
        }
    }

    public AppComponent getMainActivityComponent() {
        return AppApplication.getInstance(this).getAppComponent();
    }

    @Override
    public boolean isLoadingData() {
        return mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE;
    }

    @Override
    public void onFragmentInteraction(String action, Bundle data) {
        if (OnFragmentInteractionListener.ACTION_LOGOUT.equals(action)) {
            logout(true);
        } else if (OnFragmentInteractionListener.ACTION_USER_UPDATE.equals(action)) {
            // Since we don't destroy Main Activity, so we need to inject data again
            setupActivityComponent(data);
            resetMenu();
            checkMenu();
        } else if (OnFragmentInteractionListener.ACTION_HIDE_KEYBOARD.equals(action)) {
            hideKeyboard();
        } else if (OnFragmentInteractionListener.ACTION_CHANGE_KEYBOARD.equals(action)
                && data != null) {
            changeKeyboard(data);
        } else if (OnFragmentInteractionListener.ACTION_REGISTER_GLOBAL_LAYOUT_LISTENER.equals(action)
                && data != null) {
            registerGlobalLayoutListener(data);
        } else if (OnFragmentInteractionListener.ACTION_UNREGISTER_GLOBAL_LAYOUT_LISTENER.equals(action)
                && data != null) {
            unregisterGlobalLayoutListener(data);
        } else if (OnFragmentInteractionListener.ACTION_OPEN_PAGE.equals(action)
                && data != null) {
            openPage(data);
        } else if (OnFragmentInteractionListener.ACTION_MENU_CLICK.equals(action)) {
            menuClick();
        } else if (OnFragmentInteractionListener.ACTION_BACK_CLICK.equals(action)
                && data != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
            if (baseFragment != null && baseFragment.isVisible()) {
                Bundle bundle = baseFragment.beforeBackPress();
                if(bundle != null) {
                    data.putAll(bundle);
                }
            }
            backToPreviousPage(data);
        }
    }

    @Override
    public void onBackPressed() {
        Bundle data = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
        if (baseFragment != null && baseFragment.isVisible()) {
            data = baseFragment.beforeBackPress();
        }
        backToPreviousPage(data);
    }

    public void checkPageToOpen(Bundle data) {
        boolean isHandled = false;
        if (data != null) {
            int index = data.getInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, 0);
            Pages page = getPageIndex(index);
            if (page == Pages.Login) {
                isHandled = true;
                openPageFragment(new LoginFragment());
            } else if (page == Pages.LoginPin) {
                isHandled = true;
                openPageFragment(new LoginPinFragment());
            } else if (page == Pages.Register) {
                isHandled = true;
                openPageFragment(new RegisterFragment());
            }
        }
        if (!isHandled) {
            boolean registered = AppUtil.isRegistered(mContainerMap);
            if (registered) {
                openPageFragment(new LoginPinFragment());
            } else {
                openPageFragment(new RegisterFragment());
            }
        }
    }

    private void resetMenu() {
        mSideDrawer = null;
    }

    private void checkMenu() {
        boolean canOpenMenu = false;
        if (AppUtil.isLogged(mContainerMap)) {
            canOpenMenu = true;
        }
        if (canOpenMenu) {
            if (mSideDrawer == null) {
                mSideDrawer = new SimpleSideDrawer(this);
                AppConst.LoginType loginType = AppUtil.getLoginType(mContainerMap);
                if(loginType == AppConst.LoginType.Company) {
                    mSideDrawer.setLeftBehindContentView(R.layout.view_menu_company);
                } else if(loginType == AppConst.LoginType.Staff) {
                    mSideDrawer.setLeftBehindContentView(R.layout.view_menu_staff);
                } else {
                    mSideDrawer.setLeftBehindContentView(R.layout.view_menu);
                }
            }
            View view = mSideDrawer.getLeftBehindView();
            if (view instanceof ViewMenuLayout) {
                ((ViewMenuLayout) view).setListener(new ViewMenuLayoutListener() {
                    @Override
                    public void onFinishInflate() {

                    }

                    @Override
                    public void onItemClick(DrawerType drawerType) {
                        mSideDrawer.toggleLeftDrawer();
                        menuToPage(drawerType);
                    }
                });
            } else if (view instanceof ViewMenuCompanyLayout) {
                ((ViewMenuCompanyLayout) view).setListener(new ViewMenuLayoutListener() {
                    @Override
                    public void onFinishInflate() {

                    }

                    @Override
                    public void onItemClick(DrawerType drawerType) {
                        mSideDrawer.toggleLeftDrawer();
                        menuToPage(drawerType);
                    }
                });
            } else if (view instanceof ViewMenuStaffLayout) {
                ((ViewMenuStaffLayout) view).setListener(new ViewMenuLayoutListener() {
                    @Override
                    public void onFinishInflate() {

                    }

                    @Override
                    public void onItemClick(DrawerType drawerType) {
                        mSideDrawer.toggleLeftDrawer();
                        menuToPage(drawerType);
                    }
                });
            }
        }
    }

    private void logout(boolean confirm) {
        if (confirm) {
            if (mAlertPopup != null) {
                mAlertPopup.dismiss();
                mAlertPopup = null;
            }
            mAlertPopup = new AlertPopup(this);
            mAlertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    mAlertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertPopup.dismiss();
                            internalLogout();
                        }
                    });
                    mAlertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertPopup.dismiss();
                        }
                    });
                    mAlertPopup.setMessage(getString(R.string.logout_confirm));
                    mAlertPopup.setTitle(getString(R.string.app_name));
                }
            });
            mAlertPopup.show();
        } else {
            internalLogout();
        }
    }

    private void internalLogout() {
        mUserPreferences.logout();
        GcmPreferences gcmPreferences = GcmPreferences.getInstance(this);
        gcmPreferences.logout();
        // destroy all fragments in back stack
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager != null) {
            int count = fragmentManager.getBackStackEntryCount();
            while (count > 0) {
                fragmentManager.popBackStackImmediate();
                count = fragmentManager.getBackStackEntryCount();
            }
        }
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(AppConst.KEY_LOGOUT, true);
        startActivity(intent);
    }

    private void checkDoubleBackPress() {
        // Double press to exit
        if (mIsBackPress) {
            getHandler().removeCallbacks(mBackPressTask);
            finish();
        } else {
            mIsBackPress = true;
            showToast(R.string.back_press_alert);
            getHandler().postDelayed(mBackPressTask, 5000); // 5 second delay
        }
    }

    public Handler getHandler() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        return mHandler;
    }

    @Override
    public void displayProgress(boolean show) {
        if (show) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayMessage(boolean show, String message) {
        if (show) {
            mTvMessage.setVisibility(View.VISIBLE);
            mTvMessage.setText(message);
        } else {
            String oldMessage = mTvMessage.getText().toString();
            if (TextUtils.isEmpty(oldMessage) || !oldMessage.equals(getString(R.string.message_no_internet_tap_to_refresh))) {
                mTvMessage.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void displayMessage(boolean show, @StringRes int id) {
        displayMessage(show, getString(id));
    }

    @Override
    public void showError(Throwable t, @StringRes int id) {
        handleError(t, null, getString(id));
    }

    @Override
    public void showError(Throwable t, String message) {
       handleError(t, null, message);
    }

    @Override
    public void showError(BaseResponse response, @StringRes int id) {
        handleError(null, response, getString(id));
    }

    @Override
    public void showError(BaseResponse response, String message) {
        handleError(null, response, message);
    }

    private void handleError(Throwable t, BaseResponse baseResponse, String message) {
        String error = "";
        if(baseResponse != null && !TextUtils.isEmpty(baseResponse.getErrorCode())
                && (StringUtil.containsIgnoreCase(baseResponse.getErrorCode(), AppConst.ERROR_AUTH_TOKEN)
                    || StringUtil.containsIgnoreCase(baseResponse.getErrorCode(), AppConst.ERROR_OUT_OF_CREDIT))) {
            error = baseResponse.getErrorCode();
        } else if(baseResponse != null && !TextUtils.isEmpty(baseResponse.getError())) {
            error = baseResponse.getError();
        } else if(t instanceof HttpException) {
            HttpException httpException = ((HttpException) t);
            if (httpException.response() != null && httpException.response().errorBody() != null) {
                try {
                    error = httpException.response().errorBody().string();
                } catch (IOException e) {
                    mLogManager.log(e);
                }
            }
        }
        if (StringUtil.containsIgnoreCase(error, AppConst.ERROR_AUTH_TOKEN)
                || StringUtil.containsIgnoreCase(error, AppConst.ERROR_TOKEN_EXPIRED)
                || StringUtil.containsIgnoreCase(error, AppConst.ERROR_TOKEN_INVALID)
                || StringUtil.containsIgnoreCase(error, AppConst.ERROR_TOKEN_NOT_PROVIDED)
                || StringUtil.containsIgnoreCase(error, AppConst.ERROR_USER_NOT_FOUND)) {
            if (mAlertPopup != null) {
                mAlertPopup.dismiss();
                mAlertPopup = null;
            }
            mAlertPopup = new AlertPopup(this);
            mAlertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    mAlertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertPopup.dismiss();
                            logout(false);
                        }
                    });
                    mAlertPopup.setMessage(getString(R.string.error_user_invalid));
                    mAlertPopup.setTitle(getString(R.string.app_name));
                }
            });
            mAlertPopup.show();
        } else if (StringUtil.containsIgnoreCase(error, AppConst.ERROR_OUT_OF_CREDIT)) {
            if (mAlertPopup != null) {
                mAlertPopup.dismiss();
                mAlertPopup = null;
            }
            mAlertPopup = new AlertPopup(this);
            mAlertPopup.setListener(new AlertPopup.AlertPopupListener() {
                @Override
                public void onStart() {
                    mAlertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertPopup.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(OnFragmentInteractionListener.DATA_ADD_BACK_STACK_BOOLEAN_EXTRA, true);
                            bundle.putString(OnFragmentInteractionListener.DATA_PAGE_STRING_EXTRA, SmsCreditsFragment.TAG);
                            openPage(Pages.BuyCredits, bundle);
                        }
                    });
                    mAlertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertPopup.dismiss();
                        }
                    });
                    mAlertPopup.setMessage(getString(R.string.error_out_of_credit));
                    mAlertPopup.setTitle(getString(R.string.app_name));
                }
            });
            mAlertPopup.show();
        } else {
            if(TextUtils.isEmpty(error)) {
                error = message;
            }
            showMessage(error);
        }
    }

    private void menuClick() {
        if (mSideDrawer != null) {
            mSideDrawer.toggleLeftDrawer();
        }
        hideKeyboard();
    }

    private void menuToPage(DrawerType drawerType) {
        if (drawerType == DrawerType.Logout) {
            logout(true);
        } else {
            Pages page = getPageIndex(drawerType);
            openPage(page, null);
        }
    }

    private void openPage(Bundle data) {
        int index = data.getInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, 0);
        Pages page = getPageIndex(index);
        openPage(page, data);
    }

    private void openPage(Pages page, Bundle data) {
        switch (page) {
            case Login:
                openPageFragment(new LoginFragment(), data);
                break;
            case LoginPin:
                openPageFragment(new LoginPinFragment(), data);
                break;
            case Register:
                openPageFragment(new RegisterFragment(), data);
                break;
            case ForgotPassword:
                openPageFragment(new ForgotPasswordFragment(), data);
                break;
            case Dashboard:
                openPageFragment(new DashboardFragment(), data, false);
                break;
            case Quotes:
                openPageFragment(new QuotesFragment(), data, false);
                break;
            case QuotesDetail:
                openPageFragment(new QuotesDetailFragment(), data, true);
                break;
            case ProductDetail:
                openPageFragment(new ProductDetailFragment(), data, true);
                break;
            case QuotesCreate:
                openPageFragment(new QuotesCreateFragment(), data, true);
                break;
            case QuotesTemplates:
                openPageFragment(new QuotesTemplatesFragment(), data, true);
                break;
            case ProductCreate:
                openPageFragment(new ProductCreateFragment(), data, true);
                break;
            case AddPayment:
                openPageFragment(new AddPaymentFragment(), data, true);
                break;
            case Templates:
                openPageFragment(new TemplatesFragment(), data, false);
                break;
            case TemplatesCreate:
                openPageFragment(new TemplatesCreateFragment(), data, true);
                break;
            case UserProfile:
                openPageFragment(new UserProfileFragment(), data, false);
                break;
            case ChangePassword:
                openPageFragment(new ChangePasswordFragment(), data, false);
                break;
            case CompanyInfo:
                openPageFragment(new CompanyInfoFragment(), data, false);
                break;
            case UserList:
                openPageFragment(new UsersFragment(), data, false);
                break;
            case AddUser: {
                boolean addBackStack = false;
                if (data != null && data.getBoolean(OnFragmentInteractionListener.DATA_ADD_BACK_STACK_BOOLEAN_EXTRA)) {
                    addBackStack = true;
                }
                openPageFragment(new AddUserFragment(), data, addBackStack);
            }
            break;
            case BuyCredits: {
                boolean addBackStack = false;
                if (data != null && data.getBoolean(OnFragmentInteractionListener.DATA_ADD_BACK_STACK_BOOLEAN_EXTRA)) {
                    addBackStack = true;
                }
                openPageFragment(new BuyCreditsFragment(), data, addBackStack);
            }
            break;
            case SmsCredits:
                openPageFragment(new SmsCreditsFragment(), data, true);
                break;
        }
    }

    private Pages getPageIndex(int index) {
        for (Pages page : Pages.values()) {
            if (index == page.ordinal()) {
                return page;
            }
        }
        return Pages.Empty;
    }

    private Pages getPageIndex(DrawerType drawerType) {
        for (Pages page : Pages.values()) {
            if (drawerType.getPageIndex() == page.ordinal()) {
                return page;
            }
        }
        return Pages.Empty;
    }

    private boolean isDrawerTag(Bundle data, String entry) {
        if(data != null && data.containsKey(OnFragmentInteractionListener.DATA_IS_DRAWER_BOOLEAN_EXTRA)) {
            return data.getBoolean(OnFragmentInteractionListener.DATA_IS_DRAWER_BOOLEAN_EXTRA);
        }
        for (DrawerType drawerType : DrawerType.values()) {
            if (drawerType.getFragmentTag().equals(entry)) {
                return true;
            }
        }
        return false;
    }

    public void hideKeyboard() {
        ViewUtil.closeKeyboard(mProgressBar);
    }

    public void changeKeyboard(Bundle data) {
        int mode = data.getInt(OnFragmentInteractionListener.DATA_MODE_INT_EXTRA);
        getWindow().setSoftInputMode(mode);
    }

    private void backToPreviousPage(Bundle data) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        mLogManager.log("BackStackCount " + count);
        if (count > 0) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(count - 1);
            BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentByTag(backStackEntry.getName());
            if (RegisterFragment.TAG.equals(backStackEntry.getName())
                    || LoginFragment.TAG.equals(backStackEntry.getName())
                    || ForgotPasswordFragment.TAG.equals(backStackEntry.getName())
                    || isDrawerTag(data, backStackEntry.getName())) {
                if (baseFragment == null || !baseFragment.handleBackPress()) {
                    checkDoubleBackPress();
                }
            } else {
                if (baseFragment == null || !baseFragment.handleBackPress()) {
                    if (baseFragment != null) {
                        baseFragment.onFragmentPause(data);
                    }
                    int pageId = data != null ? data.getInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, -1) : -1;
                    boolean backToPage = data != null && data.getBoolean(OnFragmentInteractionListener.DATA_BACK_TO_PAGE_BOOLEAN_EXTRA, false);
                    if (backToPage) {
                        String pageName = data.getString(OnFragmentInteractionListener.DATA_PAGE_STRING_EXTRA, "");
                        if (fragmentManager.findFragmentByTag(pageName) == null) {
                            openPage(data);
                        } else {
                            fragmentManager.popBackStackImmediate(pageName, 0);
                        }
                    } else {
                        fragmentManager.popBackStackImmediate();
                    }
                    count = fragmentManager.getBackStackEntryCount();
                    if (count > 0) {
                        backStackEntry = fragmentManager.getBackStackEntryAt(count - 1);
                        baseFragment = (BaseFragment) fragmentManager.findFragmentByTag(backStackEntry.getName());
                        if (baseFragment != null) {
                            baseFragment.onFragmentResume(data);
                        }
                    } else {
                        baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
                        if (baseFragment != null && baseFragment.getFragmentId() != pageId) {
                            baseFragment.onFragmentResume(data);
                        }
                    }
                }
            }
        } else {
            checkDoubleBackPress();
        }
        ViewUtil.closeKeyboard(mProgressBar);
        displayProgress(false);
        displayMessage(false, null);
    }

    private void openPageFragment(BaseFragment baseFragment) {
        openPageFragment(baseFragment, true);
    }

    private void openPageFragment(BaseFragment baseFragment, Bundle data) {
        openPageFragment(baseFragment, data, true);
    }

    private void openPageFragment(BaseFragment baseFragment, boolean addToBackStack) {
        openPageFragment(baseFragment, null, addToBackStack);
    }

    private void openPageFragment(BaseFragment baseFragment, Bundle data, boolean addToBackStack) {
        if (data != null) {
            baseFragment.setArguments(data);
        }
        FragmentManager fragmentManager = getSupportFragmentManager();

        BaseFragment oldFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
        if (oldFragment != null) {
            if (oldFragment.getFragmentId() == baseFragment.getFragmentId()) {
                // The same fragment, show it
                fragmentManager.popBackStackImmediate(oldFragment.getFragmentName(), 0);
                return;
            } else {
                oldFragment.onFragmentPause(null);
                // Remove login/register/forgotPassword screen since we do not need them anymore
                if ((oldFragment.getFragmentId() == Login.ordinal() || oldFragment.getFragmentId() == LoginPin.ordinal()
                        || oldFragment.getFragmentId() == Register.ordinal()
                        || oldFragment.getFragmentId() == Pages.ForgotPassword.ordinal())
                        && isDrawerTag(data, baseFragment.getFragmentName())) {
                    int count = fragmentManager.getBackStackEntryCount();
                    for (int i = 0; i < count; ++i) {
                        fragmentManager.popBackStackImmediate();
                    }
                }
            }
        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (baseFragment.isPopupStyle()) {
            fragmentTransaction.setCustomAnimations(R.anim.fragment_stay_enter, R.anim.fragment_stay_exit_top, R.anim.fragment_stay_enter, R.anim.fragment_stay_exit_top);
        } else {
            fragmentTransaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_enter_right, R.anim.fragment_exit_right);
        }

        if (addToBackStack) {
            fragmentTransaction.add(R.id.container, baseFragment, baseFragment.getFragmentName())
                    .addToBackStack(baseFragment.getFragmentName()).commit();
        } else {
            fragmentTransaction.replace(R.id.container, baseFragment, baseFragment.getFragmentName())
                    .commit();
        }
        ViewUtil.closeKeyboard(mProgressBar);
        displayProgress(false);
        displayMessage(false, null);

        Tracker tracker = AppApplication.getInstance(this).getDefaultTracker();
        tracker.setScreenName(baseFragment.getFragmentName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void checkNetwork(NetworkStateMonitor monitor) {
        if (monitor.isConnected()) {
            if (!mIsNetworkEnable) {
                dismissDialog();
            }
            mIsNetworkEnable = true;
        } else {
            mIsNetworkEnable = false;
            //showMessage(R.string.message_no_internet);
        }
        onNetworkStateChanged(monitor);
    }

    private void setUpGlobalLayoutListener() {
        mVRootContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(mGlobalLayoutRegisters != null && mGlobalLayoutRegisters.size() > 0) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    BaseFragment baseFragment = (BaseFragment) fragmentManager.findFragmentById(R.id.container);
                    if (baseFragment != null && baseFragment.isVisible()
                            && mGlobalLayoutRegisters.contains(baseFragment.getFragmentId())) {
                        baseFragment.onGlobalLayout();
                    }
                }
            }
        });
    }

    private void registerGlobalLayoutListener(Bundle data) {
        int index = data.getInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, 0);
        if(mGlobalLayoutRegisters == null) {
            mGlobalLayoutRegisters = new HashSet<>();
        }
        mGlobalLayoutRegisters.add(index);
        mLogManager.log("registerGlobalLayoutListener " + index);
    }

    private void unregisterGlobalLayoutListener(Bundle data) {
        int index = data.getInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, 0);
        if(mGlobalLayoutRegisters != null && mGlobalLayoutRegisters.size() > 0) {
            mGlobalLayoutRegisters.remove(index);
            mLogManager.log("unregisterGlobalLayoutListener " + index);
        }
    }

    private void startGcmService() {
        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.getApplicationContext());
        return resultCode == ConnectionResult.SUCCESS;
    }

    private void checkNotification(Bundle data) {
        if (data != null) {
            if (data.containsKey(GcmPreferences.DATA_QUOTE_ID_STRING_MESSAGE)) {
                String quoteId = data.getString(GcmPreferences.DATA_QUOTE_ID_STRING_MESSAGE);
                String customerName = data.getString(GcmPreferences.DATA_CUSTOMER_NAME_STRING_MESSAGE);
                String customerMobile = data.getString(GcmPreferences.DATA_CUSTOMER_MOBILE_STRING_MESSAGE);
                mContainerMap.setInt(AppConst.KEY_QUOTE_ID, DataUtil.getInteger(quoteId));
                mContainerMap.setString(AppConst.KEY_CUSTOMER_MOBILE, customerMobile);
                mContainerMap.setString(AppConst.KEY_CUSTOMER_NAME, customerName);
                openPage(Pages.Quotes, data);
                getHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        openPage(Pages.QuotesDetail, getIntent().getExtras());
                    }
                }, 1000);
            } else {
                checkPageToOpen(data);
            }
        } else {
            checkPageToOpen(data);
        }
    }
}
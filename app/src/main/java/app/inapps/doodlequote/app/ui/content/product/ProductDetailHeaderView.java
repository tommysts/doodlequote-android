package app.inapps.doodlequote.app.ui.content.product;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultLinearLayout;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProductDetailHeaderView extends DefaultLinearLayout {
    @BindView(R.id.ivLike) DefaultImageView mIvLike;
    @BindView(R.id.ivImage) DefaultImageView mIvImage;
    @BindView(R.id.tvName) DefaultTextView mTvName;
    @BindView(R.id.tvDescription) DefaultTextView mTvDescription;
    @BindView(R.id.tvStatus) DefaultTextView mTvStatus;
    @BindView(R.id.tvPrice) DefaultTextView mTvPrice;

    private Unbinder mUnbinder;

    public ProductDetailHeaderView(Context context) {
        super(context);
    }

    public ProductDetailHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProductDetailHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ProductDetailHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void setup() {
        super.setup();
        mUnbinder = ButterKnife.bind(this, this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public ImageView[] getImageViews() {
        return new ImageView[]{mIvImage};
    }
}

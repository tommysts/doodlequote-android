package app.inapps.doodlequote.app.util.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GcmListenerService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.ui.SplashActivity;
import me.leolin.shortcutbadger.ShortcutBadger;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private static final int NOTIFICATION_ID = 1;
    private MediaPlayer mMediaPlayer;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */
        sendNotification(data);

    }
    // [END receive_message]

    private void sendNotification(Bundle data) {
        if(BuildConfig.DEBUG) {
            Log.d(TAG, "GCM Message: " + data.toString());
        }

        GcmPreferences preferences = GcmPreferences.getInstance(this);

        String quoteId = data.getString("quote_id");
        String customerName = data.getString("customer_name");
        String customerMobile = data.getString("customer_mobile");
        String message = data.getString("message");
        String title = data.getString("title");

        if(TextUtils.isEmpty(message) || "message".equals(message)){
            message = title;
        }

        boolean showNotification = true;
        String time = getCurrentTime();
        int soundId = -1;

        if(!TextUtils.isEmpty(quoteId)) {
            data.putString(GcmPreferences.DATA_QUOTE_ID_STRING_MESSAGE, quoteId);
            data.putString(GcmPreferences.DATA_CUSTOMER_NAME_STRING_MESSAGE, customerName);
            data.putString(GcmPreferences.DATA_CUSTOMER_MOBILE_STRING_MESSAGE, customerMobile);
        }

        if(showNotification) {
            // Show count in app icon
            int count = preferences.getNotificationCount() + 1;
            preferences.setNotificationCount(count);
            ShortcutBadger.applyCount(getApplicationContext(), count);

            Intent intent = new Intent(this, SplashActivity.class);
            intent.putExtras(data);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(false)
                    .setOngoing(false)
                    .setLights(0xff00ff00, 300, 100)
                    .setContentIntent(pendingIntent);

            // Sound
            if (soundId == -1) {
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);
            } else {
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
                AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
                switch (am.getRingerMode()) {
                    case AudioManager.RINGER_MODE_SILENT:
                    case AudioManager.RINGER_MODE_VIBRATE:
                        if (mMediaPlayer == null) {
                            mMediaPlayer = new MediaPlayer();
                        }
                        AssetFileDescriptor afd = getResources().openRawResourceFd(soundId);
                        if (afd != null) {
                            try {
                                if (mMediaPlayer.isPlaying()) {
                                    mMediaPlayer.stop();
                                }
                                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                                afd.close();
                                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                                //mMediaPlayer.setLooping(false);
                                mMediaPlayer.prepare();
                                mMediaPlayer.start();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case AudioManager.RINGER_MODE_NORMAL:
                        notificationBuilder.setSound(Uri.parse("android.resource://"
                                                        + getPackageName() + "/" + soundId));
                        break;
                }

            }

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder.setPriority(Notification.PRIORITY_HIGH);
            }
            RemoteViews remoteViews = getNotificationRemoteViews(message, time);
            if (remoteViews != null) {
                notificationBuilder.setContent(remoteViews);
            }
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }

        notifyUI(data);
    }

    private void forceLocale(String locale) {
        Locale myLocale = new Locale(locale);
        Resources res = getResources();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, res.getDisplayMetrics());
    }

    private RemoteViews getNotificationRemoteViews(String message, String time){
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_view);

        remoteViews.setTextViewText(R.id.tvTitle, getString(R.string.app_name));
        remoteViews.setTextViewText(R.id.tvMessage, TextUtils.isEmpty(message) ? "" : message);
        remoteViews.setTextViewText(R.id.tvTime, time);

        return remoteViews;
    }

    private String getCurrentTime(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm dd/MM/yyyy", Locale.getDefault());
        return sdf.format(calendar.getTime());
    }

    private void notifyUI(Bundle data){
        // Notify UI that there is new message
        Intent receiveMessage = new Intent(GcmPreferences.RECEIVE_MESSAGE_ACTION);
        receiveMessage.putExtras(data);
        LocalBroadcastManager.getInstance(this).sendBroadcast(receiveMessage);
    }

    protected void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }
}

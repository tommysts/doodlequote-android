package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class ListQuoteResponse extends BaseResponse {
    @SerializedName("quotes")
    @Expose
    private List<QuoteResponse> quotes;

    public List<QuoteResponse> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<QuoteResponse> quotes) {
        this.quotes = quotes;
    }
}

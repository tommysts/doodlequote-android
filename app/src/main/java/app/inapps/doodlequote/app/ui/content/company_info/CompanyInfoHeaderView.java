package app.inapps.doodlequote.app.ui.content.company_info;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.CircleImageView;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultLinearLayout;
import app.inapps.doodlequote.core.widget.DefaultViewListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CompanyInfoHeaderView extends DefaultLinearLayout {
    @BindView(R.id.ivAvatar) CircleImageView mIvAvatar;
    @BindView(R.id.ivBrowse) AnimateImageView mIvBrowse;
    @BindView(R.id.edtCompanyName) DefaultEditText mEdtCompanyName;
    @BindView(R.id.edtWebsiteUrl) DefaultEditText mEdtWebsiteUrl;
    @BindView(R.id.edtDescription) DefaultEditText mEdtDescription;
    @BindView(R.id.rlAddPayment) RelativeLayout mRlAddPayment;
    @BindView(R.id.llBrowse) ViewGroup mLlBrowse;
    @BindView(R.id.rlPayment) ViewGroup mRlPayment;

    interface CompanyInfoHeaderViewListener extends DefaultViewListener {
        void onAddPaymentClick();
        void onBrowseClick();
    }

    private Unbinder mUnbinder;

    public CompanyInfoHeaderView(Context context) {
        super(context);
    }

    public CompanyInfoHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CompanyInfoHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public CompanyInfoHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void setup() {
        super.setup();
        mUnbinder = ButterKnife.bind(this, this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public ImageView[] getImageViews() {
        return new ImageView[] {mIvAvatar, mIvBrowse};
    }

    public EditText[] getEditTexts() {
        return new EditText[]{mEdtCompanyName, mEdtWebsiteUrl, mEdtDescription};
    }

    public void setupButtons() {
        mIvBrowse.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if(mListener instanceof CompanyInfoHeaderViewListener) {
                    ((CompanyInfoHeaderViewListener) mListener).onBrowseClick();
                }
            }
        });
        mRlAddPayment.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if(mListener instanceof CompanyInfoHeaderViewListener) {
                    ((CompanyInfoHeaderViewListener) mListener).onAddPaymentClick();
                }
            }
        });
    }

}

package app.inapps.doodlequote.app.ui.content;

import app.inapps.doodlequote.app.ui.MainActivity;

public interface ViewMenuLayoutListener {
    void onFinishInflate();
    void onItemClick(MainActivity.DrawerType drawerType);
}

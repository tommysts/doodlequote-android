package app.inapps.doodlequote.app.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.Window;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BrowsePopup extends Dialog {
    @BindView(R.id.ivCoverImage) RecyclingImageView mIvCoverImage;
    @BindView(R.id.ivTakePhoto) AnimateImageView mIvTakePhoto;
    @BindView(R.id.ivOpenPhotos) AnimateImageView mIvOpenPhotos;
    @BindView(R.id.ivTakeVideo) AnimateImageView mIvTakeVideo;
    @BindView(R.id.ivOpenVideos) AnimateImageView mIvOpenVideos;
    @BindView(R.id.ivTakeRecord) AnimateImageView mIvTakeRecord;
    @BindView(R.id.ivOpenRecords) AnimateImageView mIvOpenRecords;

    protected Unbinder mUnbinder;
    protected BrowsePopupListener mListener;

    public interface BrowsePopupListener {
        void onStart();
        void onTakePhotoClick(Dialog dialog);
        void onOpenPhotosClick(Dialog dialog);
        void onTakeVideoClick(Dialog dialog);
        void onOpenVideosClick(Dialog dialog);
        void onTakeRecordClick(Dialog dialog);
        void onOpenRecordsClick(Dialog dialog);
    }

    public BrowsePopup(Context context) {
        super(context);
        setup();
    }

    public BrowsePopup(Context context, int themeResId) {
        super(context, themeResId);
        setup();
    }

    protected BrowsePopup(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setup();
    }

    protected void setup() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(getLayoutResID());
        setCancelable(true);

        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        mUnbinder = ButterKnife.bind(this);

        mIvTakePhoto.setOnClickListener(mDebouncedOnClickListener);
        mIvOpenPhotos.setOnClickListener(mDebouncedOnClickListener);
        mIvTakeVideo.setOnClickListener(mDebouncedOnClickListener);
        mIvOpenVideos.setOnClickListener(mDebouncedOnClickListener);
        mIvTakeRecord.setOnClickListener(mDebouncedOnClickListener);
        mIvOpenRecords.setOnClickListener(mDebouncedOnClickListener);
    }

    private DebouncedOnClickListener mDebouncedOnClickListener = new DebouncedOnClickListener() {
        @Override
        public void onDebouncedClick(View v) {
            if (mListener != null) {
                if(v == mIvTakePhoto) {
                    mListener.onTakePhotoClick(BrowsePopup.this);
                } else if(v == mIvOpenPhotos) {
                    mListener.onOpenPhotosClick(BrowsePopup.this);
                } else if(v == mIvTakeVideo) {
                    mListener.onTakeVideoClick(BrowsePopup.this);
                } else if(v == mIvOpenVideos) {
                    mListener.onOpenVideosClick(BrowsePopup.this);
                }
            }
        }
    };

    @LayoutRes
    protected int getLayoutResID() {
        return R.layout.popup_browse_layout;
    }

    public void setListener(BrowsePopupListener listener) {
        mListener = listener;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mUnbinder == null) {
            mUnbinder = ButterKnife.bind(this);
        }
        if (mListener != null) {
            mListener.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }
}

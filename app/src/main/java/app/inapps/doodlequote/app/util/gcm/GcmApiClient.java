package app.inapps.doodlequote.app.util.gcm;

import app.inapps.doodlequote.app.api.response.UpdateDeviceTokenResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

public interface GcmApiClient {
    @FormUrlEncoded
    @POST("/v1/device/setToken")
    Observable<UpdateDeviceTokenResponse> updateDeviceToken(@Header("Authorization: Bearer") String token,
                                                                   @Field("token") String deviceToken,
                                                                   @Field("type") String deviceType);
}

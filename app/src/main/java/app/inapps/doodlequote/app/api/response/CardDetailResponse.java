package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class CardDetailResponse extends BaseResponse {
    @SerializedName("Number")
    @Expose
    public String number;

    @SerializedName("Name")
    @Expose
    public String name;

    @SerializedName("ExpiryMonth")
    @Expose
    public String expiryMonth;

    @SerializedName("ExpiryYear")
    @Expose
    public String expiryYear;

    @SerializedName("StartMonth")
    @Expose
    public String startMonth;

    @SerializedName("StartYear")
    @Expose
    public String startYear;

    @SerializedName("IssueNumber")
    @Expose
    public String issueNumber;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }
}

package app.inapps.doodlequote.app.ui.content.product;

import android.os.Parcel;

public class UploadImageItem extends UploadItem {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    public UploadImageItem() {
    }

    protected UploadImageItem(Parcel in) {
        super(in);
    }

    public static final Creator<UploadImageItem> CREATOR = new Creator<UploadImageItem>() {
        @Override
        public UploadImageItem createFromParcel(Parcel source) {
            return new UploadImageItem(source);
        }

        @Override
        public UploadImageItem[] newArray(int size) {
            return new UploadImageItem[size];
        }
    };
}

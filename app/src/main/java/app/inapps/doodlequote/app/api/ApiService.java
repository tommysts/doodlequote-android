package app.inapps.doodlequote.app.api;

import java.util.Map;

import app.inapps.doodlequote.app.api.response.CreateCardResponse;
import app.inapps.doodlequote.app.api.response.CreateQuoteResponse;
import app.inapps.doodlequote.app.api.response.CreateTemplateResponse;
import app.inapps.doodlequote.app.api.response.CreateUserResponse;
import app.inapps.doodlequote.app.api.response.CreditResponse;
import app.inapps.doodlequote.app.api.response.DashboardResponse;
import app.inapps.doodlequote.app.api.response.DeleteCardResponse;
import app.inapps.doodlequote.app.api.response.DeleteTemplateResponse;
import app.inapps.doodlequote.app.api.response.ForgotPasswordResponse;
import app.inapps.doodlequote.app.api.response.GetProductResponse;
import app.inapps.doodlequote.app.api.response.GetQuoteResponse;
import app.inapps.doodlequote.app.api.response.GetUserProfileResponse;
import app.inapps.doodlequote.app.api.response.ListCardResponse;
import app.inapps.doodlequote.app.api.response.ListPackageResponse;
import app.inapps.doodlequote.app.api.response.ListQuoteResponse;
import app.inapps.doodlequote.app.api.response.ListTemplateResponse;
import app.inapps.doodlequote.app.api.response.ListUserResponse;
import app.inapps.doodlequote.app.api.response.LoginResponse;
import app.inapps.doodlequote.app.api.response.RegisterResponse;
import app.inapps.doodlequote.app.api.response.ResetPasswordResponse;
import app.inapps.doodlequote.app.api.response.SubmitOrderResponse;
import app.inapps.doodlequote.app.api.response.SubmitQuoteResponse;
import app.inapps.doodlequote.app.api.response.UpdateDeviceTokenResponse;
import app.inapps.doodlequote.app.api.response.UpdateUserProfileResponse;
import app.inapps.doodlequote.app.api.response.UploadResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

public interface ApiService {
    @FormUrlEncoded
    @POST("/v1/auth/login")
    Observable<LoginResponse> login(@Field("email") String email,
                                    @Field("password") String password);

    @FormUrlEncoded
    @POST("/v1/auth/register")
    Observable<RegisterResponse> register(@Field("email") String email, @Field("name") String name,
                                    @Field("password") String password, @Field("password_confirmation")
                                    String passwordConfirmation, @Field("mobile") String mobile,
                                          @Field("type") String type);

    @FormUrlEncoded
    @POST("/v1/password/forgot")
    Observable<ForgotPasswordResponse> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("/v1/password/reset")
    Observable<ResetPasswordResponse> resetPassword(@Field("email") String email, @Field("code") String code,
                                                    @Field("password") String password, @Field("password_confirmation")
                                                            String passwordConfirmation);

    @GET("/v1/user/dashboard")
    Observable<DashboardResponse> getDashboard(@Header("Authorization: Bearer") String token);


    @GET("/v1/user/listQuote")
    Observable<ListQuoteResponse> listQuote(@Header("Authorization: Bearer") String token);

    @GET("/v1/user/profile")
    Observable<GetUserProfileResponse> userProfile(@Header("Authorization: Bearer") String token);


    @GET("/v1/user/listTemplate")
    Observable<ListTemplateResponse> listTemplate(@Header("Authorization: Bearer") String token);

    @GET("/v1/user/listPackage")
    Observable<ListPackageResponse> listPackage(@Header("Authorization: Bearer") String token);

    @GET("/v1/user/listCards")
    Observable<ListCardResponse> listCard(@Header("Authorization: Bearer") String token);

    @GET("/v1/company/listUser")
    Observable<ListUserResponse> listUser(@Header("Authorization: Bearer") String token);

    @GET("/v1/user/credit")
    Observable<CreditResponse> creditInfo(@Header("Authorization: Bearer") String token);

    @FormUrlEncoded
    @POST("/v1/user/searchQuote")
    Observable<ListQuoteResponse> searchQuote(@Header("Authorization: Bearer") String token, @Field("search") String search);

    @FormUrlEncoded
    @POST("/v1/company/searchUser")
    Observable<ListUserResponse> searchUser(@Header("Authorization: Bearer") String token, @Field("search") String search);

    @FormUrlEncoded
    @POST("/v1/user/createQuote")
    Observable<CreateQuoteResponse> createQuote(@Header("Authorization: Bearer") String token,
                                                @Field("template_id") String templateId,
                                                @Field("customer_name") String customerName,
                                                @Field("customer_mobile") String customerMobile,
                                                @FieldMap(encoded = true) Map<String, String> products);
    @FormUrlEncoded
    @POST("/v1/user/submitQuote")
    Observable<SubmitQuoteResponse> submitQuote(@Header("Authorization: Bearer") String token, @Field("quote_id") String quoteId,
                                                @Field("customer_mobile") String mobile);

    @FormUrlEncoded
    @POST("/v1/user/createTemplate")
    Observable<CreateTemplateResponse> createTemplate(@Header("Authorization: Bearer") String token, @Field("body") String body);

    @FormUrlEncoded
    @POST("/v1/user/deleteTemplate")
    Observable<DeleteTemplateResponse> deleteTemplate(@Header("Authorization: Bearer") String token, @Field("template_id") String templateId);

    @Multipart
    @POST("/v1/user/uploadImages")
    Call<UploadResponse> uploadImages(@Header("Authorization: Bearer") String token,
                                      @Part("product_id") RequestBody productId,
                                      @Part MultipartBody.Part[] images);

    @FormUrlEncoded
    @POST("/v1/user/getQuote")
    Observable<GetQuoteResponse> getQuote(@Header("Authorization: Bearer") String token, @Field("quote_id") String quoteId);

    @FormUrlEncoded
    @POST("/v1/user/getProduct")
    Observable<GetProductResponse> getProduct(@Header("Authorization: Bearer") String token, @Field("product_id") String productId);

    @FormUrlEncoded
    @POST("/v1/user/profile")
    Observable<UpdateUserProfileResponse> updateUserProfile(@Header("Authorization: Bearer") String token, @FieldMap(encoded = true) Map<String, String> fields);

    @FormUrlEncoded
    @POST("/v1/user/profile")
    Observable<UpdateUserProfileResponse> updateUserCompanyProfile(@Header("Authorization: Bearer") String token, @Field("company_name") String companyName
            , @Field("website") String website, @Field("company_description") String companyDescription);

    @Multipart
    @POST("/v1/user/profile")
    Observable<UpdateUserProfileResponse> uploadUserAvatar(@Header("Authorization: Bearer") String token,
                                      @Part MultipartBody.Part image);

    @Multipart
    @POST("/v1/user/profile")
    Observable<UpdateUserProfileResponse> uploadUserAvatar(@Header("Authorization: Bearer") String token, @Part("user_id") RequestBody userId,
                                      @Part MultipartBody.Part image);

    @Multipart
    @POST("/v1/company/createUser")
    Observable<CreateUserResponse> createCompanyUser(@Header("Authorization: Bearer") String token, @Part("first_name") RequestBody firstName,
                                               @Part("last_name") RequestBody lastName, @Part("mobile") RequestBody mobile,
                                               @Part("email") RequestBody email, @Part("title") RequestBody title, @Part("is_disabled") RequestBody isDisabled);
    @Multipart
    @POST("/v1/company/createUser")
    Observable<CreateUserResponse> createCompanyUser(@Header("Authorization: Bearer") String token, @Part("first_name") RequestBody firstName,
                                               @Part("last_name") RequestBody lastName, @Part("mobile") RequestBody mobile,
                                               @Part("email") RequestBody email, @Part("title") RequestBody title,
                                               @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("/v1/user/profile")
    Observable<UpdateUserProfileResponse> updateUserPassword(@Header("Authorization: Bearer") String token, @Field("old_password") String oldPassword, @Field("password") String password,
                                                             @Field("password_confirmation") String passwordConfirmation);

    @FormUrlEncoded
    @POST("/v1/user/submitOrder")
    Observable<SubmitOrderResponse> submitOrder(@Header("Authorization: Bearer") String token, @Field("package_id") String packageId, @Field("card_id") String cardId,
                                                @Field("card_holder") String cardHolder,  @Field("card_number") String cardNumber,  @Field("card_month") String cardMonth,
                                                @Field("card_year") String cardYear, @Field("card_cvn") String cardCvn);

    @FormUrlEncoded
    @POST("/v1/user/submitOrder")
    Observable<SubmitOrderResponse> submitOrder(@Header("Authorization: Bearer") String token, @Field("card_id") String cardId, @Field("package_id") String packageId);

    @FormUrlEncoded
    @POST("/v1/user/submitOrder")
    Observable<SubmitOrderResponse> submitOrderNoCard(@Header("Authorization: Bearer") String token, @Field("card_token") String cardToken, @Field("package_id") String packageId);

    @FormUrlEncoded
    @POST("/v1/user/createCard")
    Observable<CreateCardResponse> createCard(@Header("Authorization: Bearer") String token, @Field("card_token") String cardToken);

    @FormUrlEncoded
    @POST("/v1/user/deleteCard")
    Observable<DeleteCardResponse> deleteCard(@Header("Authorization: Bearer") String token, @Field("card_id") String cardId);

    @FormUrlEncoded
    @POST("/v1/device/setToken")
    Observable<UpdateDeviceTokenResponse> updateDeviceToken(@Header("Authorization: Bearer") String token,
                                                            @Field("token") String deviceToken,
                                                            @Field("type") String deviceType);

    @Multipart
    @POST("/v1/test/upload")
    Observable<UploadResponse> uploadVideo(@Header("Authorization: Bearer") String token, @Part MultipartBody.Part file);
}

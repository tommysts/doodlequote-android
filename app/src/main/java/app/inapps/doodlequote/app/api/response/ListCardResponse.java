package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class ListCardResponse extends BaseResponse {
    @SerializedName("cards")
    @Expose
    private List<CardResponse> cards;

    public List<CardResponse> getCards() {
        return cards;
    }

    public void setCards(List<CardResponse> cards) {
        this.cards = cards;
    }
}

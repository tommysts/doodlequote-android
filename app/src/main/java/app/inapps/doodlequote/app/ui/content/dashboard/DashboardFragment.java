package app.inapps.doodlequote.app.ui.content.dashboard;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.DashboardResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class DashboardFragment extends BaseFragment {
    public static final String TAG = DashboardFragment.class.getSimpleName();

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.txtMessageSent) DefaultTextView mTxtMessageSent;
    @BindView(R.id.txtCallBack) DefaultTextView mTxtCallBack;
    @BindView(R.id.txtAccepted) DefaultTextView mTxtAccepted;
    @BindView(R.id.txtRejected) DefaultTextView mTxtRejected;
    @BindView(R.id.txtCredits) DefaultTextView mTxtCredits;
    @BindView(R.id.txtUsed) DefaultTextView mTxtUsed;
    @BindView(R.id.txtTemplates) DefaultTextView mTxtTemplates;

    @BindView(R.id.llCredits) ViewGroup mLlCredits;
    @BindView(R.id.txtCompanyUsedLabel) DefaultTextView mTxtCompanyUsedLabel;
    @BindView(R.id.txtCompanyUsed) DefaultTextView mTxtCompanyUsed;
    @BindView(R.id.llSmsUsed) LinearLayout mLlSmsUsed;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);
        mLlCredits.setOnClickListener(this);

        AppConst.LoginType loginType = AppUtil.getLoginType(mContainerMap);
        if(loginType == AppConst.LoginType.Company) {
            mLlSmsUsed.setVisibility(View.VISIBLE);
            mTxtCompanyUsedLabel.setText(R.string.company_used);
        } else {
            mLlSmsUsed.setVisibility(View.GONE);
            mTxtCompanyUsedLabel.setText(R.string.used);
        }

        loadData();
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
        loadData();
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.Dashboard.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if (v == mRlNavHome) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            menuClick();
        } else if (v == mLlCredits) {
            showCreditInfo();
        }
    }

    private void loadData() {
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        if (!TextUtils.isEmpty(token)) {
            displayProgress(true);
            Subscription subscription = mApiManager.getDashboard(token).subscribe(new SimpleObserver<DashboardResponse>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    displayProgress(false);
                    showError(t, R.string.error_loading_data);
                }

                @Override
                public void onNext(DashboardResponse dashboardResponse) {
                    super.onNext(dashboardResponse);
                    displayProgress(false);
                    if (dashboardResponse.isSuccess()) {
                        mTxtMessageSent.setText(String.valueOf(dashboardResponse.getMessageCount()));
                        mTxtCallBack.setText(String.valueOf(dashboardResponse.getCallbackCount()));
                        mTxtAccepted.setText(String.valueOf(dashboardResponse.getAcceptedCount()));
                        mTxtRejected.setText(String.valueOf(dashboardResponse.getRejectedCount()));
                        mTxtCredits.setText(String.valueOf(dashboardResponse.getCredit()));
                        mTxtTemplates.setText(String.valueOf(dashboardResponse.getTemplateCount()));
                        AppConst.LoginType loginType = AppUtil.getLoginType(mContainerMap);
                        if(loginType == AppConst.LoginType.Company) {
                            mTxtCompanyUsed.setText(String.valueOf(dashboardResponse.getCompanyUsed()));
                            mTxtUsed.setText(String.valueOf(dashboardResponse.getUsed()));
                        } else {
                            mTxtCompanyUsed.setText(String.valueOf(dashboardResponse.getUsed()));
                        }
                    } else {
                        showError(dashboardResponse, R.string.error_loading_data);
                    }
                }
            });
            addSubscription(subscription);
        }
    }

    private void showCreditInfo() {
        openPage(MainActivity.Pages.SmsCredits);
    }
}

package app.inapps.doodlequote.app.ui.content.quotes;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.GetQuoteResponse;
import app.inapps.doodlequote.app.api.response.QuoteProductResponse;
import app.inapps.doodlequote.app.api.response.QuoteResponse;
import app.inapps.doodlequote.app.api.response.SubmitQuoteResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.ui.content.product.ProductItem;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultHeaderGridView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class QuotesDetailFragment extends BaseFragment {
    public static final String TAG = QuotesDetailFragment.class.getSimpleName();

    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.gvItems) DefaultHeaderGridView mGvItems;
    @BindView(R.id.ivSubmit) AnimateImageView mIvSubmit;
    @BindView(R.id.rlSubmit) RelativeLayout mRlSubmit;

    private QuotesDetailHeaderView mHeaderView;
    private QuotesDetailAdapter mAdapter;
    private boolean mIsRefreshData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quotes_detail, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavBack.setOnClickListener(this);
        mRlSubmit.setOnClickListener(this);
        mRlSubmit.setVisibility(View.GONE);

        mHeaderView = (QuotesDetailHeaderView) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_quotes_detail_header, mGvItems, false);
        mGvItems.addHeaderView(mHeaderView);
        mAdapter = new QuotesDetailAdapter(getActivity(), mPicasso, new QuotesDetailAdapter.AdapterListener() {
            @Override
            public void onItemClick(int position) {
                ProductItem productItem = (ProductItem) mAdapter.getItem(position);
                mContainerMap.setInt(AppConst.KEY_PRODUCT_ID, productItem.getId());
                openPage(MainActivity.Pages.ProductDetail);
            }
        });
        mGvItems.setAdapter(mAdapter);

        addEditTexts("1", mHeaderView.getEditTexts());
        addImageViews(mHeaderView.getImageViews());
        addImageViews(mIvSubmit);

        populateData();
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);

        if(data != null && data.getBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA)) {
            mIsRefreshData = true;
            populateData();
        }
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.QuotesDetail.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavBack) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavBack) {
            backClick(getFragmentId());
        } else if (v == mRlSubmit) {
            submit();
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {

        return super.onInternalTouch(v, event);
    }

    private void populateData() {
        int quoteId = mContainerMap.getInt(AppConst.KEY_QUOTE_ID);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        displayProgress(true);
        Subscription subscription = mApiManager.getQuote(token, String.valueOf(quoteId)).subscribe(new SimpleObserver<GetQuoteResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(GetQuoteResponse getQuoteResponse) {
                super.onNext(getQuoteResponse);
                displayProgress(false);
                if (getQuoteResponse.isSuccess()) {
                    // TODO User avatar
                    QuoteResponse quoteResponse = getQuoteResponse.getQuote();

                    mContainerMap.setString(AppConst.KEY_CUSTOMER_NAME, quoteResponse.getCustomerName()); // store customer name

                    mHeaderView.mTvName.setText(quoteResponse.getCustomerName());
                    mHeaderView.mTvPhone.setText(quoteResponse.getCustomerMobile());
                    if (BuildConfig.DEBUG) {
                        mHeaderView.mIvAvatar.setImageResource(R.drawable.ic_avatar);
                    }
                    // TODO Quote template
                    //mHeaderView.mTvDescription.setText("This is template of the quote.");
                    AppConst.QuoteStatus status = AppUtil.getQuoteStatus(quoteResponse);
                    switch (status) {
                        case MessageSent:
                            mHeaderView.mIvStatus.setImageResource(R.drawable.ic_quotes_message_sent1);
                            mHeaderView.mTvStatus.setText(R.string.quotes_status_message_sent);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.quote_message));
                            break;
                        case Callback:
                            mHeaderView.mIvStatus.setImageResource(R.drawable.ic_quotes_callback1);
                            mHeaderView.mTvStatus.setText(R.string.quotes_status_call_back);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.quote_callback));
                            break;
                        case Approved:
                            mHeaderView.mIvStatus.setImageResource(R.drawable.ic_quotes_accepted1);
                            mHeaderView.mTvStatus.setText(R.string.quotes_status_approved);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.quote_approved));
                            break;
                        case Rejected:
                            mHeaderView.mIvStatus.setImageResource(R.drawable.ic_quotes_rejected);
                            mHeaderView.mTvStatus.setText(R.string.quotes_status_rejected);
                            mHeaderView.mTvStatus.setTextColor(getResources().getColor(R.color.quote_rejected));
                            break;
                        default:
                            mHeaderView.mIvStatus.setImageDrawable(null);
                            mHeaderView.mTvStatus.setText("");
                            break;
                    }
                    List<QuoteProductResponse> productResponses = quoteResponse.getProducts();
                    if (productResponses != null && productResponses.size() > 0) {
                        mAdapter.clear();
                        boolean haveFixedImages = false;
                        for (QuoteProductResponse response : productResponses) {
                            ProductItem productItem = new ProductItem();
                            productItem.setQuoteId(response.getQuoteId());
                            productItem.setId(response.getId());
                            productItem.setName(response.getName());
                            productItem.setDescription(response.getDescription());
                            productItem.setPrice(DataUtil.formatPriceF(response.getPrice()));
                            productItem.setStatus(AppUtil.getProductStatus(response));
                            productItem.setImages(AppUtil.getImages(response));
                            if(!TextUtils.isEmpty(response.getFixedImages())) {
                                haveFixedImages = true;
                            }
                            mAdapter.addItem(productItem);
                        }
                        mAdapter.notifyDataSetChanged();
                        if(mIsRefreshData && haveFixedImages) {
                            mRlSubmit.setVisibility(View.VISIBLE);
                            mIsRefreshData = false;
                        }
                    }
                } else {
                    showError(getQuoteResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void submit() {
        int quoteId = mContainerMap.getInt(AppConst.KEY_QUOTE_ID);
        String mobile = mContainerMap.getString(AppConst.KEY_CUSTOMER_MOBILE);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        displayProgress(true);
        Subscription subscription = mApiManager.submitQuote(token, String.valueOf(quoteId), mobile).subscribe(new SimpleObserver<SubmitQuoteResponse>(){
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(SubmitQuoteResponse submitQuoteResponse) {
                super.onNext(submitQuoteResponse);
                displayProgress(false);
                if(submitQuoteResponse.isSuccess()) {
                    showToast("Update Successful");
                    Bundle data = new Bundle();
                    data.putBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA, true);
                    backClick(getFragmentId(), data);
                } else {
                    showError(submitQuoteResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

}

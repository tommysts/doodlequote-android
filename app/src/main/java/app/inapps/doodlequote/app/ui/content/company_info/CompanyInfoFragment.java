package app.inapps.doodlequote.app.ui.content.company_info;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CardResponse;
import app.inapps.doodlequote.app.api.response.DeleteCardResponse;
import app.inapps.doodlequote.app.api.response.GetUserProfileResponse;
import app.inapps.doodlequote.app.api.response.ListCardResponse;
import app.inapps.doodlequote.app.api.response.UpdateUserProfileResponse;
import app.inapps.doodlequote.app.api.response.UserProfileResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.product.UploadImageItem;
import app.inapps.doodlequote.app.ui.widget.BrowsePopup;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.ImageItem;
import app.inapps.doodlequote.core.util.DisplayImageUtil;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.DefaultHeaderListView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class CompanyInfoFragment extends BaseFragment implements ImagePickerCallback {
    public static final String TAG = CompanyInfoFragment.class.getSimpleName();

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.rlDone) RelativeLayout mRlDone;
    @BindView(R.id.lvItems) DefaultHeaderListView mLvItems;

    private CompanyInfoHeaderView mHeaderView;
    private CompanyInfoAdapter mAdapter;
    private UploadImageItem mUploadImageItem;
    private AlertPopup mAlertPopup;
    private String mPickerPath;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraPicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_info, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);
        mRlDone.setOnClickListener(this);

        mHeaderView = (CompanyInfoHeaderView) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_company_info_header, mLvItems, false);
        mHeaderView.setListener(new CompanyInfoHeaderView.CompanyInfoHeaderViewListener() {
            @Override
            public void onFinishInflate() {
                mHeaderView.setupButtons();
                AppConst.LoginType loginType = AppUtil.getLoginType(mContainerMap);
                if(loginType == AppConst.LoginType.Staff) {
                    mHeaderView.mLlBrowse.setVisibility(View.GONE);
                    mHeaderView.mRlPayment.setVisibility(View.GONE);
                    mRlDone.setVisibility(View.GONE);
                }
            }

            @Override
            public void onBrowseClick() {
                browse();
            }

            @Override
            public void onAddPaymentClick() {
                addPayment();
            }
        });
        mLvItems.addHeaderView(mHeaderView);
        mAdapter = new CompanyInfoAdapter(getActivity(), mPicasso, new CompanyInfoAdapter.AdapterListener() {
            @Override
            public void onDeleteClick(int position) {
                confirmDeleteCard(position);
            }
        });
        mLvItems.setAdapter(mAdapter);

        addEditTexts("1", mHeaderView.getEditTexts());
        addImageViews(mHeaderView.getImageViews());

        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.userProfile(token).subscribe(new SimpleObserver<GetUserProfileResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(GetUserProfileResponse getUserProfileResponse) {
                super.onNext(getUserProfileResponse);
                displayProgress(false);
                if (getUserProfileResponse.isSuccess()) {
                    UserProfileResponse response = getUserProfileResponse.getUser();
                    if (response != null) {
                        mHeaderView.mEdtCompanyName.setText(response.getCompanyName());
                        mHeaderView.mEdtWebsiteUrl.setText(response.getWebsite());
                        mHeaderView.mEdtDescription.setText(response.getCompanyDescription());
                        if (mUploadImageItem == null && !TextUtils.isEmpty(response.getCompanyImage())) {
                            AppConst.LoginType loginType = AppUtil.getLoginType(mContainerMap);
                            if(loginType == AppConst.LoginType.Staff && response.getParentId() != null) {
                                String imageUrl = AppUtil.getAvatarImageUrl(getString(R.string.app_end_point), response.getParentId().toString(), response.getCompanyImage());
                                mPicasso.load(imageUrl).resize(ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_AVATAR_WIDTH), 0).into(mHeaderView.mIvAvatar);
                            } else {
                                String imageUrl = AppUtil.getAvatarImageUrl(getString(R.string.app_end_point), response.getId().toString(), response.getCompanyImage());
                                mPicasso.load(imageUrl).resize(ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_AVATAR_WIDTH), 0).into(mHeaderView.mIvAvatar);
                            }
                        }
                        loadCards();
                    }
                } else {
                    showError(getUserProfileResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(AppConst.KEY_PICKER_PATH)) {
                mPickerPath = savedInstanceState.getString(AppConst.KEY_PICKER_PATH);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (mImagePicker == null) {
                    mImagePicker = new ImagePicker(this);
                    mImagePicker.shouldGenerateMetadata(true);
                    mImagePicker.shouldGenerateThumbnails(false);
                    mImagePicker.setImagePickerCallback(this);
                }
                mImagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (mCameraPicker == null) {
                    mCameraPicker = new CameraImagePicker(this);
                    mCameraPicker.setImagePickerCallback(this);
                    mCameraPicker.shouldGenerateMetadata(true);
                    mCameraPicker.shouldGenerateThumbnails(false);
                    mCameraPicker.reinitialize(mPickerPath);
                }
                mCameraPicker.submit(data);
            }
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        if(images != null && images.size() > 0) {
            displayProgress(true);
            ImageItem imageItem = AppUtil.createImageItem(images.get(0));
            Subscription subscription = DisplayImageUtil.rxOptimizeUploadImage(getActivity(), imageItem).subscribe(new SimpleObserver<ImageItem>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    showToast(R.string.error_loading_data);
                    displayProgress(false);
                }

                @Override
                public void onNext(ImageItem item) {
                    super.onNext(item);
                    loadImageFromPath(item);
                    mLogManager.log("File Path: " + item);
                    displayProgress(false);
                }
            });
            addSubscription(subscription);
        }
    }

    @Override
    public void onError(String message) {
        showError(message);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(AppConst.KEY_PICKER_PATH, mPickerPath);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
        loadCards();
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.CompanyInfo.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavHome) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            menuClick();
        } else if(v == mRlDone) {
            done();
        }
    }

    private void confirmDeleteCard(final int position) {
        if (mAlertPopup != null) {
            mAlertPopup.dismiss();
            mAlertPopup = null;
        }
        mAlertPopup = new AlertPopup(getActivity());
        mAlertPopup.setListener(new AlertPopup.AlertPopupListener() {
            @Override
            public void onStart() {
                mAlertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertPopup.dismiss();
                        deleteCard(position);
                    }
                });
                mAlertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertPopup.dismiss();
                    }
                });
                mAlertPopup.setMessage(getString(R.string.delete_confirm));
                mAlertPopup.setTitle(getString(R.string.app_name));
            }
        });
        mAlertPopup.show();
    }

    private void deleteCard(final int position) {
        PaymentItem paymentItem = (PaymentItem) mAdapter.getItem(position);
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.deleteCard(token, paymentItem.getCardId()).subscribe(new SimpleObserver<DeleteCardResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(DeleteCardResponse deleteCardResponse) {
                super.onNext(deleteCardResponse);
                displayProgress(false);
                if (deleteCardResponse.isSuccess()) {
                    mAdapter.deleteItem(position);
                    mAdapter.notifyDataSetChanged();
                } else {
                    showError(deleteCardResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void addPayment() {
        openPage(MainActivity.Pages.AddPayment);
    }

    private void browse() {
        BrowsePopup browsePopup = new BrowsePopup(getActivity());
        browsePopup.show();
        browsePopup.setListener(new BrowsePopup.BrowsePopupListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onTakePhotoClick(Dialog dialog) {
                dialog.dismiss();
                takePicture();
            }

            @Override
            public void onOpenPhotosClick(Dialog dialog) {
                dialog.dismiss();
                openPhotos();
            }

            @Override
            public void onTakeVideoClick(Dialog dialog) {

            }

            @Override
            public void onOpenVideosClick(Dialog dialog) {

            }

            @Override
            public void onTakeRecordClick(Dialog dialog) {

            }

            @Override
            public void onOpenRecordsClick(Dialog dialog) {

            }
        });
    }

    private void takePicture() {
        if(!AppUtil.verifyCameraPermissions(getActivity())) {
            return;
        }
        mCameraPicker = new CameraImagePicker(this);
        mCameraPicker.shouldGenerateMetadata(true);
        mCameraPicker.shouldGenerateThumbnails(false);
        mCameraPicker.setImagePickerCallback(this);
        mPickerPath = mCameraPicker.pickImage();
    }

    private void openPhotos() {
        if(!AppUtil.verifyStoragePermissions(getActivity())) {
            return;
        }
        mImagePicker = new ImagePicker(this);
        mImagePicker.shouldGenerateMetadata(true);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.pickImage();
    }

    private void loadImageFromPath(ImageItem imageItem) {
        mUploadImageItem = new UploadImageItem();
        mUploadImageItem.setPath(imageItem.getPath());
        File file = new File(imageItem.getPath());
        if(file.exists()) {
            mPicasso.load(file).resize(ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_AVATAR_WIDTH), 0).into(mHeaderView.mIvAvatar);
        } else {
            mHeaderView.mIvAvatar.setImageResource(R.drawable.ic_avatar);
        }
    }

    private void done() {
        final String companyName = mHeaderView.mEdtCompanyName.getText().toString();
        final String websiteUrl = mHeaderView.mEdtWebsiteUrl.getText().toString();
        final String description = mHeaderView.mEdtDescription.getText().toString();

        if(!TextUtils.isEmpty(websiteUrl) && !ValidatorUtil.isValidUrl(websiteUrl)) {
            showError(R.string.error_website_url_invalid);
            return;
        }

        displayProgress(true);
        final String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        if(mUploadImageItem == null) {
            Subscription subscription = mApiManager.updateUserCompanyProfile(token, companyName, websiteUrl, description).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    displayProgress(false);
                    showError(t, R.string.error_loading_data);
                }

                @Override
                public void onNext(UpdateUserProfileResponse userProfileResponse) {
                    super.onNext(userProfileResponse);
                    displayProgress(false);
                    if (userProfileResponse.isSuccess()) {
                        mContainerMap.setString(AppConst.KEY_COMPANY_NAME, companyName);
                        showToast(R.string.update_success);
                    } else {
                        showError(userProfileResponse, R.string.error_loading_data);
                    }
                }
            });
            addSubscription(subscription);
        } else {
            Subscription subscription1 = mApiManager.uploadUserCompanyAvatar(token, mUploadImageItem.getPath()).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    displayProgress(false);
                    showError(t, R.string.error_loading_data);
                }

                @Override
                public void onNext(UpdateUserProfileResponse userProfileResponse) {
                    super.onNext(userProfileResponse);
                    if (userProfileResponse.isSuccess()) {
                        Subscription subscription2 = mApiManager.updateUserCompanyProfile(token, companyName, websiteUrl, description).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                                mLogManager.log(t);
                                displayProgress(false);
                                showError(t, R.string.error_loading_data);
                            }

                            @Override
                            public void onNext(UpdateUserProfileResponse userProfileResponse) {
                                super.onNext(userProfileResponse);
                                displayProgress(false);
                                if (userProfileResponse.isSuccess()) {
                                    mContainerMap.setString(AppConst.KEY_COMPANY_NAME, companyName);
                                    showToast(R.string.update_success);
                                } else {
                                    showError(userProfileResponse, R.string.error_loading_data);
                                }
                            }
                        });
                        addSubscription(subscription2);
                    } else {
                        displayProgress(false);
                        showError(userProfileResponse, R.string.error_loading_data);
                    }
                }
            });
            addSubscription(subscription1);
        }
    }

    private void loadCards() {
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.listCard(token).subscribe(new SimpleObserver<ListCardResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ListCardResponse listCardResponse) {
                super.onNext(listCardResponse);
                displayProgress(false);
                if (listCardResponse.isSuccess()) {
                    List<CardResponse> cards = listCardResponse.getCards();
                    List<PaymentItem> paymentItems = new ArrayList<PaymentItem>();
                    if(cards != null && cards.size() > 0) {
                        for(CardResponse cardResponse : cards) {
                            PaymentItem paymentItem =  AppUtil.createPaymentItem(cardResponse);
                            paymentItems.add(paymentItem);
                        }
                    }
                    mAdapter.clear();
                    mAdapter.addItems(paymentItems);
                    mAdapter.notifyDataSetChanged();
                } else {
                    showError(listCardResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }
}

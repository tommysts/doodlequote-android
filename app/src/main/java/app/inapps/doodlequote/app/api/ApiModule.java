package app.inapps.doodlequote.app.api;

import android.net.Uri;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppApplication;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.util.LogManager;
import app.inapps.doodlequote.core.ContainerMap;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {
    @Provides @Singleton
    public OkHttpClient provideOkHttpClient(AppApplication application){
        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(application.getCacheDir(), "http");
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .cache(new Cache(cacheDir, AppConst.DISK_CACHE_SIZE));

        if(BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        return builder.build();
    }

    @Provides @Singleton
    public Picasso providePicasso(AppApplication app, OkHttpClient client, final LogManager logManager) {
        return new Picasso.Builder(app)
                .downloader(new OkHttp3Downloader(client))
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception e) {
                        if(BuildConfig.DEBUG) {
                            logManager.log("Failed to load image: " + uri);
                        }
                    }
                }).loggingEnabled(BuildConfig.DEBUG)
                .build();
    }

    @Provides @Singleton
    public ApiService provideApiService(AppApplication application, OkHttpClient okHttpClient, ContainerMap containerMap){
        Gson gson = new GsonBuilder().create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(okHttpClient);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
        String endPoint = containerMap.getString(AppConst.KEY_END_POINT);
        if(TextUtils.isEmpty(endPoint)){
            builder.baseUrl(application.getString(R.string.app_end_point));
        } else {
            builder.baseUrl(endPoint);
        }
        return builder.build().create(ApiService.class);
    }

    @Provides @Singleton
    public ApiManager provideApiManager(ContainerMap containerMap, ApiService baseService, LogManager logManager){
        return new ApiManager(containerMap, baseService, logManager);
    }
}

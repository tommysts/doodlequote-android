package app.inapps.doodlequote.app.ui.content.buy_credits;

import android.os.Parcel;
import android.os.Parcelable;

public class PackageItem implements Parcelable {
    private int id;
    private String amount;
    private String price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.amount);
        dest.writeString(this.price);
    }

    public PackageItem() {
    }

    protected PackageItem(Parcel in) {
        this.id = in.readInt();
        this.amount = in.readString();
        this.price = in.readString();
    }

    public static final Parcelable.Creator<PackageItem> CREATOR = new Parcelable.Creator<PackageItem>() {
        @Override
        public PackageItem createFromParcel(Parcel source) {
            return new PackageItem(source);
        }

        @Override
        public PackageItem[] newArray(int size) {
            return new PackageItem[size];
        }
    };
}

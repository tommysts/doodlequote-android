package app.inapps.doodlequote.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.FullWallet;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.LineItem;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentMethodTokenizationType;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;
import com.google.android.gms.wallet.fragment.WalletFragmentInitParams;
import com.google.gson.Gson;
import com.stripe.android.model.Token;

import javax.inject.Inject;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppApplication;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.ui.content.buy_credits.BuyCreditsFragment;
import app.inapps.doodlequote.app.ui.content.buy_credits.PackageItem;
import app.inapps.doodlequote.common.util.SystemUtil;
import app.inapps.doodlequote.core.ContainerMap;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int LOAD_MASKED_WALLET_REQUEST_CODE = 1000;
    private static final int LOAD_FULL_WALLET_REQUEST_CODE = 1001;

    @BindView(R.id.rlNavBack) RelativeLayout mRlNavBack;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;
    @BindView(R.id.tvMessageStatus) TextView mTvMessageStatus;

    @Inject ContainerMap mContainerMap;

    PackageItem mPackageItem;
    SupportWalletFragment mWalletFragment;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIsDestroy = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        mUnbinder = ButterKnife.bind(this);

        Object o = mContainerMap.getObject(AppConst.KEY_PACKAGE_ITEM);
        if(o instanceof PackageItem) {
            mPackageItem = (PackageItem)o;
            mContainerMap.remove(AppConst.KEY_PACKAGE_ITEM);
        }

        mWalletFragment =
                (SupportWalletFragment) getSupportFragmentManager().findFragmentById(R.id.wallet_fragment);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wallet.API, new Wallet.WalletOptions.Builder()
                        .setEnvironment(AppConst.WALLET_ENVIRONMENT)
                        .setTheme(WalletConstants.THEME_LIGHT)
                        .build())
                .build();

        mRlNavBack.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        displayProgress(true);
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIsDestroy = true;
        SystemUtil.executeJavaVmGC();
    }

    @Override
    void setupActivityComponent(Bundle savedInstanceState) {
        AppApplication.getInstance(this).getAppComponent().inject(this);
    }

    public void displayProgress(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(mPackageItem == null || mIsDestroy) {
            return;
        }
        displayProgress(true);
        Wallet.Payments.isReadyToPay(mGoogleApiClient).setResultCallback(
                new ResultCallback<BooleanResult>() {
                    @Override
                    public void onResult(@NonNull BooleanResult booleanResult) {
                        if(mPackageItem == null || mIsDestroy) {
                            return;
                        }
                        if (booleanResult.getStatus().isSuccess()) {
                            if(booleanResult.getValue()) {
                                showAndroidPay();
                            } else {
                                mWalletFragment.setEnabled(false);
                                showMessage("Android Pay cannot be used yet.");
                            }
                        } else {
                            mWalletFragment.setEnabled(false);
                            showMessage("Android Pay cannot be used yet.");
                        }
                        displayProgress(false);
                    }
                }
        );
    }

    @Override
    public void onConnectionSuspended(int i) {
        if(mPackageItem == null || mIsDestroy) {
            return;
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if(mPackageItem == null || mIsDestroy) {
            return;
        }
        displayProgress(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_MASKED_WALLET_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                MaskedWallet maskedWallet = data.getParcelableExtra(WalletConstants.EXTRA_MASKED_WALLET);
                FullWalletRequest fullWalletRequest = FullWalletRequest.newBuilder()
                        .setCart(Cart.newBuilder()
                                .setCurrencyCode("USD")
                                .setTotalPrice(mPackageItem.getPrice())
                                .addLineItem(LineItem.newBuilder()
                                        .setCurrencyCode("USD")
                                        .setQuantity("1")
                                        .setDescription(mPackageItem.getAmount())
                                        .setTotalPrice(mPackageItem.getPrice())
                                        .setUnitPrice(mPackageItem.getPrice())
                                        .build())
                                .build())
                        .setGoogleTransactionId(maskedWallet.getGoogleTransactionId())
                        .build();
                Wallet.Payments.loadFullWallet(mGoogleApiClient, fullWalletRequest, LOAD_FULL_WALLET_REQUEST_CODE);
            }
        } else if (requestCode == LOAD_FULL_WALLET_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if(BuildConfig.DEBUG) {
                    FullWallet fullWallet = data.getParcelableExtra(WalletConstants.EXTRA_FULL_WALLET);
                    String tokenJSON = fullWallet.getPaymentMethodToken().getToken();
                    Gson gson = new Gson();
                    Token token = gson.fromJson(tokenJSON, Token.class);
                    sendTokenToServer(token.getId());
                } else {
                    FullWallet fullWallet = data.getParcelableExtra(WalletConstants.EXTRA_FULL_WALLET);
                    String tokenJSON = fullWallet.getPaymentMethodToken().getToken();
                    Gson gson = new Gson();
                    Token token = gson.fromJson(tokenJSON, Token.class);
                    sendTokenToServer(token.getId());
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showAndroidPay() {
        MaskedWalletRequest maskedWalletRequest = MaskedWalletRequest.newBuilder()
                // Request credit card tokenization with Stripe by specifying tokenization parameters:
                .setPaymentMethodTokenizationParameters(PaymentMethodTokenizationParameters.newBuilder()
                        .setPaymentMethodTokenizationType(PaymentMethodTokenizationType.PAYMENT_GATEWAY)
                        .addParameter("gateway", "stripe")
                        .addParameter("stripe:publishableKey", AppConst.STRIPE_PUBLISHABLE_KEY)
                        .addParameter("stripe:version", "2.0.0")
                        .build())

                // You want the shipping address:
                .setShippingAddressRequired(true)
                // Price set as a decimal:
                .setEstimatedTotalPrice(mPackageItem.getPrice())
                .setCurrencyCode("USD")
                .build();

        // Set the parameters:
        WalletFragmentInitParams initParams = WalletFragmentInitParams.newBuilder()
                .setMaskedWalletRequest(maskedWalletRequest)
                .setMaskedWalletRequestCode(LOAD_MASKED_WALLET_REQUEST_CODE)
                .build();

        // Initialize the fragment:
        mWalletFragment.initialize(initParams);
    }

    private void sendTokenToServer(String cardToken) {
        Intent intent = new Intent();
        intent.putExtra(BuyCreditsFragment.ANDROID_PAY_CARD_TOKEN, cardToken);
        setResult(Activity.RESULT_OK, intent);
        finishActivity(BuyCreditsFragment.ANDROID_PAY_REQUEST_CODE);
    }
}

package app.inapps.doodlequote.app.api;

import android.text.TextUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.inapps.doodlequote.app.api.response.CreateCardResponse;
import app.inapps.doodlequote.app.api.response.CreateQuoteResponse;
import app.inapps.doodlequote.app.api.response.CreateTemplateResponse;
import app.inapps.doodlequote.app.api.response.CreateUserResponse;
import app.inapps.doodlequote.app.api.response.CreditResponse;
import app.inapps.doodlequote.app.api.response.DashboardResponse;
import app.inapps.doodlequote.app.api.response.DeleteCardResponse;
import app.inapps.doodlequote.app.api.response.DeleteTemplateResponse;
import app.inapps.doodlequote.app.api.response.ForgotPasswordResponse;
import app.inapps.doodlequote.app.api.response.GetProductResponse;
import app.inapps.doodlequote.app.api.response.GetQuoteResponse;
import app.inapps.doodlequote.app.api.response.GetUserProfileResponse;
import app.inapps.doodlequote.app.api.response.ListCardResponse;
import app.inapps.doodlequote.app.api.response.ListPackageResponse;
import app.inapps.doodlequote.app.api.response.ListQuoteResponse;
import app.inapps.doodlequote.app.api.response.ListTemplateResponse;
import app.inapps.doodlequote.app.api.response.ListUserResponse;
import app.inapps.doodlequote.app.api.response.LoginResponse;
import app.inapps.doodlequote.app.api.response.RegisterResponse;
import app.inapps.doodlequote.app.api.response.ResetPasswordResponse;
import app.inapps.doodlequote.app.api.response.SubmitOrderResponse;
import app.inapps.doodlequote.app.api.response.SubmitQuoteResponse;
import app.inapps.doodlequote.app.api.response.UpdateDeviceTokenResponse;
import app.inapps.doodlequote.app.api.response.UpdateUserProfileResponse;
import app.inapps.doodlequote.app.api.response.UploadResponse;
import app.inapps.doodlequote.app.util.LogManager;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.ContainerMap;
import app.inapps.doodlequote.core.db.DbUtil;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ApiManager {
    private ContainerMap mContainerMap;
    private ApiService mApiService;
    private LogManager mLogManager;

    public ApiManager(ContainerMap containerMap, ApiService apiService, LogManager logManager) {
        mContainerMap = containerMap;
        mApiService = apiService;
        mLogManager = logManager;
    }

    public Observable<LoginResponse> login(String email, String password) {
        return mApiService.login(email, password).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<RegisterResponse> register(String email, String name, String password, String confirmPassword, String mobile, String type) {
        return mApiService.register(email, name, password, confirmPassword, mobile, type).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ForgotPasswordResponse> forgotPassword(String email) {
        return mApiService.forgotPassword(email).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResetPasswordResponse> resetPassword(String email, String code, String password, String confirmPassword) {
        return mApiService.resetPassword(email, code, password, confirmPassword).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<DashboardResponse> getDashboard(String token) {
        return mApiService.getDashboard(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ListQuoteResponse> listQuote(String token) {
        return mApiService.listQuote(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ListTemplateResponse> listTemplate(String token) {
        return mApiService.listTemplate(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ListPackageResponse> listPackage(String token) {
        return mApiService.listPackage(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ListCardResponse> listCard(String token) {
        return mApiService.listCard(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ListUserResponse> listUser(String token) {
        return mApiService.listUser(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<CreditResponse> creditInfo(String token) {
        return mApiService.creditInfo(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<GetUserProfileResponse> userProfile(String token) {
        return mApiService.userProfile(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ListQuoteResponse> searchQuote(String token, String search) {
        return mApiService.searchQuote(token, search).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ListUserResponse> searchUser(String token, String search) {
        return mApiService.searchUser(token, search).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<GetQuoteResponse> getQuote(String token, String quoteId) {
        return mApiService.getQuote(token, quoteId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<GetProductResponse> getProduct(String token, String productId) {
        return mApiService.getProduct(token, productId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SubmitQuoteResponse> submitQuote(String token, String quoteId, String mobile) {
        return mApiService.submitQuote(token, quoteId, mobile).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SubmitOrderResponse> submitOrder(String token, String packageId, String cardId,
                                                      String cardHolder,  String cardNumber, String cardMonth,
                                                       String cardYear,  String cardCvn) {
        return mApiService.submitOrder(token, packageId, cardId, cardHolder, cardNumber, cardMonth,
                cardYear, cardCvn).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SubmitOrderResponse> submitOrder(String token, String cardId, String packageId, String cardCvn) {
        return mApiService.submitOrder(token, cardId, packageId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SubmitOrderResponse> submitOrderNoCard(String token, String cardToken, String packageId, String cardCvn) {
        return mApiService.submitOrderNoCard(token, cardToken, packageId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<CreateCardResponse> createCard(String token, String cardToken) {
        return mApiService.createCard(token, cardToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<DeleteCardResponse> deleteCard(String token, String cardId) {
        return mApiService.deleteCard(token, cardId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<CreateTemplateResponse> createTemplate(String token, String body) {
        return mApiService.createTemplate(token, body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<DeleteTemplateResponse> deleteTemplate(String token, String templateId) {
        return mApiService.deleteTemplate(token, templateId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UpdateUserProfileResponse> updateUserProfile(String token, int userId,
                                                             String firstName,
                                                             String lastName,
                                                             String mobile, String title, String email, boolean isDisabled) {
        Map<String, String> fields = new HashMap<>();
        if(userId != 0) {
            fields.put("user_id", String.valueOf(userId));
        }
        if(firstName != null) {
            fields.put("first_name", firstName);
        }
        if(lastName != null) {
            fields.put("last_name", lastName);
        }
        if(mobile != null) {
            fields.put("mobile", mobile);
        }
        if(title != null) {
            fields.put("title", title);
        }
        if(email != null) {
            fields.put("email", email);
        }
        fields.put("is_disabled", DataUtil.getTrue(isDisabled));

        return mApiService.updateUserProfile(token, fields).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UpdateUserProfileResponse> updateUserCompanyProfile(String token,
                                                             String companyName,
                                                             String website,
                                                             String companyDescription) {
        return mApiService.updateUserCompanyProfile(token, companyName, website, companyDescription).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UpdateUserProfileResponse> updateUserPassword(String token, String oldPassword, String password, String passwordConfirmation) {
        return mApiService.updateUserPassword(token, oldPassword, password, passwordConfirmation).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<CreateUserResponse> createCompanyUser(String token, String firstName, String lastName, String mobile, String email, String title, boolean isDisabled, String imagePath) {
        RequestBody fistNameRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), firstName);
        RequestBody lastNameRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), lastName);
        RequestBody mobileRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), mobile);
        RequestBody emailRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), email);
        RequestBody titleRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), title);
        RequestBody disableRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), DataUtil.getTrue(isDisabled));
        if(TextUtils.isEmpty(imagePath)) {
           return mApiService.createCompanyUser(token, fistNameRequestBody, lastNameRequestBody, mobileRequestBody, emailRequestBody, titleRequestBody, disableRequestBody)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        } else {
            File file = new File(imagePath);
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part imageMultiPart = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            return mApiService.createCompanyUser(token, fistNameRequestBody, lastNameRequestBody, mobileRequestBody, emailRequestBody, titleRequestBody, imageMultiPart)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        }
    }

    public Observable<UpdateUserProfileResponse> uploadUserAvatar(String token, int userId, String imagePath) {
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part imageMultiPart = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
        if(userId == 0) {
            return mApiService.uploadUserAvatar(token, imageMultiPart).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        } else {
            RequestBody userIdRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(userId));
            return mApiService.uploadUserAvatar(token, userIdRequestBody, imageMultiPart).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        }
    }

    public Observable<UpdateUserProfileResponse> uploadUserCompanyAvatar(String token, String imagePath) {
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part imageMultiPart = MultipartBody.Part.createFormData("company_image", file.getName(), requestBody);
        return mApiService.uploadUserAvatar(token, imageMultiPart).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<CreateQuoteResponse> createQuote(String token,
                                                       String templateId,
                                                       String customerName,
                                                       String customerMobile,
                                                       Map<String, String> products) {
        return mApiService.createQuote(token, templateId, customerName, customerMobile, products).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UploadResponse> uploadVideo(String token, int userId, String filePath) {
        File file = new File(filePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("video/*"), file);
        MultipartBody.Part fileMultiPart = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        return mApiService.uploadVideo(token, fileMultiPart).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UploadResponse> uploadImages(final String token, final Map<Integer, List<String>> products) {
        return Observable.create(new Observable.OnSubscribe<UploadResponse>() {
            @Override
            public void call(Subscriber<? super UploadResponse> subscriber) {
                UploadResponse uploadResponse = new UploadResponse();
                uploadResponse.setSuccess(DbUtil.TRUE);
                for(Map.Entry<Integer, List<String>> entry : products.entrySet()) {
                    String productId = String.valueOf(entry.getKey());
                    List<String> images = entry.getValue();

                    int index = 0;
                    RequestBody productRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), productId);
                    if(images.size() > 2){
                        // Upload 2 images at a time
                        MultipartBody.Part[] imageMultiParts = new MultipartBody.Part[2];
                        for (; index < images.size() - 1; index += 2) {
                            File file1 = new File(images.get(index));
                            RequestBody requestBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
                            imageMultiParts[0] = MultipartBody.Part.createFormData("images[" + index + "]", file1.getName(), requestBody1);

                            File file2 = new File(images.get(index + 1));
                            RequestBody requestBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
                            imageMultiParts[1] = MultipartBody.Part.createFormData("images[" + (index + 1) + "]", file2.getName(), requestBody2);

                            try {
                                UploadResponse uploadResponse1 = mApiService.uploadImages(token, productRequestBody, imageMultiParts).execute().body();
                                if(!uploadResponse1.isSuccess()) {
                                    uploadResponse.setError(uploadResponse1.getError());
                                }
                                boolean isSuccess = uploadResponse.isSuccess() && uploadResponse1.isSuccess();
                                uploadResponse.setSuccess(isSuccess ? DbUtil.TRUE : DbUtil.FALSE);
                            } catch (IOException e) {
                                mLogManager.log(e);
                            }
                        }
                    }
                    if(images.size() - index > 0) {
                        MultipartBody.Part[] imageMultiParts = new MultipartBody.Part[images.size() - index];
                        for (int i = 0; index < images.size(); i++, index++) {
                            File file = new File(images.get(index));
                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                            imageMultiParts[i] = MultipartBody.Part.createFormData("images[" + index + "]", file.getName(), requestBody);
                        }
                        try {
                            UploadResponse uploadResponse1 = mApiService.uploadImages(token, productRequestBody, imageMultiParts).execute().body();
                            if (!uploadResponse1.isSuccess()) {
                                uploadResponse.setError(uploadResponse1.getError());
                            }
                            boolean isSuccess = uploadResponse.isSuccess() && uploadResponse1.isSuccess();
                            uploadResponse.setSuccess(isSuccess ? DbUtil.TRUE : DbUtil.FALSE);
                        } catch (IOException e) {
                            mLogManager.log(e);
                        }
                    }
                }
                subscriber.onNext(uploadResponse);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UploadResponse> uploadFixedImages(final String token, final Map<Integer, List<String>> products) {
        return Observable.create(new Observable.OnSubscribe<UploadResponse>() {
            @Override
            public void call(Subscriber<? super UploadResponse> subscriber) {
                UploadResponse uploadResponse = new UploadResponse();
                uploadResponse.setSuccess(DbUtil.TRUE);
                for(Map.Entry<Integer, List<String>> entry : products.entrySet()) {
                    String productId = String.valueOf(entry.getKey());
                    List<String> images = entry.getValue();

                    int index = 0;
                    RequestBody productRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), productId);
                    if(images.size() > 2){
                        // Upload 2 images at a time
                        MultipartBody.Part[] imageMultiParts = new MultipartBody.Part[2];
                        for (; index < images.size() - 1; index += 2) {
                            File file1 = new File(images.get(index));
                            RequestBody requestBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
                            imageMultiParts[0] = MultipartBody.Part.createFormData("fixed_images[" + index + "]", file1.getName(), requestBody1);

                            File file2 = new File(images.get(index + 1));
                            RequestBody requestBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
                            imageMultiParts[1] = MultipartBody.Part.createFormData("fixed_images[" + (index + 1) + "]", file2.getName(), requestBody2);

                            try {
                                UploadResponse uploadResponse1 = mApiService.uploadImages(token, productRequestBody, imageMultiParts).execute().body();
                                if(!uploadResponse1.isSuccess()) {
                                    uploadResponse.setError(uploadResponse1.getError());
                                }
                                boolean isSuccess = uploadResponse.isSuccess() && uploadResponse1.isSuccess();
                                uploadResponse.setSuccess(isSuccess ? DbUtil.TRUE : DbUtil.FALSE);
                            } catch (IOException e) {
                                mLogManager.log(e);
                            }
                        }
                    }
                    if(images.size() - index > 0) {
                        MultipartBody.Part[] imageMultiParts = new MultipartBody.Part[images.size() - index];
                        for (int i = 0; index < images.size(); i++, index++) {
                            File file = new File(images.get(index));
                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                            imageMultiParts[i] = MultipartBody.Part.createFormData("fixed_images[" + index + "]", file.getName(), requestBody);
                        }
                        try {
                            UploadResponse uploadResponse1 = mApiService.uploadImages(token, productRequestBody, imageMultiParts).execute().body();
                            if (!uploadResponse1.isSuccess()) {
                                uploadResponse.setError(uploadResponse1.getError());
                            }
                            boolean isSuccess = uploadResponse.isSuccess() && uploadResponse1.isSuccess();
                            uploadResponse.setSuccess(isSuccess ? DbUtil.TRUE : DbUtil.FALSE);
                        } catch (IOException e) {
                            mLogManager.log(e);
                        }
                    }
                }
                subscriber.onNext(uploadResponse);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UpdateDeviceTokenResponse> updateDeviceToken(final String token, final String deviceToken, final String deviceType) {
        return mApiService.updateDeviceToken(token, deviceToken, deviceType).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}

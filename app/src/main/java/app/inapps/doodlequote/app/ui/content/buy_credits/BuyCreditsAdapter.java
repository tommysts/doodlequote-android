package app.inapps.doodlequote.app.ui.content.buy_credits;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.api.response.PackageResponse;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultCheckbox;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BuyCreditsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<CreditItem> mItems;
    private Context mContext;
    private AdapterListener mListener;
    private boolean mIsCheckTerms;

    public interface AdapterListener {
        void onItemClick(PackageResponse packageResponse);
    }

    public BuyCreditsAdapter(Context context, AdapterListener listener) {
        super();
        mItems = new ArrayList<CreditItem>();
        mContext = context;
        mListener = listener;
    }

    public void clear() {
        mItems.clear();
    }

    public void addItems(List<CreditItem> items) {
        mItems.addAll(items);
    }

    public void addItem(CreditItem item) {
        mItems.add(item);
    }

    public CreditItem getItem(int position) {
        if (position >= 0 && position < mItems.size()) {
            return mItems.get(position);
        }
        return null;
    }

    public boolean isCheckTerms() {
        return mIsCheckTerms;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_buy_credits_header, parent, false);
            return new HeaderViewHolder(view);
        } else if(viewType == 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_buy_credits_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_buy_credits_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CreditItem item = getItem(position);

        if(holder instanceof FooterViewHolder) {
            ((FooterViewHolder) holder).cbTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mIsCheckTerms = isChecked;
                }
            });
        } else if (holder instanceof CustomViewHolder) {
            CustomViewHolder customViewHolder = (CustomViewHolder) holder;
            if(item.isSelected1) {
                customViewHolder.layout1.setBackgroundResource(R.drawable.ic_package_active_bg);
                customViewHolder.tvDescription1.setTextColor(mContext.getResources().getColor(R.color.white));
            } else {
                customViewHolder.layout1.setBackgroundResource(R.drawable.ic_package_bg);
                customViewHolder.tvDescription1.setTextColor(Color.parseColor("#55dffb"));
            }
            if(item.isSelected2) {
                customViewHolder.layout2.setBackgroundResource(R.drawable.ic_package_active_bg);
                customViewHolder.tvDescription2.setTextColor(mContext.getResources().getColor(R.color.white));
            } else {
                customViewHolder.layout2.setBackgroundResource(R.drawable.ic_package_bg);
                customViewHolder.tvDescription2.setTextColor(Color.parseColor("#55dffb"));
            }
            customViewHolder.layout1.setTag(position);
            customViewHolder.layout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(CreditItem c : mItems) {
                        c.isSelected1 = false;
                        c.isSelected2 = false;
                    }
                    int pos = DataUtil.getInteger(v.getTag().toString(), 0);
                    CreditItem selected = getItem(pos);
                    selected.isSelected1 = true;
                    notifyDataSetChanged();
                    mListener.onItemClick(selected.package1);
                }
            });
            customViewHolder.layout2.setTag(position);
            customViewHolder.layout2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(CreditItem c : mItems) {
                        c.isSelected1 = false;
                        c.isSelected2 = false;
                    }
                    int pos = DataUtil.getInteger(v.getTag().toString(), 0);
                    CreditItem selected = getItem(pos);
                    selected.isSelected2 = true;
                    notifyDataSetChanged();
                    mListener.onItemClick(selected.package2);
                }
            });
            if(item.package1 != null) {
                customViewHolder.tvDescription1.setText(item.package1.getAmount());
                customViewHolder.layout1.setVisibility(View.VISIBLE);
            } else {
                customViewHolder.layout1.setVisibility(View.GONE);
            }
            if(item.package2 != null) {
                customViewHolder.tvDescription2.setText(item.package2.getAmount());
                customViewHolder.layout2.setVisibility(View.VISIBLE);
            } else {
                customViewHolder.layout2.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0) {
            return 0;
        } else if (position == mItems.size() - 1) {
            return 1;
        }
        return 2;
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cbTerms) DefaultCheckbox cbTerms;

        private FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout1) ViewGroup layout1;
        @BindView(R.id.tvDescription1) DefaultTextView tvDescription1;
        @BindView(R.id.layout2) ViewGroup layout2;
        @BindView(R.id.tvDescription2) DefaultTextView tvDescription2;

        private CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


package app.inapps.doodlequote.app.ui.content.templates;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.DeleteTemplateResponse;
import app.inapps.doodlequote.app.api.response.ListTemplateResponse;
import app.inapps.doodlequote.app.api.response.TemplateResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TemplatesFragment extends BaseFragment {
    public static final String TAG = TemplatesFragment.class.getSimpleName();

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.rlEdit) RelativeLayout mRlEdit;
    @BindView(R.id.rcvItems) RecyclerView mRcvItems;
    @BindView(R.id.ivRecent) AnimateImageView mIvRecent;
    @BindView(R.id.rlRecent) RelativeLayout mRlRecent;
    @BindView(R.id.ivMostUsed) AnimateImageView mIvMostUsed;
    @BindView(R.id.rlMostUsed) RelativeLayout mRlMostUsed;
    @BindView(R.id.tvRecent) DefaultTextView mTvRecent;
    @BindView(R.id.tvMostUsed) DefaultTextView mTvMostUsed;

    private TemplatesAdapter mAdapter;
    private AlertPopup mAlertPopup;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_templates, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);
        mRlEdit.setOnClickListener(this);
        mRlRecent.setOnClickListener(this);
        mRlMostUsed.setOnClickListener(this);

        mAdapter = new TemplatesAdapter(getActivity(), new TemplatesAdapter.AdapterListener() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onDeleteClick(final int position) {
                if (mAlertPopup != null) {
                    mAlertPopup.dismiss();
                    mAlertPopup = null;
                }
                mAlertPopup = new AlertPopup(getActivity());
                mAlertPopup.setListener(new AlertPopup.AlertPopupListener() {
                    @Override
                    public void onStart() {
                        mAlertPopup.setButton1Action(getString(R.string.ok_text), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAlertPopup.dismiss();
                                deleteTemplate(position);
                            }
                        });
                        mAlertPopup.setButton2Action(getString(R.string.cancel_text), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAlertPopup.dismiss();
                            }
                        });
                        mAlertPopup.setMessage(getString(R.string.delete_confirm));
                        mAlertPopup.setTitle(getString(R.string.app_name));
                    }
                });
                mAlertPopup.show();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRcvItems.setLayoutManager(linearLayoutManager);
        mRcvItems.setAdapter(mAdapter);

        addImageViews(mIvMostUsed, mIvRecent);

        listTemplate();
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);

        if (data != null && data.getBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA)) {
            listTemplate();
        }
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.Templates.ordinal();
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            menuClick();
        } else if (v == mRlEdit) {
            addTemplate();
        } else if (v == mRlRecent) {
            sort(true, false);
        } else if (v == mRlMostUsed) {
            sort(false, true);
        }
    }

    private void listTemplate() {
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.listTemplate(token).subscribe(new SimpleObserver<ListTemplateResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ListTemplateResponse listTemplateResponse) {
                super.onNext(listTemplateResponse);
                displayProgress(false);
                if (listTemplateResponse.isSuccess()) {
                    List<TemplateItem> items = new ArrayList<TemplateItem>();
                    List<TemplateResponse> templateResponses = listTemplateResponse.getTemplates();
                    if (templateResponses != null && templateResponses.size() > 0) {
                        for (TemplateResponse response : templateResponses) {
                            TemplateItem item = AppUtil.createTemplateItem(response);
                            items.add(item);
                        }
                    }
                    mAdapter.clear();
                    mAdapter.addItems(items);
                    sort(true, false);
                } else {
                    showError(listTemplateResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void addTemplate() {
        openPage(MainActivity.Pages.TemplatesCreate);
    }

    private void deleteTemplate(final int position) {
        TemplateItem templateItem = mAdapter.getItem(position);
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.deleteTemplate(token, String.valueOf(templateItem.getId())).subscribe(new SimpleObserver<DeleteTemplateResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(DeleteTemplateResponse deleteTemplateResponse) {
                super.onNext(deleteTemplateResponse);
                displayProgress(false);
                if (deleteTemplateResponse.isSuccess()) {
                    mAdapter.deleteItem(position);
                    mAdapter.notifyDataSetChanged();
                } else {
                    showError(deleteTemplateResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void sort(final boolean sortRecent, final boolean sortMostUsed) {
        if (sortRecent) {
            mIvRecent.setImageResource(R.drawable.button_rec_active_bg);
            mIvMostUsed.setImageResource(R.drawable.button_rec_inactive_bg);
            mTvRecent.setTextColor(getResources().getColor(R.color.white));
            mTvMostUsed.setTextColor(Color.parseColor("#55dffb"));
        } else if (sortMostUsed) {
            mIvRecent.setImageResource(R.drawable.button_rec_inactive_bg);
            mIvMostUsed.setImageResource(R.drawable.button_rec_active_bg);
            mTvRecent.setTextColor(Color.parseColor("#55dffb"));
            mTvMostUsed.setTextColor(getResources().getColor(R.color.white));
        }
        displayProgress(true);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                Collections.sort(mAdapter.getItems(), new Comparator<TemplateItem>() {
                    @Override
                    public int compare(TemplateItem o1, TemplateItem o2) {
                        if (sortRecent || sortMostUsed) {
                            long diff = o2.getCreatedTime() - o1.getCreatedTime();
                            if (diff > 0) {
                                return 1;
                            } else if (diff < 0) {
                                return -1;
                            }
                        }
                        return 0;
                    }
                });
                subscriber.onNext(true);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SimpleObserver<Boolean>() {

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(Boolean aBoolean) {
                super.onNext(aBoolean);
                displayProgress(false);
                mAdapter.updateDisplayItems();
                mAdapter.notifyDataSetChanged();
            }
        });
        addSubscription(subscription);
    }
}

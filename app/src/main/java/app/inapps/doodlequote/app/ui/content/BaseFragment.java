package app.inapps.doodlequote.app.ui.content;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.app.AppApplication;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.api.ApiManager;
import app.inapps.doodlequote.app.db.DatabaseManager;
import app.inapps.doodlequote.app.prefs.UserPreferences;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.util.LogManager;
import app.inapps.doodlequote.common.util.SystemUtil;
import app.inapps.doodlequote.core.ContainerMap;
import app.inapps.doodlequote.core.SystemConst;
import app.inapps.doodlequote.core.api.response.BaseResponse;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.DebouncedOnTouchListener;
import butterknife.Unbinder;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    @Inject
    protected ApiManager mApiManager;
    @Inject
    protected DatabaseManager mDatabaseManager;
    @Inject
    protected UserPreferences mUserPreferences;
    @Inject
    protected LogManager mLogManager;
    @Inject
    protected Picasso mPicasso;
    @Inject
    protected ContainerMap mContainerMap;

    OnFragmentInteractionListener mListener;

    private boolean mNeedCreateSubscription;
    private CompositeSubscription mCompositeSubscription;
    private Handler mHandler;
    private LoadDataRunnable mLoadDataRunnable;
    private boolean mIsDestroyView;
    protected Unbinder mUnBinder;

    private DebouncedOnClickListener mOnClickListener = new DebouncedOnClickListener() {
        @Override
        public void onDebouncedClick(View v) {
            if(canHandleClick(v)) {
                onInternalClick(v);
            }
        }
    };
    private DebouncedOnTouchListener mOnTouchListener = new DebouncedOnTouchListener() {
        @Override
        public boolean onDebouncedTouch(View v, MotionEvent event) {
            if(canHandleTouch(v, event)) {
                return onInternalTouch(v, event);
            }
            return false;
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException("Activity must be OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) activity;
        } else {
            throw new RuntimeException("Activity must be OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        onFragmentActivityCreated(savedInstanceState);

        // Delay load data
        if (mLoadDataRunnable != null) {
            long interval = delayInterval();
            Handler handler = getHandler();
            handler.removeCallbacks(mLoadDataRunnable);
            handler.postDelayed(mLoadDataRunnable, interval);
        } else {
            loadData(null);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIsDestroyView = true;

        if(mImageViews != null){
            for (int i = 0; i < mImageViews.size(); i++) {
                WeakReference<ImageView> viewWeakReference = mImageViews.get(i);
                ImageView imageView = viewWeakReference.get();
                if(imageView != null){
                    imageView.setImageDrawable(null);
                }
            }
            mImageViews.clear();
            mImageViews = null;
        }
        if(mMapEditTexts != null){
            if(mMapEditTexts.size() > 0) {
                for (Map.Entry<String, List<WeakReference<EditText>>> entry : mMapEditTexts.entrySet()) {
                    List<WeakReference<EditText>> editTexts = entry.getValue();
                    if (editTexts != null && editTexts.size() > 0) {
                        for (int i = 0; i < editTexts.size(); i++) {
                            WeakReference<EditText> weakReference = editTexts.get(i);
                            EditText editText = weakReference.get();
                            if (editText != null) {
                                editText.setCursorVisible(false);
                                editText.setOnEditorActionListener(null);
                            }
                        }
                    }
                }
                mMapEditTexts.clear();
            }
            mMapEditTexts = null;
        }
        if(mUnBinder != null){
            mUnBinder.unbind();
            mUnBinder = null;
        }
        mOnClickListener = null;
        mOnTouchListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mNeedCreateSubscription = true;
            mCompositeSubscription = null;
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }

        mLogManager.log(getFragmentName() + " onDestroy");

        // Ref watcher
        Activity activity = getActivity();
        if(activity != null) {
            AppApplication.getInstance(activity).watch(this, getFragmentName());
        }
        // Clear memory
        SystemUtil.executeJavaVmGC();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Should NOT call onFragmentResume() here
    }

    @Override
    public void onPause() {
        super.onPause();
        // Should NOT call onFragmentPause() here
    }

    /**
     * Called when activity is created. Use this to catch onActivityCreated instead of override onActivityCreated method
     **/
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        setupFragmentComponent(savedInstanceState);

        mLogManager.log(getFragmentName() + " onFragmentActivityCreated");

        View root = getView();
        if (root != null) {
            root.setClickable(true);
        }

        mLoadDataRunnable = new LoadDataRunnable(savedInstanceState);
    }

    /**
     * Called when fragment is paused. Use this to catch pause event instead of override onPause method
     **/
    public void onFragmentPause(Bundle data) {
        if (BuildConfig.DEBUG) {
            Log.d("BaseLogManager", getFragmentName() + " onFragmentPause");
        }
    }

    /**
     * Called when fragment is resumed. Use this to catch resume event instead of override onResume method
     **/
    public void onFragmentResume(Bundle data) {
        if (BuildConfig.DEBUG) {
            Log.d("BaseLogManager", getFragmentName() + " onFragmentResume");
        }
    }

    /** Should we use popup animation, default is False **/
    public boolean isPopupStyle(){
        return false;
    }

    /** Should we load the background, default is False **/
    public boolean isDelayLoadBackground(){
        return false;
    }

    /**
     * Override this method to handle Back Press event
     **/
    public Bundle beforeBackPress() {
        return null;
    }

    /**
     * Override this method to handle Back Press event
     **/
    public boolean handleBackPress() {
        return false;
    }

    /**
     * Override this method to handle Global Layout event
     **/
    public void onGlobalLayout() {
    }

    /**
     * Fragment should load it's data in this method
     */
    public void loadData(Bundle data){
    }

    /**
     * Fragment should refresh it's data in this method
     */
    public void refreshData(Bundle data){

    }

    /**
     * Override this method to change request data delay interval
     **/
    protected long delayInterval() {
        return SystemConst.REQUEST_DATA_DELAY;
    }

    protected Handler getHandler(){
        if(mHandler == null){
            mHandler = new Handler();
        }
        return mHandler;
    }

    protected AppComponent getActivityComponent() {
        if (mListener == null) {
            throw new RuntimeException("Activity Listener is NULL");
        }
        return mListener.getMainActivityComponent();
    }

    private CompositeSubscription getCompositeSubscription(){
        if(mCompositeSubscription == null || mNeedCreateSubscription) {
            mNeedCreateSubscription = false;
            mCompositeSubscription = new CompositeSubscription();
        }
        return mCompositeSubscription;
    }

    public void addSubscription(Subscription subscription) {
        getCompositeSubscription().add(subscription);
    }

    protected abstract void setupFragmentComponent(Bundle savedInstanceState);

    public abstract String getFragmentName();

    public abstract int getFragmentId();

    @Override
    public void onClick(View v) {
        // DO NOT override this method, use onInternalClick instead
        if(mOnClickListener != null) {
            mOnClickListener.onClick(v);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // DO NOT override this method, use onInternalTouch instead
        return mOnTouchListener != null && mOnTouchListener.onTouch(v, event);
    }

    /**
     * Override this method to check if user can click
     **/
    protected boolean canHandleClick(View v) {
        if(isLoadingData()) {
            return false;
        }
        return true;
    }

    /**
     * Override this method to get onClick event
     **/
    protected void onInternalClick(View v) {
    }

    /**
     * Override this method to check if user can touch
     **/
    protected boolean canHandleTouch(View v, MotionEvent event) {
        if(isLoadingData()) {
            return false;
        }
        return true;
    }

    /**
     * Override this method to get onTouch event
     **/
    protected boolean onInternalTouch(View v, MotionEvent event) {
        return false;
    }

    /** Progress bar is shown for retrieving data **/
    public boolean isLoadingData() {
        if (mListener != null) {
            return mListener.isLoadingData();
        }
        return false;
    }

    public void logout() {
        if (mListener != null) {
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_LOGOUT, null);
        }
    }

    public interface LoadCoverImageListener{
        void onSuccess();
        void onError();
    }

    private class LoadDataRunnable implements Runnable {
        Bundle bundle;

        public LoadDataRunnable(Bundle bundle) {
            this.bundle = bundle;
        }

        @Override
        public void run() {
            if(isAdded() && !mIsDestroyView) {
                loadData(bundle);
            }
        }
    };

    public void hideKeyboard() {
        if (mListener != null) {
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_HIDE_KEYBOARD, null);
        }
    }

    public void changeKeyboard(int mode) {
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putInt(OnFragmentInteractionListener.DATA_MODE_INT_EXTRA, mode);
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_CHANGE_KEYBOARD, data);
        }
    }

    public void registerGlobalLayoutListener(int page) {
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page);
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_REGISTER_GLOBAL_LAYOUT_LISTENER, data);
        }
    }

    public void unregisterGlobalLayoutListener(int page) {
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page);
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_UNREGISTER_GLOBAL_LAYOUT_LISTENER, data);
        }
    }

    public void updateUser() {
        if (mListener != null) {
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_USER_UPDATE, null);
        }
    }

    public void showError(String message) {
        if (mListener != null) {
            mListener.showError(message);
        }
    }

    public void showError(@StringRes int id) {
        if (mListener != null) {
            mListener.showError(id);
        }
    }

    public void showError(Throwable t, @StringRes int id) {
        if (mListener != null) {
            mListener.showError(t, id);
        }
    }

    public void showError(BaseResponse response, @StringRes int id) {
        if (mListener != null) {
            mListener.showError(response, id);
        }
    }

    public void showError(BaseResponse response, String message) {
        if (mListener != null) {
            mListener.showError(response, message);
        }
    }

    public void showMessage(String message) {
        if (mListener != null) {
            mListener.showMessage(message);
        }
    }

    public void showMessage(@StringRes int id) {
        if (mListener != null) {
            mListener.showMessage(id);
        }
    }

    public void showToast(String message) {
        if (mListener != null) {
            mListener.showToast(message);
        }
    }

    public void showToast(@StringRes int id) {
        if (mListener != null) {
            mListener.showToast(id);
        }
    }

    public void displayProgress(boolean show) {
        if (mListener != null) {
            mListener.displayProgress(show);
        }
    }

    public void displayMessage(boolean show, @StringRes int id) {
        if (mListener != null) {
            mListener.displayMessage(show, id);
        }
    }

    public void displayMessage(boolean show, String message) {
        if (mListener != null) {
            mListener.displayMessage(show, message);
        }
    }

    public void openPage(int page) {
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page);
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_OPEN_PAGE, data);
        }
    }

    public void openPage(MainActivity.Pages page) {
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page.ordinal());
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_OPEN_PAGE, data);
        }
    }

    public void openPage(int page, @NonNull Bundle data) {
        if (mListener != null) {
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page);
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_OPEN_PAGE, data);
        }
    }

    public void menuClick() {
        if (mListener != null) {
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_MENU_CLICK, null);
        }
    }

    public void backClick(MainActivity.Pages page) {
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page.ordinal());
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_BACK_CLICK, data);
        }
    }

    public void backClick(int page) {
        if (mListener != null) {
            Bundle data = new Bundle();
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page);
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_BACK_CLICK, data);
        }
    }

    public void backClick(int page, @NonNull Bundle data) {
        if (mListener != null) {
            data.putInt(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page);
            mListener.onFragmentInteraction(OnFragmentInteractionListener.ACTION_BACK_CLICK, data);
        }
    }

    private List<WeakReference<ImageView>> mImageViews;

    protected void addImageViews(ImageView... imageViews){
        if(imageViews == null || imageViews.length == 0) {
            return;
        }

        if(mImageViews == null){
            mImageViews = new ArrayList<>();
        }
        for (int i = 0; i < imageViews.length; i++) {
            boolean exist = false;
            for (int i1 = 0; i1 < mImageViews.size(); i1++) {
                WeakReference<ImageView> weakReference = mImageViews.get(i1);
                ImageView imageView = weakReference.get();
                if(imageView != null && imageView == imageViews[i]){
                    exist = true;
                    break;
                }
            }
            if(!exist) {
                //mImageViews.add(new WeakReference<ImageView>(imageViews[i]));
            }
        }
    }

    private Map<String, List<WeakReference<EditText>>> mMapEditTexts;

    protected void addEditTexts(String group, EditText... editTexts){
        if(editTexts == null || editTexts.length == 0) {
            return;
        }

        if(mMapEditTexts == null){
            mMapEditTexts = new HashMap<>();
        }

        List<WeakReference<EditText>> list = mMapEditTexts.get(group);
        if(list == null){
            list = new ArrayList<>();
            mMapEditTexts.put(group, list);
        }
        for(EditText editText : editTexts){
            boolean exist = false;
            for (int i = 0; i < list.size(); i++) {
                WeakReference<EditText> item = list.get(i);
                EditText editText1 = item.get();
                if(editText1 != null && editText1 == editText){
                    exist = true;
                    break;
                }
            }
            if(!exist){
                editText.setTag(group);
                editText.setOnEditorActionListener(
                        new EditText.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                onInternalEditorAction(v, actionId, event);
                                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                                    String group = v.getTag() != null ? v.getTag().toString() : "";
                                    List<WeakReference<EditText>> list1 = mMapEditTexts.get(group);
                                    if(list1 != null && list1.size() > 0){
                                        int pos = -1;
                                        for(int i = 0; i < list1.size(); i++){
                                            if(list1.get(i) != null && list1.get(i).get() == v){
                                                pos = i;
                                                break;
                                            }
                                        }
                                        if(pos != -1 && pos < list1.size() - 1){
                                            EditText editText1 = list1.get(pos + 1).get();
                                            if(editText1 != null) {
                                                editText1.requestFocus();
                                            }
                                            return true;
                                        }
                                    }
                                }
                                return false;
                            }
                        });

                list.add(new WeakReference<EditText>(editText));
            }
        }
    }

    /**
     * Override this method to catch editor action
     */
    protected boolean onInternalEditorAction(TextView v, int actionId, KeyEvent event) {
        return true;
    }
}

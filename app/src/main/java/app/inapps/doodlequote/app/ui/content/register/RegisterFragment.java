package app.inapps.doodlequote.app.ui.content.register;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.RegisterResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.util.CoreUtil;
import app.inapps.doodlequote.core.widget.DefaultButton;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultSpinner;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class RegisterFragment extends BaseFragment {
    public static final String TAG = RegisterFragment.class.getSimpleName();

    @BindView(R.id.ivCoverImage) ImageView mIvCoverImage;
    @BindView(R.id.ivLogoImage) ImageView mIvLogoImage;
    @BindView(R.id.edt_your_name) DefaultEditText mEdtYourName;
    @BindView(R.id.edt_password) DefaultEditText mEdtPassword;
    @BindView(R.id.edt_email) DefaultEditText mEdtEmail;
    @BindView(R.id.edt_phone) DefaultEditText mEdtPhone;
    @BindView(R.id.btn_create_account) DefaultButton mBtnCreateAccount;
    @BindView(R.id.txtVersion) DefaultTextView mTxtVersion;
    @BindView(R.id.txtCopyrightInfo) DefaultTextView mTxtCopyrightInfo;
    @BindView(R.id.llLogin) LinearLayout mLlLogin;
    @BindView(R.id.llHelp) LinearLayout mLlHelp;
    @BindView(R.id.spUserType) DefaultSpinner mSpUserType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        //mPicasso.load(R.drawable.back_ground).fit().into(mIvCoverImage);
        //mPicasso.load(R.drawable.logo).fit().into(mIvLogoImage);
        mTxtVersion.setText(getString(R.string.app_version, CoreUtil.getAppVersion(getActivity())));

        mBtnCreateAccount.setOnClickListener(this);
        mTxtCopyrightInfo.setOnClickListener(this);
        mLlHelp.setOnClickListener(this);
        mLlLogin.setOnClickListener(this);

        List<String> list = new ArrayList<String>();
        list.add(getString(R.string.individual));
        list.add(getString(R.string.company));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpUserType.setAdapter(dataAdapter);

        addEditTexts("1", mEdtYourName, mEdtPassword, mEdtEmail, mEdtPhone);
        addImageViews(mIvLogoImage, mIvCoverImage);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.Register.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mLlLogin) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mBtnCreateAccount) {
            register();
        } else if (v == mTxtCopyrightInfo) {

        } else if (v == mLlLogin) {
            openPage(MainActivity.Pages.Login);
        } else if (v == mLlHelp) {

        }
    }

    private void register() {
        String name = mEdtYourName.getText().toString();
        final String email = mEdtEmail.getText().toString();
        String password = mEdtPassword.getText().toString();
        String phone = mEdtPhone.getText().toString();
        if (TextUtils.isEmpty(name)) {
            showError(R.string.error_name_require);
            return;
        }
        if (TextUtils.isEmpty(email)) {
            showError(R.string.error_email_require);
            return;
        }
        if (!ValidatorUtil.isValidEmail(email)) {
            showError(R.string.error_email_invalid);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            showError(R.string.error_pin_require);
            return;
        }
        if (!AppUtil.isValidPassword(password)) {
            showError(R.string.error_pin_invalid);
            return;
        }
        if (TextUtils.isEmpty(phone)) {
            showError(R.string.error_phone_require);
            return;
        }
        if (!ValidatorUtil.isValidPhone(phone)) {
            showError(R.string.error_phone_invalid);
            return;
        }
        int type = AppConst.LoginType.User.getType();
        switch (mSpUserType.getSelectedItemPosition()) {
            case 0:
                type = AppConst.LoginType.User.getType();
                break;
            case 1:
                type = AppConst.LoginType.Company.getType();
                break;
        }
        displayProgress(true);
        Subscription subscription = mApiManager.register(email, name, password, password, phone, String.valueOf(type)).subscribe(new SimpleObserver<RegisterResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(RegisterResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    mUserPreferences.setEmail(email);
                    mContainerMap.setString(AppConst.KEY_EMAIL, email);
                    showToast(R.string.register_success);
                    openPage(MainActivity.Pages.LoginPin);
                } else {
                    showError(response, R.string.error_register);
                }
            }
        });
        addSubscription(subscription);
    }
}

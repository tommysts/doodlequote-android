package app.inapps.doodlequote.app.ui.content.user_profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.GetUserProfileResponse;
import app.inapps.doodlequote.app.api.response.UpdateUserProfileResponse;
import app.inapps.doodlequote.app.api.response.UserProfileResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.product.UploadImageItem;
import app.inapps.doodlequote.app.ui.widget.BrowsePopup;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.ImageItem;
import app.inapps.doodlequote.core.util.DisplayImageUtil;
import app.inapps.doodlequote.core.widget.CircleImageView;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class UserProfileFragment extends BaseFragment implements ImagePickerCallback{
    public static final String TAG = UserProfileFragment.class.getSimpleName();

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.ivAvatar) CircleImageView mIvAvatar;
    @BindView(R.id.tvName) DefaultTextView mTvName;
    @BindView(R.id.edtFirstName) DefaultEditText mEdtFirstName;
    @BindView(R.id.edtLastName) DefaultEditText mEdtLastName;
    @BindView(R.id.edtMobile) DefaultEditText mEdtMobile;
    @BindView(R.id.ivUpdate) RecyclingImageView mIvUpdate;
    @BindView(R.id.rlUpdate) RelativeLayout mRlUpdate;
    @BindView(R.id.ivEditAvatar) RecyclingImageView mIvEditAvatar;

    private UploadImageItem mUploadImageItem;
    private String mPickerPath;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraPicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);
        mIvUpdate.setOnClickListener(this);
        mIvEditAvatar.setOnClickListener(this);

        addImageViews(mIvAvatar, mIvUpdate);
        addEditTexts("1", mEdtFirstName, mEdtLastName, mEdtMobile);

        mIvAvatar.setImageResource(R.drawable.ic_avatar);

        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.userProfile(token).subscribe(new SimpleObserver<GetUserProfileResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(GetUserProfileResponse getUserProfileResponse) {
                super.onNext(getUserProfileResponse);
                displayProgress(false);
                if (getUserProfileResponse.isSuccess()) {
                    UserProfileResponse response = getUserProfileResponse.getUser();
                    if (response != null) {
                        mTvName.setText(response.getName());
                        mEdtFirstName.setText(response.getFirstName());
                        mEdtLastName.setText(response.getLastName());
                        mEdtMobile.setText(response.getMobile());
                        if (mUploadImageItem == null && !TextUtils.isEmpty(response.getUserImage())) {
                            String imageUrl = AppUtil.getAvatarImageUrl(getString(R.string.app_end_point), response.getId().toString(), response.getUserImage());
                            mPicasso.load(imageUrl).resize(ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_AVATAR_WIDTH), 0).into(mIvAvatar);
                        }
                    }
                } else {
                    showError(getUserProfileResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(AppConst.KEY_PICKER_PATH)) {
                mPickerPath = savedInstanceState.getString(AppConst.KEY_PICKER_PATH);
            }
        }
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.UserProfile.ordinal();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (mImagePicker == null) {
                    mImagePicker = new ImagePicker(this);
                    mImagePicker.shouldGenerateMetadata(true);
                    mImagePicker.shouldGenerateThumbnails(false);
                    mImagePicker.setImagePickerCallback(this);
                }
                mImagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (mCameraPicker == null) {
                    mCameraPicker = new CameraImagePicker(this);
                    mCameraPicker.setImagePickerCallback(this);
                    mCameraPicker.shouldGenerateMetadata(true);
                    mCameraPicker.shouldGenerateThumbnails(false);
                    mCameraPicker.reinitialize(mPickerPath);
                }
                mCameraPicker.submit(data);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(AppConst.KEY_PICKER_PATH, mPickerPath);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        if(images != null && images.size() > 0) {
            displayProgress(true);
            ImageItem imageItem = AppUtil.createImageItem(images.get(0));
            Subscription subscription = DisplayImageUtil.rxOptimizeUploadImage(getActivity(), imageItem).subscribe(new SimpleObserver<ImageItem>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    showToast(R.string.error_loading_data);
                    displayProgress(false);
                }

                @Override
                public void onNext(ImageItem item) {
                    super.onNext(item);
                    loadImageFromPath(item);
                    mLogManager.log("File Path: " + item);
                    displayProgress(false);
                }
            });
            addSubscription(subscription);
        }
    }

    @Override
    public void onError(String message) {
       showToast(message);
    }

    @Override
    protected boolean canHandleClick(View v) {
        if (v == mRlNavHome) {
            return true;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            menuClick();
        } else if (v == mIvUpdate) {
            update();
        } else if (v == mIvEditAvatar) {
            browse();
        }
    }

    public void update() {
        final String firstName = mEdtFirstName.getText().toString();
        final String lastName = mEdtLastName.getText().toString();
        final String mobile = mEdtMobile.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            showError(R.string.error_first_name_require);
            return;
        }
        if (TextUtils.isEmpty(lastName)) {
            showError(R.string.error_last_name_require);
            return;
        }
        if (TextUtils.isEmpty(mobile)) {
            showError(R.string.error_mobile_require);
            return;
        }
        if (!ValidatorUtil.isValidPhone(mobile)) {
            showError(R.string.error_mobile_invalid);
            return;
        }
        displayProgress(true);
        final String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        if(mUploadImageItem == null) {
            Subscription subscription = mApiManager.updateUserProfile(token, 0, firstName, lastName, mobile, null, null, false).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    displayProgress(false);
                    showError(t, R.string.error_loading_data);
                }

                @Override
                public void onNext(UpdateUserProfileResponse userProfileResponse) {
                    super.onNext(userProfileResponse);
                    displayProgress(false);
                    if (userProfileResponse.isSuccess()) {
                        showToast(R.string.update_success);
                    } else {
                        showError(userProfileResponse, R.string.error_loading_data);
                    }
                }
            });
            addSubscription(subscription);
        } else {
            Subscription subscription1 = mApiManager.uploadUserAvatar(token, 0, mUploadImageItem.getPath()).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mLogManager.log(t);
                    displayProgress(false);
                    showError(t, R.string.error_loading_data);
                }

                @Override
                public void onNext(UpdateUserProfileResponse userProfileResponse) {
                    super.onNext(userProfileResponse);
                    if (userProfileResponse.isSuccess()) {
                        Subscription subscription2 = mApiManager.updateUserProfile(token, 0, firstName, lastName, mobile, null, null, false).subscribe(new SimpleObserver<UpdateUserProfileResponse>() {
                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                                mLogManager.log(t);
                                displayProgress(false);
                                showError(t, R.string.error_loading_data);
                            }

                            @Override
                            public void onNext(UpdateUserProfileResponse userProfileResponse) {
                                super.onNext(userProfileResponse);
                                displayProgress(false);
                                if (userProfileResponse.isSuccess()) {
                                    showToast(R.string.update_success);
                                } else {
                                    showError(userProfileResponse, R.string.error_loading_data);
                                }
                            }
                        });
                        addSubscription(subscription2);
                    } else {
                        displayProgress(false);
                        showError(userProfileResponse, R.string.error_loading_data);
                    }
                }
            });
            addSubscription(subscription1);
        }
    }

    private void browse() {
        BrowsePopup browsePopup = new BrowsePopup(getActivity());
        browsePopup.show();
        browsePopup.setListener(new BrowsePopup.BrowsePopupListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onTakePhotoClick(Dialog dialog) {
                dialog.dismiss();
                takePicture();
            }

            @Override
            public void onOpenPhotosClick(Dialog dialog) {
                dialog.dismiss();
                openPhotos();
            }

            @Override
            public void onTakeVideoClick(Dialog dialog) {

            }

            @Override
            public void onOpenVideosClick(Dialog dialog) {

            }

            @Override
            public void onTakeRecordClick(Dialog dialog) {

            }

            @Override
            public void onOpenRecordsClick(Dialog dialog) {

            }
        });
    }

    private void takePicture() {
        if(!AppUtil.verifyCameraPermissions(getActivity())) {
            return;
        }
        mCameraPicker = new CameraImagePicker(this);
        mCameraPicker.shouldGenerateMetadata(true);
        mCameraPicker.shouldGenerateThumbnails(false);
        mCameraPicker.setImagePickerCallback(this);
        mPickerPath = mCameraPicker.pickImage();
    }

    private void openPhotos() {
        if(!AppUtil.verifyStoragePermissions(getActivity())) {
            return;
        }
        mImagePicker = new ImagePicker(this);
        mImagePicker.shouldGenerateMetadata(true);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.pickImage();
    }

    private void loadImageFromPath(ImageItem imageItem) {
        mUploadImageItem = new UploadImageItem();
        mUploadImageItem.setPath(imageItem.getPath());
        File file = new File(imageItem.getPath());
        if(file.exists()) {
            mPicasso.load(file).resize(ViewUtil.convertDpToPixel(getActivity(), AppConst.DEFAULT_AVATAR_WIDTH), 0).into(mIvAvatar);
        } else {
            mIvAvatar.setImageResource(R.drawable.ic_avatar);
        }
    }
}

package app.inapps.doodlequote.app.ui.content.product;

import android.annotation.TargetApi;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.text.NumberFormat;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.common.util.StringUtil;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultLinearLayout;
import app.inapps.doodlequote.core.widget.DefaultViewListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProductCreateHeaderView extends DefaultLinearLayout {
    @BindView(R.id.edtName) DefaultEditText mEdtName;
    @BindView(R.id.edtPrice) DefaultEditText mEdtPrice;
    @BindView(R.id.edtDescription) DefaultEditText mEdtDescription;
    @BindView(R.id.ivBrowse) DefaultImageView mIvBrowse;

    interface ProductCreateHeaderViewListener extends DefaultViewListener {
        void onBrowseClick();
    }

    private Unbinder mUnbinder;

    public ProductCreateHeaderView(Context context) {
        super(context);
    }

    public ProductCreateHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProductCreateHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ProductCreateHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void setup() {
        super.setup();
        mUnbinder = ButterKnife.bind(this, this);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(mUnbinder != null) {
            mEdtPrice.removeTextChangedListener(mTextWatcher);
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public ImageView[] getImageViews() {
        return null;
    }

    public EditText[] getEditTexts() {
        return new EditText[]{mEdtName, mEdtPrice, mEdtDescription};
    }

    public String getProductName() {
        return mEdtName.getText().toString();
    }

    public String getProductPrice() {
        return mEdtPrice.getText().toString();
    }

    public String getProductDescription() {
        return mEdtDescription.getText().toString();
    }

    public void setupPriceFormatter() {
        mEdtPrice.addTextChangedListener(mTextWatcher);
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        NumberFormat numberFormat = AppUtil.getCurrencyInstance();
        String current = "";
        String clearStringPattern = ",";
        String replacePattern = String.format("(^%s)|(\\.0+$)", AppUtil.getRegexCurrencySymbol());

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String input = s.toString();
            if(!input.equals(current)) {
                mEdtPrice.removeTextChangedListener(this);
                String cleanString = input.replaceAll(clearStringPattern, "");
                cleanString = StringUtil.formatNumber(cleanString);
                String formatted = "";
                if(cleanString.contains(".")) {
                    formatted = input;
                } else {
                    if (TextUtils.isEmpty(cleanString)) {
                        cleanString = "0";
                    }
                    double parsed = DataUtil.getDouble(cleanString, -1);
                    if(parsed != -1) {
                        try {
                            formatted = numberFormat.format(parsed).replaceAll(replacePattern, "");
                            formatted = StringUtil.formatNumber(formatted);
                        } catch (Exception e) {
                            if(BuildConfig.DEBUG) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                current = formatted;
                mEdtPrice.setText(formatted);
                mEdtPrice.setSelection(formatted.length());
                mEdtPrice.addTextChangedListener(this);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void registerActions() {
        mIvBrowse.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if(mListener instanceof ProductCreateHeaderViewListener) {
                    ((ProductCreateHeaderViewListener) mListener).onBrowseClick();
                }
            }
        });
    }
}

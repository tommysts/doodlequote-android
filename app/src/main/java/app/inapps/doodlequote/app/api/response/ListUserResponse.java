package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class ListUserResponse extends BaseResponse {
    @SerializedName("users")
    @Expose
    private List<UserProfileResponse> users;

    public List<UserProfileResponse> getUsers() {
        return users;
    }

    public void setUsers(List<UserProfileResponse> users) {
        this.users = users;
    }
}

package app.inapps.doodlequote.app.ui.content.buy_credits;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.core.widget.DefaultCheckbox;
import app.inapps.doodlequote.core.widget.DefaultLinearLayout;
import app.inapps.doodlequote.core.widget.DefaultViewListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BuyCreditsFooterView extends DefaultLinearLayout {

    @BindView(R.id.cbTerms) DefaultCheckbox mCbTerms;

    interface BuyCreditsFooterViewListener extends DefaultViewListener {
        void onCheckChanged(boolean checked);
    }

    private Unbinder mUnbinder;

    public BuyCreditsFooterView(Context context) {
        super(context);
    }

    public BuyCreditsFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BuyCreditsFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public BuyCreditsFooterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void setup() {
        super.setup();
        mUnbinder = ButterKnife.bind(this, this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    public ImageView[] getImageViews() {
        return null;
    }

    public EditText[] getEditTexts() {
        return new EditText[]{};
    }

    public void registerAction() {
        mCbTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mListener instanceof BuyCreditsFooterViewListener) {
                    ((BuyCreditsFooterViewListener) mListener).onCheckChanged(isChecked);
                }
            }
        });
    }
}

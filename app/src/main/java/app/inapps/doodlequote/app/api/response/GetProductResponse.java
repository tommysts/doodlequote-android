package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class GetProductResponse extends BaseResponse {
    @SerializedName("product")
    @Expose
    private QuoteProductResponse product;

    public QuoteProductResponse getProduct() {
        return product;
    }

    public void setProduct(QuoteProductResponse product) {
        this.product = product;
    }
}

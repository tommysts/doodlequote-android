package app.inapps.doodlequote.app.util;

import android.text.TextUtils;

import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.prefs.UserPreferences;
import app.inapps.doodlequote.core.ContainerMap;

public class ContainerMapUtil {
    private ContainerMapUtil(){}

    public static void populateContainerFromPrefs(ContainerMap containerMap, UserPreferences userPreferences) {
        String token = userPreferences.getToken();
        containerMap.setBoolean(AppConst.KEY_LOG_IN, !TextUtils.isEmpty(token));
        containerMap.setString(AppConst.KEY_TOKEN, token);
        containerMap.setInt(AppConst.KEY_USER_TYPE, userPreferences.getLoginType());
        containerMap.setString(AppConst.KEY_EMAIL, userPreferences.getEmail());
    }

    public static void populatePrefFromContainer(UserPreferences userPreferences, ContainerMap containerMap) {
    }
}

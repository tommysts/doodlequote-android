package app.inapps.doodlequote.app.ui.content.quotes;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.CustomViewHolder> {
    private List<QuotesItem> mItems;
    private List<QuotesItem> mDisplayItems;
    private Context mContext;
    private AdapterListener mListener;
    private int mColorMessage;
    private int mColorApproved;
    private int mColorCallback;
    private int mColorRejected;
    private int mColor1;
    private int mColor2;

    public interface AdapterListener {
        void onItemClick(int position);
    }

    public QuotesAdapter(Context context, AdapterListener listener) {
        super();
        mItems = new ArrayList<QuotesItem>();
        mDisplayItems = new ArrayList<QuotesItem>();
        mContext = context;
        mListener = listener;
        mColorMessage = mContext.getResources().getColor(R.color.quote_message);
        mColorApproved = mContext.getResources().getColor(R.color.quote_approved);
        mColorCallback = mContext.getResources().getColor(R.color.quote_callback);
        mColorRejected = mContext.getResources().getColor(R.color.quote_rejected);
        mColor1 = Color.parseColor("#ffffff");
        mColor2 = Color.parseColor("#f6f6f6");
    }

    public void clear() {
        mItems.clear();
        mDisplayItems.clear();
    }

    public void addItems(List<QuotesItem> items) {
        mItems.addAll(items);
        mDisplayItems.addAll(items);
    }

    public void updateDisplayItems() {
        mDisplayItems.clear();
        mDisplayItems.addAll(mItems);
    }

    public List<QuotesItem> getItems() {
        return mItems;
    }

    public QuotesItem getItem(int position) {
        if (position >= 0 && position < mDisplayItems.size()) {
            return mDisplayItems.get(position);
        }
        return null;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_quotes_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final QuotesItem item = getItem(position);

        holder.layout.setTag(position);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = DataUtil.getInteger(v.getTag().toString(), 0);
                mListener.onItemClick(pos);
            }
        });

        if(position % 2 == 0) {
            holder.layout.setBackgroundColor(mColor1);
        } else {
            holder.layout.setBackgroundColor(mColor2);
        }

        holder.tvName.setText(item.getName());
        switch (item.getStatus()) {
            case MessageSent:
                holder.ivQuoteStatus.setImageResource(R.drawable.ic_quotes_message_sent);
                holder.tvStatus.setText(R.string.quotes_status_message_sent);
                holder.tvStatus.setTextColor(mColorMessage);
                break;
            case Approved:
                holder.ivQuoteStatus.setImageResource(R.drawable.ic_quotes_accepted);
                holder.tvStatus.setText(R.string.quotes_status_approved);
                holder.tvStatus.setTextColor(mColorApproved);
                break;
            case Callback:
                holder.ivQuoteStatus.setImageResource(R.drawable.ic_quotes_callback);
                holder.tvStatus.setText(R.string.quotes_status_call_back);
                holder.tvStatus.setTextColor(mColorCallback);
                break;
            case Rejected:
                holder.ivQuoteStatus.setImageResource(R.drawable.ic_quotes_rejected);
                holder.tvStatus.setText(R.string.quotes_status_rejected);
                holder.tvStatus.setTextColor(mColorRejected);
                break;
            default:
                holder.ivQuoteStatus.setImageDrawable(null);
                holder.tvStatus.setText("");
                break;
        }
        holder.tvCount.setText(item.getCount());
    }

    @Override
    public int getItemCount() {
        return mDisplayItems != null ? mDisplayItems.size() : 0;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.ivQuoteStatus) DefaultImageView ivQuoteStatus;
        @BindView(R.id.tvName) DefaultTextView tvName;
        @BindView(R.id.tvCount) DefaultTextView tvCount;
        @BindView(R.id.tvStatus) DefaultTextView tvStatus;

        private CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class LoginResponse extends BaseResponse {
    @SerializedName("token")String token;
    @SerializedName("user")UserProfileResponse user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserProfileResponse getUser() {
        return user;
    }

    public void setUser(UserProfileResponse user) {
        this.user = user;
    }
}

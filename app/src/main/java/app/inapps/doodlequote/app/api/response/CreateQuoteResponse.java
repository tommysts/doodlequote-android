package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class CreateQuoteResponse extends BaseResponse {
    @SerializedName("quote_id")String quoteId;
    @SerializedName("quote")QuoteResponse quote;

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public QuoteResponse getQuote() {
        return quote;
    }

    public void setQuote(QuoteResponse quote) {
        this.quote = quote;
    }
}

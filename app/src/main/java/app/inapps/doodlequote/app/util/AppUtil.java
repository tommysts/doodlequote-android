package app.inapps.doodlequote.app.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kbeanie.multipicker.api.entity.ChosenVideo;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CardResponse;
import app.inapps.doodlequote.app.api.response.PackageResponse;
import app.inapps.doodlequote.app.api.response.QuoteProductResponse;
import app.inapps.doodlequote.app.api.response.QuoteResponse;
import app.inapps.doodlequote.app.api.response.TemplateResponse;
import app.inapps.doodlequote.app.api.response.UserProfileResponse;
import app.inapps.doodlequote.app.ui.content.buy_credits.PackageItem;
import app.inapps.doodlequote.app.ui.content.company_info.PaymentItem;
import app.inapps.doodlequote.app.ui.content.quotes.QuotesItem;
import app.inapps.doodlequote.app.ui.content.templates.TemplateItem;
import app.inapps.doodlequote.app.ui.content.user.UsersItem;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.common.util.DateUtil;
import app.inapps.doodlequote.common.util.StringUtil;
import app.inapps.doodlequote.common.util.ValidatorUtil;
import app.inapps.doodlequote.core.ContainerMap;
import app.inapps.doodlequote.core.ImageItem;
import app.inapps.doodlequote.core.VideoItem;

public class AppUtil {
    private static NumberFormat sCurrencyInstance = NumberFormat.getCurrencyInstance();

    // Storage Permissions
    private static final int REQUEST_CAMERA = 1;
    private static String[] PERMISSION_CAMERA = {
            Manifest.permission.CAMERA
    };

    public static boolean verifyCameraPermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSION_CAMERA,
                    REQUEST_CAMERA
            );
            return false;
        }
        return true;
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static boolean verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            return false;
        }

        return true;
    }

    public static NumberFormat getCurrencyInstance(){
        sCurrencyInstance.setRoundingMode(RoundingMode.UNNECESSARY);
        return sCurrencyInstance;
    }

    public static String getCurrencySymbol() {
        return getCurrencyInstance().getCurrency().getSymbol();
    }

    public static String getRegexCurrencySymbol() {
        String symbol = getCurrencyInstance().getCurrency().getSymbol();
        if("$".equals(symbol)) {
            return "\\$";
        }
        return symbol;
    }

    public static boolean isValidPassword(String password){
        return !TextUtils.isEmpty(password) && password.length() == AppConst.PASSWORD_LENGTH;
    }

    public static AppConst.QuoteStatus getQuoteStatus(QuoteResponse quoteResponse) {
        String isApproved = quoteResponse.getIsApproved();
        if("1".equals(isApproved)) {
            return AppConst.QuoteStatus.Approved;
        } else if("2".equals(isApproved)) {
            return AppConst.QuoteStatus.Rejected;
        } else if(DataUtil.isTrue(quoteResponse.getIsCallback())) {
            return AppConst.QuoteStatus.Callback;
        } else if(DataUtil.isTrue(quoteResponse.getIsSent())) {
            return AppConst.QuoteStatus.MessageSent;
        }

        return AppConst.QuoteStatus.None;
    }

    public static AppConst.ProductStatus getProductStatus(QuoteProductResponse quoteProductResponse) {
        //0 - NewItem
        //1 - Approved
        //2 - Rejected
        //3 - Completed
        String status = quoteProductResponse.getStatus();
        if("0".equals(status)) {
            return AppConst.ProductStatus.NewItem;
        } else if("1".equals(status)) {
            return AppConst.ProductStatus.Approved;
        } else if("2".equals(status)) {
            return AppConst.ProductStatus.Rejected;
        } else if("3".equals(status)) {
            return AppConst.ProductStatus.Completed;
        }
        return AppConst.ProductStatus.NewItem;
    }

    public static boolean isLogged(ContainerMap containerMap) {
        return containerMap.getBoolean(AppConst.KEY_LOG_IN);
    }

    public static boolean isRegistered(ContainerMap containerMap) {
        String email = containerMap.getString(AppConst.KEY_EMAIL);
        return !TextUtils.isEmpty(email);
    }

    public static AppConst.LoginType getLoginType(ContainerMap containerMap) {
        int type = containerMap.getInt(AppConst.KEY_USER_TYPE);
        if(AppConst.LoginType.Company.getType() == type) {
            return AppConst.LoginType.Company;
        } else if(AppConst.LoginType.Staff.getType() == type) {
            return AppConst.LoginType.Staff;
        }
        return AppConst.LoginType.User;
    }

    public static AppConst.LoginType getLoginType(UserProfileResponse userResponse) {
        int type = userResponse.getType();
        if(AppConst.LoginType.Company.getType() == type) {
            return AppConst.LoginType.Company;
        } else if(AppConst.LoginType.Staff.getType() == type) {
            return AppConst.LoginType.Staff;
        }
        return AppConst.LoginType.User;
    }

    public static AppConst.CardType getCardType(CardResponse cardResponse) {
        String brand = cardResponse.getBrand();
        if(StringUtil.containsIgnoreCase(AppConst.CARD_VISA, brand)) {
            return AppConst.CardType.VISA;
        }
        if(StringUtil.containsIgnoreCase(AppConst.CARD_MASTERCARD, brand)) {
            return AppConst.CardType.MASTERCARD;
        }
        return AppConst.CardType.UNKNOWN;
    }

    public static QuotesItem createQuoteItem(QuoteResponse quoteResponse) {
        QuotesItem quotesItem = new QuotesItem();
        quotesItem.setId(quoteResponse.getId());
        quotesItem.setMobile(quoteResponse.getCustomerMobile());
        quotesItem.setName(quoteResponse.getCustomerName());
        int approved = 0;
        int total = 0;
        List<QuoteProductResponse> products = quoteResponse.getProducts();
        if(products != null) {
            total = products.size();
            for(QuoteProductResponse product : products) {
                AppConst.ProductStatus status = getProductStatus(product);
                if(status == AppConst.ProductStatus.Approved) {
                    approved++;
                }
            }
        }
        String count = String.format(Locale.getDefault(), "%1$d/%2$d", approved, total);
        if(total > 1) {
            count += " Items";
        } else {
            count += " Item";
        }
        quotesItem.setCount(count);
        quotesItem.setStatus(AppUtil.getQuoteStatus(quoteResponse));
        Date createdAt = DateUtil.getServerFormattedDate(quoteResponse.getCreatedAt());
        if(createdAt != null) {
            quotesItem.setCreatedTime(createdAt.getTime());
        }
        return quotesItem;
    }

    public static UsersItem createUserItem(UserProfileResponse userResponse) {
        UsersItem item = new UsersItem();
        item.setId(userResponse.getId());
        if(!TextUtils.isEmpty(userResponse.getFirstName()) && !TextUtils.isEmpty(userResponse.getLastName())) {
            item.setName(userResponse.getFirstName() + " " + userResponse.getLastName());
        } else if(!TextUtils.isEmpty(userResponse.getFirstName())) {
            item.setName(userResponse.getFirstName());
        } else if(!TextUtils.isEmpty(userResponse.getLastName())) {
            item.setName(userResponse.getLastName());
        } else if(!TextUtils.isEmpty(userResponse.getName())) {
            item.setName(userResponse.getName());
        }
        item.setFirstName(userResponse.getFirstName());
        item.setLastName(userResponse.getLastName());
        item.setDisabled(DataUtil.isTrue(userResponse.getIsDisabled()));
        item.setEmail(userResponse.getEmail());
        item.setMobile(userResponse.getMobile());
        item.setTitle(userResponse.getTitle());
        item.setImageName(userResponse.getUserImage());
        item.setType(userResponse.getType());
        item.setUsedCredit(userResponse.getUsedCredit());
        Date createdAt = DateUtil.getServerFormattedDate(userResponse.getCreatedAt());
        if(createdAt != null) {
            item.setCreatedTime(createdAt.getTime());
        }
        return item;
    }

    public static TemplateItem createTemplateItem(TemplateResponse templateResponse) {
        TemplateItem item = new TemplateItem();
        item.setId(templateResponse.getId());
        item.setDescription(templateResponse.getBody());
        item.setCanDelete(true);
        Date createdAt = DateUtil.getServerFormattedDate(templateResponse.getCreatedAt());
        if(createdAt != null) {
            item.setCreatedTime(createdAt.getTime());
        }
        return item;
    }

    public static PaymentItem createPaymentItem(CardResponse cardResponse) {
        PaymentItem paymentItem = new PaymentItem();
        paymentItem.setCardId(cardResponse.getId());
        AppConst.CardType cardType = AppUtil.getCardType(cardResponse);
        if (cardType == AppConst.CardType.VISA) {
            paymentItem.setCardType("VISA");
        } else if (cardType == AppConst.CardType.MASTERCARD) {
            paymentItem.setCardType("MASTER CARD");
        }
        paymentItem.setCardNumber(AppConst.CARD_NUMBER + cardResponse.getLast4());
        paymentItem.setExpiry(cardResponse.getExpMonth() + "/" + cardResponse.getExpYear());

        return paymentItem;
    }

    public static PackageItem createPackageItem(PackageResponse packageResponse) {
        PackageItem packageItem = new PackageItem();
        packageItem.setId(packageResponse.getId());
        packageItem.setAmount(packageResponse.getAmount());
        packageItem.setPrice(packageResponse.getPrice());
        return packageItem;
    }

    public static ImageItem createImageItem(ChosenImage chosenImage) {
        return new ImageItem(chosenImage.getOriginalPath(), chosenImage.getOrientation());
    }

    public static VideoItem createVideoItem(ChosenVideo chosenVideo) {
        return new VideoItem(chosenVideo.getOriginalPath(), chosenVideo.getWidth(),
                chosenVideo.getHeight(), chosenVideo.getDuration(), chosenVideo.getPreviewImage(), chosenVideo.getOrientation());
    }

    public static List<String> getImages(QuoteProductResponse quoteProductResponse) {
        String images = quoteProductResponse.getImages();
        if(TextUtils.isEmpty(images)) {
            return null;
        }
        String[] values = images.split(",");
        List<String> items = new ArrayList<>();
        for(String value : values) {
            items.add(value);
        }
        return items;
    }

    public static List<String> getFixedImages(QuoteProductResponse quoteProductResponse) {
        String images = quoteProductResponse.getFixedImages();
        if(TextUtils.isEmpty(images)) {
            return null;
        }
        String[] values = images.split(",");
        List<String> items = new ArrayList<>();
        for(String value : values) {
            items.add(value);
        }
        return items;
    }

    public static String getImageUrl(String domain, String quoteId, String productId, String imageName) {
        return domain + "/media/quote/" + quoteId + "/" + productId + "/" + imageName;
    }

    public static String getAvatarImageUrl(String domain, String userId, String imageName) {
        return domain + "/media/user/" + userId + "/" + imageName;
    }

    public static String formatPrice(String price) {
        double value = DataUtil.getDouble(price, 0);
        return String.format(Locale.getDefault(), "$%,.2f", value);
    }

    public static String formatPrice(double price) {
        return String.format(Locale.getDefault(), "$%,.2f", price);
    }

    public static String trimCommaOfString(String string) {
        if (string.contains(",")) {
            return string.replaceAll(",", "");
        } else {
            return string;
        }
    }

    public static String optimizeMobile(String value) {
        if(!ValidatorUtil.isValidPhone(value)) {
            value = AppConst.DEFAULT_COUNTRY_CODE + value.replaceAll("^0+", "");
        }
        return value;
    }

    public static String formatUploadTime(String value) {
        Date date = DateUtil.getServerFormattedDate(value);
        if(date != null) {
            Calendar calendar = Calendar.getInstance();
            int cYear = calendar.get(Calendar.YEAR);
            int cMonth = calendar.get(Calendar.MONTH);
            int cDay = calendar.get(Calendar.DAY_OF_MONTH);
            calendar.setTime(date);
            int nYear = calendar.get(Calendar.YEAR);
            int nMonth = calendar.get(Calendar.MONTH);
            int nDay = calendar.get(Calendar.DAY_OF_MONTH);
            if(cYear == nYear && cMonth == nMonth && cDay == nDay) {
                return "Today " + DateUtil.format(date, DateUtil.FMT_HH_MM);
            } else if(cYear == nYear && cMonth == nMonth && cDay == nDay + 1) {
                return "Yesterday " + DateUtil.format(date, DateUtil.FMT_HH_MM);
            } else {
                return DateUtil.format(date, DateUtil.FMT_YYYY_MM_DD_HH_MM_SS_WITH_HYPHEN);
            }
        }
        return "";
    }
}

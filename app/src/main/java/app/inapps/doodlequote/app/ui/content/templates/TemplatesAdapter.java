package app.inapps.doodlequote.app.ui.content.templates;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import app.inapps.doodlequote.core.widget.ViewUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TemplatesAdapter extends RecyclerView.Adapter<TemplatesAdapter.CustomViewHolder> {
    private List<TemplateItem> mItems;
    private List<TemplateItem> mDisplayItems;
    private Context mContext;
    private AdapterListener mListener;

    public interface AdapterListener {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public TemplatesAdapter(Context context, TemplatesAdapter.AdapterListener listener) {
        super();
        mItems = new ArrayList<TemplateItem>();
        mDisplayItems = new ArrayList<TemplateItem>();
        mContext = context;
        mListener = listener;
    }

    public void clear() {
        mItems.clear();
        mDisplayItems.clear();
    }

    public void addItems(List<TemplateItem> items) {
        mItems.addAll(items);
        mDisplayItems.addAll(items);
    }

    public void updateDisplayItems() {
        mDisplayItems.clear();
        mDisplayItems.addAll(mItems);
    }

    public List<TemplateItem> getItems() {
        return mItems;
    }

    public TemplateItem getItem(int position) {
        if (position >= 0 && position < mDisplayItems.size()) {
            return mDisplayItems.get(position);
        }
        return null;
    }

    public void deleteItem(int position) {
        TemplateItem item = mDisplayItems.get(position);
        mDisplayItems.remove(position);
        mItems.remove(item);
    }

    @Override
    public TemplatesAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_templates_item, parent, false);
        return new TemplatesAdapter.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TemplatesAdapter.CustomViewHolder holder, int position) {
        final TemplateItem item = getItem(position);

        holder.layout.setTag(position);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = DataUtil.getInteger(v.getTag().toString(), 0);
                mListener.onItemClick(pos);
            }
        });

        if(item.isCanDelete()) {
            holder.ivDelete.setVisibility(View.VISIBLE);
            holder.ivDelete.setTag(position);
            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = DataUtil.getInteger(v.getTag().toString(), 0);
                    mListener.onDeleteClick(pos);
                }
            });
        } else {
            holder.ivDelete.setVisibility(View.GONE);
        }

        holder.tvDescription.setText(item.getDescription());
        ViewUtil.setColorsText(mContext, holder.tvDescription, AppConst.TEMPLATE_KEYS, R.color.template);

        holder.tvDescription.setTag(position);
        holder.tvDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = DataUtil.getInteger(v.getTag().toString(), 0);
                mListener.onItemClick(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDisplayItems != null ? mDisplayItems.size() : 0;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.tvDescription) DefaultTextView tvDescription;
        @BindView(R.id.ivDelete) DefaultImageView ivDelete;

        private CustomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


package app.inapps.doodlequote.app.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.ui.content.company_info.PaymentItem;
import app.inapps.doodlequote.common.util.DataUtil;
import app.inapps.doodlequote.core.widget.DefaultTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CardInfoAdapter extends BaseAdapter {
    private List<PaymentItem> mItems;
    private Context mContext;
    private AdapterListener mListener;
    private int mSelectedIndex = -1;

    public interface AdapterListener {
        void onItemClick(int position);
    }

    public CardInfoAdapter(Context context, AdapterListener listener) {
        super();
        mContext = context;
        mListener = listener;
        mItems = new ArrayList<>();
    }

    public void clear() {
        mItems.clear();
    }

    public void addItem(PaymentItem item) {
        mItems.add(item);
    }

    public List<PaymentItem> getItems() {
        return mItems;
    }

    public void addItems(List<PaymentItem> items) {
        mItems.addAll(items);
    }

    public void deleteItem(int position) {
        mItems.remove(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.popup_card_info_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        PaymentItem item = (PaymentItem) getItem(position);

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.tvCardType.setText(item.getCardType());
        viewHolder.tvCardNumber.setText(item.getCardNumber());
        viewHolder.tvExpiry.setText(item.getExpiry());

        viewHolder.layout.setTag(position);
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = DataUtil.getInteger(view.getTag().toString(), 0);
                mSelectedIndex = pos;
                mListener.onItemClick(pos);
            }
        });

        if(mSelectedIndex == position) {
            viewHolder.layout.setBackgroundColor(Color.parseColor("#f6f6f6"));
        } else {
            viewHolder.layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.layout) ViewGroup layout;
        @BindView(R.id.tvCardType) DefaultTextView tvCardType;
        @BindView(R.id.tvCardNumber) DefaultTextView tvCardNumber;
        @BindView(R.id.tvExpiry) DefaultTextView tvExpiry;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}


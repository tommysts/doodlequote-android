package app.inapps.doodlequote.app.ui.content.templates;

import android.os.Parcel;
import android.os.Parcelable;

public class TemplateItem implements Parcelable {
    int id;
    String description;
    long createdTime;
    boolean canDelete = true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public TemplateItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.description);
        dest.writeLong(this.createdTime);
        dest.writeByte(this.canDelete ? (byte) 1 : (byte) 0);
    }

    protected TemplateItem(Parcel in) {
        this.id = in.readInt();
        this.description = in.readString();
        this.createdTime = in.readLong();
        this.canDelete = in.readByte() != 0;
    }

    public static final Creator<TemplateItem> CREATOR = new Creator<TemplateItem>() {
        @Override
        public TemplateItem createFromParcel(Parcel source) {
            return new TemplateItem(source);
        }

        @Override
        public TemplateItem[] newArray(int size) {
            return new TemplateItem[size];
        }
    };
}

package app.inapps.doodlequote.app.ui.content.quotes;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.QuoteResponse;
import app.inapps.doodlequote.app.api.response.ListQuoteResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.DefaultEditText;
import app.inapps.doodlequote.core.widget.RecyclingImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class QuotesFragment extends BaseFragment {
    public static final String TAG = QuotesFragment.class.getSimpleName();

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.rlEdit) RelativeLayout mRlEdit;
    @BindView(R.id.edtSearch) DefaultEditText mEdtSearch;
    @BindView(R.id.ivSort) RecyclingImageView mIvSort;
    @BindView(R.id.rcvItems) RecyclerView mRcvItems;
    @BindView(R.id.ivCheckRecent) RecyclingImageView mIvCheckRecent;
    @BindView(R.id.ivCheckAZ) RecyclingImageView mIvCheckAZ;
    @BindView(R.id.ivCheckByStatus) RecyclingImageView mIvCheckByStatus;
    @BindView(R.id.rlSortOptions) RelativeLayout mRlSortOptions;
    @BindView(R.id.ivSortBackground) RecyclingImageView mIvSortBackground;
    @BindView(R.id.vSortTouchArea) View mVSortTouchArea;
    @BindView(R.id.rlCheckRecent) RelativeLayout mRlCheckRecent;
    @BindView(R.id.rlCheckAZ) RelativeLayout mRlCheckAZ;
    @BindView(R.id.rlCheckByStatus) RelativeLayout mRlCheckByStatus;

    private QuotesAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quotes, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);
        mRlEdit.setOnClickListener(this);
        mIvSort.setOnClickListener(this);
        mRlCheckRecent.setOnClickListener(this);
        mRlCheckAZ.setOnClickListener(this);
        mRlCheckByStatus.setOnClickListener(this);
        mVSortTouchArea.setOnTouchListener(this);

        addImageViews(mIvSortBackground);
        addEditTexts("1", mEdtSearch);

        mAdapter = new QuotesAdapter(getActivity(), new QuotesAdapter.AdapterListener() {
            @Override
            public void onItemClick(int position) {
                QuotesItem quotesItem = mAdapter.getItem(position);
                mContainerMap.setInt(AppConst.KEY_QUOTE_ID, quotesItem.getId());
                mContainerMap.setString(AppConst.KEY_CUSTOMER_MOBILE, quotesItem.getMobile());
                openPage(MainActivity.Pages.QuotesDetail);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRcvItems.setLayoutManager(linearLayoutManager);
        mRcvItems.setAdapter(mAdapter);

        Subscription subscription = RxTextView.textChangeEvents(mEdtSearch)
                .skip(1)
                .debounce(AppConst.DELAY_SEARCH_INTERVAL_MILLISECONDS, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<TextViewTextChangeEvent>() {
                    @Override
                    public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                        String keyword = textViewTextChangeEvent.text().toString();
                        mLogManager.log("Search Quotes = " + keyword);
                        searchQuote(keyword);
                    }
                });
        addSubscription(subscription);

        listQuote();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavHome || v == mRlEdit) {
            return true;
        }
        return super.canHandleClick(v);
    }

    public void listQuote() {
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.listQuote(token).subscribe(new SimpleObserver<ListQuoteResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ListQuoteResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    mAdapter.clear();
                    List<QuoteResponse> quotes = response.getQuotes();
                    if(quotes != null && quotes.size() > 0) {
                        List<QuotesItem> quotesItems = new ArrayList<QuotesItem>();
                        for(QuoteResponse quoteResponse : quotes) {
                            QuotesItem quotesItem = AppUtil.createQuoteItem(quoteResponse);
                            quotesItems.add(quotesItem);
                        }
                        // Sort by Recent
                        checkSort(mIvCheckRecent, true);
                        Collections.sort(quotesItems, new Comparator<QuotesItem>() {
                            @Override
                            public int compare(QuotesItem o1, QuotesItem o2) {
                                long diff = o2.getCreatedTime() - o1.getCreatedTime();
                                if(diff > 0) {
                                    return 1;
                                } else if(diff < 0) {
                                    return  -1;
                                }
                                return 0;
                            }
                        });
                        mAdapter.addItems(quotesItems);
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    showError(response, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    public void searchQuote(String keyword) {
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.searchQuote(token, keyword).subscribe(new SimpleObserver<ListQuoteResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ListQuoteResponse response) {
                super.onNext(response);
                displayProgress(false);
                if (response.isSuccess()) {
                    mAdapter.clear();
                    List<QuoteResponse> quotes = response.getQuotes();
                    if(quotes != null && quotes.size() > 0) {
                        List<QuotesItem> quotesItems = new ArrayList<QuotesItem>();
                        for(QuoteResponse quoteResponse : quotes) {
                            QuotesItem quotesItem = AppUtil.createQuoteItem(quoteResponse);
                            quotesItems.add(quotesItem);
                        }
                        mAdapter.addItems(quotesItems);
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    showError(response, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);

        if(data != null && data.getBoolean(OnFragmentInteractionListener.DATA_REFRESH_BOOLEAN_EXTRA)) {
            listQuote();
        }
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.Quotes.ordinal();
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            menuClick();
        } else if (v == mRlEdit) {
            openPage(MainActivity.Pages.QuotesCreate);
        } else if (v == mIvSort) {
            showSortOptions(true);
        } else if (v == mRlCheckRecent) {
            checkSort(mIvCheckRecent, true);
            showSortOptions(false);
            doSort(false, true, false);
        } else if (v == mRlCheckAZ) {
            checkSort(mIvCheckAZ, true);
            showSortOptions(false);
            doSort(true, false, false);
        } else if (v == mRlCheckByStatus) {
            checkSort(mIvCheckByStatus, true);
            showSortOptions(false);
            doSort(false, false, true);
        }
    }

    @Override
    protected boolean onInternalTouch(View v, MotionEvent event) {
        if (v == mVSortTouchArea) {
            showSortOptions(false);
        }
        return super.onInternalTouch(v, event);
    }

    @Override
    protected boolean onInternalEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            hideKeyboard();
        }
        return super.onInternalEditorAction(v, actionId, event);
    }

    private void showSortOptions(boolean show) {
        boolean isShow = mRlSortOptions.getVisibility() == View.VISIBLE;
        if (show && !isShow) {
            hideKeyboard();
            mRlSortOptions.setVisibility(View.VISIBLE);
        } else if (!show && isShow) {
            mRlSortOptions.setVisibility(View.GONE);
        }
    }

    private void checkSort(ImageView iv, boolean check) {
        if(iv != mIvCheckAZ) {
            mIvCheckAZ.setImageDrawable(null);
        }
        if(iv != mIvCheckRecent) {
            mIvCheckRecent.setImageDrawable(null);
        }
        if(iv != mIvCheckByStatus) {
            mIvCheckByStatus.setImageDrawable(null);
        }
        if(check) {
            iv.setImageResource(R.drawable.ic_tick);
        } else {
            iv.setImageDrawable(null);
        }
    }

    private void doSort(final boolean sortAZ, final boolean sortRecent, final boolean sortStatus) {
        displayProgress(true);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                Collections.sort(mAdapter.getItems(), new Comparator<QuotesItem>() {
                    @Override
                    public int compare(QuotesItem o1, QuotesItem o2) {
                        if(sortAZ) {
                            return o1.getName().compareTo(o2.getName());
                        } else if(sortRecent) {
                            long diff = o2.getCreatedTime() - o1.getCreatedTime();
                            if(diff > 0) {
                                return  1;
                            } else if(diff < 0) {
                                return  -1;
                            }
                        } else if(sortStatus) {
                            return o1.getStatus().getOrder() - o2.getStatus().getOrder();
                        }
                        return 0;
                    }
                });
                subscriber.onNext(true);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SimpleObserver<Boolean>(){

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(Boolean aBoolean) {
                super.onNext(aBoolean);
                displayProgress(false);
                mAdapter.updateDisplayItems();
                mAdapter.notifyDataSetChanged();
            }
        });
        addSubscription(subscription);
    }
}

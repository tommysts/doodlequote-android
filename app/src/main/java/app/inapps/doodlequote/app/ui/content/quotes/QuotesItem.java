package app.inapps.doodlequote.app.ui.content.quotes;

import app.inapps.doodlequote.app.AppConst;

public class QuotesItem {
    private Integer id;
    private String name;
    private String mobile;
    private String count;
    private AppConst.QuoteStatus status;
    private long createdTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public AppConst.QuoteStatus getStatus() {
        return status;
    }

    public void setStatus(AppConst.QuoteStatus status) {
        this.status = status;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }
}

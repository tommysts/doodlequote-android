package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class RegisterResponse extends BaseResponse {
    @SerializedName("user_id")String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

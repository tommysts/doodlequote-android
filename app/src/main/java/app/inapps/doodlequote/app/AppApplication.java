package app.inapps.doodlequote.app;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;

import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import java.util.Date;
import java.util.List;

import app.inapps.doodlequote.BuildConfig;
import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.api.ApiModule;
import app.inapps.doodlequote.app.util.DebugUtil;
import app.inapps.doodlequote.common.util.AssertUtil;
import app.inapps.doodlequote.core.ContainerMap;
import app.inapps.doodlequote.core.application.ContextHelper;
import timber.log.Timber;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.GINGERBREAD;

public class AppApplication extends Application {
    private ContextHelper mContextHelper;
    private Date mStartDateTime;
    private AppComponent mAppComponent;
    private RefWatcher mRefWatcher;
    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        mStartDateTime = new Date();
        mContextHelper = new ContextHelper(this);
        if(BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            // Enable LeakCanary
            if(DebugUtil.isDebugMode(this)) {
                enabledStrictMode();
                mRefWatcher = LeakCanary.install(this);
            }
        }

        // Create component
        createBaseComponent(new ContainerMap());
    }

    public static AppApplication getInstance(Context context) {
        return (AppApplication)context.getApplicationContext();
    }

    protected void enabledStrictMode() {
        if (SDK_INT >= GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder() //
                    .detectAll() //
                    .penaltyLog() //
                    .penaltyDeath() //
                    .build());
        }
    }

    public void createBaseComponent(ContainerMap containerMap) {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this, containerMap))
                .apiModule(new ApiModule())
                .build();
    }

    public ContextHelper getContextHelper() {
        AssertUtil.assertNotNull(mContextHelper);
        return mContextHelper;
    }

    @NonNull
    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public Date getApplicationStartDateTime() {
        AssertUtil.assertNotNull(mStartDateTime);
        return new Date(mStartDateTime.getTime());
    }

    public boolean isForeground() {
        boolean ret = false;
        String packageName = getContextHelper().getContextWrapper().getPackageName();
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processInfoList = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : processInfoList) {
            if (info.processName.equals(packageName)) {
                if (info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    protected String[] getPermissionList() {
        return mContextHelper.getPermissionList();
    }

    public void watch(Object object){
        watch(object, "");
    }

    public void watch(Object object, String name){
        RefWatcher refWatcher = mRefWatcher;
        if(refWatcher != null){
            refWatcher.watch(object, name);
        }
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
            Thread.UncaughtExceptionHandler myHandler = new ExceptionReporter(
                    mTracker,
                    Thread.getDefaultUncaughtExceptionHandler(),
                    this);

            // Make myHandler the new default uncaught exception handler.
            Thread.setDefaultUncaughtExceptionHandler(myHandler);
        }
        return mTracker;
    }
}

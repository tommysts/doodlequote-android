package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class GetQuoteResponse extends BaseResponse {
    @SerializedName("quote")
    @Expose
    private QuoteResponse quote;

    public QuoteResponse getQuote() {
        return quote;
    }

    public void setQuote(QuoteResponse quote) {
        this.quote = quote;
    }
}

package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.SerializedName;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class CreateTemplateResponse extends BaseResponse {
    @SerializedName("template_id")String templateId;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }
}

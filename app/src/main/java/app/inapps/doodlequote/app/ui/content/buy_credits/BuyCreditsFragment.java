package app.inapps.doodlequote.app.ui.content.buy_credits;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.stripe.android.Stripe;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.util.ArrayList;
import java.util.List;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppComponent;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.api.response.CardResponse;
import app.inapps.doodlequote.app.api.response.CreateCardResponse;
import app.inapps.doodlequote.app.api.response.ListCardResponse;
import app.inapps.doodlequote.app.api.response.ListPackageResponse;
import app.inapps.doodlequote.app.api.response.PackageResponse;
import app.inapps.doodlequote.app.api.response.SubmitOrderResponse;
import app.inapps.doodlequote.app.ui.MainActivity;
import app.inapps.doodlequote.app.ui.PaymentActivity;
import app.inapps.doodlequote.app.ui.content.BaseFragment;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.ui.content.company_info.PaymentItem;
import app.inapps.doodlequote.app.ui.widget.CardInfoPopup;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.common.util.SimpleObserver;
import app.inapps.doodlequote.core.widget.AlertPopup;
import app.inapps.doodlequote.core.widget.AnimateImageView;
import app.inapps.doodlequote.core.widget.DefaultImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BuyCreditsFragment extends BaseFragment {
    public static final String TAG = BuyCreditsFragment.class.getSimpleName();
    public static final int ANDROID_PAY_REQUEST_CODE = 1000;
    public static final String ANDROID_PAY_CARD_TOKEN = "ANDROID_PAY_CARD_TOKEN";

    @BindView(R.id.rlNavHome) RelativeLayout mRlNavHome;
    @BindView(R.id.ivNavHome) DefaultImageView mIvNavHome;
    @BindView(R.id.ivBuy) AnimateImageView mIvBuy;
    @BindView(R.id.rcvItems) RecyclerView mRcvItems;

    private boolean mEditMode;
    private BuyCreditsAdapter mAdapter;
    private PackageItem mSelectedPackage;
    private CardInfoPopup mCardInfoPopup;
    private List<PaymentItem> mPaymentItems = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buy_credits, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupFragmentComponent(Bundle savedInstanceState) {
        AppComponent fragmentComponent = getActivityComponent();
        fragmentComponent.inject(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    @Override
    public void onFragmentActivityCreated(Bundle savedInstanceState) {
        super.onFragmentActivityCreated(savedInstanceState);

        mRlNavHome.setOnClickListener(this);

        Bundle data = getArguments();
        if(data != null && data.getString(OnFragmentInteractionListener.DATA_PAGE_STRING_EXTRA, "").equals(SmsCreditsFragment.TAG)) {
            mIvNavHome.setImageResource(R.drawable.ic_menu_back);
            mEditMode = true;
        }

        mIvBuy.setOnClickListener(this);
        mAdapter = new BuyCreditsAdapter(getActivity(), new BuyCreditsAdapter.AdapterListener() {
            @Override
            public void onItemClick(PackageResponse packageResponse) {
                mSelectedPackage = AppUtil.createPackageItem(packageResponse);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRcvItems.setLayoutManager(linearLayoutManager);
        mRcvItems.setAdapter(mAdapter);

        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        displayProgress(true);
        Subscription subscription = mApiManager.listPackage(token).subscribe(new SimpleObserver<ListPackageResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                displayProgress(false);
            }

            @Override
            public void onNext(ListPackageResponse listPackageResponse) {
                super.onNext(listPackageResponse);
                displayProgress(false);
                if (listPackageResponse.isSuccess()) {
                    List<PackageResponse> packageResponses = listPackageResponse.getPackages();
                    if (packageResponses != null && packageResponses.size() > 0) {
                        // header
                        CreditItem header = new CreditItem();
                        header.isHeader = true;
                        mAdapter.addItem(header);
                        // items
                        int i = 0;
                        CreditItem item = new CreditItem();
                        for(; i < packageResponses.size(); i++) {
                            if(i%2 == 0) {
                                item.package1 = packageResponses.get(i);
                            } else {
                                item.package2 = packageResponses.get(i);
                            }
                            if(i%2 != 0 || i == packageResponses.size() - 1) {
                                mAdapter.addItem(item);
                                item = new CreditItem();
                            }
                        }
                        // footer
                        CreditItem footer = new CreditItem();
                        footer.isFooter = true;
                        mAdapter.addItem(footer);
                        mAdapter.notifyDataSetChanged();
                        loadCards();
                    }
                } else {
                    showError(listPackageResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ANDROID_PAY_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                String cardToken = data.getStringExtra(ANDROID_PAY_CARD_TOKEN);
                submitOrderNoCard(cardToken, "");
            }
        }
    }

    private void loadCards() {
        displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.listCard(token).subscribe(new SimpleObserver<ListCardResponse>() {
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                displayProgress(false);
                showError(t, R.string.error_loading_data);
            }

            @Override
            public void onNext(ListCardResponse listCardResponse) {
                super.onNext(listCardResponse);
                displayProgress(false);
                if (listCardResponse.isSuccess()) {
                    List<CardResponse> cards = listCardResponse.getCards();
                    if(cards != null && cards.size() > 0) {
                        mPaymentItems.clear();
                        for(CardResponse cardResponse : cards) {
                            PaymentItem paymentItem = AppUtil.createPaymentItem(cardResponse);
                            mPaymentItems.add(paymentItem);
                        }
                        if(mCardInfoPopup != null) {
                            mCardInfoPopup.addItems(mPaymentItems);
                        }
                    }

                } else {
                    showError(listCardResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    @Override
    public void loadData(Bundle data) {
        super.loadData(data);
    }

    @Override
    public boolean handleBackPress() {
        return super.handleBackPress();
    }

    @Override
    public Bundle beforeBackPress() {
        Bundle bundle = super.beforeBackPress();
        if(mEditMode) {
            if(bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean(OnFragmentInteractionListener.DATA_IS_DRAWER_BOOLEAN_EXTRA, false);
        }
        return bundle;
    }

    @Override
    public void onFragmentResume(Bundle data) {
        super.onFragmentResume(data);
    }

    @Override
    public void onFragmentPause(Bundle data) {
        super.onFragmentPause(data);
    }

    @Override
    public int getFragmentId() {
        return MainActivity.Pages.BuyCredits.ordinal();
    }

    @Override
    protected boolean canHandleClick(View v) {
        if(v == mRlNavHome) {
            return true;
        }
        if(mCardInfoPopup != null && mCardInfoPopup.isLoading()) {
            return false;
        }
        return super.canHandleClick(v);
    }

    @Override
    public void onInternalClick(View v) {
        super.onInternalClick(v);
        if (v == mRlNavHome) {
            if(mEditMode) {
                backClick(getFragmentId());
            } else {
                menuClick();
            }
        } else if (v == mIvBuy) {
            showCard();
        }
    }

    void showCard() {
        if(mSelectedPackage == null) {
            showError("Please select 1 package");
            return;
        }
        if(!mAdapter.isCheckTerms()) {
            showError("You must agree with the terms and conditions");
            return;
        }
        mCardInfoPopup = new CardInfoPopup(getActivity());
        mCardInfoPopup.setListener(new CardInfoPopup.CardInfoPopupListener() {
            @Override
            public void onStart() {
                mCardInfoPopup.setPrice(mSelectedPackage.getPrice());
                mCardInfoPopup.addItems(mPaymentItems);
            }

            @Override
            public void showAndroidPay(CardInfoPopup dialog) {
                processAndroidPay();
            }

            @Override
            public void onOkClick(CardInfoPopup dialog) {
                if(dialog.isLoading()) {
                    return;
                }
                buy(dialog.getName(), dialog.getNumber(), dialog.getMonth(), dialog.getYear(), dialog.getCvn());
            }

            @Override
            public void onCancelClick(CardInfoPopup dialog) {
                dialog.dismiss();
            }
        });
        mCardInfoPopup.show();
    }

    private void processAndroidPay() {
        mContainerMap.setObject(AppConst.KEY_PACKAGE_ITEM, mSelectedPackage);
        startActivityForResult(new Intent(getActivity(), PaymentActivity.class), ANDROID_PAY_REQUEST_CODE);
    }

    private void buy(final String cardHolder, final String cardNumber, final int expiryMonth, final int expiryYear, final String cardCvn) {
        if(mCardInfoPopup.isAddNew()) {
            if (TextUtils.isEmpty(cardHolder)) {
                showError(R.string.error_name_require);
                return;
            }
            if (TextUtils.isEmpty(cardNumber)) {
                showError(R.string.error_card_number_require);
                return;
            }
            if (TextUtils.isEmpty(cardCvn)) {
                showError(R.string.error_card_cvn_require);
                return;
            }

            final Card card = new Card(cardNumber, expiryMonth, expiryYear, cardCvn, cardHolder,
                    null, null, null, null, null, null, null);
            if (!card.validateCard()) {
                showError(R.string.error_card_invalid);
                return;
            }
            mCardInfoPopup.displayProgress(true);
            Subscription subscription = Observable.create(new Observable.OnSubscribe<Token>() {
                @Override
                public void call(Subscriber<? super Token> subscriber) {
                    try {
                        Stripe stripe = new Stripe(AppConst.STRIPE_PUBLISHABLE_KEY);
                        Token token = stripe.createTokenSynchronous(card);
                        subscriber.onNext(token);
                    } catch (AuthenticationException | APIException | InvalidRequestException | CardException | APIConnectionException e) {
                        subscriber.onError(e);
                    }
                }
            }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Token>() {
                        @Override
                        public void onCompleted() {
                        }
                        @Override
                        public void onError(Throwable e) {
                            mLogManager.log(e);
                            showError(e, R.string.error_loading_data);
                            mCardInfoPopup.displayProgress(false);
                        }
                        @Override
                        public void onNext(final Token token) {
                            mCardInfoPopup.displayProgress(false);
                            if (token != null) {
                                final AlertPopup alertPopup = new AlertPopup(getActivity());
                                alertPopup.setListener(new AlertPopup.AlertPopupListener() {
                                    @Override
                                    public void onStart() {
                                        alertPopup.setButton1Action(getString(R.string.yes_text), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertPopup.dismiss();
                                                createCard(cardHolder, cardNumber, expiryMonth, expiryYear, cardCvn, token.getId());
                                            }
                                        });
                                        alertPopup.setButton2Action(getString(R.string.no_text), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertPopup.dismiss();
                                                submitOrderNoCard(token.getId(), "");
                                            }
                                        });
                                        alertPopup.setMessage(getString(R.string.save_card_confirm));
                                        alertPopup.setTitle(getString(R.string.app_name));
                                    }
                                });
                                alertPopup.show();
                            } else {
                                showError(R.string.error_loading_data);
                            }
                        }
                    });
            addSubscription(subscription);
        } else if(mCardInfoPopup.isAndroidPay()) {
            processAndroidPay();
        } else {
            if(mCardInfoPopup.getSelectedPayment() == null) {
                showError(R.string.error_card_require);
                return;
            }
            submitOrder(mCardInfoPopup.getSelectedPayment().getCardId(), "");
        }
    }

    private void createCard(final String cardHolder, final String cardNumber, final int expiryMonth, final int expiryYear, final String cardCvn, final String cardToken) {
        mCardInfoPopup.displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.createCard(token, cardToken).subscribe(new SimpleObserver<CreateCardResponse>(){
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                mCardInfoPopup.displayProgress(false);
            }

            @Override
            public void onNext(CreateCardResponse createCardResponse) {
                super.onNext(createCardResponse);
                mCardInfoPopup.displayProgress(false);
                if(createCardResponse.isSuccess() && createCardResponse.getCard() != null) {
                    loadCards();
                    submitOrder(createCardResponse.getCard().getId(), cardHolder, cardNumber, expiryMonth, expiryYear, cardCvn);
                } else {
                    showError(createCardResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void submitOrder(String cardId, String cardHolder, String cardNumber, int expiryMonth, int expiryYear, String cardCvn) {
        mCardInfoPopup.displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.submitOrder(token, String.valueOf(mSelectedPackage.getId()), cardId, cardHolder, cardNumber, String.valueOf(expiryMonth), String.valueOf(expiryYear),
                cardCvn).subscribe(new SimpleObserver<SubmitOrderResponse>(){
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                mCardInfoPopup.displayProgress(false);
            }

            @Override
            public void onNext(SubmitOrderResponse submitOrderResponse) {
                super.onNext(submitOrderResponse);
                mCardInfoPopup.displayProgress(false);
                if(submitOrderResponse.isSuccess()) {
                    showToast("Buy Successful");
                    mCardInfoPopup.dismiss();
                } else {
                    showError(submitOrderResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void submitOrder(String cardId, String cardCvn) {
        mCardInfoPopup.displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.submitOrder(token, cardId, String.valueOf(mSelectedPackage.getId()), cardCvn).subscribe(new SimpleObserver<SubmitOrderResponse>(){
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                mCardInfoPopup.displayProgress(false);
            }

            @Override
            public void onNext(SubmitOrderResponse submitOrderResponse) {
                super.onNext(submitOrderResponse);
                mCardInfoPopup.displayProgress(false);
                if(submitOrderResponse.isSuccess()) {
                    showToast("Buy Successful");
                    mCardInfoPopup.dismiss();
                } else {
                    showError(submitOrderResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }

    private void submitOrderNoCard(String cardToken, String cardCvn) {
        mCardInfoPopup.displayProgress(true);
        String token = mContainerMap.getString(AppConst.KEY_TOKEN);
        Subscription subscription = mApiManager.submitOrderNoCard(token, cardToken, String.valueOf(mSelectedPackage.getId()), cardCvn).subscribe(new SimpleObserver<SubmitOrderResponse>(){
            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mLogManager.log(t);
                showError(t, R.string.error_loading_data);
                mCardInfoPopup.displayProgress(false);
            }

            @Override
            public void onNext(SubmitOrderResponse submitOrderResponse) {
                super.onNext(submitOrderResponse);
                mCardInfoPopup.displayProgress(false);
                if(submitOrderResponse.isSuccess()) {
                    showToast("Buy Successful");
                    mCardInfoPopup.dismiss();
                } else {
                    showError(submitOrderResponse, R.string.error_loading_data);
                }
            }
        });
        addSubscription(subscription);
    }
}

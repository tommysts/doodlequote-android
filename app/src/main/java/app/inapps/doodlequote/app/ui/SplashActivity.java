package app.inapps.doodlequote.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import app.inapps.doodlequote.R;
import app.inapps.doodlequote.app.AppApplication;
import app.inapps.doodlequote.app.AppConst;
import app.inapps.doodlequote.app.ui.content.OnFragmentInteractionListener;
import app.inapps.doodlequote.app.util.AppUtil;
import app.inapps.doodlequote.app.util.ContainerMapUtil;
import app.inapps.doodlequote.app.util.gcm.GcmPreferences;
import app.inapps.doodlequote.app.util.gcm.RegistrationIntentService;
import app.inapps.doodlequote.common.util.SystemUtil;
import app.inapps.doodlequote.core.ContainerMap;
import app.inapps.doodlequote.core.util.CoreUtil;
import app.inapps.doodlequote.core.widget.AnimateButton;
import app.inapps.doodlequote.core.widget.DebouncedOnClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.leolin.shortcutbadger.ShortcutBadger;
import rx.subscriptions.CompositeSubscription;

import static app.inapps.doodlequote.app.AppConst.LoginType.User;

public class SplashActivity extends BaseActivity {
    Handler mHandler;
    CompositeSubscription mCompositeSubscription;

    @BindView(R.id.ivCoverImage) ImageView mIvCoverImage;
    @BindView(R.id.ivLogoImage) ImageView mIvLogoImage;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;
    @BindView(R.id.txtVersion) TextView mTxtVersion;
    @BindView(R.id.btn_sign_in) AnimateButton mBtnSignIn;
    @BindView(R.id.btn_sign_up) AnimateButton mBtnSignUp;

    @Inject ContainerMap mContainerMap;
    @Inject Picasso mPicasso;

    BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mUnbinder = ButterKnife.bind(this);

        mHandler = new Handler();
        //mPicasso.load(R.drawable.back_ground).fit().into(mIvCoverImage);
        //mPicasso.load(R.drawable.logo).fit().into(mIvLogoImage);
        mTxtVersion.setText(getString(R.string.app_version, CoreUtil.getAppVersion(this)));
        mBtnSignIn.setOnClickListener(mOnClickListener);
        mBtnSignUp.setOnClickListener(mOnClickListener);

        addImageViews(mIvCoverImage, mIvLogoImage);

        // GCM
        GcmPreferences gcmPreferences = GcmPreferences.getInstance(this);
        gcmPreferences.setDeviceType(AppConst.DEVICE_TYPE);

        // GCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                GcmPreferences gcmPreferences = GcmPreferences.getInstance(SplashActivity.this);
                mUserPreferences.setDeviceToken(gcmPreferences.getRegistrationId());
            }
        };
        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GcmPreferences.REGISTRATION_COMPLETE_ACTION));

        checkNotification();

        Tracker tracker = AppApplication.getInstance(this).getDefaultTracker();
        tracker.setScreenName(SplashActivity.class.getSimpleName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());

        ContainerMapUtil.populateContainerFromPrefs(mContainerMap, mUserPreferences);
        boolean registered = AppUtil.isRegistered(mContainerMap);
        if (registered) {
            showMainActivity(User, AppConst.DeviceType.Phone, MainActivity.Pages.LoginPin);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //checkLogin();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mRegistrationBroadcastReceiver != null){
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(GcmPreferences.REGISTRATION_COMPLETE_ACTION));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mRegistrationBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        if(mRegistrationBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            mRegistrationBroadcastReceiver = null;
        }
        SystemUtil.executeJavaVmGC();
    }

    @Override
    void setupActivityComponent(Bundle savedInstanceState) {
        AppApplication.getInstance(this).getAppComponent().inject(this);
    }

    private void checkNotification() {
        GcmPreferences gcmPreferences = GcmPreferences.getInstance(this);
        gcmPreferences.setNotificationCount(0);
        ShortcutBadger.removeCount(getApplicationContext());
    }

    public CompositeSubscription getCompositeSubscription() {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        return mCompositeSubscription;
    }

    public void displayProgress(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private DebouncedOnClickListener mOnClickListener = new DebouncedOnClickListener() {
        @Override
        public void onDebouncedClick(View v) {
            if(v == mBtnSignIn) {
                showMainActivity(User, AppConst.DeviceType.Phone, MainActivity.Pages.Login);
            } else if(v == mBtnSignUp) {
                showMainActivity(User, AppConst.DeviceType.Phone, MainActivity.Pages.Register);
            }
        }
    };

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        return resultCode == ConnectionResult.SUCCESS;
    }

    private void showMainActivity(AppConst.LoginType loginType, AppConst.DeviceType deviceType, MainActivity.Pages page) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setAction(getIntent().getAction());
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            intent.putExtras(extras);
        }
        intent.putExtra(OnFragmentInteractionListener.DATA_PAGE_INT_EXTRA, page.ordinal());
        startActivity(intent);
        finish();
    }
}

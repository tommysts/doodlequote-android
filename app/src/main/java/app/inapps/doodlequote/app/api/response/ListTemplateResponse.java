package app.inapps.doodlequote.app.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.inapps.doodlequote.core.api.response.BaseResponse;

public class ListTemplateResponse extends BaseResponse {
    @SerializedName("templates")
    @Expose
    private List<TemplateResponse> templates;

    public List<TemplateResponse> getTemplates() {
        return templates;
    }

    public void setTemplates(List<TemplateResponse> templates) {
        this.templates = templates;
    }
}

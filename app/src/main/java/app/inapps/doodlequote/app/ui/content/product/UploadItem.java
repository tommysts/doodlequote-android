package app.inapps.doodlequote.app.ui.content.product;

import android.os.Parcel;
import android.os.Parcelable;

import app.inapps.doodlequote.app.AppConst;

public class UploadItem implements Parcelable{
    private String name;
    private String path;
    private String uploadBy;
    private String uploadTime;
    private boolean isSelected;
    private AppConst.UploadType type = AppConst.UploadType.Normal;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(String uploadTime) {
        this.uploadTime = uploadTime;
    }

    public AppConst.UploadType getType() {
        return type;
    }

    public void setType(AppConst.UploadType type) {
        this.type = type;
    }

    public UploadItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.path);
        dest.writeString(this.uploadBy);
        dest.writeString(this.uploadTime);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
    }

    protected UploadItem(Parcel in) {
        this.name = in.readString();
        this.path = in.readString();
        this.uploadBy = in.readString();
        this.uploadTime = in.readString();
        this.isSelected = in.readByte() != 0;
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : AppConst.UploadType.values()[tmpType];
    }

}
